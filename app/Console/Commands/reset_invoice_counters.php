<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Invoice;
use App\Models\Zone;
use App\Models\Setting;


class reset_invoice_counters extends Command implements SelfHandling
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $name = 'mazkara:reset.count.invoices';

    protected $description = 'Update Invoice counts.';

    public function __construct()
    {
        parent::__construct();

    }
    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
      $cities = Zone::cities()->get();
      foreach ($cities as $city){
        $city_id = $city->id;
        $count_invoices = Invoice::select()->where('city_id', '=', $city_id)->get()->count();
        $setting = Setting::select()->byLocale($city_id)->byType('finance')->where('name', '=', 'count_invoices')->get()->first();
        $setting->value = $count_invoices;
        $setting->save();
        $this->line($setting->value.' invoices set for '.$city->name);
      }


      $this->line('All Done :)');
    }

    protected function getOptions()
    {
        return array(
        );
    }


}
