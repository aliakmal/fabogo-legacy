<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Counter;
use App\Models\Counterbase;
use App\Models\Business;

class populate_counters_with_todays_counts extends Command
{
  /**
   * Create a new command instance.
   *
   * @return void
   */
  protected $name = 'mazkara:populate.counters';

  protected $description = 'Populate Counters.';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the command.
   *
   * @return void
   */
  public function handle(){
    //$c = Counter::orderBy('dated', 'asc')->first();
    //$date_to_copy = $c->dated;
    //$counts = Counter::select()->where('dated', '=', $date_to_copy)->whereRaw('dated < DATE_ADD(NOW(), INTERVAL -2 MONTH)')->get();
    //$this->line(count($counts).' entries for the date '.$date_to_copy.' selected');



    $c = Counter::groupBy('dated')->orderBy('dated', 'asc')->take(5)->lists('dated', 'dated')->all();
    $dates_to_copy = $c;

    $counts = Counter::select()->whereIn('dated', $dates_to_copy)->whereRaw('dated < DATE_ADD(NOW(), INTERVAL -2 MONTH)')->get();
    $this->line(count($counts).' entries for the dates '.join(',', $dates_to_copy).' selected');

    $newCountDB = new Counterbase();
    $deletable = [];

    foreach($counts as $one_count){
      $this->line($one_count->id.' picked');

      $count = $newCountDB->firstOrCreate(['id'=>$one_count->id]);
      $count->id = $one_count->id;
      $count->countable_id = $one_count->countable_id;
      $count->dated = $one_count->dated;
      $count->views = $one_count->views;
      $count->created_at = $one_count->created_at;
      $count->updated_at = $one_count->updated_at;
      $count->type = $one_count->type;
      $count->countable_type = $one_count->countable_type;

      if($one_count->countable_type == 'Business'){
        $b = Business::find($one_count->countable_id);
        if($b){
          $count->locale_id = $b->city_id;
        }
      }

      $count->save();

      $deletable[] = $one_count->id;
    }

    //$this->line(count($deletable).' entries for the date '.$date_to_copy.' to be removed from main table');
    $this->line(count($deletable).' entries for the date '.join(',', $dates_to_copy).' to be removed from main table');
    Counter::destroy($deletable);
    $this->info('All done :)');
  }
}
