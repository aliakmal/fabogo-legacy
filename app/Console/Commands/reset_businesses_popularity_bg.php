<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Zone;
use App\Models\Business;
use File;

class reset_businesses_popularity_bg extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:popularity.update.bg';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update Popularity Background.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire(){
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');

		$counter_file = storage_path().'/media/counter-popularity-fix';
		$skip_id = $this->option('skip_id');

		$photos_done = [];
		$photos_missed = [];

		$main_time_start = microtime(true);

		if(!$skip_id):
			if(File::exists($counter_file)){
				$skip_id = trim(File::get($counter_file));
			}
		endif;

		if($skip_id){
			$businesses = Business::select()->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' businesses records after business id '.$skip_id.' taken from page '.$page);
		}else{
			$businesses = Business::select()->orderby('id', 'asc')->take($count)->skip($page*$count)->get();
			$this->line($count.' businesses records taken from page '.$page);
		}

		if(count($businesses)==0){
			File::put($counter_file, '0');
		}

		foreach($businesses as $business):
			$time_start = microtime(true);
			$business->setPopularity();
			$time_end = microtime(true);
			$time = round($time_end - $time_start, 4);
			$this->info('Business ID '.$business->id.' popularity updated in '.$time.' seconds');
			$businesses_done[] = 'Business ID '.$business->id.' popularity updated in '.$time.' seconds';
			File::put($counter_file, $business->id);
		endforeach;

		$main_time_end = microtime(true);

		$total_time = round($main_time_end - $main_time_start, 4);
		$this->line($count.' Business popularity updated in '.$total_time.' seconds.... :)');

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 500),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
