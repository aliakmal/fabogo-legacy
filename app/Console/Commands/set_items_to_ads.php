<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Ad;
use App\Models\Group;
use App\Models\Business;
use App\Models\Slug;

use MazkaraHelper;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;

class set_items_to_ads extends Command implements SelfHandling
{
    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $name = 'mazkara:set.ads.items';

    protected $description = 'Update item links for ads.';

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $ads = Ad::select()->where('itemable_type', '=', '')->orWhere('itemable_id', '=', '0')->get();
        $this->line('Retrieved '.count($ads).' ads with missing item associations');
        $business = new Business();
        foreach($ads as $ad){
            
            $url = explode('/', $ad->url);
            $slug = array_pop($url);

            if(strstr($slug, '?')){
                $sl = explode('?', $slug);
                $slug = $sl[0];
            }
            if(strstr($ad->url, '/salons-spas-and-fitness-chain') 
                || (substr($ad->url, -(strlen('/outlets'))) === '/outlets')
                || strstr($ad->url, '/beauty-and-wellness-centers-chain') 
                ){

                if((substr($ad->url, -(strlen('/outlets'))) === '/outlets')){
                    $slug = array_pop($url);
                }

                $chain = Group::findBySlug($slug);
                if(is_object($chain)){
                    $this->line($slug.' '.$chain->name);
                    $ad->itemable_type = 'Group';
                    $ad->itemable_id = $chain->id;
                    $ad->save();

                }
                continue;
            }


            $b = $business->getBySlug($slug);
            if(is_object($b) || is_numeric($b)){
                if(is_numeric($b)){
                    $b = Business::find($b);
                }
                $this->line($slug.' '.$b->name);
                $ad->itemable_type = 'Business';
                $ad->itemable_id = $b->id;
                $ad->save();
                continue;
            }

            $ad->itemable_type = 'Link';
            $ad->itemable_id = 0;
            $ad->save();

            $this->error($ad->url.' saved as link');
        }
    }
}
