<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Comment;
use DB;

class convert_comments_to_reviews extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:comments.to.reviews';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Comments to Reviews.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
		$comments = Comment::query()->where('commentable_type', '=', 'Review')->get();
		foreach($comments as $ii=>$comment){
			$data = [	'body'	=>	$comment->body,
								'user_id'	=>	$comment->user_id,
								'business_id'	=>	$comment->commentable->business_id,
								'rating'	=>	$comment->commentable->rating,
								'flags'	=>	'comment.'.$comment->id,
								'created_at'	=>	$comment->created_at,
								'updated_at'	=>	$comment->updated_at];
			DB::table('reviews')->insertGetId($data);
			$this->line('Comment ID '.$comment->id.' converted to a review');
		}

		$this->line(count($comments).' comments converted to reviews');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
