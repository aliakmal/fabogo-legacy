<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Bus\SelfHandling;
use App\Models\Zone;

use Excel;

class allocate_lng_lat extends Command 
{
    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $name = 'mazkara:allocate.latlng';

    protected $description = 'Allocate Nails Hairs.';

    public function __construct()
    {
      parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
      $directory = storage_path().'/media/';
      $file = $directory.'centroid.xlsx';
      $csv = Excel::load($file, function($reader) {
      })->get();

      foreach($csv as $row){
        $city_name = $row->getTitle();
        $this->line($city_name);
        $city = Zone::where('name', '=', $city_name)->first();
        if($city):
          foreach($row as $zone){
            $sub_zone = Zone::where('name', '=', trim($zone->zone))->where('city_id', '=', $city->id)->first();
            $latitude = str_replace(',', '', trim($zone->latitude));
            $longitude = str_replace(',', '', trim($zone->longitude));

            if($sub_zone){
              $sub_zone->latitude = $latitude;
              $sub_zone->longitude = $longitude;
              $sub_zone->save();
              $this->line($sub_zone->name.' : '.$zone->zone.' - '.($latitude).' / '.($longitude));
            }else{
              $this->error($zone->zone.' - '.($latitude).' / '.($longitude));
            }
          }
        endif;
      }
    }
}
