<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Business;
use App\Models\Service;
use App\Models\Category;


class reallocate_home_new_category extends Command {
    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $name = 'mazkara:allocate.home.services';

    protected $description = 'Allocate Home services to Home Service Category.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $home_category = Category::where('slug', '=', 'home-service')->get()->first();

        $home_service_business_ids = \DB::table('business_highlight')
                                ->whereIn('highlight_id', [13])
                                ->lists('business_id', 'business_id');

        $this->line(count($home_service_business_ids).' venues for home services');
        DB::delete('delete from business_category where category_id = '.$home_category->id);

        // make an entry for business_id
        $hs = [];
        foreach($home_service_business_ids as $vv){
            $hs[] = ['category_id'=>$home_category->id, 'business_id'=>$vv];
        }

        $home_service_businesses = Business::where('type', '=', 'home-service')->get()->lists('id', 'id')->all();
        foreach($home_service_businesses as $vv){
            $hs[] = ['category_id'=>$home_category->id, 'business_id'=>$vv];
        }


        DB::table('business_category')->insert($hs);

        $this->line(count($home_service_business_ids).' venues updated for home category');
    }
}
