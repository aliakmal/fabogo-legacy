<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
 
use App\Models\Counter;
use DB;

class CheatPageViews extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:cheat.page.views';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update Users Counters.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$date = $this->argument('date');
		if($date == null){
			$this->error('INVALID DATE');
			return;
		}
		$bids = DB::table('business_merchant')->lists('business_id', 'business_id');
		foreach($bids as $bid){
	    $c = Counter::firstOrCreate(['countable_type'=>'Business', 'countable_id'=>$bid, 'type'=>Counter::PAGE_VIEWS, 'dated'=>$date]);
	    $c->views = rand(3, 6);
	    $c->save();
			$this->info($c->views.' views added to business of id '.$bid);
		}
		$this->line('All done :) ');


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('date', InputArgument::OPTIONAL, 'date', null),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
