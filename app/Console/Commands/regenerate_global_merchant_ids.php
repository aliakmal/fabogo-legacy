<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Models\Merchant as Merchant;

class regenerate_global_merchant_ids extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:merchant.global.ids';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Regenerate empty merchant ids.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler $scheduler
	 * @return \Indatus\Dispatcher\Scheduling\Schedulable
	 */

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$merchants = \Merchant::all();
    foreach($merchants as $merchant){
    	if(trim($merchant->global_id) == ''){
				$merchant->generateGlobalID();
				$merchant->save();
				$this->line($merchant->id.' - '.$merchant->name.' updated with global id '.$merchant->global_id);
    	}else{
				$this->info($merchant->id.' - '.$merchant->name.' skipped as already has id');
    	}
    }
		$this->info('All Done :)');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
