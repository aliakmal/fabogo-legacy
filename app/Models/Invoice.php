<?php
namespace App\Models;

use DateTime;

use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentTaggable\Taggable;

use App\Models\Invoice_item;
use App\Models\Merchant;
use App\Models\Call_log;
use App\Models\User;
use App\Models\Zone;
use App\Models\Payment;

class Invoice extends Ardent {
	protected $guarded = array();
  use Taggable;
  protected $morphClass = 'Invoice';

	public static $rules = array(
//		'title' => 'required',
		'merchant_id' => 'required',
//		'dated' => 'required',
//		'type' => 'required',
//		'currency' => 'required',
//		'amount' => 'required',
//		'state' => 'required'
	);

	public static $fields = array(
		'merchant_id', 'dated', 'amount', 'entity_id', 'start_date', 'end_date', 'user_id', 'city_id', 'currency'
	);

	public static function getStates(){
		return [ 'pending'=>'pending', 'bad-debt'=>'bad-debt', 'credit note'=>'credit note', 'cleared'=>'cleared'];
	}

	public function beforeCreate(){
		//$this->setCurrency();
		$this->setState();
		$this->setAmountDue();
    $this->setInitialTotalAmount();
	}

	public function setCurrency(){
		$this->currency = mzk_currency_symbol();
	}

	public function beforeSave(){
		$this->setState();
    $this->setTotal();

	}

  public function setAsPPL(){
    $this->is_ppl = 1;
  }

  public function isPPL(){
    return (isset($this->is_ppl)) && ($this->is_ppl == 1) ? true : false;
  }

	public function credit_notes(){
		return $this->hasMany('App\Models\Credit_note');
	}

  public function taxes(){
    return $this->hasMany('App\Models\Tax');
  }

  public function payment_applicables(){
    return $this->hasMany('App\Models\Payment_applicable');
  }

  public function getCycleApplicableForPayment(){
    $closed_date_ranges = [];
    foreach($this->payment_applicables as $p){
      $closed_date_ranges[] = [ 'start_date'=>$p->start_date,
                                'end_date'=>$p->end_date];
    }

    return $closed_date_ranges;
  }

	public function setAmountDue(){
    //$this->setTotalPlusTaxedAmount();
		$this->amount_due = $this->amount;
	}

  public function deductFromAmountApplicable($applied_amount){
    // is this cash or a cleared cheque?
    if($this->amount_due >= $applied_amount){
      $this->amount_due = $this->amount_due - $applied_amount;
      $this->save();
    }
  }

  public function undeductFromAmountApplicable($applied_amount){
    $this->amount_due = $this->amount_due + $applied_amount;
    $this->save();
  }


  public function setInitialTotalAmount(){
    $this->total_amount = $this->amount;

  }

  public function attachments(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::ORIGINAL);
  }


  public function saveAttachments($images, $name){
    $result = [];
    if(!is_array($images)){
      return $result;
    }

    $type = Photo::ORIGINAL;

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      $photo = new Photo(['type'=>$type]);
      $photo->image = $image;
      $photo->type = $type;
      $photo->name = $name;
      $photo->save();
      $this->attachments()->save($photo);
      $result[] = $photo;
    }
    return $result;
  }


  public function businesses() {
    return $this->belongsToMany('App\Models\Business'); 
  }

  public function setTaxedAmount(){
    $this->total_applicable_tax = 0;
    foreach($this->taxes as $tax){
      $this->total_applicable_tax+=$tax->amount;  
    }
    $this->save();
  }

  public function setTotalPlusTaxedAmount(){
    $this->setTaxedAmount();
    $this->setTotal();
    $this->save();
  }

  public function setExcess(){
    $this->total_excess = 0;

    foreach($this->items()->notItems()->get() as $item){
      $this->total_excess+=$item->total;  
    }

    $this->setTotal();
    $this->save();
  }


  public function setTotal(){
    $this->total_amount = $this->amount + $this->total_applicable_tax + $this->total_excess;
    //$this->save();
  }

  public function scopeByMonthYear($query, $month = false, $year = false, $end_month = false, $end_year = false){

    // get offers which have expired in the last 7 days
    $month = $month ? $month : date('m');
    $year = $year ? $year : date('Y');

    $end_month = $end_month ? $end_month : $month;
    $end_year = $end_year ? $end_year : $year;

    $start_of = \Carbon\Carbon::createFromDate($year, $month, 1);
    $end_on = \Carbon\Carbon::createFromDate($end_year, $end_month, 1);
    
    $end_of = \Carbon\Carbon::createFromDate($end_year, $end_month, date("t", strtotime($end_on)));

    return $query->whereRaw('(start_date < "'.$end_of->toDateString().'" AND end_date > "'.$start_of->toDateString().'")');
  }


  public function scopeByMonthYearRange($query, $start_month = false, $start_year = false, $end_month = false, $end_year = false){

    // get offers which have expired in the last 7 days
    $start_month = $start_month ? $start_month : date('m');
    $start_year = $start_year ? $start_year : date('Y');

    $end_month = $end_month ? $end_month : date('m');
    $end_year = $end_year ? $end_year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($start_year, $start_month, 1);
    $end_of = \Carbon\Carbon::createFromDate($end_year, $end_month, date("t", strtotime($end_year.'-'.$end_month.'-16')));

    return $query->whereRaw('(start_date < "'.$end_of->toDateString().'" AND end_date > "'.$start_of->toDateString().'")');
  }



	public function setState(){
		if($this->amount_due == 0){
			if($this->credit_notes()->get()->count() > 0){
				$this->state = $this->credit_notes()->get()->first()->type == 'bad-debt'?'bad debt':'credit note';
			}else{
				$this->state = 'cleared';
			}
		}else{
				$this->state = 'pending';
		}
	}
  public function reset(){
    $this->amount_due = $this->total_amount;
    $this->save();
  }

	public function afterCreate(){
    $this->increaseInvoiceCount($this);
		$this->assignInvoiceNumber();
		$this->save();
	}

	public function assignInvoiceNumber(){
		$this->title = $this->generateInvoiceNumber();
	}

	public function items(){
		return $this->hasMany('App\Models\Invoice_item');
	}
	public function merchant(){
		return $this->belongsTo('App\Models\Merchant', 'merchant_id');
	}

  public function call_logs()
  {
    return $this->hasManyThrough('App\Models\Call_log', 'App\Models\Invoice_item');
  }

  public function poc(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }

  public function poc_name(){
    return $this->poc() ? $this->poc()->get()->first()->name:'';
  }
  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function city_name(){
    return $this->city() ? $this->city()->get()->first()->name:'';
  }

  protected function getInvoiceCountSettingForCity($city_id){
    $setting = Setting::select()->byLocale($city_id)->byType('finance')->where('name', '=', 'count_invoices')->get()->first();
    return $setting;
  }

  protected function getInvoiceCount($city_id){
    $setting = Setting::select()->byLocale($city_id)->byType('finance')->where('name', '=', 'count_invoices')->get()->first();
    return $setting->value;
  }

  public function increaseInvoiceCount($invoice){
    $setting = $this->getInvoiceCountSettingForCity($invoice->city_d);
    $setting->value = (int)$setting->value+1;
    $setting->save();
  }

  public function scopeByMerchants($query, $merchants){

  	if(!is_array($merchants)){
  		$merchants = [$merchants];
  	}

  	return $query->whereIn('merchant_id', $merchants);
  }

  public function scopeByCity($query, $city){
    $lists = Merchant::select()->byLocale($city)->get()->lists('id', 'id')->all();

    return $query->byMerchants($lists);
  }

  public function scopeByPOCs($query, $pocs){
    if(!is_array($pocs)){
       $pocs = [ $pocs];
    }
    return $query->whereIn('user_id', $pocs);
  }

  public function scopeByStates($query, $states){
  	if(!is_array($states)){
  		$states = [$states];
  	}

  	return $query->whereIn('state', $states);
  }
  public function scopeSearch($query, $search){
  	return $query->where('title', 'like', $search.'%');
  }

  public function isCleared(){
  	return ($this->amount_due == 0) ? true : false ;
  }


  public function payments(){
  	return $this->belongsToMany('App\Models\Payment');
  }

  public function contractValue(){
    return $this->amount;
  }



  public function resetAmountApplicable(){
    $total_amount_applied = 0;

    foreach($this->payments()->withPivot('amount')->get() as $payment):
      if($payment->isApplicablePayment()):
        $invoice = self::find($this->id);
        $amount_applied = $payment->pivot->amount;
        $payment->markApplicableAmountForInvoice($invoice, $amount_applied);
        $total_amount_applied += $amount_applied;
        //$this->amount_applicable = $this->amount_applicable - $total_amount_applied;
        //$this->save();
      endif;
    endforeach;
    
    // if invoice amount due is more than available on check
    if($this->amount > $total_amount_applied){
      $this->amount_due = $this->total_amount - $total_amount_applied;
    }else{ // else subtract the amount due from applicable and mark check as cleared
      $this->amount_due = 0; 
    }
    $this->save();


  }

  public function getCycleIntervals(){
    
  }


  public function monthlyAverage(){
    $months = round(mzk_date_intervals($this->start_date, $this->end_date)/30);
    $months = $months>0?$months:1;
    return round($this->total_amount/$months, 2);

    // return round($this->amount/$months, 2);
  }

  public function monthlyTaxApplicable(){
    $months = round(mzk_date_intervals($this->start_date, $this->end_date)/30);
    $months = $months>0?$months:1;
    return round($this->total_applicable_tax/$months, 2);

    // return round($this->amount/$months, 2);
  }

  public function getActualAverage($m = false, $y = false){
    $m = $m ? $m : date('n');
    $y = $y ? $y : date('Y');
    $sum = Payment_applicable::select()->byMonthYear($m, $y)->byInvoice($this->id)->remember(10)->sum('amount');
    return $sum;
  }

  public function currentMonthContribution($m = false, $y = false){
    $m = $m ? $m : date('n');
    $y = $y ? $y : date('Y');

    $avg = $this->getActualAverage($m, $y);
    $avg = $avg <= 0? $this->monthlyAverage():$avg;
    
    // is this starting in the mid of the current month
    //date('nY')
    if($m.$y == date('nY', strtotime($this->start_date)) && ($m.$y!=date('nY', strtotime($this->end_date))) && (date('d', strtotime($this->start_date))>1)){
      return $avg/2;
    }

    if($m.$y == date('nY', strtotime($this->end_date))  && ( $m.$y!=date('nY', strtotime($this->start_date))) && (  date('d', strtotime($this->end_date))!=(date('t', strtotime($this->end_date)))   )){
      return $avg/2;
    }

    // is this within the current month to begin with?
    // loop from 1st month to the last month
    $start_from = strtotime($this->start_date);
    $end_at = strtotime($this->end_date);


    $start_month = date('Y-m-d', strtotime($y.'-'.$m.'-15'));
    $end_month = date('Y-m-t', strtotime($y.'-'.$m.'-15'));


    // loop through intervals
    $overlap = mzk_dates_overlap($this->start_date, $this->end_date, $start_month, $end_month);
    if($overlap>0){
      return round($avg, 2);
    }



    return 0;

  }




  public function currentMonthTax($m = false, $y = false){
    $m = $m ? $m : date('n');
    $y = $y ? $y : date('Y');

    $avg = $this->monthlyTaxApplicable();
    
    // is this starting in the mid of the current month
    //date('nY')
    if($m.$y == date('nY', strtotime($this->start_date)) && ($m.$y!=date('nY', strtotime($this->end_date))) && (date('d', strtotime($this->start_date))>1)){
      return $avg/2;
    }

    if($m.$y == date('nY', strtotime($this->end_date))  && ( $m.$y!=date('nY', strtotime($this->start_date))) && (  date('d', strtotime($this->end_date))!=(date('t', strtotime($this->end_date)))   )){
      return $avg/2;
    }

    // is this within the current month to begin with?
    // loop from 1st month to the last month
    $start_from = strtotime($this->start_date);
    $end_at = strtotime($this->end_date);


    $start_month = date('Y-m-d', strtotime($y.'-'.$m.'-15'));
    $end_month = date('Y-m-t', strtotime($y.'-'.$m.'-15'));


    // loop through intervals
    $overlap = mzk_dates_overlap($this->start_date, $this->end_date, $start_month, $end_month);
    if($overlap>0){
      return round($avg, 2);
    }

    return 0;
  }

  public function resetInvoiceNumber($increment)
  {
    $this->assignInvoiceNumber($increment);
  }

	public function generateInvoiceNumber(){
    // get most recent invoice by locale
    $locale = mzk_get_localeID();
    
    $increment = $this->getInvoiceCount($locale);

    $num = ['PF'];
		$num[] = $locale;
		$num[] = date('my');
		$num[] = str_pad(($increment), 10, "0", STR_PAD_LEFT);//str_pad($this->id, 10, "0", STR_PAD_LEFT);
		return join('/', $num);
	}

  public function getAmountInrAttribute(){
    if($this->currency == 'INR'){
      return $this->amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'INR');
    }

  }

  public function getAmountAedAttribute(){
    if($this->currency == 'AED'){
      return $this->amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'AED');
    }

  }

  public function getAmountUsdAttribute(){
    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'USD');
    }

  }

  public function currentMonthContributionINR($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);
    if($this->currency == 'INR'){
      return $amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'INR');
    }

  }

  

  public function currentMonthTaxAED($m = false, $y = false){
    $amount = $this->currentMonthTax($m, $y);
    if($this->currency == 'AED'){
      return $amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'AED');
    }

  }

  public function currentMonthTaxINR($m = false, $y = false){
    $amount = $this->currentMonthTax($m, $y);
    if($this->currency == 'INR'){
      return $amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'INR');
    }

  }
  public function currentMonthTaxUSD($m = false, $y = false){
    $amount = $this->currentMonthTax($m, $y);
    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'USD');
    }
  }



  public function currentMonthContributionAED($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);
    if($this->currency == 'AED'){
      return $amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'AED');
    }

  }


  public function currentMonthContributionUSD($m = false, $y = false){
    $amount = $this->currentMonthContribution($m, $y);

    if($this->currency == 'AED'){
      return mzk_currency_exchange($amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($amount, 'INR', 'USD');
    }
  }




}
