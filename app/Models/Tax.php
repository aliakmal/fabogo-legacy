<?php
namespace App\Models;

use DateTime;

use LaravelArdent\Ardent\Ardent;

use App\Models\Invoice_item;
use App\Models\Merchant;
use App\Models\Tax;
use App\Models\Invoice;
use App\Models\User;
use App\Models\Zone;
use App\Models\Payment;

class Tax extends Ardent {
	protected $guarded = array();

	public static $rules = array(
//		'title' => 'required',
//		'merchant_id' => 'required',
//		'dated' => 'required',
//		'type' => 'required',
//		'currency' => 'required',
//		'amount' => 'required',
//		'state' => 'required'
	);

	public static $fields = array('name','percentage','amount','invoice_id');

	public function invoice(){
    return $this->belongsTo('App\Models\Invoice', 'invoice_id');
	}

	public function afterCreate(){
		$this->setAmount();
		$this->save();
    $this->setInvoiceTotalPlusTaxedAmount();
	}

  public function beforeCreate(){
    $this->setDefaults();
  }


  public function setDefaults(){
    $this->name = $this->name == ''? 'Tax':$this->name;
  }

  public function setAmount(){
    $invoice = Invoice::find($this->invoice_id);
    $amount = $invoice->amount;
    if($this->percentage>0){
      $this->amount = ($this->percentage/100)*$amount;
    }
  }

  public function setInvoiceTotalPlusTaxedAmount(){
    $invoice = Invoice::find($this->invoice_id);
    $invoice->setTotalPlusTaxedAmount();
    $invoice->resetAmountApplicable();
  }


  public function scopeByInvoice($query, $invoice_id){
  	return $query->whereIn('invoice_id', [$invoice_id]);
  }

}
