<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

use App\Models\User;
use App\Models\Business;

class Check_in extends Ardent {
  //use GetStream\StreamLaravel\Eloquent\ActivityTrait;
    
	protected $guarded = array();

	public static $rules = array(
		'user_id' => 'required',
		'business_id' => 'required'
	);

  public function afterCreate() {
    $this->updateBusinessCheckInCount();
  }

  public function beforeDelete(){
    $this->decrementBusinessCheckInCount();
  }

  public function updateBusinessCheckInCount(){
    $this->business->updateCheckInCount();
    $this->user->updateCheckInCount();
  }

  public function decrementBusinessCheckInCount(){
    $this->business()->decrement('checkins_count');
  }


  public function user(){
    return $this->belongsTo('App\Models\User');
  }

  public function business(){
    return $this->belongsTo('App\Models\Business');
  }

}
