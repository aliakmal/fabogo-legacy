<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;

class Virtual_number_allocation extends Ardent {
	protected $guarded = array();

  public function afterCreate(){
    $this->business->updateCurrentNumberCache();
  }

  public function afterSave(){
    $this->business->updateCurrentNumberCache();
  }

  function business(){
    return $this->belongsTo('App\Models\Business', 'business_id');
  }

  function virtual_number(){
    return $this->belongsTo('App\Models\Virtual_number', 'virtual_number_id');
  }

  function isDeallocatable(){
    return $this->state == 'active';
  }

  function isEditable(){
    return $this->state == 'active';
  }

  function isPPL(){
    return $this->is_ppl == 1 ? true:false;
  }

  function virtual_number_text(){
    return $this->virtual_number()->count() > 0 ?$this->virtual_number()->first()->body:'';
  }

  function deallocate(){
    $this->state = 'inactive';
    $this->ends = mzk_current_datetime();
    $this->save();
    $this->business->updateCurrentNumberCache();

  }

  function hold(){
    $this->state = 'hold';
    $this->save();
  }

  function unhold(){
    $this->state = 'active';
    $this->save();
  }

  function toggleHold(){
    if($this->state == 'active'){
      $this->hold();
    }elseif($this->state == 'hold'){
      $this->unhold();
    }
  }

  function isIVREnabled(){
    return $this->is_ivr_enabled == 0 ? false : true;
  }


	public static $rules = array(
		//'desc' => 'required',
		'business_id' => 'required',
		'virtual_number_id' => 'required',
    'phone_1'=>'required',
		//'start' => 'required',
		//'end' => 'required',
		'state' => 'required'
	);

  public function scopeOnlyActive($query){
    return $query->whereNotIn('state', 'active');
  }

  public function scopeJustActive($query){
    return $query->where('state', '=', 'active');
  }

  public function scopeByNumbers($query, $v_number_ids){
    return $query->whereIn('virtual_number_id', $v_number_ids);
  }


}
