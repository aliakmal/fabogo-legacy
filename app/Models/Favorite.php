<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Watson\Rememberable\Rememberable;

use App\Models\User;

class Favorite extends Ardent {
  use Rememberable;

  //use GetStream\StreamLaravel\Eloquent\ActivityTrait;
  const FAVOURITE = 'favorite';
  const BOOKMARK = 'bookmark';
  
	protected $guarded = array();

	public static $rules = array(
		'user_id' => 'required',
		'favorable_id' => 'required',
		'favorable_type' => 'required'
	);



  public function afterCreate() {
    $this->updateFavouriteCount();
  }

  public function beforeDelete(){
    $this->decrementFavouriteCount();
  }

  public function afterDelete() {
    $this->updateFavouriteCount();
  }

  public function updateFavouriteCount(){
    if($this->favorable_type!='Post'){
      $this->favorable->updateFavoriteCount();
    }

    if($this->favorable_type=='User'){
      $this->favorable->updateFollowersCount();
    }elseif($this->favorable_type=='Post'){
    }else{
      $this->user->updateFollowsCount();

    }
  }

  public function decrementFavouriteCount(){

    $this->favorable()->decrement('favorites_count');
  }



  public function user(){
    return $this->belongsTo('App\Models\User', 'user_id');
  }
  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Invoice'=>'App\Models\Invoice',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Review'=>'App\Models\Review',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
  ];

  public function getFavorableTypeAttribute($cls) {
      
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }

  public function favorable()
  {
      return $this->morphTo();
  }  

}
