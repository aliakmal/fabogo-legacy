<?php
namespace App\Models;

use LaravelArdent\Ardent\Ardent;
use Auth;
use App\Models\Zone;

class Finance extends Ardent {
	protected $guarded = array();

  public static $fields = array('desc','type','amount','currency',
                                'usd_amount','user_id','dated','city_id');
  const DEBIT = 'debit';
  const CREDIT = 'credit';

	public static $rules = array(
		'type' => 'required',
		'amount' => 'required',
		'currency' => 'required'
	);

  public function afterCreate(){
    $this->assignNumber();
    $this->save();
  }

  public function assignNumber(){
    $this->title = $this->generateNumber();
  }

  public function city(){
    return $this->belongsTo('App\Models\Zone', 'city_id');
  }

  public function generateNumber(){
    $num = ['EX'];
    $num[] = $this->city_id;
    $num[] = date('my');
    $num[] = str_pad($this->id, 10, "0", STR_PAD_LEFT);
    return join('/', $num);
  }
  public function scopeOnlyExpenses($query){
    return $query->onlyCredits();
  }

  public function scopeOnlyCredits($query){
    return $query->where('type', '=', Finance::CREDIT);
  }


  public function scopeByMonthYear($query, $month = false, $year = false){

    // get offers which have expired in the last 7 days
    $month = $month ? $month : date('m');
    $year = $year ? $year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($year, $month, 1);
    $end_of = \Carbon\Carbon::createFromDate($year, $month, date("t", strtotime($start_of)));

    return $query->whereRaw('(dated <= "'.$end_of->toDateString().'" AND dated >= "'.$start_of->toDateString().'")');
  }


  public function scopeByMonthYearRange($query, $start_month = false, $start_year = false, $end_month = false, $end_year = false){

    // get offers which have expired in the last 7 days
    $start_month = $start_month ? $start_month : date('m');
    $start_year = $start_year ? $start_year : date('Y');

    $end_month = $end_month ? $end_month : date('m');
    $end_year = $end_year ? $end_year : date('Y');

    $start_of = \Carbon\Carbon::createFromDate($start_year, $start_month, 1);
    $end_of = \Carbon\Carbon::createFromDate($end_year, $end_month, date("t", strtotime($end_year.'-'.$end_month.'-16')));

    return $query->whereRaw('(dated < "'.$end_of->toDateString().'" AND dated > "'.$start_of->toDateString().'")');
  }


  public function getAmountInrAttribute(){
    if($this->currency == 'INR'){
      return $this->amount;
    }

    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'INR');
    }

  }

  public function getAmountAedAttribute(){
    if($this->currency == 'AED'){
      return $this->amount;
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'AED');
    }

  }

  public function getAmountUsdAttribute(){
    if($this->currency == 'AED'){
      return mzk_currency_exchange($this->amount, 'AED', 'USD');
    }

    if($this->currency == 'INR'){
      return mzk_currency_exchange($this->amount, 'INR', 'USD');
    }

  }

}
