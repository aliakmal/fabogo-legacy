<?php
namespace App\Models;


class Service_item extends BaseModel {
  protected $table = 'service_items';
  protected $stiClassField = 'class_name';
  protected $stiBaseClass = 'App\Models\Service_item';
	protected $guarded = array();

  public function business(){
    return $this->belongsTo('App\Models\Business', 'business_id');
  }

  public function service(){
    return $this->belongsTo('App\Models\Service', 'service_id');
  }

  public function scopeByGroup($query, $group){
    return $query->where('grouping', '=', $group);
  }

  public function scopeByBusiness($query, $business_id){
    return $query->where('business_id', '=', $business_id);
  }
  
  public function scopeByMenuGroup($query, $group){
    return $query->where('menu_group_id', '=', $group);
  }

  public function menu_group(){
    return $this->belongsTo('App\Models\Menu_group', 'menu_group_id');
  }

  public function services(){
    return $this->belongsToMany('App\Models\Service');
  }


	public static $rules = array(
		//'service_id' => 'required',
		'business_id' => 'required',
		'name' => 'required',
	);
}
