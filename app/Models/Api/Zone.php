<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class Zone extends Eloquent{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_zone';
  }

  public static $rules = [];

  public $fields = array('description','depth','name','slug','active','business_count','latitude','longitude','old_id','city_id','parent_id');

  public $fillables = array('description','depth','name','slug','active','business_count','latitude','longitude','old_id','city_id','parent_id');

}
