<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;

class Service extends Eloquent{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_service';
  }

  public static $rules = [];

  public $fields = array('description','parent_id','name','slug','gender','business_count','active','old_id','female_image_id','icon_image_id','male_image_id');

  public $fillables = array('description','parent_id','name','slug','gender','business_count','active','old_id','female_image_id','icon_image_id','male_image_id');

}
