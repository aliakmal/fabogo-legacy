<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Model;
use Eloquent;


use App\Models\Share;
use App\Models\Ad_set;
use App\Models\Photo;


class Wiki extends Eloquent{
  protected $guarded = array();
  protected $connection = 'mysql_adhacks';

  public function __construct(){
    parent::__construct();
    $this->table = 'api_servicedetail';
  }

  public static $rules = [];

  public $fields = array('description', 'title', 'service_details', 'display_json', 'active', 'service_id', 'cover_image_id');

  public $fillables = array('description', 'title', 'service_details', 'display_json', 'active', 'service_id', 'cover_image_id');

  public function service(){
    return $this->belongsTo('App\Models\Api\Service');
  }

  public function cover(){
    return $this->belongsTo('App\Models\Api\Image', 'cover_image_id');
  }

  public function getSectionsAttribute(){
    $data = json_decode($this->display_json);
    $d = [];
    if(isset($data->data_array)){
      foreach($data->data_array as $ii=>$vv){
        $d[] = $vv;

      }

      return $d;

    }

    return [];
  }

  public function getSeoKeywordsAttribute(){
    $data = json_decode($this->display_json);
    return isset($data->seo_keywords)?$data->seo_keywords:'';
  }

  public function getMetaKeywordsAttribute(){
    $data = json_decode($this->display_json);
    return isset($data->meta_keywords)?$data->meta_keywords:'';
  }


}
