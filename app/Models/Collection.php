<?php
namespace App\Models;

use Eloquent;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Business;
use App\Models\Photo;


class Collection extends Eloquent implements SluggableInterface{
  use SluggableTrait;
  protected $morphClass = 'Collection';

  protected $sluggable = array(
      'build_from' => 'name',
      'save_to'    => 'slug',
      'max_length' => 200

  );

	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		//'type' => 'required'
	);


  public function getLabelAttribute(){
    return $this->id.' - '.$this->name;
  }

  public function businesses(){
    return $this->belongsToMany('App\Models\Business');
  }



  public function scopeByLocale($query, $locale = false){
    $locale = $locale == false ? mzk_get_localeID() : $locale;
    return $query->where('city_id', '=', $locale);
  }


  public function scopeSelector($query, $title = 'Select') {
      $selectVals[''] = $title;
      $selectVals += $this->lists('name', 'id')->all();
      return $selectVals;
  }

  public function scopeSearch($query, $search){
    return $query->where('name', 'like', $search.'%');
  }


}
