<?php
namespace App\Models;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use LaravelArdent\Ardent\Ardent;

use Auth, File;

use App\Models\Comment;
use App\Models\Business;

class Call_log extends Ardent implements StaplerableInterface{
	protected $guarded = array();
  use EloquentTrait;

  const STATE_CLEAR = 'clear';
	const STATE_UNBILLED = 'unbilled';
	const STATE_DISPUTED = 'disputed';
	const STATE_BILLED = 'billed';
	const STATE_RESOLVED = 'resolved';
  protected $morphClass = 'Call_log';


	public static $rules = array(
		'dated' 	=> 'required',
		'timed' 	=> 'required',
		'called_number' 	=> 'required',
		'caller_number' 	=> 'required',
		'caller_duration' => 'required',
		//'agent_list' => 'required',
		//'call_connected_to' => 'required',
		//'call_transfer_status' => 'required',
		//'call_transfer_duration' => 'required',
		//'call_recording_url' => 'required',
		//'call_start_time' => 'required',
		//'call_pickup_time' => 'required',
		//'caller_circle' => 'required',
		//'business_id' => 'required'
	);

	public static $fields = array('dated', 'timed', 'called_number', 'caller_number', 
		'caller_duration', 'agent_list', 'call_connected_to', 'call_transfer_status',
		'call_transfer_duration', 'call_recording_url', 'call_start_time', 'call_pickup_time', 'caller_circle');


  public function __construct(array $attributes = array()) {
    $this->hasAttachedFile('media', [
      'storage' => 's3',
      's3_client_config' => [
        'key' => 'AKIAI37S25ETHXHU7YMQ',
        'secret' => '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',

        // 'key' => 'AKIAJB6C26YXI6SME7EQ',
        // 'secret' => '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',
        'region' => 'us-east-1',
            'version'=> 'latest',
        
        'credentials'=>[
          'key' => 'AKIAI37S25ETHXHU7YMQ',
          'secret' => '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',

          // 'key' => 'AKIAJB6C26YXI6SME7EQ',
          // 'secret' => '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',
        ]

      ],
      's3_object_config' => [
        'Bucket' => 'mazkaracdn'
      ],
      'default_url' => '/defaults/:style/missing.png',
      'keep_old_files' => true        
    ]);

    // IMPORTANT:  the call to the parent constructor method
    // should always come after we define our attachments.
    parent::__construct($attributes);
  }


  function getMediaUrlAttribute(){
    if(($this->is_downloaded > 0) && ($this->call_recording_url!='None') && ($this->call_recording_url!='')){
      return $this->media->url();
    }

    if(($this->call_recording_url!='None')&&($this->call_recording_url!='')){
      return $this->call_recording_url;
    }

    return false;
  }

  function download(){
    $src = $this->call_recording_url;

    if(!filter_var($src, FILTER_VALIDATE_URL)){
      $this->markDownloaded();
      $this->save();
      return;
    }

    $url = explode('/', $src);
    $fname = $url[count($url)-1];

    $dir = storage_path().'/media/audio_'.$this->id.md5(time());
    mkdir($dir);

    $filename = $dir.'/'.$fname;
    $file_exists = (@fopen($src, "r")) ? true : false;
    if($file_exists == true){//File::exists($src)){


    try {
      File::copy($src, $filename);
    } catch (ErrorException $e) {
      return false; 
    }
    }else{
        return false; 
    }


    $this->media = $filename ;
    $this->save();
    $this->markDownloaded();
    $this->save();
    return true;
  }

  function getMediaMatter(){
    $url = $this->media->url();
    return strstr($url, 'missing.png')?'none':$url;
  }

  function markDownloaded($v = true){
    $this->is_downloaded = $v == true?1:0;
    $this->save();
  }

  function markNotDownloaded($v = true){
    $this->is_downloaded = $v == true?0:1;
    $this->save();
  }

  function markIfCannotBeDownloaded(){
    $now = \Carbon\Carbon::now();
    $created_at = new \Carbon\Carbon($this->created_at);

    if($created_at->diffInDays($now)>120){
      $now = \Carbon\Carbon::now();
      $this->inaccessible = 1;
      $this->last_access_attempt = (date('Y-m-d H:i', strtotime($now)));
      $this->save();
      return true;
    }

    return false;
  }

  public function scopeMayBeDownloadable($query){
    return $query->where('inaccessible', '=', 0);
  }


  public function scopeOnlyDownloaded($query){
    return $query->where('is_downloaded', '=', 1);
  }

  public function scopeAccessibleByClient($query){
    return $query->whereIn('type_of_call', [0,3]);
  }

  public function isAccessibleByClient(){
    if(in_array($this->type_of_call, [0,3])){
      return true;
    }

    return false;
  }

  public function scopeOnlyNotDownloaded($query){
    return $query->where('is_downloaded', '<>', 1);
  }


  public function scopeWithUrl($query){
    return $query->whereNotIn('call_recording_url', ['None', ' ', '']);
  }

  public function scopeHoursAgo($query, $hours = 3){
    $now = \Carbon\Carbon::now();

    $now->subHours($hours);                  //

    return $query->where('created_at', '<', (date('Y-m-d H:i', strtotime($now))));
  }



	public function scopeByBusiness($query, $business_id){
		return $query->where('business_id', '=', $business_id);
	}

  public function scopeByBusinesses($query, $business_ids){
    return $query->whereIn('business_id', $business_ids);
  }

  public function scopeBillable($query){
    return $query->whereIn('state', [self::STATE_UNBILLED, self::STATE_RESOLVED]);
  }

	public function scopeOnlyConnected($query){
		return $query->where('call_transfer_status', '=', 'Connected');
	}
  public function isConnected(){
    return $this->call_transfer_status == 'Connected' ? true:false;
  }

  public function scopeBetweenDates($query, $start, $end){
    return $query->where('dated', '>', $start)->where('dated', '<=', $end);
  }

  public function beforeCreate(){
  	$this->setInitialStatus();
  }

  public function dispute(){
  	$this->state = self::STATE_DISPUTED;
    $this->save();
  }

  public function resolve(){
    $this->state = self::STATE_RESOLVED;
    $this->is_lead = 1;
    $this->save();
  }

  public function resetLead(){
    $this->state = self::STATE_CLEAR;
    $this->is_lead = 0;
    $this->save();
  }

  public function makeClearToBillable(){
    $this->state = self::STATE_UNBILLED;
    $this->is_lead = 1;
    $this->save();
  }

  public function comments(){
    return $this->morphMany('Comment', 'commentable');
  }

  public function getDurationAttribute(){
  	if(!strstr($this->caller_duration, ':')){
      return $this->caller_duration>0?$this->caller_duration:0;
    }else{
      sscanf($this->caller_duration, "%d:%d:%d", $hours, $minutes, $seconds);

      $time_seconds = isset($seconds) ? ($hours * 3600) + ($minutes * 60) + $seconds : ($hours * 60) + $minutes;
      return $time_seconds;
    }
  }

  public function isDisputable(){
  	
  	if($this->isLead()){
  		if($this->state!=self::STATE_DISPUTED){
  			return true;
	  	}
  	}

  	return false;
  }

  public function isClear(){

    if($this->state==self::STATE_CLEAR){
      return true;
    }

    return false;
  }

  public function isResolvable(){

    if($this->isLead()){
      if($this->state==self::STATE_DISPUTED){
        return true;
      }
    }

    return false;
  }
  public function isResetable(){
  
    if(in_array($this->state, [self::STATE_UNBILLED, self::STATE_DISPUTED, self::STATE_RESOLVED])){
      return true;
    }else{
      return false;
    }
  }

  public function canBeResolvedByUser($user = false){
    $user = $user ? $user : Auth::user();
    if($user->hasRole('admin') || $user->hasRole('moderator') || $user->hasRole('sales-admin') || $user->hasRole('sales')){
      return true;
    }else{
      return false;
    }
  }


  public function isPotentialLead(){
    return true;
  	if($this->duration >= 30){
  		return true;
  	}else{
  		return false;
  	}
  }

  public function isLead(){
    if($this->is_lead == 1){
      return true;
    }else{
      return false;
    }
  }
  
  function isPPL(){
    return $this->is_ppl == 1 ? true:false;
  }

  public function setInitialStatus(){
  	if($this->isPotentialLead()){
  		$this->is_lead = 1;
		  $this->state = self::STATE_UNBILLED;
  	}else{
  		$this->is_lead = 0;
		  $this->state = self::STATE_CLEAR;
  	}
  }

}
