<?php
namespace App\Models;

use Eloquent;

class Slug extends Eloquent {
	protected $guarded = array();

	public static $rules = array();

  protected $classes = [
    'Activity'=>'App\Models\Activity',
    'Business'=>'App\Models\Business',
    'Category'=>'App\Models\Category',
    'Invoice'=>'App\Models\Invoice',
    'Highlight'=>'App\Models\Highlight',
    'Photo'=>'App\Models\Photo',
    'Post'=>'App\Models\Post',
    'Selfie'=>'App\Models\Selfie',
    'Video'=>'App\Models\Video',
    'Favorite'=>'App\Models\Favorite',
    'Group'=>'App\Models\Group',
    'Review'=>'App\Models\Review',
    'Service'=>'App\Models\Service',
    'User'=>'App\Models\User',
    'Zone'=>'App\Models\Zone',
  ];

  public function getSluggableTypeAttribute($cls) {
      
      $cls = ucwords($cls);
      // to make sure this returns value from the array
      return array_get($this->classes, $cls, $cls);
      // which is always safe, because new 'class'
      // will work just the same as new 'Class'
  }


  public function sluggable()
  {
      return $this->morphTo();
  }  

}
