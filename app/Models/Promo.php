<?php
namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Promo extends Deal implements SluggableInterface{
	protected $guarded = array();
  use SluggableTrait;

  public function getSlugnameAttribute(){
    return $this->title.' '.$this->business->name;
  }

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'     => true,      
  );

  protected static $singleTableType = 'Promo';
	//protected $table_type = 'Discount';
	//protected static $table_type_field = 'type';
	protected static $my_attributes = array('starts', 'ends');
  protected static $persisted = ['starts','ends'];
	public static $rules = array(
		'title' => 'required',
		'caption' => 'required',
		'description' => 'required',
		'fine_print' => 'required',
		'offer_amount' => 'required',
		'starts' => 'required',
		'ends' => 'required'
	);
}
