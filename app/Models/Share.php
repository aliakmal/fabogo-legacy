<?php
namespace App\Models;

use Eloquent;


class Share extends Eloquent {
	const FACEBOOK = 'facebook';
  
  protected $guarded = array();

	public static $rules = array(
		'type' => 'required',
		'user_id' => 'required'
	);
}
