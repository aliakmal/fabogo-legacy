<?php
namespace App\Models;

use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use LaravelArdent\Ardent\Ardent;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Models\Favorite;
use App\Models\Business;
use App\Models\Photo;

class Deal extends Ardent implements SluggableInterface {
  use SluggableTrait;

  public function getSlugnameAttribute(){
    return $this->title.' '.$this->business->name;
  }

  public function getSlug(){
    return $this->slug;
  }

  protected $sluggable = array(
    'build_from' => 'slugname',
    'save_to'    => 'slug',
    'max_length' => 200,
    'unique'     => true,      
  );

	protected $guarded = array();


  use SingleTableInheritanceTrait;

  protected $table = "deals";

  protected static $singleTableTypeField = 'type';

  protected static $singleTableSubclasses = ['Discount', 'Promo', 'Package'];

	public static $rules = array(
		'title' => 'required',
		'description' => 'required',
		'offer_amount' => 'required',
	);

  public function favourites(){
    return $this->morphMany('Favorite', 'favorable');
  }

  public function favourited($user_id){
    return $this->favourites()->where('user_id', '=', $user_id)->count() > 0;
  }

  function hasEnded(){
    return !(strtotime($this->ends) > strtotime("now"));
  }

  public function beforeDelete() {
    $this->business->updateDealsCount();
    if($this->isActive()){
      $this->business->updateActiveDealsCount();
    }
  }

  public function afterSave() {
    if($this->getOriginal('status')!=$this->status){
      $this->business->updateActiveDealsCount();
    }
  }

  public function afterCreate() {
    $this->business->updateDealsCount();
    if($this->isActive()){
      $this->business->updateActiveDealsCount();
    }
  }

  public function isActive(){
    return $this->status == 'active';
  }
  public function scopeHasStatus($query, $state){
    return $query->where('status', $state);
  }

  public function scopeActive($query){
    return $query->where('status', 'active');
  }


	public static function getTypeClass($type){
		switch($type){
			case 'discount':
				return 'Discount';
			break;
			case 'promo':
				return 'Promo';
			break;
			case 'package':
				return 'Package';
			break;
		}
	}

  protected static $persisted = ['title','caption','description','fine_print','offer_amount','original_amount','starts','ends','type','business_id','status'];
	//protected static $table_type_field = 'type';
	protected static $my_attributes = array('id','title', 'description', 
  														'type', 'business_id', 'status', 'caption',
  														'fine_print','offer_amount'); 

  public static $types = array('discount'=>'Discount', 'promo'=>'Promo', 'package'=>'Package');
  public static $statuses = array('active'=>'Active', 'inactive'=>'Inactive');

  public function business(){
  	return $this->belongsTo('App\Models\Business');
  }
  public function artworks(){
    return $this->morphMany('App\Models\Photo', 'imageable')->where('type', Photo::IMAGE);
  }


  public function saveImages($images){
    if(!is_array($images))
    {
        $images = [$images];
    }

    foreach($images as $image)
    {
      if(is_null($image)){
        continue;
      }
      $photo = new Photo();
      $photo->image = $image;
      $photo->type = Photo::IMAGE;
      $photo->save();
      $this->artworks()->save($photo);
    }
  } 
  public function removeAllImages($ids){
    if(!is_array($ids)){
      $ids = [$ids];
    }

    foreach($ids as $id){
      $p = Photo::findOrFail($id);
      $p->delete($id);
    }
  }


}
