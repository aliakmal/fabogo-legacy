<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Mail, Validator, Request, Lang;
use MazkaraHelper;

use App\Models\Ad;
use App\Models\Offer;
use App\Models\Business;
use App\Models\Post;
use App\Models\Service;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Review;
use App\Models\Activity;
use App\Models\Group;
use App\Models\Zone;
use App\Models\Job;
use App\Models\User;

//include_once app_path().'/../shieldsquare/ss2.php';

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $meta;

  public function __construct() {
    $this->meta = new \RyanNielson\Meta\Meta();

  }

  public function incrementCTR(){
    $inp = Input::all();
    $ad = Ad::find($inp['ad_id']);
    $ad->incrementCTRCount();
  }

  private function getButtons(){
    $buttons = [];


    $buttons[] = [
                    'title'=>'Luxury Salon & Spas',
                    'sub-title'=>'150+ listings',
                    'image'=>'luxury.jpg',
                    'params'=>['highlights'=>[12]]
                  ];

//    $buttons[] = [
//                    'title'=>'Kid Services',
//                    'sub-title'=>'70+ listings',
//                    'image'=>'kid-services.jpg',
//                    'params'=>['highlights'=>[14]]
//                  ];
//    $buttons[] = [
//                    'title'=>'Medi-Spas',
//                    'sub-title'=>'40+ listings',
//                    'image'=>'medispa.jpg',
//                    'params'=>['highlights'=>[17]]
//                  ];
//    $buttons[] = [
//                    'title'=>'Wedding',
//                    'sub-title'=>'300+ listings',
//                    'image'=>'wedding.jpg',
//                    'params'=>['service'=>[30]]
//                  ];
    $buttons[] = [
                    'title'=>'Hair Consulting',
                    'sub-title'=>'150+ listings',
                    'image'=>'hair-consult.jpg',
                    'params'=>['service'=>[24]]
                  ];

    $buttons[] = [
                    'title'=>'Home Services',
                    'sub-title'=>'40+ listings',
                    'image'=>'home.jpg',
                    'params'=>['highlights'=>[13]]
                  ];

    $buttons[] = [
                    'title'=>'Gel Nails',
                    'sub-title'=>'600+ listings',
                    'image'=>'gel.jpg',
                    'params'=>['service'=>[46]]
                  ];

    $buttons[] = [
                    'title'=>'Weight Loss',
                    'sub-title'=>'80+ listings',
                    'image'=>'weight-loss.jpg',
                    'params'=>['service'=>[12]]
                  ];


    $buttons[] = [
                    'title'=>'Manicure',
                    'sub-title'=>'1000+ listings',
                    'image'=>'manicure.jpg',
                    'params'=>['service'=>[47]]
                  ];

    $buttons[] = [
                    'title'=>'Moroccan Bath',
                    'sub-title'=>'250+ listings',
                    'image'=>'moroccon.jpg',
                    'params'=>['service'=>[8]]
                  ];

    $buttons[] = [
                    'title'=>'Couple Massage',
                    'sub-title'=>'40+ listings',
                    'image'=>'couple.jpg',
                    'params'=>['service'=>[75]]
                  ];
    return $buttons;

  }

  public function email(){
    $user = User::where('email', '=', 'ali@fabogo.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->take(5)->get();


    return View::make('emails.weekly.posts', compact('user', 'posts'));
  }

  public function sitemapForCity(){
    $segment = Request::segment(1);
    $city = $segment;
    $letters = range('A', 'Z');
    $totals = [];

    foreach ($letters as $one) {
      $totals[$one] = ['venues'=>0, 'locations'=>0];
      $totals[$one]['venues'] = Business::select()->byLocale()->where('name', 'like', $one.'%')->get()->count();
      $totals[$one]['locations'] = Zone::select()->where('city_id', '=', MazkaraHelper::getLocaleID())->where('name', 'like', $one.'%')->get()->count();
    }

    return View::make('sitemap', compact('totals', 'city'));
  }

  public function sitemapVenuesForCity(){
    $segment = Request::segment(1);
    $city = $segment;
    $letter = Input::get('letter');
    $venues = Business::select()->byLocale()->where('name', 'like', $letter.'%')->paginate(80);
    return View::make('sitemap-venues', compact('letter', 'venues', 'city'));
  }

  public function sitemapLocationsForCity(){
    $segment = Request::segment(1);
    $city = $segment;

    $categories = Category::query()->byLocaleActive()->get();
    $letter = Input::get('letter');
    $locations = Zone::select()->where('city_id', '=', MazkaraHelper::getLocaleID())->where('name', 'like', $letter.'%')->paginate(30);
    return View::make('sitemap-locations', compact('letter', 'categories', 'locations', 'city'));
  }



  private function getIpAddressType($addr) {
    if (ip2long($addr) !== false) {
      return "ipv4";
    } else if (preg_match('/^[0-9a-fA-F:]+$/', $addr) && @inet_pton($addr)) {
      return "ipv6";
    }
    return false;
  }

  public function IPAddressLookup($addr) {
    if ($ret = $this->DoIPAddressLookup($this->getIpAddressType($addr), inet_pton($addr))) {
      $ret->ip_start = inet_ntop($ret->ip_start);
      $ret->ip_end = inet_ntop($ret->ip_end);
      return $ret;
    } else {
      return false;
    }
  }

  protected function DoIPAddressLookup($addr_type, $addr_start) {
    $res = \DB::table('dbip_lookup')
                  ->where('addr_type', '=', $addr_type)
                  ->where('ip_start', '<=', $addr_start)
                  ->orderby('ip_start', 'desc')->first();
    if($res){
      return $res;
    }else{
      return false;
    }

  }


  public function fabFinds(){
    $ip = Request::getClientIp(); //'27.106.17.74';//
    //$result = $this->IPAddressLookup($ip);

    $cc = Location::get($ip)->isoCode;
    if($cc == 'IN'){
      $cityName = Location::get($ip)->cityName;
      if(strstr(strtolower($cityName), 'mumbai')){
        $city = 'mumbai';
      }else{
        $city = 'pune';
      }
    }else{
      $city = 'dubai';
    }

    // if($result == false){
    //   $city = 'dubai';
    // }else{
    //   if(in_array(str_slug($result->city), array('dubai', 'sharjah', 'abu-dhabi', 'pune', 'mumbai'))){
    //     $city = str_slug($result->city);
    //   }elseif($result->country == 'IN'){
    //     $city = 'pune';
    //   }else{
    //     $city = 'dubai';
    //   }
    // }



    MazkaraHelper::setLocale($city);

    return Redirect::to(MazkaraHelper::slugCity(null, ['specials'=>'1']));

  }

  function getCards(){
      $business_count = Service::getBusinessTotalCount();

      $locale = strtolower(MazkaraHelper::getLocaleLabel());

      $cards = [];
      $cards[] =  [
          'name'=>'Blow Dry',
          'business_count'=>$business_count[22],          
          'image'=>(mzk_assets('services/hair/blowdry-5.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[22]]).'?'._apfabtrack('body')
        ];

      if(in_array($locale, ['pune', 'mumbai'])){
      $cards[] =  [
          'name'=>'Liposuction',
          'business_count'=>$business_count[7],
          'image'=>(mzk_assets('services/clinical/clinical-6.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[7]]).'?'._apfabtrack('body')
        ];

      }else{
        $cards[] =  [
            'name'=>'Body Scrub',
            'business_count'=>$business_count[59],
            'image'=>(mzk_assets('services/body/body-scrub-6.jpg')),
            'url'=>MazkaraHelper::slugCity(null, ['service'=>[59]]).'?'._apfabtrack('body')
          ];

      }
      $cards[] =  [
          'name'=>'Gel Nails',
          'business_count'=>$business_count[46],
          'image'=>(mzk_assets('services/nails/gel-nails-4.png')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[46]]).'?'._apfabtrack('body')
        ];
      $cards[] =  [
          'name'=>'Full Body Massage',
          'business_count'=>$business_count[53],
          'image'=>(mzk_assets('services/massage/full-body-massage-5.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[53]]).'?'._apfabtrack('body')
        ];

      if(in_array($locale, ['pune', 'mumbai'])){
        $cards[] =  [
            'name'=>'Gym',
            'business_count'=>$business_count[86],
            'image'=>(mzk_assets('services/fitness/gym-1.jpg')),
            'url'=>MazkaraHelper::slugCity(null, ['service'=>[86]]).'?'._apfabtrack('body')
          ];


      }else{
      $cards[] =  [
          'name'=>'Yoga',
          'business_count'=>$business_count[87],
          'image'=>(mzk_assets('services/fitness/yoga-4.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[87]]).'?'._apfabtrack('body')
        ];

      }
      $cards[] =  [
          'name'=>'Manicure',
          'business_count'=>$business_count[47],
          'image'=>(mzk_assets('services/nails/manicure-6.png')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[47]]).'?'._apfabtrack('body')
        ];
      $cards[] =  [
          'name'=>'Waxing',
          'business_count'=>$business_count[35],
          'image'=>(mzk_assets('services/hair-removal/waxing-1.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[35]]).'?'._apfabtrack('body')
        ];
      $cards[] =  [
          'name'=>'Hair Cuts',
          'business_count'=>$business_count[25],
          'image'=>(mzk_assets('services/hair/hair-cuts-and-hair-styling-9.jpg')),
          'url'=>MazkaraHelper::slugCity(null, ['service'=>[25]]).'?'._apfabtrack('body')
        ];


//        [
//          'name'=>'Shaving',
//          'business_count'=>Service::find(62)->getBusinessCount(),
//          'image'=>(mzk_assets('services/face/shaving-1.jpg')),
//          'url'=>MazkaraHelper::slugCity(null, ['service'=>[62]])
//        ],

      return $cards;


  }



	public function index(){

    $segment = Request::segment(1);
    if(!MazkaraHelper::isSetLocale()):
      $cc = Location::get(Request::getClientIp())->isoCode;
      
//      $ip = Request::getClientIp(); //'27.106.17.74';//
//      $result = $this->IPAddressLookup($ip);
//
//      if($result == false){
//        $city = 'dubai';
//      }else{
//        if(in_array(str_slug($result->city), array('dubai', 'sharjah', 'abu-dhabi', 'pune', 'mumbai'))){
//          $city = str_slug($result->city);
//        }elseif($result->country == 'IN'){
//          $city = 'pune';
//        }else{
//          $city = 'dubai';
//        }
//      }

      $segment = !is_null($segment) ? $segment :(($cc == "IN") ? 'pune' : $segment);//  $city; //
      //if($cc=='IN'){
        MazkaraHelper::setLocale($segment);
      //}
    endif;

    $description = !is_null($segment) ? sprintf(Lang::get('seo.city-description'), ucwords($segment)) : Lang::get('seo.description');

    $this->meta->set(
    		array('title' => Lang::get('seo.title').' - Discover salons and spas in your neighborhood', 
    					'description' => $description,
    					'og'=>[
    						'title' => Lang::get('seo.title'),
								'description' => $description
    					]
			)
  	);



    $business_count = 4500;

    $posts = Post::with('cover', 'likes', 'comments')->onlyPosts()->isViewable()->remember(180)->orderBy('published_on', 'DESC')->take(5)->get();
    $post_ids = [];
    foreach($posts as $post){
      $post_ids[] = $post->id;
    }

    $favorited_posts = [];

    if(Auth::check()){
      $user = Auth::user();
      $favorited_posts = $user->hasFavouritedMultiple('Post', $post_ids);
    }

    foreach($posts as $post){
      $post->has_cover = $post->hasCover();
      $post->image_medium_url = $post->has_cover?$post->cover->image->url('medium'):'';
      $post->image_small_url = $post->has_cover?$post->cover->image->url('small'):'';
      $post->isFavorite = in_array($post->id, $favorited_posts)?true:false;
    }

    $featured_venues = [];
    $featured = Business::select()->byLocale()->isFeatured()->remember(120)->orderBy('is_featured', 'ASC')->get();
    for($i=1;$i<=10;$i++){
      $featured_venues[$i] = null;
    }

    foreach($featured as $feature){
      $featured_venues[$feature->is_featured] = $feature;
    }

    if(count($featured) < 10){
      $es = Business::select()->byLocale()->hasPhotos()
                                          ->onlyActive()->where('reviews_count', '>', 2)
                                          ->remember(120)->orderBy('rating_average', 'DESC')->take(10)->get();
      $extras = [];

      if(count($es) == 0){
        $es = Business::select()->byLocale()->hasPhotos()
                                          ->onlyActive()
                                          ->remember(120)->orderBy('rating_average', 'DESC')->take(10)->get();
      }

      foreach($es as $e){
        $extras[] = $e;
      }

      foreach($featured_venues as $ii=>$feature){
        if($feature == null){
          $featured_venues[$ii] = array_pop($extras);
        }
      }

    }


    //$author_ids = \DB::table('posts')->where('type', '=', 'post')->where('state', '=', 'published')->groupby('author_id')->take(8)->lists('author_id','author_id');
    //$users = User::whereIn('id', $author_ids)->get();

    $offers = [];//Offer::select()->with('business')->byLocale()->groupby('business_id')->get()->take(4);
    
    $buttons = $this->getButtons();
    $cards = $this->getCards();

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;


    return View::make('index', compact('cards', 'offers', 'services', 'featured_venues', 'posts')) //'users',
                      ->with('canonical_url', $canonical_url)
                      ->with('meta', $this->meta)
                      ->with('business_count', $business_count);;

	}


  public function showCanonicalUrl(){
    $input = Input::all();
    $vars = ['page', 'sort', 'highlights', 'zones', 'fabtrack'];
    foreach($input as $ii=>$vv){
      if(in_array($ii, $vars)){
        
        return true;
      }
    }

    return false;
  }



  public function suspicious(){
    return View::make('suspicious');

  }



function getCurlData($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
    $curlData = curl_exec($curl);
    curl_close($curl);
    return $curlData;
}


  public function captcha(){


$siteKey="6LdXKAYTAAAAAHsjF7aoyORLse3Ppy8xRS0yPuwy"; //Enter your sitekey obtained from reCaptcha Vebsite
$secret="6LdXKAYTAAAAAOp0yC77CoKiRlrxrWIt4YJUvodQ"; // Enter your secret key obtained from reCaptcha Vebsite

$lang = 'en';   //Language of website here -- See Google's Recaptcha V2 for options

      $shieldsquare_username = "roy@mazkara.com"; // Enter the UserID of the user
      $shieldsquare_calltype = 1;
      $shieldsquare_pid = "";
    $shieldsquare_response = shieldsquare_ValidateRequest($shieldsquare_userid, $shieldsquare_calltype, $shieldsquare_pid);

    if(isset($_POST['g-recaptcha-response'])) {
      $response = $this->getCurlData("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$_POST['g-recaptcha-response']);
      $response = json_decode($response, true);
    if($response["success"] === true) {
      $shieldsquare_config_data = new \shieldsquare_config();

      $redirPage = isset($_COOKIE["currentPagename"])?$_COOKIE["currentPagename"]:'/';
      setcookie(md5("captchaResponse"),md5("1".$redirPage.$_SERVER[$shieldsquare_config_data->_ipaddress].$shieldsquare_config_data->_sid),time()+60*60,"/");
      return Redirect::to($redirPage);
    }else{
      return Redirect::to($_SERVER["PHP_SELF"]);
    }
}


    return View::make('captcha', compact('siteKey', 'lang'));

  }


  public function feed(){
    //if (App::environment('production')){
    //  return View::make('hello');
    //}
    //remember(60)
    $zones = Zone::select()->showActive()->byLocale()->defaultOrder()->get()->linkNodes();
    $this->meta->set(
        array('title' => Lang::get('seo.title').' - Discover salons and spas in your neighborhood', 
              'description' => Lang::get('seo.description'),
              'og'=>[
                'title' => Lang::get('seo.title'),
                'description' => Lang::get('seo.description')
              ]
      )
    );

    $buttons = $this->getButtons();

    $this->layout = View::make('layouts.page')->with('meta', $this->meta);
    if(Auth::check()){
      $user = User::find(Auth::user()->id);
      $followed_users = array_keys($user->followsUsers()->lists('favorable_id','favorable_id'));
      if(Auth::user()->hasRole('admin')){
      $activities = Activity::select()->orderby('id', 'desc')->paginate(10);

      }else{
        $activities = Activity::select()->byUserIds($followed_users)->orderby('id', 'desc')->paginate(10);
      }
      $suggested_salons = Business::select()->hasPhotos()->onlyActive()->where('zone_id', '>', 0)->takeRandom(10)->get();

      $suggested_users_to_follow = User::select()->excludeUser($user->id)->orderBy('id', 'DESC')->take(10);//notFollowedBy($user->id);
      $this->layout->content = View::make('site.users.index', compact('zones', 'buttons', 'suggested_salons', 'suggested_users_to_follow', 'user', 'services', 'activities' ));
    }
  }

  public function theFabReviewContest(){

    $this->meta->set(
        array('title' => 'The Fab Review Contest - '.mzk_seo_name(), 
              'description' => Lang::get('seo.description'),
              'og'=>[
                'title' => 'The Fab Review Contest - '.mzk_seo_name(),
                'description' => Lang::get('seo.description')
              ]
      )
    );

    $dubai_user = User::find(3138);
    $pune_user = User::find(3090);

    $dubai_review = Review::find(11170);
    $pune_review = Review::find(11157);


    return View::make('review-contest', compact('dubai_user', 'dubai_review', 'pune_review', 'pune_user'))->with('meta', $this->meta);
  }


	public function jobs(){
		$cities = Zone::getCitiesArray();

    $this->meta->set(
    		array('title' => Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities), 
    					'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities),
    					'og'=>[
    						'title' => Lang::get('seo.jobs_title').', Jobs in '.join(',',$cities),
								'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities)
    					]
			)
  	);

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
    $jobs = Job::where('state','=', 'active')->get();

		return View::make('jobs', compact('jobs', 'cities'))->with('meta', $this->meta)
                                  ->with('canonical_url', $canonical_url);
	}

	public function showJob($id){
		$cities = Zone::getCitiesArray();
		$job = Job::find($id);

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    $this->meta->set(
    		array('title' => $job->title.','.Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities), 
    					'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities),
    					'og'=>[
    						'title' => $job->title.','.Lang::get('seo.jobs_title').', Jobs in '.join(', ',$cities),
								'description' => Lang::get('seo.jobs_description').', Jobs in '.join(', ',$cities)
    					]
			)
  	);

		return View::make('job', compact('job', 'cities'))->with('meta', $this->meta)
                                  ->with('canonical_url', $canonical_url);
	}

  public function changeLocale($from, $to){
    $redirect_to = URL::previous();
    MazkaraHelper::setLocale($to);

    if(strstr($redirect_to.'/', '/'.$from.'/') || (rtrim($redirect_to, '/') == Request::root())){
      $redirect_to = rtrim(Request::root(), '/').'/'.$to;
    }

    return Redirect::to($redirect_to);
  }


  public function applyJob(){

    $input = Input::all();


    $rules = array(
        'name' => 'Required|Min:7',
        'email'     => 'Required|Email',
        'phone' => 'Required',
        'message' => 'Required',
        'cv' => 'Required'
    );

    $validation = Validator::make(Input::all(), $rules);

    if ($validation->fails()){
        // Validation has failed.
        return Redirect::back()->withInput()->withErrors($validation);
    }

    $input['job'] = Job::find($input['job_id']);
    $data = $input;
    Mail::send('emails.application',['data'=>$input, 'server'=>$_SERVER], function($message) use ($input)
    {
        $message->to('jobs@fabogo.com');
        $message->subject($input['name'].' applied for post of '.$input['job']->title.' on '.mzk_seo_name().' ['.time().']');
        $message->from($input['email']);
        $message->attach($input['cv']->getRealPath(), array(
            'as' => 'resume.' . $input['cv']->getClientOriginalExtension(), 
            'mime' => $input['cv']->getMimeType())
        );
    });

    return Redirect::back()->with('notice', 'Thanks for applying we will get back to you soon');
  }

  public function redirectToClientDashboard(){
    $input = Input::all();
    mzk_client_set_default_business($input['id']);
    return Redirect::to('/partner/'.$input['id']);
  }

  public function culture(){
    $this->meta->set(['title'=>'Culture at '.mzk_seo_name()]);
    return View::make('culture')->with('meta', $this->meta);
  }

  public function newsletterSubscribe(){
    $user = User::find(Auth::user()->id);
    $user->allowNewsletter();
    $user->save();
    return View::make('subscribed-newsletter');
  }


	public function about(){
		$this->meta->set(['title'=>'About '.mzk_seo_name()]);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		return View::make('about')->with('meta', $this->meta)
                              ->with('canonical_url', $canonical_url);
	}

	public function contact(){
		$this->meta->set(['title'=>'Contact us at '.mzk_seo_name()]);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		return View::make('contact')->with('meta', $this->meta)
                                ->with('canonical_url', $canonical_url);
	}

	public function tos(){
		$this->meta->set(['title'=>mzk_seo_name().' - Terms of Service']);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		return View::make('tos')->with('meta', $this->meta)
                            ->with('canonical_url', $canonical_url);
	}

	public function privacy(){
		$this->meta->set(['title'=>mzk_seo_name().' - Privacy Policy']);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		return View::make('privacy')->with('meta', $this->meta)
                                ->with('canonical_url', $canonical_url);
	}

	public function cookie(){
		$this->meta->set(['title'=>mzk_seo_name().' - Cookie Policy']);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		return View::make('cookie')->with('meta', $this->meta)
                              ->with('canonical_url', $canonical_url);
	}
  
  public function faq(){
    $this->meta->set(['title'=>mzk_seo_name().' - FAQ']);
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
    return View::make('faq')->with('meta', $this->meta)
                            ->with('canonical_url', $canonical_url);
  }

	public function postContact(){
		$rules = array('name'=>'required','phone'=>'required','email'=>'required','message'=>'required');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.contact', ['data'=>$input, 'server'=>$_SERVER], function($message) {
				$message->to('hello@fabogo.com', 'Mazkara')
                ->cc('business@fabogo.com', 'Mazkara')
                ->subject('Mazkara: Message from contact form ['.time().']');
			});			

			return Redirect::back()->with('notice', 'Thanks for getting in touch.');
		}
		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}



  public function unsubscribe(){
    $rules = array('email'=>'required');
    $input = Input::all();
    $validation = Validator::make($input, $rules);

    if ($validation->passes()){

      Mail::send('emails.unsubscribe', ['data'=>$input['email'], 'server'=>$_SERVER], function($message) use ($input) {
        $message->to('marketing@fabogo.com', 'Mazkara')
                ->subject('Mazkara: Unsubscribe '.$input['email'].'  ['.time().']');
      });     
      $this->layout = View::make('layouts.page');
      $this->layout->content = View::make('unsubscribe', ['email'=>$input['email']]);
    }else{
      return Redirect::to('/error');
    }
  }

  public function apiRedirectHack(){
    $url = 'http://api.mazkara.com/white?'.$_SERVER['QUERY_STRING'];
    return Redirect::to($url);
  }

  public function showWelcome()
  {
    return View::make('hello');
  }
  
  public function blocked()
  {
    return View::make('blocked');
  }

	public function zones(){
	}

	public function getReport(){
		$input = Input::all();
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;
		$url = $input['url'];
		return View::make('report', compact('url'))->with('canonical_url', $canonical_url);
	}

	public function postReport(){
		$rules = array('name'=>'required','phone'=>'required','email'=>'required','message'=>'required');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.report', ['data'=>$input, 'server'=>$_SERVER], function($message) {
				$message->to('moderation@fabogo.com', 'Mazkara')
                ->subject('Mazkara['.MazkaraHelper::getLocaleLabel().']: Has a Report ['.time().']');
			});			

			return Redirect::back()->with('notice', 'Thanks - a customer support executive would contact you soon.');
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}

	public function getAddBusiness(){
		$this->meta->set(['title'=>'Add your business on Fabogo']);
    $siteKey="6LdXKAYTAAAAAHsjF7aoyORLse3Ppy8xRS0yPuwy"; //Enter your sitekey obtained from reCaptcha Vebsite
    $secret="6LdXKAYTAAAAAOp0yC77CoKiRlrxrWIt4YJUvodQ"; // Enter your secret key obtained from reCaptcha Vebsite

    $lang = 'en';   //Language of website here -- See Google's Recaptcha V2 for options
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

		return View::make('add-business', compact('siteKey','secret','lang'))->with('meta', $this->meta)
                        ->with('canonical_url', $canonical_url);
	}

	public function postAddBusiness(){
		$rules = array('name'=>'required', 'phone'=>'required', 'email'=>'required|email');
		$input = Input::all();
		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

			Mail::send('emails.add-business', ['data'=>$input, 'server'=>$_SERVER], function($message)  use ($input) {
				$message->to('business@fabogo.com', 'Fabogo')
                ->subject('Fabogo['.MazkaraHelper::getLocaleLabel().']: '.$input['name'].' wants to add a new business ['.time(). ']');
			});			
			return Redirect::back()->with('notice', 'We have received your request and will get in touch with you shortly');
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

		return Redirect::back();
	}


  public function share(){
    $data = Input::all();
    $status_update = ['message' => $data['message']];
  	$status_update['link'] = $data['url'];

  	if($data['sharable']=='Business'):
	  	$business = Business::find($data['sharable_id']);
			$status_update['name'] = $business->getSeoTitle();
			$status_update['description'] = $business->getSeoDesc();
			$status_update['picture'] = $business->getSeoThumbnailUrl();
		elseif($data['sharable']=='Review'):
	  	$review = Review::find($data['sharable_id']);
	  	$business = $review->business;

	  	if(Auth::user()->id == $review->user_id):
				$status_update['name'] = 'I just reviewed '.$business->getSeoTitle();
			else:
				$status_update['name'] = 'Check out this review for '.$business->getSeoTitle();
			endif;
			$status_update['description'] = $review->body;
			$status_update['picture'] = $business->getSeoThumbnailUrl();
		endif;

    $token =  MazkaraHelper::fbToken();
    Facebook::setAccessToken($token);
    $share_data = array('type'=>Share::FACEBOOK, 
                        'user_id'=>Auth::user()->id,
                        'sharable_type'=>$data['sharable_type'],
                        'sharable_id'=>$data['sharable_id']);
    Share::create($share_data);

    $response = Facebook::object('me/feed')->with($status_update)->post();
  }

  public function searchZones(){
    //$cities = Zone::query()->cities()->showActive()->get()->lists('id', 'id');
    $result = [];
    //->whereIn('city_id', $cities+[null,0])
    $url = URL::previous();

    if(Input::has('term')):
      $search = Input::get('term');
      $zones = Zone::query()->search($search)->showActive()->get()->take(10);
      foreach($zones as $zone){
        $result[] = [ 'id' => $zone->id, 'value' => $zone->id, 'name' => $zone->name, 
                      'label' => $zone->isCity() ? join(' ', [$zone->name, ' ('.$zone->countryFromCountryCode().')']) : join(' ', [$zone->name, ' ('.$zone->parent->name.')']), 
                      'url'=> $zone->isCity()? mzk_url_city_change($url, $zone->slug):false,
                      'type' => $zone->isCity() ? 'city' : 'zone'];
      }
      if(count($result)<10){
        $zs = Zone::query()->search($search)->showNotActive()->lists('id','id');
        $znes = Zone::query()->showActive()->whereIn('parent_id', $zs)->get()->take(10-count($result));
        
        foreach($znes as $zone){
          $result[] = [ 'id' => $zone->id, 'value' => $zone->id, 'name' => $zone->name, 
                        'label' => $zone->isCity() ? join(' ', [$zone->name, ' ('.$zone->countryFromCountryCode().')']) : join(' ', [$zone->name, ' ('.$zone->parent->name.')']), 
                        'url'=> $zone->isCity()? mzk_url_city_change($url, $zone->slug):false,
                        'type' => $zone->isCity() ? 'city' : 'zone'];
        }

      }
    endif;

    return Response::Json($result);
  }

	public function search(){
		$q = Input::get('term');
		$service_lists = Service::query()->showOnly()->byLocaleActive()->showOnly()->search($q)->take(5)->get();
    $num_outlets = count($service_lists) < 10 ?(10 - count($service_lists))+2:2; // changed 5 to 10
    $outlets = Business::query()->select('name', 'zone_cache', 'id', 'slug')->searchBasic($q)->byLocale(mzk_get_localeID())->onlyActive()->take($num_outlets)->get();
    $categories = Category::query()->byLocaleActiveDisplayable()->search($q)->take(3)->get()->toArray();

		$chains = Group::search($q)->byType('chain')->byLocale()->take(3)->get();
		$result = [];
		$zone = Input::get('zone');
		if(empty($zone)){
			$zone = false;
		}else{
			$zone = Zone::find($zone);
		}
    $services = [];
    foreach($service_lists as $v){
      
      $p = [
            'service' =>[
                          $v->id
                        ]
            ];
      $service_name = '';
      if($v->parent_id > 0){
        $service_name = '('.$v->parent->name.') ';
      }

      $service_name.= $v->name;

      $result[] = [ 'name'=>$service_name, 'id'=>$v->id, 'type'=>'SERVICE', 
                    'url' => ($zone ? MazkaraHelper::slugCityZone($zone, $p):MazkaraHelper::slugCity(null, $p)) ];

    }

		foreach($outlets as $v){
			$result[] = ['name'=>$v->name, 
										'zone'=>$v->zone_cache,
									'type'=>'OUTLET', 
									'url'=>MazkaraHelper::slugSingle($v)];
		}

	  foreach($categories as $v){
			$p = ['category'=>[$v['id']]];
			$result[] = ['id'=>$v['id'],'name'=>$v['name'], 
									'type'=>'CATEGORY', 
									//'url' => ($zone?MazkaraHelper::slugCityZone($zone, $p):MazkaraHelper::slugCity(null, $p)) 
                  ];
		}


		foreach($chains as $v){
			$result[] = [	'name'=>$v->name, 'type'=>'CHAIN', 'label'=>$v->businesses->count().' OUTLET(S)', 
									'url' => (MazkaraHelper::slugCityChain(null, $v)) ];
  	}

		return Response::json($result);

	}

}
