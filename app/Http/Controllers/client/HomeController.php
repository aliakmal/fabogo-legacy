<?php
namespace App\Http\Controllers\client;
use App\Http\Controllers\Controller;

use Confide, View, Config, Validator, Redirect, Input;
use  App, Auth, DB, Response;

use App\Models\User;
use App\Models\Ad_set;
use App\Models\Call_log;
use App\Models\Check_in;
use App\Models\Ad_slot;
use App\Models\Campaign;
use App\Models\Business;
use App\Models\Counter;
use App\Models\Counterbase;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function dashboard($bid = false)
	{
		$user = User::find(Auth::user()->id);
		$businesses = $user->businesses();

		$current_business_id = $bid ? $bid : mzk_client_get_default_business();
		$business = Business::find($current_business_id);
    if(!$business->canCurrentUserAccessClientDashboard(Auth::user())){
    	return Redirect::to('/');
    }

		$data = [];

		$start_date = \Carbon\Carbon::now()->subDays(30);
		$end_date  = \Carbon\Carbon::now();
		$counts = Counter::select()->byBusiness($current_business_id)->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$ix = strtotime($vv->dated)*1000;
			if(!isset($buffer[$ix])){
				$buffer[$ix] = 0;
			}
			$buffer[$ix] += $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));
		$interval = 0;

		if(count($date_range) > 30){
			$interval = ceil($date_range/10);
		}

		foreach($date_range as $ii=>$vv){
			if($interval > 0){
				if($ii>0){
					if($ii%$interval!=0){
						continue;
					}
				}
			}
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_page_views = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->sum('views');
		$count_avg_page_views = ceil($count_page_views/$ii);
		$count_checkins = $business->check_ins()->count();
		$count_favorites = $business->favourites()->count();


		$counts = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000] = $vv->views;
		}
		
		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_call_views = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');

		return View::make('client.home.dashboard', compact( 'user', 'count_call_views', 'start_date', 'end_date',
																											  'business', 'count_page_views', 'count_avg_page_views',
																											  'count_checkins', 'count_favorites',
																											  'businesses', 'data'));
	}

	public function setCurrentBusiness($id){
		mzk_client_set_default_business($id);
    return Redirect::back();
	}

	public function clientHomeRedirector(){
		if(Auth::user()->hasRole('admin')){
			$b_id = Business::onlyActive()->orderBy(DB::raw('RAND()'))->take(1)->get()->first()->id;
		}else{
			$u = User::find(Auth::user()->id);
	    
	    $users_businesses = $u->businesses();
	    $ids = [];

	    foreach($users_businesses as $business){
	      $ids[] = $business->id;
	    }

	    $b_id = $ids[0];
		}

    mzk_client_set_default_business($b_id);
		return Redirect::to('/partner/'.$b_id);
	}


	public function getViewsDataApi($bid = false)
	{
		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$data = [];
		$current_business_id = $input['business_id'];
		$interval = isset($input['interval'])?$input['interval']:'daily';
		
		$counterBase = new Counterbase();
		
		$view_counts = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->get();
		$call_counts = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();

    if(mzk_is_counters_divided()){
			$view_counts_archive = $counterBase->select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->get();
			$call_counts_archive = $counterBase->select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();
    }else{
			$view_counts_archive = [];
			$call_counts_archive = [];
    }

		$view_buffer = [];
		foreach($view_counts as $vv){
			$ix = strtotime($vv->dated)*1000;
			if(!isset($view_buffer[$ix])){
				$view_buffer[$ix] = 0;
			}
			$view_buffer[$ix]+= $vv->views;
		}

		foreach($view_counts_archive as $vv){
			$ix = strtotime($vv->dated)*1000;
			if(!isset($view_buffer[$ix])){
				$view_buffer[$ix] = 0;
			}

			$view_buffer[$ix]+= $vv->views;
		}

		$call_counts_buffer = [];
		foreach($call_counts as $vv){
			$ix = strtotime($vv->dated)*1000;
			if(!isset($call_counts_buffer[$ix])){
				$call_counts_buffer[$ix] = 0;
			}

			$call_counts_buffer[$ix]+= $vv->views;
		}

		foreach($call_counts_archive as $vv){
			$ix = strtotime($vv->dated)*1000;
			if(!isset($call_counts_buffer[$ix])){
				$call_counts_buffer[$ix] = 0;
			}

			$call_counts_buffer[$ix]+= $vv->views;
		}

		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));
		

		$datediff = strtotime($end_date) - strtotime($start_date);
		$increments = floor($datediff/(60*60*24));
   
		$call_counts = 0;
		$view_counts = 0;
		$cc = 0;

		foreach($date_range as $ii=>$vv){
			$cc++;
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			if($interval == 'weekly'){
				// skip except the 7th entry
				if(($cc!=1)&&($cc%7 != 0)){
					$call_counts += isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_counts += isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;
				}else{
					$call_counts += isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_counts += isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;

					$call_count = $call_counts; // isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_count = $view_counts; // isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;

					$data[] = ['dates'=>$dt, 'calls'=>$call_count, 'views'=>$view_count ];
					$call_counts = 0;
					$view_counts = 0;
				}
			}elseif($interval == 'monthly'){
				// skip except the 7th entry
				if(($cc!=1)&&($cc%29 != 0)){
					$call_counts += isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_counts += isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;
				}else{
					$call_counts += isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_counts += isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;

					$call_count = $call_counts;// isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
					$view_count = $view_counts;// isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;

					$start_month_epoch = strtotime(date('Y-m-01', $dt/1000))*1000;

					$data[] = ['dates'=>$start_month_epoch/*$dt*/, 'calls'=>$call_count, 'views'=>$view_count ];
					$call_counts = 0;
					$view_counts = 0;
				}

			}else{
				$call_counts = isset($call_counts_buffer[$dt]) ? $call_counts_buffer[$dt] : 0;
				$view_counts = isset($view_buffer[$dt]) ? $view_buffer[$dt] : 0;

				$data[] = ['dates'=>$dt, 'calls'=>$call_counts, 'views'=>$view_counts ];
			}
		}

		if(count($data)<2){
			$data = [];
			$call_counts = reset($call_counts_buffer);
			$view_counts = reset($view_buffer);
			$data[] = ['dates'=>key($call_counts_buffer), 'calls'=>$call_counts, 'views'=>$view_counts ];

			$call_counts = end($call_counts_buffer);
			$view_counts = end($view_buffer);
			$data[] = ['dates'=>key($call_counts_buffer), 'calls'=>array_sum($call_counts_buffer), 'views'=>array_sum($view_buffer) ];
		}
		//$dt = strtotime($vv->format('Y-m-d'))*1000;
		//$data[] = ['dates'=>$dt, 'views'=>( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		$count_call_views = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');
		$count_page_views = Counter::select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->sum('views');

		$count_call_views += $counterBase->select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');
		$count_page_views += $counterBase->select()->byBusiness($current_business_id)->byType('page-views')->betweenDates($start_date, $end_date)->sum('views');

		$count_avg_page_views = ceil($count_page_views/$ii);
		$response = ['data'=>$data, 'interval'=>($interval=='daily'?1:7), 'count_call_views'=>$count_call_views,  'count_avg_page_views'=>$count_avg_page_views, 'count_page_views'=>$count_page_views, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);
	}


	public function getCallsDataApi($bid = false){

		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$current_business_id = $input['business_id'];
		$counterBase = new Counterbase();
		$counts = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();

    if(mzk_is_counters_divided()){
			$counts_archive = $counterBase->select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->get();
    }else{
			$counts_archive = [];
    }

		$buffer = [];
		foreach($counts as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}

		foreach($counts_archive as $vv){
			$buffer[strtotime($vv->dated)*1000]= $vv->views;
		}

		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = ['dates'=>$dt, 'views'=>( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$count_call_views = Counter::select()->byBusiness($current_business_id)->byType('call-views')->betweenDates($start_date, $end_date)->sum('views');

		$response = ['data'=>$data, 'count_call_views'=>$count_call_views, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);
	}

	public function getCallLogsDataApi(){
		$input = Input::all();

		$start_date = $input['start'];
		$end_date  = $input['end'];
		$current_business_id = $input['business_id'];
		$call_logs = DB::table('call_logs')->select('id', DB::raw('count(*) as total'))
													->where('business_id', '=', $current_business_id)
													->where('dated', '>', $start_date)->where('dated', '<=', $end_date)
													->groupBy('dated')->get();
		$buffer = [];
		foreach($call_logs as $vv){
			$buffer[strtotime($vv->dated)*1000] = $vv->total;
		}

		$date_range = mzk_create_date_range_array(mzk_f_date($start_date), mzk_f_date($end_date));

		foreach($date_range as $vv){
			$dt = strtotime($vv->format('Y-m-d'))*1000;
			$data[] = [$dt, ( isset($buffer[$dt]) ? $buffer[$dt] : 0) ];
		}

		$response = ['data'=>$data, 'start'=>strtotime($start_date)*1000, 'end'=>strtotime($end_date)*1000];

		return Response::Json($response);
	}
}
