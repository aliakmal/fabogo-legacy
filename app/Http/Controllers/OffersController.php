<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, Mail, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Models\Offer;

class OffersController extends Controller {

	/**
	 * Package Repository
	 *
	 * @var Package
	 */
	protected $offer;

	public function __construct(Offer $offer)
	{
		$this->offer = $offer;
	}

	public function my(){
		$input = Input::all();
		$user = User::find(Auth::user()->id);
		$vouchers = Voucher::query()->byUser($user->id);
		if(isset($input['state'])){
			$vouchers->byState($input['state']);
		}

		$params = $input;
		$vouchers->orderBy('id', 'DESC');
    $vouchers = $vouchers->paginate(10);
    $this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.users.offers.index', compact('user', 'params', 'vouchers'));

	}

	public function pdf($id){
		$user = User::find(Auth::user()->id);
		$voucher = Voucher::find($id);

		if(!($voucher->user_id == $user->id)){
			return Redirect::back();
		}

		$html =   View::make('site.offers.pdf', compact('voucher'))->render();
		$pdf = App::make('dompdf');
		return $pdf->loadHTML($html)->download('voucher-'.$voucher->id.'.pdf');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getClaim($id){
		$offer = $this->offer->find($id);
		$user = User::find(Auth::user()->id);
		$business = $offer->business;
		return View::make('site.offers.claim', compact('offer', 'business', 'user'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postClaim($id)
	{
		$offer = Offer::find($id);
		$business = $offer->business;

		$input = Input::all();
		// update the users phone
		$user = User::find(Auth::user()->id);
		$user->phone = $input['phone'];
		$user->save();
		if($offer->isClaimable()){

			if(Voucher::byUser($user->id)->byOffer($offer->id)->get()->count()>=3){
				$result = array();
				$view = View::make('site.offers.limit-claim', compact('user', 'business', 'offer', 'voucher'));
				$result['html'] = $view->render();
			}else{
				$data = array('offer_id'=>$id, 'user_id'=>$user->id);

				$voucher = Voucher::create($data);

				$result = array();
				$view = View::make('site.offers.success-claim', compact('user', 'offer', 'business', 'voucher'));
				$result['html'] = $view->render();


				$html =   View::make('site.offers.pdf', compact('voucher'))->render();

				$user = $user->toArray();
				$offer = $voucher->offer->toArray();
				$voucher = $voucher->toArray();
				$d = array(	'voucher'	=>	$voucher, 
										'user'		=>	$user, 
										'offer'		=>	$offer);

				Mail::queue('emails.claim-voucher', ['data'=>$d], function($message) use($offer, $voucher, $user, $html)
				{
						$pdf = App::make('dompdf');

						$path = storage_path().'/media/voucher-'.md5(time()).'-'.$voucher['id'].'.pdf';
						$pdf->loadHTML($html)->save($path);

				    $message->from('no-reply@fabogo.com', 'Mazkara');

				    $message->to($user['email'])->subject('Here is your Mazkara voucher for '.$offer['title']);

				    $message->attach($path);
				});



			}

		}else{
			$result = array();
			$view = View::make('site.offers.fail-claim', compact('user', 'offer', 'business', 'voucher'));
			$result['html'] = $view->render();

		}

		return Response::Json($result);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$package = $this->package->findOrFail($id);

		return View::make('packages.show', compact('package'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$package = $this->package->find($id);

		if (is_null($package))
		{
			return Redirect::route('packages.index');
		}

		return View::make('packages.edit', compact('package'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Package::$rules);

		if ($validation->passes())
		{
			$package = $this->package->find($id);
			$package->update($input);

			return Redirect::route('packages.show', $id);
		}

		return Redirect::route('packages.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->package->find($id)->delete();

		return Redirect::route('packages.index');
	}

}
