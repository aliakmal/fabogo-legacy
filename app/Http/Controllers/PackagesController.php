<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;

class PackagesController extends Controller {

	/**
	 * Package Repository
	 *
	 * @var Package
	 */
	protected $package;

	public function __construct(Package $package)
	{
		$this->package = $package;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$packages = $this->package->all();

		return View::make('packages.index', compact('packages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('packages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Package::$rules);

		if ($validation->passes())
		{
			$this->package->create($input);

			return Redirect::route('packages.index');
		}

		return Redirect::route('packages.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$package = $this->package->findOrFail($id);

		return View::make('packages.show', compact('package'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$package = $this->package->find($id);

		if (is_null($package))
		{
			return Redirect::route('packages.index');
		}

		return View::make('packages.edit', compact('package'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Package::$rules);

		if ($validation->passes())
		{
			$package = $this->package->find($id);
			$package->update($input);

			return Redirect::route('packages.show', $id);
		}

		return Redirect::route('packages.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->package->find($id)->delete();

		return Redirect::route('packages.index');
	}

}
