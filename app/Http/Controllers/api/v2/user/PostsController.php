<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Business, Input, Validator;
use Chrisbjr\ApiGuard\Controllers\ApiGuardController;
use Chrisbjr\ApiGuard\Models\ApiKey;
use Chrisbjr\ApiGuard\Transformers\ApiKeyTransformer;
use Confide, Category, Service, Timing, Highlight, Photo;
use User, DB,  Response, Zone, Post, Favorite, Selfie, MazkaraHelper, Paginator;

class PostsController extends \BaseController {

  use ControllerTrait;

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
	protected $post;
	protected $params;

	protected $filters;

	public function __construct(Post $post){
		$this->post = $post;
		$this->params = [];
		$this->filters = [];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $input = Input::all();
    $count = isset($input['count'])?$input['count']:10;
		$this->post = $this->post->with('cover', 'likes', 'comments')->isViewable();
		$this->filterAllPosts();
		$posts = $this->post->orderBy('id', 'DESC')->paginate($count);
    $user_id = $this->getUserIdFromApi();

    foreach($posts as $ii=>$post){
      $posts[$ii]->cover_url = $post->cover_url;//?$post->cover->image->url():$post->cover;
      $posts[$ii]->hasLiked = $post->liked($user_id);
      $posts[$ii]->likeCount = $post->num_likes();
      $posts[$ii]->commentCount = $post->num_comments();
      $posts[$ii]->cover = null;

      $posts[$ii]->bookmarked = $post->isBookmarked($user_id)?true:false;
    }

    $data = $posts->toArray();
    $result = mzk_api_response($data, 200, true, 'Success');
    return $this->response->array($result);
	}

	public function filterAllPosts(){
    $this->filterByType();
		$this->filterByAuthor();
    $this->filterBySearch();
    $this->filterByBookmarks();
		$this->filterByServiceTag();
	}

  public function filterByType(){
    if(Input::has('type') && (Input::get('type')!='')){
      $type = Input::get('type');
      switch($type){
        case POST::ARTICLE:
          $this->post->onlyPosts();
        break;
        case POST::SELFIE:
          $this->post->onlySelfies();
        break;
        case POST::VIDEO:
          $this->post->onlyVideos();
        break;
      }
    }
  }
  public function filterByBookmarks(){
    if(Input::has('bookmark') && (Input::get('bookmark')!='')){
      if(Input::has('bookmarker_id') && (Input::get('bookmarker_id')!='')){
        $user_id = Input::get('bookmarker_id');
      }else{
        $user_id = $this->getUserIdFromApi();
      }


      $this->post->ofBookmarkedBy($user_id);
      $this->setParams('bookmark', Input::get('bookmark'));
    }
  }

  public function filterByAuthor(){
    if(Input::has('author') && (Input::get('author')!='')){
      $author = Input::get('author');
      $this->post->ofAuthors($author);
      $this->setParams('author', $author);
      $author = User::find($author);
      $this->setFilters('second-heading', 'Stories by '.$author->full_name);
    }
  }

	public function filterBySearch(){
    if(Input::has('search') && (Input::get('search')!='')){
    	$search = Input::get('search');
    	$this->post->ofSearch($search);
	    $this->setParams('search', $search);
    }
	}

	public function filterByServiceTag(){
    if(Input::has('st') && (Input::get('st')!='')){
    	$st = Input::get('st');
    	$this->post->ofServices($st);
	    $this->setParams('st', $st);
    }
	}
  protected function setFilters($key, $val){
    $this->filters[$key] = $val;
  }

  protected function getFilters($key){
    return isset($this->filters[$key]) ? $this->filters[$key] : '';
  }

	protected function setParams($key, $val){
		$this->params[$key] = $val;
	}

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(){
    $id = Input::get('id');
    $post = $this->post->find($id);

    if(!$post){
      $result = mzk_api_response([], 200, true, 'Error - no post found');
      return $this->response->array($result);
    }
		switch($post->type){
      case POST::ARTICLE:
        return $this->showPost($post);
      break;
      case POST::VIDEO:
        return $this->showVideo($post);
      break;
      case POST::SELFIE:
        return $this->showPhoto($post);
      break;
    }
	}

  public function showPost($post){
    $user = User::find($this->getUserIdFromApi());
    $bookmarked = $post->isBookmarked($user->id)?true:false;
    $data = $post->toArray();
    $data['cover_url'] = $post->cover_url;//?$post->cover->image->url():$post->cover;
    $data['bookmarked'] = $bookmarked;//?$post->cover->image->url():$post->cover;

    $service = $post->services()->first();

    $suggested_posts = Post::with('cover')->isViewable()->onlyPosts()->ofServices([$service->id])->take(5)->get();
    $businesses_with_prices = Business::withServices([$service->id], true)
                                            ->isDisplayable()
                                            ->byLocale($this->getUsersLocaleFromApi())
                                            ->get()->take(4);

    $offers = Offer::select()->with('business')
                              ->ofServices([$service->id])
                              ->onlyActive()->get()->take(4);


    $result = [ 'post'=>$data, 'suggested_posts'=>$suggested_posts, 
                'businesses'=>$businesses_with_prices->toArray(),
                'offers'=>$offer->toArray()];

    $result = mzk_api_response($result, 200, true, 'Success');
    return $this->response->array($result);
  }

  public function showPhoto($post){
    $user = User::find($this->getUserIdFromApi());
    $bookmarked = $post->isBookmarked($user->id)?true:false;

    $data = $post->toArray();
    $data['cover_url'] = $post->cover_url;//?$post->cover->image->url():$post->cover;
    $data['bookmarked'] = $bookmarked;//?$post->cover->image->url():$post->cover;
    $service = $post->services()->first();

    $suggested_posts = Post::with('cover')->isViewable()->onlyPosts()->ofServices([$service->id])->take(5)->get();
    $businesses_with_prices = Business::withServices([$service->id], true)
                                            ->isDisplayable()
                                            ->byLocale($this->getUsersLocaleFromApi())
                                            ->get()->take(4);

    $offers = Offer::select()->with('business')
                              ->ofServices([$service->id])
                              ->onlyActive()->get()->take(4);


    $result = [ 'post'=>$data, 'suggested_posts'=>$suggested_posts, 
                'businesses'=>$businesses_with_prices->toArray(),
                'offers'=>$offer->toArray()];
    $result = mzk_api_response($result, 200, true, 'Success');

    return $this->response->array($result);
  }

  public function showVideo($post){
    $user = User::find($this->getUserIdFromApi());
    $bookmarked = $post->isBookmarked($user->id)?true:false;
    $data = $post->toArray();
    $data['cover_url'] = $post->cover_url;//?$post->cover->image->url():$post->cover;
    $data['url']  = $post->videoUrl();
    $data['bookmarked'] = $bookmarked;//?$post->cover->image->url():$post->cover;
    $service = $post->services()->first();

    $suggested_posts = Post::with('cover')->isViewable()->onlyPosts()->ofServices([$service->id])->take(5)->get();
    $businesses_with_prices = Business::withServices([$service->id], true)
                                            ->isDisplayable()
                                            ->byLocale($this->getUsersLocaleFromApi())
                                            ->get()->take(4);

    $offers = Offer::select()->with('business')
                              ->ofServices([$service->id])
                              ->onlyActive()->get()->take(4);


    $result = [ 'post'=>$data, 'suggested_posts'=>$suggested_posts, 
                'businesses'=>$businesses_with_prices->toArray(),
                'offers'=>$offer->toArray()];
    $result = mzk_api_response($result, 200, true, 'Success');

    return $this->response->array($result);
  }

  public function follow(){
    $user = User::find($this->getUserIdFromApi());
    $input = Input::all();
    $id = $input['id'];    
    if(!$user->hasFavourited('Post', $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>'Post', 'favorable_id'=>$id]);
      $result = mzk_api_response([], 200, true, 'Success - User has liked');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already liked');
    }
    return $this->response->array($result);
  }

  public function unfollow(){
    $user = User::find($this->getUserIdFromApi());
    $input = Input::all();
    $id = $input['id'];    

    if($user->hasFavourited('Post', $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Post')->delete();
      $result = mzk_api_response([], 200, true, 'Success - User has unliked');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already unliked');
    }
    return $this->response->array($result);
  }


  public function bookmark(){
    $user = User::find($this->getUserIdFromApi());
    $input = Input::all();
    $id = $input['id'];  
    $post = Post::find($id);
    if(!$post->isBookmarked($user->id)){
      $post->bookmark($user->id);
      $result = mzk_api_response([], 200, true, 'Success - Bookmarked');
    }else{
      $result = mzk_api_response([], 200, false, 'User had already Bookmarked');
    }
    return $this->response->array($result);
  }

  public function unbookmark(){
    $user = User::find($this->getUserIdFromApi());
    $input = Input::all();
    $id = $input['id'];    

    $post = Post::find($id);
    if($post->isBookmarked($user->id)){
      $post->unbookmark($user->id);
      $result = mzk_api_response([], 200, true, 'Success - Removed from bookmarks');
    }else{
      $result = mzk_api_response([], 200, false, 'User had not bookmarked');
    }
    return $this->response->array($result);
  }




  public function store(){
    $input = Input::all();
    $validation = Validator::make($input, Selfie::$rules);
    if ($validation->passes())
    {
      $data = Input::only(Post::$fields);
      $data['type'] = $input['type'];
      $data['author_id'] = $this->getUserIdFromApi();
      $post = $this->post->create($data);
      $post->saveCover(isset($input['cover'])?$input['cover']:null);
      if(isset($input['services']) && is_array($input['services'])){
        $post->services()->sync($input['services']);
      }

      $result = mzk_api_response([], 200, true, 'Success - You added a Post');
    }else{
      $result = mzk_api_response([], 200, false, $validation->errors());
    }

    return $this->response->array($result);
  }

  public function destroy()
  {
    $input = Input::all();
    $post = $this->post->find($input['id']);
    if($this->getUserIdFromApi() == $post->author_id){
      $result = mzk_api_response([], 200, true, 'Permission denied - Post not deleted');

    }else{
      $post->delete();
      $result = mzk_api_response([], 200, true, 'Success - You deleted your post');
    }
    return $this->response->array($result);
  }

  protected function getUserIdFromApi($key = false){
    if(!$key){
      $input = Input::all();
      $key = $input['key'];
    }

    $apiKey = ApiKey::where('key', '=', $key)->first();
    if(!$apiKey){
      return false;
    }else{
      return $apiKey->user_id;
    }
  }

  protected function getUsersLocaleFromApi($key = false){
    if(!$key){
      $input = Input::all();
      $key = $input['key'];
    }

    $apiKey = ApiKey::where('key', '=', $key)->get()->first();
    $z = false;
    if($apiKey){
      if($apiKey->current_zone>0){
        $z = Zone::find($apiKey->current_zone);
        $z = $z->isCity()?$z->id:$z->city_id;
      }
    }

    return $z;

  }


}
