<?php
namespace App\Http\Controllers\api\v2\user;

use Illuminate\Routing\Controller;
use Dingo\Api\Routing\ControllerTrait;
use Category;
use App\Models\Check_in;

class CategoriesController extends \BaseController {

	/**
	 * Category Repository
	 *
	 * @var Category
	 */
  use ControllerTrait;

	protected $category;

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = $this->category->all();
    return $this->response->array($categories->toArray());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Category::$rules);
		$data = Input::only(Category::$fields);

		if ($validation->passes())
		{
			$category = $this->category->create($data);
			$images = Input::only('images');
			$category->saveImages($images['images']);

			return Redirect::route('admin.categories.index');
		}

		return Redirect::route('admin.categories.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = $this->category->findOrFail($id);

		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.show', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = $this->category->find($id);

		if (is_null($category))
		{
			return Redirect::route('admin.categories.index');
		}

		$this->layout = View::make('layouts.admin');
    $this->layout->content =   View::make('administration.categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Category::$rules);
		$data = Input::only(Category::$fields);

		if ($validation->passes())
		{
			$category = $this->category->find($id);
			$category->update($input);
			$images = Input::only('images');
			$category->saveImages($images['images']);

			return Redirect::route('admin.categories.show', $id);
		}

		return Redirect::route('admin.categories.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}



	public function checkin($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
    
		if(!$user->hasCheckedIn($id)){

			Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
		}
	}
	public function checkout($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if($user->hasCheckedIn($id)){
			$user->check_ins()->where('favorable_id', '=', $id)->where('favorable_type', '=', 'Business')->delete();
		}
	}


	public function favourite($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if(!$user->hasFavourited('Business', $id)){
      Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);
      $data = array('user_id' =>  $user->id, 
                    'verb'  =>  'likes', 
                    'itemable_type' =>  'Business', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id; 
      $feed->verb = 'likes'; 
      $feed->itemable_type = 'Business';
      $feed->itemable_id = $id;
      $feed->save();

		}
	}
	public function unfavourite($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
		if($user->hasFavourited('Business', $id)){
			$user->favorites()->where('favorable_id', '=', $id)
												->where('favorable_type', '=', 'Business')->delete();
		}
	}




	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->category->find($id)->delete();

		return Redirect::route('admin.categories.index');
	}

}
