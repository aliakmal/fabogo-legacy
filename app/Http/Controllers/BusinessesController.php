<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;

use Illuminate\Support\Collection;
use JsonLd\Context;

use App\Models\Activity;
use App\Models\Business;
use App\Models\Category;
use App\Models\Highlight;
use App\Models\Photo;
use App\Models\Post;
use App\Models\Favorite;
use App\Models\Group;
use App\Models\Review;
use App\Models\Service;
use App\Models\User;
use App\Models\Zone;
use App\Models\Check_in;


use Auth, DB, Input, App, Queue, Validator, Mail, URL, View, Redirect, Response;
use Location, Request, Lang;

use Creitive\Breadcrumbs\Breadcrumbs as Breadcrumbs;
use MazkaraHelper;

class BusinessesController extends Controller {

	/**
	 * Business Repository
	 *
	 * @var Business
	 */
	protected $business;
  protected $feed_manager;
	protected $title;
	protected $breadcrumbs;
	protected $businesses;
	protected $params;
  protected $ads;
	protected $filters;
  protected $meta;
  protected $where;
  protected $what;
  protected $deliverables;
  protected $favorites;

	public function __construct(Business $business, Activity $activity){
		$this->business = $business;
    $this->feed_manager = $activity;
    $this->meta = new \RyanNielson\Meta\Meta();

		$this->breadcrumbs = new Breadcrumbs;
		$this->breadcrumbs->addCrumb('Home', '/');
    $this->breadcrumbs->addCssClasses('breadcrumb');
    $this->breadcrumbs->setDivider('›');

		$this->businesses = [];
    $this->nearby_businesses = [];
		$this->params = [];
		$this->filters = [];

    $this->favorites = [];
    
    $this->what = '';
    $this->where = '';

    $this->deliverables = [];
	}

  public function searchBase(){
    $zone = Input::get('zone');
    $zone = array_filter( $zone, 'strlen' );
    $search = Input::get('search');
    $category = Input::get('category');
    $category = array_filter($category, 'strlen');
    $service = Input::get('service');
    $service = array_filter($service, 'strlen');
    $reset_search = true;
    
    if((count($service)>0)||(count($category)>0)){// ensure that the searched text is dummy or just the name of service
      $srvs = Service::whereIn('id', $service)->get();
      foreach($srvs as $servce){
        if((( '('.$servce->parent->name.') '.$servce->name) != $search) && (($servce->name) != $search)){
          $reset_search = false;
        }
      }

      if(count($category)>0){
        $cat = Category::whereIn('id', $category)->get();
        foreach($cat as $vv){
          if($vv->name == $search){
            $reset_search = true;
          }
        }
      }

      if($reset_search == false){
        $params = ['search'=>$search];
        $service = [];
      }else{
        $params = [];
      }

    }else{
      // check the search box here
      // is it a service being searched
      $params = [];
      if(trim($search)!=''){
        $s = Service::query()->where('name', '=', $search)->showOnly()->get();
        if($s->count() > 0){
          if($s->first()->isParent()){
            $service = Service::query()->where('parent_id', '=', $s->id)->get()->lists('id','id')->all();
          }else{
            $service = [$s->first()->id];
          }
        }else{
          $params = $search?['search'=>$search]:[];
        }
      }
    }


    if(count($category)>0){
      $params = array_merge(['category'=>$category], $params);
    }


    if(count($service)>0){
      $params = array_merge(['service'=>$service], $params);
    }

    if(count($zone)>0){ // we have a zone decided
      $zone = Zone::find(array_pop($zone));
      return Redirect::to(MazkaraHelper::slugCityZone($zone, $params));
    }else{
      return Redirect::to(MazkaraHelper::slugCity(null, $params));
    }

  }

  public function getUsersFavorites($list){
    $favorites = [];

    if(Auth::check()){
      $favorites = User::find(Auth::user()->id)->followedBusinessesByIds($list)->get()->lists('favorable_id','favorable_id')->all();
    }


    $this->favorites = $favorites;
  }

  protected function getDeliverableParam($index){
    return isset($this->deliverables[$index])?$this->deliverables[$index]:false;
  }

  protected function getViewDeliverables($count = 20, $params = null, $instructions = false){

    mzk_timer_start('deliverables - categories');
    //$categories = Category::remember(1800, 'categories.listing')->byLocaleActive();
    $categories = Category::query()->byLocaleActiveDisplayable()->remember(120);

    $this->deliverables['categories'] = $categories->get()->toArray();
    mzk_timer_stop('deliverables - categories');


    mzk_timer_start('deliverables - highlights');
    //$highlights = Highlight::remember(1800, 'highlights.listing')->byLocaleActive();
    $highlight_ids =  Highlight::query()->byLocaleActive()->lists('highlight_id');

    $this->deliverables['highlights'] = Highlight::whereIn('id', $highlight_ids)->remember(120)->get()->toArray();
    mzk_timer_stop('deliverables - highlights');

    mzk_timer_start('deliverables - services');
    //$services = Service::remember(1800, 'services.listing')->byLocaleActive();
    $services = Service::query()->byLocaleActive();

    $this->deliverables['services'] = $services->get();
    mzk_timer_stop('deliverables - services');

    
    $category = $this->getParams('category');
    $filterable_services = [];

    if(is_array($category) && count($category)>0){
      $c = Category::find(array_pop($category));
      $filterable_services = $c->services()->byLocaleActive()->remember(120)->take(15)->get();
    }

    $srs = $this->getParams('service');
    


    if(is_array($srs) && count($srs)>0){
      $srs = Service::find(array_pop($srs));
      if(is_object($srs)){
        if($srs->isParent()){
          $s = Service::query()->byLocaleActive()->where('parent_id', '=', $srs->id)->orderByRaw('id='.$srs->id.' desc, name asc')->remember(120)->take(15)->get();
        }else{
          $s = Service::query()->byLocaleActive()->where('parent_id', '=', $srs->parent_id)->orderByRaw('id='.$srs->id.' desc, name asc')->remember(120)->take(15)->get();
        }
        $filterable_services = $s;
      }
    }
    if(count($filterable_services)==0){
      $s = Service::query()->byLocaleActive()->where('parent_id', '>', 0)->remember(120)->take(15)->get();
      $filterable_services = $s;//[];
    }
    $this->deliverables['filterable_services'] = $filterable_services;

    mzk_timer_start('deliverables - zones');
    //$zones = Zone::remember(1800, 'zones.listing'.mzk_get_localeID())->byLocale()->showActive();
    $zones = Zone::query()->byLocale()->showActive()->remember(120);

    $this->deliverables['zones'] = $zones->get();
    mzk_timer_stop('deliverables - zones');

    $this->deliverables['ads'] = $this->ads;

    mzk_timer_start('deliverables - suggested');

    if(!isset($instructions['no-suggested'])){
      // Business::select()->hasPhotos()->byLocale()->takeRandom(10)->onlyActive()->remember(120)->get();
      $this->deliverables['suggested_spas'] = Business::select()->byLocale()->isFeatured()->remember(120)->orderBy('is_featured', 'ASC')->take(10);
      $this->deliverables['suggested_salons'] = Business::select()->byLocale()->isFeatured()->remember(120)->orderBy('is_featured', 'DESC')->take(10);
    }

    mzk_timer_stop('deliverables - suggested');

    mzk_timer_start('deliverables - params');
    if(is_array($params)){
      foreach($params as $ii=>$vv){
        $this->deliverables[$ii] = $vv;
      }
    }
    mzk_timer_stop('deliverables - params');

    $this->deliverables['title'] = $this->title;
    $this->deliverables['params'] = $this->params;
    $this->deliverables['all_params'] = $this->params;
    $this->deliverables['filter'] = $this->filters;
    $this->deliverables['what'] = $this->what == 'Gym'?'Gyms':$this->what;
    $this->deliverables['where'] = $this->where;

    $this->deliverables['secondary_title'] = $this->getDefaultSecondaryTitle();
    
    mzk_timer_start('deliverables - pagination');
    $this->deliverables['businesses'] =     $this->paginate($count);
    if(count($this->deliverables['businesses'])==0){
      $this->deliverables['suggestions'] = Business::select()->remember(180)->hasPhotos()->byLocale()->onlyActive()->orderby('popularity', 'asc')->take(3)->get();
      $this->getUsersFavorites($this->deliverables['suggestions']->lists('id','id')->all());
    }else{
      $this->deliverables['suggestions'] = [];
      $this->getUsersFavorites($this->deliverables['businesses']->lists('id','id')->all());
    }
    //$this->businesses->paginate($count);
    mzk_timer_stop('deliverables - pagination');
    $this->deliverables['breadcrumbs'] = $this->breadcrumbs;
    $this->deliverables['seo-title'] = $this->getDefaultSEOTitle();
    $this->deliverables['seo-description'] = $this->getDefaultSEODescription();

    $this->deliverables['favorites'] = $this->favorites;

    $prices = [];

    if($this->getParams('service')!=''){
      $ids = $this->deliverables['businesses']->lists('id', 'id')->all();
      $ps = \DB::table('business_service')->whereIn('business_id', $ids)->whereIn('service_id', $this->getParams('service'))->orderby('business_id', 'ASC')->get();
      
      foreach($ps as $p){
        if(!isset($prices[$p->business_id])){
          $prices[$p->business_id] = [];
        }
        $prices[$p->business_id][$p->service_id] = ['service_id'=>$p->service_id, 
                                                    'service_name'=>MazkaraHelper::getServicesAttribute($p->service_id), 
                                                    'starting_price'=>$p->starting_price];
      }
    }

    $this->deliverables['prices'] = $prices;

    return $this->deliverables;
  }

  public function listByCityAngular($subzone = false, $selected_services = null){
    $city = MazkaraHelper::getLocale();//Request::segment(1);

    $zone = Zone::findBySlug($city);
    $sub_zone = false;

    $zones = Zone::select('id', 'name', 'slug')->byLocale()->showActive()->remember(120)->get()->toArray();
    $services = Service::select('id', 'name', 'slug')->byLocale()->showActive()->remember(120)->get()->toArray();

    if(!is_null($selected_services)){
      $selected_service = Service::select()->whereIn('slug', explode(',', $selected_services))->first();
    }else{
      $selected_service = false;
    }

    $categories = Category::select('id', 'name', 'slug')->byLocaleActive()->remember(120)->get()->toArray();
    $highlights = Highlight::select('id', 'name', 'slug')->byLocaleActive()->remember(120)->get()->toArray();

    $category_slug = Request::segment(2);

    if($subzone){
      $sub_zone = Zone::find($subzone);
      $category_slug = str_replace($sub_zone->slug.'-', '', Request::segment(2));
    }

    if($category_slug != 'salons-and-spas'){
      $selected_category = Category::findBySlug(str_singular($category_slug));
    }else{
      $selected_category = false;
    }


    return View::make('site.businesses.angular.index', compact('sub_zone', 'selected_service', 'selected_category', 'categories', 'highlights', 'zone', 'zones', 'services'));
  }


	public function listByCity($services = null){
//    if(App::environment('local') || App::environment('demo')){
//      return $this->listByCityAngular(false, $services);
//    }
    
    $redirector = MazkaraHelper::redirectToUsersCity();
    if($redirector!=false){
      return $redirector;
    }

    mzk_timer_start('list-city');
    $city = MazkaraHelper::getLocale();//Request::segment(1);

    $services = array_filter(explode(',', Request::segment(3)));
    $businesses = $this->initListings(count($services)==0 ? null : ['businesses.*', 'business_service.*']);

    if(count($services)>0){
      $this->setservices($services);
      $default_service_ids = $this->params['service'][0];
    }

    $default_service_ids = '';

		$title = 'Get Pampered';
		$this->what = 'Salons and Spas ';
		$this->where = ucwords($city);

		$zone = Zone::findBySlug($city);

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $this->breadcrumbs->addCrumb('Salons and Spas  '.$zone->name, MazkaraHelper::slugCity($zone));

    $this->filterAllListings();
    //$this->byOnlyZoneListings([$zone]);
    $this->setParams('city', $zone->slug);

    $category = Category::findBySlug(str_singular(Request::segment(2)));
    $categories = [];

    if($category){
      $this->setCategory($category);
      $categories = [$category->id];

    }

    $this->where = $zone->name;
    $this->SortListings();
    $this->what = $this->getDefaultWhatTitle($this->what);

    $this->setupAdsForDisplay($categories);

    $this->setTitle($this->what, $this->where);

    $deliverables = $this->getViewDeliverables(20, null, ['no-suggested'=>true]);

    $this->meta->set(
        array('title' =>  $this->title.' - '.mzk_seo_name(), //$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $deliverables['seo-description'],
              'og'=>[
                'title' => $deliverables['seo-title'].' - '.Lang::get('seo.title'), 
                //'image'=> mzk_assets('assets/0'.(rand(1,7)).'.jpg'),
                'description' => $deliverables['seo-description'],
              ]
      )
    );

    $category = $this->getParams('category');
    $category = is_array($category)?join(',',$category):'';

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    $deliverables['zone'] = $zone;

    $view =   View::make('site.businesses.index', $deliverables)
                      ->with('current_subzone', $zone)
                      ->with('meta', $this->meta)
                      ->with('canonical_url', $canonical_url)
                      ->with('default_search', $this->getDefaultSearchText())
                      ->with('default_service', $default_service_ids)
                      ->with('default_category', $category);
    mzk_timer_stop('list-city');

      return $view;
	}

  public function showCanonicalUrl(){
    $input = Input::all();
    $vars = ['page', 'sort', 'highlights', 'zones', 'fabtrack'];
    foreach($input as $ii=>$vv){
      if(in_array($ii, $vars)){
        
        return true;
      }
    }

    return false;
  }


  public function listChainsByCity($chain){
    $city = MazkaraHelper::getLocale();//Request::segment(1);

    $services = array_filter(explode(',', Request::segment(3)));

    $businesses = $this->initListings(null);
    //business->with('zone')->select();
    $default_service_ids = '';


    $title = 'Get Pampered';
    $zone = Zone::findBySlug($city);

    $chain = Group::findBySlug($chain);

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $this->breadcrumbs->addCrumb('Salons and Spas  '.$zone->name, MazkaraHelper::slugCity($zone));

    $this->breadcrumbs->addCrumb($chain->name, MazkaraHelper::slugCityChain($zone, $chain));

    $this->setParams('city', $zone->slug);
    $this->filterAllListings();
    $this->businesses->ofChains([$chain->id]);

    $this->where = $zone->name;
    $this->SortListings();
    $this->setupAdsForDisplay();

    $this->what = 'Outlets of '.$chain->name;
    $this->setTitle($this->what, $this->where);
    $deliverables = $this->getViewDeliverables(20, ['zone'=>$zone, 'chain'=>$chain], ['no-suggested'=>true]);

    $this->meta->set(
        array('title' => $deliverables['title'].','.$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $deliverables['seo-description'],
              'og'=>[
                'title' => $deliverables['seo-title'].' - '.Lang::get('seo.title'), 
                //'image'=> mzk_assets('assets/splash.jpg'),
                'description' => $deliverables['seo-description'],
              ]
      )
    );
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    $layout_name = $chain->isCustomActivated()?'layouts.parallax-with-search':'layouts.master-open';
    $view_name = $chain->isCustomActivated()?'site.businesses.chains':'site.businesses.index';

    //$this->layout = View::make($layout_name);
    //$this->layout

    return View::make($view_name, $deliverables)->with('current_subzone', $deliverables['zone'])
                  ->with('meta', $this->meta)->with('layout', $layout_name)
                  ->with('canonical_url', $canonical_url)
                  ->with('default_search', $deliverables['what']);;

  }

  public function listGroupsByCity($chain){
    $city = MazkaraHelper::getLocale();//Request::segment(1);

    $services = array_filter(explode(',', Request::segment(3)));

    $businesses = $this->initListings(null);
    //business->with('zone')->select();
    $default_service_ids = '';


    $title = 'Get Pampered';
    $zone = Zone::findBySlug($city);

    $chain = Group::findBySlug($chain);

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $this->breadcrumbs->addCrumb('Salons and Spas  '.$zone->name, MazkaraHelper::slugCity($zone));

    $this->breadcrumbs->addCrumb($chain->name, MazkaraHelper::slugCityChain($zone, $chain));

    $this->setParams('city', $zone->slug);
    $this->filterAllListings();
    $this->businesses->ofGroups([$chain->id]);

    $this->where = $zone->name;
    $this->SortListings();
    $this->setupAdsForDisplay();

    $this->what = 'Outlets of '.$chain->name;
    $this->setTitle($this->what, $this->where);
    $deliverables = $this->getViewDeliverables(20, ['zone'=>$zone, 'chain'=>$chain], ['no-suggested'=>true]);

    $this->meta->set(
        array('title' => $deliverables['title'].','.$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $deliverables['seo-description'],
              'og'=>[
                'title' => $deliverables['seo-title'].' - '.Lang::get('seo.title'), 
                //'image'=> mzk_assets('assets/splash.jpg'),
                'description' => $deliverables['seo-description'],
              ]
      )
    );
    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    $layout_name = $chain->isCustomActivated()?'layouts.parallax-with-search':'layouts.master-open';
    $view_name = $chain->isCustomActivated()?'site.businesses.chains':'site.businesses.index';

    //$this->layout = View::make($layout_name);
    //$this->layout

    return View::make($view_name, $deliverables)->with('current_subzone', $deliverables['zone'])
                  ->with('meta', $this->meta)->with('layout', $layout_name)
                  ->with('canonical_url', $canonical_url)
                  ->with('default_search', $deliverables['what']);;

  }


  public function showNearbyListings(){
    if(!(Input::has('nearby')&&(Input::get('nearby')=='false'))){
      return true;
    }else{
      $this->setParams('nearby', 'false');
      return false;
    }
  }

	public function listByCityAndZone($subzone, $services = null){
//    if(App::environment('local') || App::environment('demo')){
//      return $this->listByCityAngular($subzone, $services);
//    }

    $redirector = MazkaraHelper::redirectToUsersCity();
    if($redirector!=false){
      return $redirector;
    }

    $city = MazkaraHelper::getLocale();//Request::segment(1);

    $services = array_filter(explode(',', Request::segment(3)));
    
    $businesses = $this->initListings(count($services)==0 ? null:['businesses.*', 'business_service.*']);
    //business->with('zone')->select();
    $default_service_ids = '';

    if(count($services)>0){
      $this->setservices($services);
      $default_service_ids = $this->params['service'][0];
    }
		//business->with('zone')->select();

		$title = 'Get Pampered';

		$what = 'Salons and Spas ';
		$where = ucwords($city);//'Dubai';

		$zone = Zone::findBySlug($city);
		$sub_zone = Zone::find($subzone);

    // check is the sub_zone the subzone of the city here???

    //    if($sub_zone->ancestors()->first()->id != $zone->id){
    if($sub_zone->city_id != $zone->id){
      return Redirect::to(MazkaraHelper::slugCityZone($sub_zone));
    }

    $this->filterAllListings();
    $category_slug = str_replace($sub_zone->slug.'-', '', Request::segment(2));
    $category = Category::findBySlug(str_singular($category_slug));
    $categories = [];
    if($category){
      $this->setCategory($category);
      $categories = [$category->id];
    }



    if(!$this->getFilters('search')):
      if($this->showNearbyListings()){
        $this->byLeveledZoneListings([$subzone]);//->ofZones($znes);
        $this->businesses->orderBy(DB::RAW('zone_id="'.$subzone.'"'), 'DESC');
      }else{
        $this->byOnlyZoneListings([$subzone]);//->ofZones($znes);
      }
    endif;

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    // does this subzone have a parent zone that is not a city?
    if($sub_zone->isSubZone()){
      $p = $sub_zone->parent()->get()->first();
      $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
    }
    $this->breadcrumbs->addCrumb($sub_zone->name, MazkaraHelper::slugCityZone($sub_zone));
    $this->breadcrumbs->addCrumb('Salons and Spas '.$sub_zone->name, MazkaraHelper::slugCityZone($sub_zone));

    $where = $sub_zone->name;
    $what = $this->getDefaultWhatTitle($what);
    $this->setTitle($what, $where);

    $this->where = $where;

    $this->what = $what;

    $this->SortListings();



    $this->setupAdsForDisplay($categories, $sub_zone);

    $deliverables = $this->getViewDeliverables(20, ['zone'=>$zone, 'sub_zone'=>$sub_zone], ['no-suggested'=>true]);
    if(!$this->getFilters('search')):

    $deliverables['nearby_businesses'] = false;
    if($this->showNearbyListings()){
      $deliverables['nearby_businesses'] = $this->nearby_businesses->paginate(20);
    }
    endif;

    $category = $this->getParams('category');
    $category = is_array($category)?join(',',$category):'';

    $surrounding_zones = [];
    if($sub_zone->isSubZone()){
      $surrounding_zones = Zone::where('parent_id', '=', $sub_zone->parent_id)->remember(120)->get();
    }
    if($sub_zone->isZone()){
      $surrounding_zones = Zone::where('parent_id', '=', $sub_zone->id)->remember(120)->get();
    }


    $this->meta->set(
        array('title' => $this->title.' - '.mzk_seo_name(), //$deliverables['seo-title'].' - '.Lang::get('seo.title'), 
              'description' => $deliverables['seo-description'],
              'og'=>[
                'title' => $deliverables['seo-title'].' - '.Lang::get('seo.title'), 
                'image'=> mzk_assets('assets/splash.jpg'),
                'description' => $deliverables['seo-description'],
              ]
      )
    );

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;


    return  View::make('site.businesses.index', $deliverables)
                      ->with('current_subzone', $sub_zone)
                      ->with('meta', $this->meta)
                      ->with('surrounding_zones', $surrounding_zones)
                      ->with('canonical_url', $canonical_url)
                      ->with('default_search', $this->getDefaultSearchText())
                      ->with('default_service', $default_service_ids)
                      ->with('default_category', $category);

		$this->layout = View::make('layouts.master');
    $this->layout->with('current_subzone', $sub_zone)
                  ->with('meta', $this->meta)
                  ->with('default_search', $this->getDefaultSearchText() )
                  ->with('default_service', $default_service_ids)
                  ->with('default_category', $category);


    $this->layout->content = View::make('site.businesses.index', $deliverables);

	}




	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected function setParams($key, $val){
		$this->params[$key] = $val;
	}

  protected function getParams($key){
    return isset($this->params[$key]) ? $this->params[$key] : '';
  }

  protected function getDefaultWhatTitle($default = 'Salons and Spas'){
    $str = '';
    $search = $this->getFilters('search');
    $categories = $this->getFilters('category');
    $services = $this->getFilters('service');
    if($search){
      $str.=$default.' matching "'.$search.'" ';
    }
    if((!$categories)&&(!$services)&&(!$search)){
      $str.= $default;
    }else{
      if($services){
        $str.= array_pop($services);
      }elseif($categories){
        foreach($categories as $ii=>$vv){
          $categories[$ii] = trim(MazkaraHelper::getPluralName($vv));
        }

        $str.= trim(join(',', $categories));
      }
      

    }
    $deals = $this->getFilters('packages');

    if($deals){
      $str.= ', with Special Packages';
    }
    $offers = $this->getFilters('specials');

    if($offers){
      $str=(trim($str)). ', with '.(mzk_label('specials'));
    }

    return $str;
  }

  protected function getDefaultSEOTitle($default = 'Salons and Spas'){
    $str = '';
    $categories = $this->getFilters('category');
    $services = $this->getFilters('service');
    if((!$categories)&&(!$services)){
      $str.= $default;
    }else{
      if($services){
        $str.= array_pop($services).', ';
      }

      if($categories){
        foreach($categories as $ii=>$vv){
          $categories[$ii] = MazkaraHelper::getPluralName($vv);
        }

        $str.= join(',', $categories);
      }else{
        $str.= $default;
      }
      

    }

    if($this->getDeliverableParam('sub_zone')){
      $zone = $this->getDeliverableParam('sub_zone');
      $str.= ' in '.$zone->name.', '.MazkaraHelper::getLocaleLabel();
    }else{
      $str.= ' in '.MazkaraHelper::getLocaleLabel();
    }

    if($this->getFilters('packages')){    
      $str.= ' offering packages ';
    }

    if($this->getFilters('specials')){    
      $str.= ' with '.(mzk_label('specials'));
    }


    $str.=join(', ', array_filter([$this->getDefaultServicesTitle('', ' for services in '),$this->getDefaultHighlightsTitle()]));


    return $str;
  }

  protected function getDefaultSEODescription($default = 'Salons and Spas '){


    $str = 'Discover ';
    $str2 = 'Rate and Review the best ';
    $categories = $this->getFilters('category')?$this->getFilters('category'):[];
    $cat_city = [];
    $services = $this->getFilters('service')?$this->getFilters('service'):[];
    $sub_zone = $this->getDeliverableParam('sub_zone');
    $city = ($sub_zone ? $sub_zone->name.', ':'').MazkaraHelper::getLocaleLabel();
    $categories_string = '';
    foreach($categories as $ii=>$vv){
      $categories[$ii] = MazkaraHelper::getPluralName($vv);
      $cat_city[] = MazkaraHelper::getPluralName($vv).' in '.$city;
    }

    $categories_string = join(', ', $categories);

    $services_string = '';
    $service_city = [];
    foreach($services as $ii=>$vv){
      $services[$ii] = MazkaraHelper::getPluralName($vv).' in '.$city;
    }

    $services_string = join(', ', $services);



    // no categories, no subzone, no services
    if((count($categories)==0)&&(count($services)==0)){
      $str = sprintf("Discover amazing specials on Beauty & Wellness services in %s. 
                      View Ratings, Reviews, Photos and location of Salons, Spas and Fitness Centers in %s.",
       $city, $city);
    }

    // no categories, no subzone, yes services
    if((count($categories)==0)&&(count($services)!=0)){
      $str = sprintf('Discover amazing specials on '.$services_string);
    }


    // no categories, yes subzone, yes services
    if((count($categories)==0)&&(count($services)!=0)&&($sub_zone)){
      $str = sprintf('Discover amazing specials on '.$services_string);
    }

    // no categories, yes subzone, yes services
    if((count($categories)==0)&&(count($services)!=0)){
      $str = sprintf('Discover amazing specials on '.$services_string);
    }

    // yes categories, no services
    if((count($categories)!=0)&&(count($services)==0)){

      $str = sprintf('Discover amazing specials and promotions of %s. View Ratings, Reviews, Photos and location of %s', 
                      join(',',$cat_city),join(',',$cat_city));
    }

    // yes categories, yes services
    if((count($categories)!=0)&&(count($services)!=0)){

      $str = sprintf('Discover amazing specials on '.$services_string);
    }

    return $str;


    if(!$categories){
      $str.= $default;
      $str2.= $default;
    }else{

      foreach($categories as $ii=>$vv){
        $categories[$ii] = MazkaraHelper::getPluralName($vv);
      }

      $str.= join(',', $categories);
      $str2.= join(',', $categories);

    }
    if($this->getDeliverableParam('sub_zone')){
      $zone = $this->getDeliverableParam('sub_zone');
      $str.= ' in '.$zone->name.', '.MazkaraHelper::getLocaleLabel();
      $str2.= ' in '.$zone->name.', '.MazkaraHelper::getLocaleLabel();
    }else{
      $str.= ' in '.MazkaraHelper::getLocaleLabel();
      $str2.= ' in '.MazkaraHelper::getLocaleLabel();
    }

    $str.=join(', ', array_filter([$this->getDefaultServicesTitle('', ' for services in '),$this->getDefaultHighlightsTitle()]));
    $str2.=join(', ', array_filter([$this->getDefaultServicesTitle('', ' for services in '),$this->getDefaultHighlightsTitle()]));

    return join(', ', [$str, $str2]);
  }


  protected function getDefaultSearchText($default = ''){
    if(isset($this->params['search'])){
      return $this->params['search'];
    }

    $str = $this->getDefaultWhatTitle('');
    if(strlen($str)>0){
      return $str;
    }

    $str = $this->getDefaultServicesTitle('', '');
    if(strlen($str)>0){
      return $str;
    }

    $str = $this->getDefaultHighlightsTitle('', '');
    if(strlen($str)>0){
      return $str;
    }


    return '';
  }

  protected function getDefaultSecondaryTitle($default = ''){
    return join(' - ', array_filter([$this->getDefaultServicesTitle(),$this->getDefaultHighlightsTitle()]));
  }

  protected function getDefaultServicesTitle($default = '', $prepend = null){
    $services = $this->getFilters('service');
    if(!$services){
      return $default;
    }

    if(is_null($prepend)){
      $categories = $this->getFilters('category');
      if($categories){
        foreach($categories as $ii=>$vv){
          $categories[$ii] = MazkaraHelper::getPluralName($vv);
        }
        $prepend.= join(',', $categories).' offering services in ';
      }else{
        $prepend.= 'Salons and Spas offering services in ';
      }


    }

    return $prepend.join(', ', $services);
  }

  protected function getDefaultHighlightsTitle($default = '', $prepend = ' with '){
    $highlights = $this->getFilters('highlights');
    if(!$highlights){
      return $default;
    }


    $deals = $this->getFilters('packages');
    if($deals){
      if(!is_array($highlights)){
        $highlights = [];
      }
      $highlights[] = 'Special Packages';
    }


    return $prepend.join(', ', $highlights);
  }


	protected function setFilters($key, $val){
		$this->filters[$key] = $val;
	}

  protected function getFilters($key){
    return isset($this->filters[$key])?$this->filters[$key]:false;
  }

	protected function initListings($tables = null){
    $tables = is_null($tables) ? ['businesses.*'] : $tables;
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      
      $tables = 'businesses.*';
    }else{
      $tables = '*';
    }
    $this->businesses = $this->business->select($tables)//->remember(30)
                              /*->with( 'photos', 'favourites', 'offers',
                                      'active_offers', 'virtual_number_allocations',
                                       'cover', 'highlights', 'zone', 'services')*/
                              ->isDisplayable()->byLocale();
		return $this->businesses;
	}

  protected function initListingsNoLocale(){
    //onlyActive()
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      
      $tables = 'businesses.*';
    }else{
      $tables = '*';
    }

    $this->businesses = $this->business->select($tables)->with('photos');
    return $this->businesses;
  }






  protected function initListingsNoLocaleMinimal(){
    $this->businesses = $this->business->select('businesses.id', 'businesses.name', 'businesses.geolocated', 
                                                'businesses.zone_id', 'businesses.active', 'businesses.zone_cache', 
                                                'businesses.geolocation_address', 'businesses.geolocation_city', 
                                                'businesses.total_ratings_count', 'businesses.phone', 'businesses.rating_average')
                                        ->with('photos')->isDisplayable();//->onlyActive();
    return $this->businesses;
  }


  protected function setupAdsForDisplay($categories = false, $zone = false){
    //$category = $this->getParams('category')?$this->getParams('category'):[];
    mzk_timer_start('ads - 01');
    $categories = $categories ? $categories : mzk_categories_from_services($this->getParams('service'));
    mzk_timer_stop('ads - 01');
    mzk_timer_start('ads - 02');
    $zone = $zone ? $zone : ($this->getParams('zone') ? [$this->getParams('zone')]:false);

    $zones = $zone ? array_merge([$zone->id], array_keys(Zone::descendantsOf($zone)->lists('id', 'id')->all())):[];
    //$zones = $zone ? array_merge([$zone->id], MazkaraHelper::getZonesChildrenListAttributes($zone->id)):[];
    mzk_timer_stop('ads - 02');

    //$business_zones = Business_zone::select()->byZones($zones)->lists('id', 'id');
    $business_zones = Zone::whereIn('id', $zones)->lists('business_zone_id','business_zone_id')->all();
    mzk_timer_start('ads - 03');

    $ads = mzk_get_ad_lists_to_show_today(['categories'=>$categories, 
                                           'business_zones'=>$business_zones]);
    mzk_timer_stop('ads - 03');

    //Queue::push(function($ads){
      foreach($ads as $ad){
        //$ad->incrementViewCount();
      }
    //});

    $this->ads = $ads;

  }


	protected function bySearchListings($search){
		//$this->businesses->searchNameServices($search);
    $this->businesses->searchBasic($search);



    $this->setParams('search', $search);
    $this->setFilters('search', $search);

		return $this->businesses;
	}



  protected function mergeNearbyWithSearch(){
    $this->businesses = new Collection($this->businesses);
    $this->businesses = $this->businesses($this->nearby_businesses); 
  }

  protected function byLeveledZoneListings($zones, $force = false){
    //$this->nearby_businesses = $this->businesses;
    $znes = [];
    $szones = [];

    foreach($zones as $z):
      // get all the siblings of each zone now
      $znes[] = $z;// get all desendents of this zone
      //$mrgers = MazkaraHelper::getZonesChildrenListAttributes($z);
      //$mrgers = array_keys(Zone::remember(60, 'zones.descendents.of.'.$z)->descendantsOf($z)->lists('name', 'id')->all());
      $mrgers = array_keys(Zone::descendantsOf($z)->lists('name', 'id')->all());
      $znes = array_merge($znes, $mrgers);

      //$znes = array_merge($znes, array_keys(Zone::descendantsOf($z)->lists('name', 'id')));
      // if this is a first level subzone - then don't get the siblings
      // but if it is a second level subzone
      if(Zone::find($z)->isSubZone()):
        // get all the siblings here
        $siblings = array_keys(Zone::find($z)->siblings()->get()->lists('name', 'id')->all());
        $szones = array_merge($szones, $siblings);
        foreach($siblings as $zn){
          // get decendents of each zone here
          $znes[] = $zn;
          
          //$des = MazkaraHelper::getZonesChildrenListAttributes($zn);//Zone::descendantsOf($zn)->lists('name', 'id');
          $des = Zone::descendantsOf($zn)->lists('name', 'id')->all();

          $znes = array_merge($znes, array_keys($des));
          $szones = array_merge($szones, array_keys($des));

        }
      endif;

    endforeach;

    $this->nearby_businesses = clone($this->businesses);
    $this->businesses->ofZones($znes);
    if($force == true){
      $this->nearby_businesses->ofZones($szones);
    }

    //$this->nearby_businesses = $this->businesses;
  }

  protected function byOnlyZoneListings($zones){
    $znes = $zones;
    $zz = $zones;

    foreach($zz as $z):
      //$mrgers = MazkaraHelper::getZonesChildrenListAttributes($z->id);
      //$mrgers = array_keys(Zone::remember(60, 'zones.descendents.of.'.$z)->descendantsOf($z)->lists('name', 'id')->all());
      $mrgers = array_keys(Zone::descendantsOf($z)->lists('name', 'id')->all());
      $znes = array_merge($znes, $mrgers);
    endforeach;


    $this->businesses->ofZones($znes);
  }

	protected function byZoneListings($zones){
  	$this->byOnlyZoneListings($zones);
    $this->setParams('zone', $zones);
    $this->setFilters('zone',  Zone::whereIn('id', $zones)->lists('name', 'id')->all());
	}

  protected function byCoordinatesNearby($latitude, $longitude){
    $this->businesses->NearLatLng($latitude, $longitude);
    return $this->businesses;
  }

	protected function byDealListings($deals){
    $this->businesses->hasActivePackages();
    $this->setParams('deals', $deals);
    $this->setFilters('deals', 'Deals/Packages');

		return $this->businesses;
	}

  protected function byActiveOffers(){
    $this->businesses->hasActiveOffers();
    $this->setParams('specials', 'true');
    $this->setFilters('specials', 'Specials');

    return $this->businesses;
  }

  protected function byPackageListings($deals){
    $this->businesses->hasActivePackages();
    $this->setParams('packages', $deals);
    $this->setFilters('packages', 'Packages Available');

    return $this->businesses;
  }

  protected function byOpenNow(){
    $this->businesses->openNow();
    $this->setParams('open', 'now');
    $this->setFilters('open', 'Open Now');

    return $this->businesses;
  }

  protected function byChains($chains){
    $this->businesses->ofChains($chains);

    return $this->businesses;
  }

  protected function byCostEstimate($estimate){
    $this->businesses->byCostEstimate($estimate);
    $this->setParams('cost', $estimate);
    $this->setFilters('cost', $estimate);

    return $this->businesses;
  }

	protected function byServiceListings($services){

		$this->businesses->withServices($services);
    $this->setParams('service', $services);
    $this->setFilters('service', Service::whereIn('id', $services)->lists('name', 'id')->all());

		return $this->businesses;
	}

	protected function byCategoryListings($categories){
    $this->businesses->ofCategories($categories);

    $this->setParams('category', $categories);
    $this->setFilters('category', Category::whereIn('id', $categories)->lists('name', 'id')->all());
		return $this->businesses;
	}

	protected function byHighlightListings($highlights){
    $this->businesses->withHighlights($highlights);

    $this->setParams('highlights', $highlights);
    $this->setFilters('highlights', Highlight::whereIn('id', $highlights)->lists('name', 'id')->all());
		return $this->businesses;
	}

	protected function byActiveStateListings($state){
    $this->businesses->where('active', $state);
    $this->setParams('active', $state);
    $this->setFilters('active', ucwords(str_replace('.', ' ', Input::get('active'))));
		return $this->businesses;
	}
	protected function SortListings($force_sortable = false){
    if(!(Input::has('sort') && (Input::get('sort')!=''))){
      $sort = 'popularity';
    }else{
      $sort = Input::get('sort');
    }

    $sort = $force_sortable != false?$force_sortable:$sort;

    $this->setParams('sort', $sort);
    if(!(Input::has('zone') && (Input::get('zone')==-1))){
      $this->businesses->groupBy('businesses.id');
    }

  	switch($sort){
  		case 'nameAsc':
      case 'name':
    		$this->businesses->orderBy('name', 'ASC');
      break; 
      case 'popularity':
        $this->businesses->orderBy('popularity', 'DESC');
      break; 
      case 'distance':
        $this->businesses->orderBy('distance', 'ASC');
      break; 
      case 'likes':
        $this->businesses->orderBy('favorites_count', 'DESC');
      break; 
      case 'ratings':
      case 'rating':
        $this->businesses->orderBy('rating_average', 'DESC')->orderby('total_ratings_count', 'DESC');//->orderby('reviews_count', 'DESC');
      break; 
			case 'nameDesc':
    		$this->businesses->orderBy('name', 'DESC');
  		break; 
			case 'lastUpdateAsc':
    		$this->businesses->orderBy('updated_at', 'ASC');
  		break; 
			case 'lastUpdateDesc':
    		$this->businesses->orderBy('updated_at', 'DESC');
  		break; 
  	}


		return $this->businesses;
	}

	protected function filterAllListings($params = false){
    if(Input::has('search') && (Input::get('search')!='')){
      $this->bySearchListings(Input::get('search'));

    }else{
      if(Input::has('zone') && is_array(Input::get('zone')) &&(count(array_filter(Input::get('zone')))>0)){
      
          if(is_array($params) && isset($params['nearby']) ){
            $subzone = Input::get('zone');
            $this->byLeveledZoneListings($subzone, true);//->ofZones($znes);
            $this->businesses->orderBy(DB::RAW('zone_id="'.$subzone[0].'"'), 'DESC');
          }else{ 
            $this->byZoneListings(Input::get('zone'));//->ofZones($znes);
          //$where = join(',', $this->filters['zone']);
        }

      }

    }

    if(Input::has('zone') && (Input::get('zone')==-1)){
      $this->byCoordinatesNearby(Input::get('latitude'), Input::get('longitude'));
    }

    if(Input::has('deals') && (Input::get('deals')!='')){
      $this->byDealListings(Input::get('deals'));
    }


    if(Input::has('specials') && (Input::get('specials')!='')){
      $this->byActiveOffers();
    }

    if(Input::has('offers') && (Input::get('offers')!='')){
      $this->byActiveOffers();
    }


    if(Input::has('packages') && (Input::get('packages')!='')){
      $this->byPackageListings(Input::get('packages'));
    }

    if(Input::has('open') && (Input::get('open')!='')){
      $this->byOpenNow();
    }

    if(Input::has('cost') && (Input::get('cost')!='')){
      $this->byCostEstimate(Input::get('cost'));
    }


    if(Input::has('service') && (Input::get('service')!='')){
      $this->byServiceListings(Input::get('service'));
    }

    if(Input::has('category')  && (count(Input::get('category'))>0)){
      $this->byCategoryListings(Input::get('category'));
      //$what = join(',', $this->filters['category']);
    }


    if(Input::has('highlights') && (count(Input::get('highlights'))>0)){
      $this->byHighlightListings(Input::get('highlights'));
    }

    if(Input::has('active') && (Input::get('active')!='')){
      $this->byActiveStateListings(Input::get('active'));
    }




	}

	public function index(){
		//$businesses = 
		$this->initListings();
		$this->breadcrumbs->addCrumb('Salons and Spas', '/businesses');
		$title = 'Get Pampered';
		$what = 'Salons and Spas';
		$where = ucwords($city);//'Dubai';

    $categories = Category::remember(60, 'categories.listing')->get()->toArray();
    $highlights = Highlight::remember(60, 'highlights.listing')->get()->toArray();
    $services = Service::remember(60, 'services.listing')->defaultOrder()->get()->linkNodes();
    $zones = Zone::remember(60, 'zones.listing')->defaultOrder()->get()->linkNodes();

    //$businesses = 
    $this->filterAllListings();
        $this->SortListings();

    $suggested_spas = Business::take(10)->onlyActive()->get();
    $suggested_salons = Business::take(10)->onlyActive()->get();
    $this->setTitle($what, $where);
    $title = $this->title;
    $breadcrumbs = $this->breadcrumbs;
    $secondary_title = $this->getDefaultServicesTitle();

    $businesses = $this->paginate();
    $params = $this->params;
    $filter = $this->filters;
		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.businesses.index', compact( 'businesses', 'where', 'secondary_title', 'breadcrumbs', 
                                                                          'title', 'categories', 'highlights', 'services', 'zones', 'params', 
                                                                          'suggested_spas', 'suggested_salons', 'filter'));
	}

  public function paginate($count = 20){
    $input = Input::all();
    $count = $count ? $count : 20;
    $page = isset($input['page']) ? $input['page'] - 1 : 0;


    return $this->businesses->paginate($count);

  }

	protected function setTitle($what = 'Salons and Spas', $where = '', $default = ''){
    $what =  $what == 'Gym'?'Gyms':$what;
		$this->title = $what.' in '.(empty(trim($where)) ? MazkaraHelper::getLocaleLabel() : $where);
	}

	public function checkin($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
    
		if(!$user->hasCheckedIn($id)){

			Check_in::create(['user_id'=>$user->id, 'business_id'=>$id]);
		}
	}
	public function checkout($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if($user->hasCheckedIn($id)){
			$user->check_ins()->where('business_id', '=', $id)->delete();
		}
	}


	public function favourite($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());

		if(!$user->hasFavourited('Business', $id)){
      Favorite::create(['user_id'=>$user->id, 'favorable_type'=>'Business', 'favorable_id'=>$id]);

      Business::find($id)->setPopularity();

      $data = array('user_id' =>  $user->id, 
                    'verb'  =>  'likes', 
                    'itemable_type' =>  'Business', 
                    'itemable_id' =>  $id);
      $feed = $this->feed_manager->create($data);
      $feed->user_id = $user->id; 
      $feed->verb = 'likes'; 
      $feed->itemable_type = 'Business';
      $feed->itemable_id = $id;
      $feed->save();

		}
	}
	public function unfavourite($id){
		$user = Auth::user();
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
		if($user->hasFavourited('Business', $id)){
			$user->favorites()->where('favorable_id', '=', $id)
                        ->where('favorable_type', '=', 'Business')->delete();
		}
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout = View::make('layouts.admin');
		$current_location = Location::get();
		$categories = Category::where('parent_id', null)->get();
		$services = Service::where('parent_id', null)->get();

		$highlights = Highlight::all();

		$selected_categories =[];
		$selected_services =[];
		$selected_highlights =[];
    $this->layout->content = View::make('administration.businesses.create', compact('selected_categories', 'categories', 'services', 'selected_services', 'highlights', 'selected_highlights'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');


		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');
		//$data = Input::except('images', 'rate_card','categories', 'highlights', 'services');
		if ($validation->passes())
		{
			$business = $this->business->create($data);
			$cat = Input::only('categories');
			$business->categories()->attach($cat['categories']);
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);
			
			return Redirect::route('admin.businesses.index');
		}

		return Redirect::route('admin.businesses.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($id)
	{
		$business = $this->business->find($id);

    $suggested_spas = Business::get()->take(5);
    $suggested_salons = Business::get()->take(5);

		$this->layout = View::make('layouts.master');
    $this->layout->content = View::make('site.businesses.show', compact('business', 'breadcrumbs', 'suggested_spas', 'suggested_salons'));
	}

  protected function verifySlugCityLink($business, $zone){
  }

  public function incrementPageView(){
    $inp = Input::all();
    $business = $this->business->find($inp['business_id']);
    $business->incrementViewCount();
    $business->incrementViewCount();
  }

  public function incrementCallView(){
    $inp = Input::all();
    $business = $this->business->find($inp['business_id']);
    
    $business->incrementCallsCount();
    

    //$business->incrementCallsCount();
  }

	public function slug($slug){

    $city = Request::segment(1); //MazkaraHelper::getLocale();//

    $zone = Zone::findBySlug($city);
    $show_inactive = Auth::check() ? Auth::user()->hasRole('admin') : false;
		$business = $this->business->getBySlug($slug, $show_inactive);

    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSingle($this->business->find($business)));
      }else{
        return Redirect::to('/');
      }
    }

    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSingle($business));
    }

    $current_user = Auth::check()?User::find(Auth::user()->id):null;;

    $suggested_spas = Business::select(Business::$niblet_fields)->byLocale()->hasPhotos()->orderBy('is_featured', 'DESC')->take(5)->get();
    $favorited_suggested_spas = [];

    if($current_user){
      $favorited_suggested_spas = $current_user->followedBusinessesByIds($suggested_spas->lists('id','id')->all())->get()->lists('favorable_id','favorable_id')->all();
    }

    $suggested_salons = [];
    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
 
    $sub_zone = $business->zone;

    if($business->isHomeService()){
      $h = Category::remember(3600, 'category.slug.home.services')->where('slug', '=', 'home-service')->get()->first();
      $this->breadcrumbs->addCrumb('Home Services in '.$zone->name, MazkaraHelper::slugCity($zone, ['category'=>[$h->id]]));

    }else{

      if($sub_zone){
        if($sub_zone->isSubZone()){
          $p = Zone::find($sub_zone->parent_id);
          $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
        }

        $this->breadcrumbs->addCrumb('Salons and Spas '.$sub_zone->name, MazkaraHelper::slugCityZone($sub_zone));
      }
    }

    $business_meta_categories = $business->getMeta('categories');
    $this->breadcrumbs->addCrumb($business->name);
    $business_categories = is_array($business_meta_categories) ? join(', ', $business_meta_categories).' in '.$business->zone_cache.' '.$zone->name:'';
    $business_categories_ids = is_array($business_meta_categories) ? array_keys($business_meta_categories):'';
    
    // mark increment
    //Queue::push(function()use($business){
    //});


    $this->setupAdsForDisplay($business_categories_ids, $business->zone);
    $ads = $this->ads;

    $jsonLD = $this->getJsonLD($business);

    $all_photos = $business->all_photos()->get();

    $rateCards = [];
    $photos = [];
    $cover = null;

    foreach($all_photos as $one_photo){
      if($one_photo->type == Photo::COVER){
        $cover = $one_photo;
      }
      if($one_photo->type == Photo::RATECARD){
        $rateCards[] = $one_photo;
      }
      if(in_array($one_photo->type, [Photo::IMAGE, Photo::SERVICE, Photo::COVER])){
        $photos[] = $one_photo;
      }
    }

    $business->cover = $cover;
    $offers = $business->offers()->onlyActive()->get();
    $highlights = $business->highlights;

    //see if we have a Slug(currently existing review
    /*if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
    }*/

    $myreview = null;
    if($current_user){
      $reviews = $business->completedActiveReviews()->with('services', 'comments')->orderBy(DB::RAW('user_id="'.Auth::user()->id.'"'), 'DESC')->get();
      foreach($reviews as $one_review){
        if($current_user->id == $one_review->user_id){
          $myreview = $one_review;
        }
      }
    }else{
      $reviews = $business->completedActiveReviews()->with('services', 'comments')->orderby('id', 'desc')->get();
    }

    $phones = $business->displayablePhone();

    if($business->chain_id > 0){
      $chain_businesses = $business->chain->numActiveBusinesses();
    }else{
      $chain_businesses = 0;
    }

    $review_ids = $reviews->lists('id', 'id')->all();
    $sql = "SET @c =1;UPDATE counters SET `views` = `views` + @c WHERE countable_type = 'Review' AND countable_id in (".join(',', $review_ids).");";
    \DB::raw($sql);

    $breadcrumbs = $this->breadcrumbs;
    //$business->incrementViewCount();

    $metas = [$business->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories.','.$business->zone_cache.', '.$zone->name,
              'Rate and Review '.$business->name.' in '.$business->zone_cache.', '.$zone->name, 
              is_array($business->getMeta('services')) ? 'offering services for '.join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];

    $desc = $business->name.' in '.$business->zone_cache.', '.$zone->name.'. Exclusive Deals and Specials only on '.mzk_seo_name().'. Find reviews, rate cards, photos and location.';
    $seo_title = $business->name.' in '.$business->zone_cache.', '.mzk_slug_to_words($city).' - '.mzk_seo_name();
    $this->meta->set(
        array('title' => $seo_title, 
              'description' => $desc,
              'og'=>[
                'title' => $business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                //'image'=> $desc, 
                'description' => $desc,
              ]
      )
    );

    $services = mzk_get_active_services();
    $locally_active_service_ids = mzk_get_active_services_ids();

    $business_services = $business->services()->whereIn('service_id', $locally_active_service_ids)->orderby('parent_id', 'ASC')->get();

    $canonical_url = $this->showCanonicalUrl()? Request::url():false;

    if(Request::ajax()):
      $result = compact('business', 'favorited_suggested_spas',  'cover',
                        'ads', 'breadcrumbs', 'suggested_spas', 'business_services',
                        'suggested_salons','rateCards','photos','current_user',
                        'offers','highlights', 'phones', 'reviews', 'chain_businesses');
      return Response::Json($result);
    else:
      return View::make('site.businesses.show', 
                        compact('business','rateCards','photos', 'current_user',
                                'offers','highlights','reviews', 'cover', 'chain_businesses',
                                'favorited_suggested_spas', 'ads', 'business_services',
                                'breadcrumbs', 'phones', 'myreview', 'services',
                                'suggested_spas', 'suggested_salons'))
                    ->with('current_subzone', $sub_zone)
                    ->with('meta', $this->meta)
                    ->with('canonical_url', $canonical_url)
                    ->with('where', $business->zone_cache)
                    ->with('jsonLD', $jsonLD)
                    ->with('default_search', $business->name)
                    ->with('javascript_include', 'application.with.map.ratings.gallery');

    endif;
	}



  public function slugPhotos($slug){

    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $show_inactive = Auth::check() ? Auth::user()->hasRole('admin') : false;
    $business = $this->business->getBySlug($slug, $show_inactive);

    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSinglePhotos($this->business->find($business)));
      }else{
        return Redirect::to('/');
      }
    }

    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSinglePhotos($business));
    }

    $current_user = Auth::check()?User::find(Auth::user()->id):null;;


    $suggested_spas = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }

    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));
    $this->breadcrumbs->addCrumb($business->name.' Photos');

    $business_categories = is_array($business->getMeta('categories')) ? join(', ', $business->getMeta('categories')).' in '.$business->zone_cache.' '.$zone->name:'';
    $business_categories_ids = is_array($business->getMeta('categories')) ? array_keys($business->getMeta('categories')):'';

    $this->setupAdsForDisplay($business_categories_ids, $business->zone);
    $ads = $this->ads;

    $breadcrumbs = $this->breadcrumbs;
    $metas = ['Photos of '.$business->name,
              $business->name.' in '.$zone->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories,
              is_array($business->getMeta('services')) ? join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];
    
    if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
      $myreview = null;
    }
    if(isset($myreview) && !is_null($myreview)){
      $reviews = $business->completedReviews()->with('services', 'comments')->orderBy(DB::RAW('user_id="'.Auth::user()->id.'"'), 'DESC')->get();
    }else{
      $reviews = $business->completedReviews()->with('services', 'comments')->orderby('id', 'desc')->get();
    }

    $desc = 'Photos of '.$business->name.' in '.$business->zone_cache.', '.$zone->name.'. '.$business->name.' photos on '.mzk_seo_name().'!';
    $seo_title = $business->name.' Photos, '.$business->zone_cache.', '.mzk_slug_to_words($city).' - '.mzk_seo_name();
    $services = mzk_get_active_services();

    $this->meta->set(
        array('title' => $seo_title,
              'description' => $desc,
              'og'=>[
                'title' => 'Photos of '.$business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                //'image'=> count($business->photos)>0 ? $business->thumbnail('medium') : mzk_assets('assets/splash.jpg'),
                'description' => $desc, //join(',',array_filter($metas)),
              ]
      )
    );
    $highlights = $business->highlights;
    $phones = $business->displayablePhone();

    if($business->chain_id > 0){
      $chain_businesses = $business->chain->numActiveBusinesses();
    }else{
      $chain_businesses = 0;
    }


    $photos = $business->photos;
    $canonical_url = MazkaraHelper::slugSingle($business);

    $business_services = $business->services()->orderby('parent_id', 'ASC')->get();


    return View::make('site.businesses.photos', compact('business', 'myreview', 'business_services', 'chain_businesses', 'highlights','phones', 'ads', 'photos', 'breadcrumbs', 'suggested_spas', 'suggested_salons'))->with('current_subzone', $sub_zone)
                  ->with('meta', $this->meta)
                  ->with('where', $business->zone_cache)
                  ->with('canonical_url', $canonical_url)
                  ->with('default_search', $business->name)
                  ->with('javascript_include', 'application.with.map.ratings.gallery');

  }


  public function slugRateCard($slug){

    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $business = $this->business->getBySlug($slug);

    
    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSingleRateCards($this->business->find($business)));
      }else{
        return Redirect::to('/');
      }
    }

    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSingleRateCards($business));
    }


    $suggested_spas = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }
    $current_user = Auth::check()?User::find(Auth::user()->id):null;;

    if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
      $myreview = null;
    }
    if(isset($myreview) && !is_null($myreview)){
      $reviews = $business->completedReviews()->with('services', 'comments')->orderBy(DB::RAW('user_id="'.Auth::user()->id.'"'), 'DESC')->get();
    }else{
      $reviews = $business->completedReviews()->with('services', 'comments')->orderby('id', 'desc')->get();
    }

    if($business->chain_id > 0){
      $chain_businesses = $business->chain->numActiveBusinesses();
    }else{
      $chain_businesses = 0;
    }

    $business_services = $business->services()->orderby('parent_id', 'ASC')->get();

    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));
    $this->breadcrumbs->addCrumb($business->name.' Rate Cards');

    $business_categories = is_array($business->getMeta('categories')) ? join(', ', $business->getMeta('categories')).' in '.$business->zone_cache.' '.$zone->name:'';
    $desc = 'Rate Cards of '.$business->name.' in '.$business->zone_cache.', '.$zone->name.'. '.$business->name.' photos on '.mzk_seo_name().'!';
    $business_categories_ids = is_array($business->getMeta('categories')) ? array_keys($business->getMeta('categories')):'';
    $this->setupAdsForDisplay($business_categories_ids, $business->zone);
    $ads = $this->ads;
    $services = mzk_get_active_services();

    $breadcrumbs = $this->breadcrumbs;
    $metas = ['Photos of '.$business->name,
              $business->name.' in '.$zone->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories,
              is_array($business->getMeta('services')) ? join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];

    $seo_title = $business->name.' Rate Cards, '.$business->zone_cache.', '.mzk_slug_to_words($city).' - '.mzk_seo_name();
    $this->meta->set(
        array('title' => $seo_title, 
              'description' => $desc,
              'og'=>[
                'title' => 'Rate Cards of '.$business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                //'image'=> count($business->photos)>0 ? $business->thumbnail('medium') : mzk_assets('assets/splash.jpg'),
                'description' => $desc,
              ]
      )
    );
    $highlights = $business->highlights;
    $phones = $business->displayablePhone();

    $rateCards = $business->rateCards()->get();
    $photos = $business->photos()->get();


    $photos = $business->photos;
    $canonical_url = MazkaraHelper::slugSingle($business);

    return View::make('site.businesses.rate_cards', compact('business', 
'current_user','myreview','chain_businesses','business_services',     'rateCards', 'highlights','phones','ads', 'photos', 'breadcrumbs', 'suggested_spas', 'suggested_salons'))->with('current_subzone', $sub_zone)
                  ->with('meta', $this->meta)
                  ->with('where', $business->zone_cache)
                  ->with('canonical_url', $canonical_url)
                  ->with('default_search', $business->name)
                  ->with('javascript_include', 'application.with.map.ratings.gallery');

  }

  public function slugPackages($slug){

    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $business = $this->business->findBySlug($slug);

    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSinglePackages($this->business->find($business)));
      }
    }


    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSinglePackages($business));
    }


    $suggested_spas = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }

    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));
    $this->breadcrumbs->addCrumb($business->name.' Packages');

    $business_categories = is_array($business->getMeta('categories')) ? join(', ', $business->getMeta('categories')).' in '.$business->zone_cache.' '.$zone->name:'';
    $desc = 'Packages of '.$business->name.' in '.$business->zone_cache.', '.$zone->name.'. '.$business->name.' photos on '.mzk_seo_name().'!';

    $breadcrumbs = $this->breadcrumbs;
    $metas = ['Photos of '.$business->name,
              $business->name.' in '.$zone->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories,
              is_array($business->getMeta('services')) ? join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];
    $this->meta->set(
        array('title' => 'Packages of '.$business->name.' in '.$business->zone_cache.', '.$business_categories.' - '.Lang::get('seo.title'), 
              'description' => $desc,
              'og'=>[
                'title' => 'Packages of '.$business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                //'image'=> count($business->photos)>0 ? $business->thumbnail('medium') : mzk_assets('assets/splash.jpg'),
                'description' => $desc,
              ]
      )
    );

    $photos = $business->photos;
    $canonical_url = MazkaraHelper::slugSingle($business);

    $this->layout = View::make('layouts.parallax');
    $this->layout->with('current_subzone', $sub_zone)
                  ->with('meta', $this->meta)
                  ->with('canonical_url', $canonical_url)
                  ->with('default_search', $business->name);

    return View::make('site.businesses.packages', compact('business', 'photos', 'breadcrumbs', 'suggested_spas', 'suggested_salons'));
  }


  public function slugReviews($slug){

    $city = MazkaraHelper::getLocaleLabel();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $business = $this->business->getBySlug($slug);

    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSingleReviews($this->business->find($business)));
      }
    }

    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSingleReviews($business));
    }


    $suggested_spas = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }
    $current_user = Auth::check()?User::find(Auth::user()->id):null;;

    if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
      $myreview = null;
    }
    if(isset($myreview) && !is_null($myreview)){
      $reviews = $business->completedReviews()->with('services', 'comments')->orderBy(DB::RAW('user_id="'.Auth::user()->id.'"'), 'DESC')->get();
    }else{
      $reviews = $business->completedReviews()->with('services', 'comments')->orderby('id', 'desc')->get();
    }

    $business_services = $business->services()->orderby('parent_id', 'ASC')->get();

    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));
    $this->breadcrumbs->addCrumb($business->name.' Reviews');

    $business_categories = is_array($business->getMeta('categories')) ? join(', ', $business->getMeta('categories')).' in '.$business->zone_cache.' '.$zone->name:'';
    $desc = 'Review of '.$business->name.' in '.$business->zone_cache.', '.$zone->name.'. '.$business->name.' photos on '.mzk_seo_name().'!';
    $business_categories_ids = is_array($business->getMeta('categories')) ? array_keys($business->getMeta('categories')):'';
    $this->setupAdsForDisplay($business_categories_ids, $business->zone);
    $ads = $this->ads;
    $services = mzk_get_active_services();

    $breadcrumbs = $this->breadcrumbs;
    $metas = ['Reviews of '.$business->name,
              $business->name.' in '.$zone->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories,
              is_array($business->getMeta('services')) ? join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];

    $seo_title = $business->name.' Reviews, '.$business->zone_cache.', '.mzk_slug_to_words($city).' - '.mzk_seo_name();

    $this->meta->set(
        array('title' => $seo_title, 
              'description' => $desc,
              'og'=>[
                'title' => 'Reviews of '.$business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                'image'=> count($business->photos) > 0 ? $business->thumbnail('medium') : mzk_assets('assets/splash.jpg'),
                'description' => $desc,
              ]
      )
    );

    $canonical_url = MazkaraHelper::slugSingle($business);
    $highlights = $business->highlights;
    $phones = $business->displayablePhone();


    return View::make('site.businesses.reviews', compact('business', 'services', 'current_user','reviews','business_services',  'myreview',   'highlights','phones','ads', 'photos', 'breadcrumbs', 'suggested_spas', 'suggested_salons'))                    
                    ->with('meta', $this->meta)
                    ->with('where', $business->zone_cache)
                    ->with('default_search', $business->name)
                    ->with('javascript_include', 'application.with.map.ratings.gallery');

;
  }


  public function slugLocation($slug){

    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $business = $this->business->getBySlug($slug);

    // check is this a valid business
    if(!is_object($business)){
      if(is_numeric($business)){
        return Redirect::to(MazkaraHelper::slugSingleLocation($this->business->find($business)));
      }
    }

    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to(MazkaraHelper::slugSingleLocation($business));
    }

    $current_user = Auth::check()?User::find(Auth::user()->id):null;;

    if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
      $myreview = null;
    }



    if($business->chain_id > 0){
      $chain_businesses = $business->chain->numActiveBusinesses();
    }else{
      $chain_businesses = 0;
    }


    $services = mzk_get_active_services();

    $business_services = $business->services()->orderby('parent_id', 'ASC')->get();

    $suggested_spas = Business::select()->hasPhotos()->byLocale()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->byLocale()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }
    if($current_user){
      $favorited_suggested_spas = $current_user->followedBusinessesByIds($suggested_spas->lists('id','id')->all())->get()->lists('favorable_id','favorable_id')->all();
    }


    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));
    $this->breadcrumbs->addCrumb($business->name.' Map');

    $business_categories = is_array($business->getMeta('categories')) ? join(', ', $business->getMeta('categories')).' in '.$business->zone_cache.' '.$zone->name:'';
    $desc = 'Location of '.$business->name.' in '.$business->zone_cache.', '.$zone->name.'. '.$business->name.' photos on Mazkara!';
    $business_categories_ids = is_array($business->getMeta('categories')) ? array_keys($business->getMeta('categories')):'';
    $this->setupAdsForDisplay($business_categories_ids, $business->zone);
    $ads = $this->ads;
    $breadcrumbs = $this->breadcrumbs;
    $metas = ['Map of '.$business->name,
              $business->name.' in '.$zone->name, 
              $business->zone_cache.', '.$zone->name, 
              $business_categories,
              is_array($business->getMeta('services')) ? join(', ', $business->getMeta('services')).' in '.$business->zone_cache.', '.$zone->name:'',
              is_array($business->getMeta('highlights')) ? join(', ', $business->getMeta('highlights')).' in '.$business->zone_cache.', '.$zone->name:'',
              ];

    $seo_title = 'Address of '.$business->name.' in '.$business->zone_cache.', '.mzk_slug_to_words($city).' - Mazkara';

    $this->meta->set(
        array('title' => $seo_title, 
              'description' => $desc,
              'og'=>[
                'title' => 'Map of '.$business->name.' in '.$business->zone_cache.' - '.Lang::get('seo.title'), 
                //'image'=> count($business->photos) > 0 ? $business->thumbnail('medium') : mzk_assets('assets/splash.jpg'),
                'description' => $desc,
              ]
      )
    );
    $highlights = $business->highlights;
    $phones = $business->displayablePhone();

    $canonical_url = MazkaraHelper::slugSingle($business);

    return View::make('site.businesses.map', compact('business','current_user','myreview','chain_businesses',   'suggested_spas',  'highlights','phones','ads', 'photos', 'breadcrumbs', 'suggested_spas', 'suggested_salons'))
    ->with('current_subzone', $sub_zone)
                  ->with('canonical_url', $canonical_url)
                  ->with('meta', $this->meta)
                  ->with('where', $business->zone_cache)
                  ->with('default_search', $business->name)
                  ->with('javascript_include', 'application.with.map.ratings.gallery');
;
  }




  public function getClaim($id){

    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $business = $this->business->find($id);
    // make sure the businesses city is the same city as this 
    if($zone->id != $business->city_id){
      return Redirect::to('/');
    }


    $suggested_spas = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();
    $suggested_salons = Business::select()->hasPhotos()->onlyActive()->orderBy('is_featured', 'DESC')->take(5)->get();

    $this->breadcrumbs->addCrumb($zone->name, MazkaraHelper::slugCity($zone));
    $sub_zone = $business->zone;
    if($sub_zone){
      if($sub_zone->isSubZone()){
        $p = $sub_zone->parent()->get()->first();
        $this->breadcrumbs->addCrumb($p->name, MazkaraHelper::slugCityZone($p));
      }
    }
    $current_user = Auth::check()?User::find(Auth::user()->id):null;;

    if($current_user){
      $myreview = $business->isReviewedBy($current_user->id);
    }else{
      $myreview = null;
    }
    $services = mzk_get_active_services();

    if($business->chain_id > 0){
      $chain_businesses = $business->chain->numActiveBusinesses();
    }else{
      $chain_businesses = 0;
    }


    $business_services = $business->services()->orderby('parent_id', 'ASC')->get();

    $this->breadcrumbs->addCrumb('Salons and Spas '.$business->zone->name, MazkaraHelper::slugCityZone($business->zone));
    $this->breadcrumbs->addCrumb($business->name, MazkaraHelper::slugSingle($business));

    $breadcrumbs = $this->breadcrumbs;

    $this->layout = View::make('layouts.parallax');
    $this->layout->with('current_subzone', $sub_zone)->with('default_search', $business->name);

    return View::make('site.businesses.claim', compact('business', 'myreview', 'business_services', 'breadcrumbs', 'suggested_spas', 'suggested_salons'))->with('current_subzone', $sub_zone)->with('default_search', $business->name);;
  }

  public function postClaim($id){
    $rules = array('name'=>'required','phone'=>'required','email'=>'required');//, 'g-recaptcha-response' => 'required|captcha'
    $input = Input::all();
    $validation = Validator::make($input, $rules);
    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
    $input['business'] = Business::find($input['business_id']);
    $input['city'] = Zone::find($input['business']->city_id);

    if ($validation->passes()){


      Mail::queue('emails.claim', ['data'=>$input, 'server'=>$_SERVER], function($message)  use($input) {
        $message->to('business@fabogo.com', 'Fabogo')
                ->subject('Fabogo['.$input['city']->slug.']: Someone is Claiming a Business ['.time().']');
      });     

      return Redirect::back()->with('notice', 'Thanks - a customer support executive would contact you soon.');
    }

    return Redirect::back()
      ->withInput()
      ->withErrors($validation)
      ->with('message', 'There were validation errors.');

    return Redirect::back();
  }




	public function getRateCards($id)
	{
		$business = $this->business->find($id);

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.rate-cards', compact('business'));
	}

	public function postRateCards($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		if (1)
		{
			$images = Input::only('images');
			$rate_cards = Input::only('rate_card');
			$business->saveRateCards($rate_cards['rate_card']);
			$deletablePhotos = Input::only('deletablePhotos');

			$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);


			return Redirect::action('BusinessesController@getRateCards', $id);
		}

		return Redirect::action('BusinessesController@getRateCards', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}




	public function getPhotos($id)
	{
		$business = $this->business->find($id);

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.photos', compact('business'));
	}

	public function getPhoto($id, $photoId)
	{
		$business = $this->business->find($id);
		$photo = Photo::find($photoId);
		$image = Image::make($photo->image->url());


		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.photo', compact('business', 'photo', 'image'));
	}


	public function postPhoto($id, $photoId)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$photo = Photo::find($photoId);

		if (1)
		{
			
			$img = Image::make($photo->image->url());
			$img->crop(round($input['w']), round($input['h']), round($input['x']), round($input['y']));
			$filename = storage_path().'/media/'.md5(time()).'.jpg';
			$img->save($filename);

			//$img->save($photo->image->url());//exit;
			$photo->image = $filename ;
			$photo->save();
			
			return Redirect::action('BusinessesController@getPhoto', array('id'=>$id, 'photoId'=>$photoId));
		}

		return Redirect::action('BusinessesController@getPhoto', array('id'=>$id, 'photoId'=>$photoId))
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}



	public function postPhotos($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');

		if (1)
		{
			$images = Input::only('images');
			$business->saveImages($images['images']);

			$deletablePhotos = Input::only('deletablePhotos');
			$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);


			return Redirect::action('BusinessesController@getPhotos', $id);
		}

		return Redirect::action('BusinessesController@getPhotos', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}

	public function getBasic($id)
	{
		$business = $this->business->find($id);
		$categories = Category::where('parent_id', null)->get();
		$selected_categories = $business->categories()->lists('category_id')->all();

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.basic', compact('business', 'categories', 'selected_categories'));
	}


	public function postBasic($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if ($validation->passes())
		{
			$business->update($data);
			$cat = Input::only('categories');
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);
			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::action('BusinessesController@getBasic', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}



	public function getServices($id)
	{
		$business = $this->business->find($id);
		$services = Service::where('parent_id', null)->get();
		$selected_services = $business->services()->lists('service_id')->all();

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.services', compact('business', 'services', 'selected_services'));
	}


	public function postServices($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if (1)
		{
			$business->update($data);
			$cat = Input::only('services');
			$business->services()->sync($cat['services']?$cat['services']:[]);

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::action('BusinessesController@getServices', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}


	public function getHighlights($id)
	{
		$business = $this->business->find($id);
		$highlights = Highlight::all();

		$selected_highlights = $business->highlights()->lists('highlight_id')->all();

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.highlights', compact('business', 'highlights', 'selected_highlights'));
	}


	public function postHighlights($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if (1)
		{
			$business->update($data);
			$hi = Input::only('highlights');
			$business->highlights()->sync($hi['highlights']?$hi['highlights']:[]);

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::action('BusinessesController@getHighlights', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}

	public function getTimings($id)
	{
		$business = $this->business->find($id);
		$highlights = Highlight::all();

		$selected_highlights = $business->highlights()->lists('highlight_id')->all();

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.timings', compact('business', 'highlights', 'selected_highlights'));
	}


	public function postTimings($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if (1)
		{
			$business->update($data);
			$timings = Input::only('timings');
			$business->saveTimings($timings['timings']);

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::action('BusinessesController@getTimings', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}
	public function getLocation($id)
	{
		$business = $this->business->find($id);
		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.location', compact('business'));
	}

  public function getJsonLD($business){
    $base = [
            "@context"=> "http://schema.org",
            "@type"=> "LocalBusiness",
            "currenciesAccepted"=> mzk_currency_symbol(),
            'name' => $business->name,
            'description' => $business->quick_description,
            'image'=>$business->getCoverUrl(),
            'address' => [
              "@type"=> "PostalAddress",
              'streetAddress' => $business->landmark,
              'addressLocality' => $business->zone_cache,
              'addressRegion' => $business->city ? $business->city->name : '',
            ],
            "aggregateRating"=>[
              "@type"=> "AggregateRating",
              "ratingValue"=> $business->average_rating,
              "ratingCount"=> $business->accumulatedReviewsCount(),
              "bestRating"=> "5",
              "worstRating"=> "0"
            ],
            'geo' => [
              "@type"=>"GeoCoordinates",
              'latitude' => $business->geolocation_latitude,
              'longitude' => $business->geolocation_longitude,
            ],
        ];

        $phones = $business->displayablePhone();
        if($phones>0){
          $base['telephone'] = array_pop($phones);

        }

      $reviews = array();
      foreach($business->completedActiveReviews()->get() as $review):
        $reviews[] = ["@type"   =>"Review",
                      'author'  =>$review->author_name,
                      'datePublished' =>mzk_f_date($review->created_at),
                      'description'   =>$review->body,
                      'reviewRating'  =>[
                        "@type"=> "Rating",
                        "ratingValue" => $review->rating,
                        "bestRating"  => "5",
                        "worstRating" => "0"
                      ]];
      endforeach;
      if(count($reviews)>0){
        $base['reviews'] = $reviews;
      }


      $opening_hours = [];
      foreach(['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', "Sat"] as $day):
        foreach($business->timings as $timing):
          if(strstr( $timing->daysOfWeek, $day)):
            $opening_hours[] = array(
                                      "@type"=>"OpeningHoursSpecification",
                                      'dayOfWeek'=>[mzk_quick_day($day)],
                                      'opens'=>($timing->open),
                                      'closes'=>($timing->close)
                                      );
          endif;
        endforeach;
      endforeach;

      if(count($opening_hours)>0){
        $base['openingHoursSpecification'] = $opening_hours;
      }




    return $base;
  }


	public function postLocation($id)
	{
		$business = $this->business->find($id);
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if (1)
		{
			$business->update($data);
			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::action('BusinessesController@getLocation', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');

	}

  public function setCategory($category){

    $this->businesses->ofCategories([$category->id]);

    $this->setParams('category', [$category->id]);
    $this->setFilters('category', Category::whereIn('id', [$category->id])->lists('name', 'id')->all());
    return $this->businesses;
  
  }

  public function setservices($servicez){
    $services = [];
    foreach($servicez as $s){
      $services[MazkaraHelper::getServicesAttributeFrom($s, 'slug', 'id')] = MazkaraHelper::getServicesAttributeFrom($s, 'slug');
     }
    $this->businesses->withServices(array_keys($services));

    $this->setParams('service', array_keys($services));
    $this->setFilters('service', $services);
    return $this->businesses;

  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit($id){

		$business = $this->business->find($id);

		if (is_null($business)){
			return Redirect::route('admin.businesses.index');
		}

		$selected_categories = $business->categories()->lists('category_id')->all();

		$categories = Category::where('parent_id', null)->get();
		$services = Service::where('parent_id', null)->get();
		$highlights = Highlight::all();

		$selected_services = $business->services()->lists('service_id')->all();
		$selected_highlights = $business->highlights()->lists('highlight_id')->all();

		$this->layout = View::make('layouts.admin');
    $this->layout->content = View::make('administration.businesses.edit', compact('business', 'selected_categories', 'categories', 'services', 'selected_services', 'highlights', 'selected_highlights'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services'), '_method');
		$validation = Validator::make($input, Business::$rules);
		$data = Input::except('images', 'rate_card', 'categories', 'timings', 'highlights', 'deletablePhotos', 'services');

		if ($validation->passes())
		{
			$business = $this->business->find($id);
			$business->update($data);

			$images = Input::only('images');
			$business->saveImages($images['images']);
			$rate_cards = Input::only('rate_card');
			$business->saveRateCards($rate_cards['rate_card']);
			$deletablePhotos = Input::only('deletablePhotos');
			$business->removeAllImages($deletablePhotos['deletablePhotos']?$deletablePhotos['deletablePhotos']:[]);

			$cat = Input::only('categories');
			$business->categories()->sync($cat['categories']?$cat['categories']:[]);

			$cat = Input::only('services');
			$business->services()->sync($cat['services']?$cat['services']:[]);

			$hi = Input::only('highlights');
			$business->highlights()->sync($hi['highlights']?$hi['highlights']:[]);

			$timings = Input::only('timings');
			$business->saveTimings($timings['timings']);

			return Redirect::route('admin.businesses.show', $id);
		}

		return Redirect::route('admin.businesses.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

  public function rate($id){
    $business = $this->business->find($id);
    $data = ['rating'=>Input::only('rating')];


    MazkaraHelper::clearCurrentPageCacheName(URL::previous());
    if(Auth::check()){
      $data['user_id'] = Auth::user()->id;
    }else{
      $data['ip'] = Request::getClientIp();
    }

    $business->rate($data);
  }

  public function deleteRating($id){
    $business = $this->business->find($id);
    $data = ['rating'=>Input::only('rating')];

    // get the instace of this rating
    if(Auth::check()){
      $rating = Review::select()->isRating()
                          ->byBusiness($business->id)
                          ->byUser(Auth::user()->id)->get();
    }else{
      $rating = Review::select()->isRating()
                          ->byBusiness($business->id)
                          ->byIP(Request::getClientIp())->get();
    }

    if($rating->count()>0){
      $rating->first()->delete();
    }

    $business->updateRatingAndReviewsCount();
  }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->business->find($id)->delete();

		return Redirect::route('admin.businesses.index');
	}

  public function getReviewForm($id){
    $city = MazkaraHelper::getLocale();//Request::segment(1);
    $zone = Zone::findBySlug($city);
    $show_inactive = Auth::check() ? Auth::user()->hasRole('admin') : false;
    $business = $this->business->find($id);

    //see if we have a currently existing review
    $review = $business->isReviewedBy(Auth::user()->id);
    $services = (mzk_get_active_services()) ;

    $view =  View::make('site.reviews.form', compact('services','business','review'));
    $result = array();
    $result['html'] = $view->render();
    return Response::Json($result);
  }



}
