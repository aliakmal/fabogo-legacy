<?php
namespace App\Http\Controllers\Administration;

use BaseController, View, Config, Validator, Redirect, Input;
use App\Models\Zone;
use App\Models\Ad_zone;
use App\Models\User;	

use App\Http\Controllers\Controller;

class Ad_zonesController extends Controller {

	/**
	 * Ad_zone Repository
	 *
	 * @var Ad_zone
	 */
	protected $ad_zone;

	public function __construct(Ad_zone $ad_zone)
	{
		$this->ad_zone = $ad_zone;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ad_zones = $this->ad_zone->all();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_zones.index', compact('ad_zones'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_zones.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Ad_zone::$rules);

		if ($validation->passes())
		{
			$this->ad_zone->create($input);

			return Redirect::route('admin.ad_zones.index');
		}

		return Redirect::route('admin.ad_zones.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ad_zone = $this->ad_zone->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_zones.show', compact('ad_zone'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ad_zone = $this->ad_zone->find($id);

		if (is_null($ad_zone))
		{
			return Redirect::route('admin.ad_zones.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_zones.edit', compact('ad_zone'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ad_zone::$rules);

		if ($validation->passes())
		{
			$ad_zone = $this->ad_zone->find($id);
			$ad_zone->update($input);

			return Redirect::route('admin.ad_zones.show', $id);
		}

		return Redirect::route('admin.ad_zones.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ad_zone->find($id)->delete();

		return Redirect::route('admin.ad_zones.index');
	}

}
