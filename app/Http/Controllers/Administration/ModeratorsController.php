<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\User;

use App\Http\Controllers\Controller;

class ModeratorsController extends Controller {

	/**
	 * Account Repository
	 *
	 * @var Account
	 */
	protected $account;

	public function __construct(User $account)
	{
		$this->account = $account;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$accounts = $this->account->all();

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.accounts.index', compact('accounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.accounts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//$validation = Validator::make($input, User::$rules);
    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    if($validation->passes()):

	    $repo = App::make('UserRepository');
	    $user = $repo->signup(Input::all());

	    if ($user->id) {
	    	$user->confirmed = 1;
	    	$user->save();
	      $user->update($data);
		    $roles = Input::only('roles');

	      $user->roles()->attach($roles['roles']?$roles['roles']:[]);

				//$this->account->create($input);

				return Redirect::route('admin.accounts.index');
			}else{
		    $error = $user->errors()->all(':message');


			return Redirect::route('admin.accounts.create')
	                ->withInput(Input::except('password'));
	    }
    else:
            //$error = $user->errors()->all(':message');

            return Redirect::back()->withInput(Input::except('password'))
                    ->withErrors($validation);
        endif;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$account = $this->account->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.accounts.show', compact('account'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$account = $this->account->find($id);

		if (is_null($account))
		{
			return Redirect::route('admin.accounts.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.accounts.edit', compact('account'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		//$validation = Validator::make($input, Account::$rules);

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    $roles = Input::only('roles');

    if($validation->passes())
		{
			if(strlen($input['password'])==0){
				unset($input['password']);
				unset($input['password_confirmation']);
			}
			$account = $this->account->find($id);
			$account->update($input);
      $account->update($data);
      $account->roles()->sync($roles['roles']?$roles['roles']:[]);

			return Redirect::route('admin.accounts.show', $id);
		}

		return Redirect::route('admin.accounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->account->find($id)->delete();

		return Redirect::route('admin.accounts.index');
	}

}
