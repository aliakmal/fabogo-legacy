<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Job;

use App\Http\Controllers\Controller;

class JobsController extends Controller {

	/**
	 * Job Repository
	 *
	 * @var Job
	 */
	protected $job;

	public function __construct(Job $job)
	{
		$this->job = $job;
    $this->layout = 'layouts.admin-content';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$jobs = $this->job->all();

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.index', compact('jobs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Job::$rules);

		if ($validation->passes())
		{
			$this->job->create($input);

			return Redirect::route('admin.jobs.index');
		}

		return Redirect::route('admin.jobs.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$job = $this->job->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.show', compact('job'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$job = $this->job->find($id);

		if (is_null($job))
		{
			return Redirect::route('jobs.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.jobs.edit', compact('job'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Job::$rules);

		if ($validation->passes())
		{
			$job = $this->job->find($id);
			$job->update($input);

			return Redirect::route('admin.jobs.show', $id);
		}

		return Redirect::route('admin.jobs.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->job->find($id)->delete();

		return Redirect::route('admin.jobs.index');
	}

}
