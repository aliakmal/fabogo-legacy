<?php

namespace App\Http\Controllers\Administration;


use Confide;
use App\Http\Controllers\BaseController as BaseController;
use View;
use Config;
use Validator;
use Redirect;
use Input;

class Controller extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	function __contruct()
	{
  	$this->setupLayout();
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
