<?php
namespace App\Http\Controllers\Administration;


use  BaseController, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\App;
use App\Models\Ad;
use App\Models\Campaign;
use App\Models\Ad_set;

use App\Http\Controllers\Controller;


class Ad_setsController extends Controller {

	/**
	 * Ad_set Repository
	 *
	 * @var Ad_set
	 */
	protected $ad_set, $ad;

	public function __construct(Ad_set $ad_set, Ad $ad)
	{
		$this->ad_set = $ad_set;
		$this->ad = $ad;
    $this->layout = 'layouts.admin-crm';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ad_sets = $this->ad_set->all();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_sets.index', compact('ad_sets'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_sets.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Ad_set::$rules);

		if ($validation->passes())
		{
			$ad_set = $this->ad_set->create($input);
			$ad_set->setCity();
			$ad_set->save();
			
			return Redirect::back();//route('admin.ad_sets.index');
		}

		return Redirect::route('admin.ad_sets.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$ad_set = $this->ad_set->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_sets.show', compact('ad_set'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$adset = $this->ad_set->find($id);

		return View::make('administration.ad_sets.form', compact('adset'));
		////$this->layout = View::make('layouts.admin');
    //$this->layout->content = View::make('administration.ad_sets.edit', compact('ad_set'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ad_set::$rules);

		//if($validation->passes())
		{
			$ad_set = $this->ad_set->find($id);
			$ad_set->update($input);

			return Redirect::back();//('admin.ad_sets.show', $id);
		}

		return Redirect::route('admin.ad_sets.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->ad_set->find($id)->delete();
		return Redirect::back();
		//return Redirect::route('admin.ad_sets.index');
	}



	public function getCreateAd($adset_id)
	{
		$adset = Ad_set::find($adset_id);
		$campaign = $adset->campaign;
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_sets.ads.create', compact('adset', 'campaign'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function postCreateAd($adset_id)
	{
		$input = Input::all();
		$validation = Validator::make($input, Ad::$rules);
		$adset = Ad_set::find($adset_id);

		if ($validation->passes())
		{
			$data = Input::except('photo');
			$ad = $this->ad->create($data);
			$ad->saveImage($input['photo']);

			return Redirect::route('admin.campaigns.show', $adset->campaign_id);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	public function getEditAd($ad_id)
	{
		$id = $ad_id;
		$ad = $this->ad->find($id);

		if (is_null($ad))
		{
			return Redirect::route('admin.ads.index');
		}
		$adset = Ad_set::find($ad->ad_set_id);
		$campaign = Campaign::find($adset->campaign_id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.ad_sets.ads.edit', compact('ad', 'campaign', 'adset'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEditAd($ad_id)
	{
		$id = $ad_id;

		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Ad::$rules);

		if ($validation->passes())
		{
			$data = Input::except('photo', '_method', 'deletablePhotos');
			$ad = $this->ad->find($id);
			$ad->update($data);
			$deletable = Input::get('deletablePhotos');
			if(count($deletable)>0){
				$ad->removeImage();
			}
			if(isset($input['photo'])){
				$ad->saveImage($input['photo']);
			}

			$adset = Ad_set::find($ad->ad_set_id);


			return Redirect::route('admin.campaigns.show', $adset->campaign_id);
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroyAd($id)
	{
		$this->ad->find($id)->delete();

		return Redirect::back();
	}




}
