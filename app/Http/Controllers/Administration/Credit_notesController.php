<?php
namespace App\Http\Controllers\Administration;

use View, Config, Validator, App, PDF, Redirect, Input;

use App\Models\Credit_note;
use App\Models\User;

use App\Http\Controllers\Controller;

class Credit_notesController extends Controller {

	/**
	 * Credit_note Repository
	 *
	 * @var Credit_note
	 */
	protected $credit_note;

	public function __construct(Credit_note $credit_note)
	{
		$this->credit_note = $credit_note;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$credit_notes = $this->credit_note->all();

		return View::make('credit_notes.index', compact('credit_notes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('credit_notes.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Credit_note::$rules);

		if ($validation->passes())
		{
			$credit_note = $this->credit_note->create($input);
	    $credit_note->invoice->setState();
	    $credit_note->invoice->save();

			return Redirect::back();
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}
	public function updatable(){
		$input = Input::all();

		$data = ['id' => $input['pk']];

		$credit_note = $this->credit_note->find($data['id']);

		$credit_note->$input['name'] = $input['value'];
		$credit_note->save();
		return Redirect::back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$credit_note = $this->credit_note->findOrFail($id);

		return View::make('credit_notes.show', compact('credit_note'));
	}

	public function getPdf($id){
		$credit_note = $this->credit_note->findOrFail($id);
		$html =   View::make('administration.credit_notes.pdf', compact('credit_note'))->render();
		$pdf = App::make('dompdf.wrapper');
		$pdf->loadHTML($html);
		return $pdf->download($credit_note->type.'-'.($credit_note->title).'.pdf');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$credit_note = $this->credit_note->find($id);

		if (is_null($credit_note))
		{
			return Redirect::route('credit_notes.index');
		}

		return View::make('credit_notes.edit', compact('credit_note'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Credit_note::$rules);

		if ($validation->passes())
		{
			$credit_note = $this->credit_note->find($id);
			$credit_note->update($input);

			return Redirect::route('credit_notes.show', $id);
		}

		return Redirect::route('credit_notes.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->credit_note->find($id)->delete();

		return Redirect::route('credit_notes.index');
	}

}
