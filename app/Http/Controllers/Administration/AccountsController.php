<?php
namespace App\Http\Controllers\Administration;

use App, Excel, DB,  Response, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Zone;
#use Chrisbjr\ApiGuard\Models\ApiKey;
use App\Http\Controllers\Controller;


class AccountsController extends Controller {

	/**
	 * Account Repository
	 *
	 * @var Account
	 */
	protected $account;

	public function export(){

    $input = Input::all();

    $nm = 'Mazkara_Data_Dump_Users__';//.($input['page']+1);

Excel::create($nm, function($excel) {

    $excel->sheet('Users', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


			$columns = DB::connection()->getSchemaBuilder()->getColumnListing("users");

			$users = User::select('id', 'username', 'email', 'confirmed', 'created_at', 'updated_at', 
					'name', 'about', 'gender', 'twitter', 'instagram', 'favorites_count', 
					'slug', 'ratings_count', 'reviews_count', 'check_ins_count', 'followers_count', 
					'follows_count', 'phone', 'designation', 'designated_at_text', 'designated_at_url', 
					'contact_email_address', 'location', 'location_id', 'current_locale')->get();



			//byLocale()->take(1000)->skip($skip)->get();
			$users = $users->toArray();
//			foreach($users as $ii=>$user){
//				$user['current_zone'] = '';
//				$user['current_city'] = '';
//				$ap = ApiKey::where('user_id', '=', $user['id'])->get()->first();
//				if($ap){
//					if($ap->current_zone > 0){
//						$zone = Zone::find($ap->current_zone);
//						$user['current_zone'] = $zone->name;
//						$user['current_city'] = $zone->city_id;
//					}
//				}
//				$users[$ii] = $user;
//			}

        $sheet->fromArray($users);

    });

})->export('csv');
	}




	public function __construct(User $account)
	{
		$this->account = $account;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$accounts = $this->account->query();

		$input = Input::all();

		if(isset($input['search'])&&($input['search']!='')){
			$accounts->search($input['search']);
		}

		if(isset($input['roles'])&&($input['roles']!='')){
			$accounts->byRoles([$input['roles']]);
		}

    $accounts = $accounts->paginate(100);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.index', compact('accounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function hackValidate($zones, $roles){
    $roles = is_array($roles)?$roles:[];
		if(in_array('2', $roles) && (count($zones)==0)){
			return false;
		}else{
			return true;
		}
	}

	public function store()
	{

		//$validation = Validator::make($input, User::$rules);
    $inp = Input::all();

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);
    $roles = Input::only('roles');
    $zones = Input::only('zones');

    if($validation->passes() && ($this->hackValidate($zones['zones'], $roles['roles'])==true) ):

//	    $repo = App::make('UserRepository');
//	    $user = $repo->signup(Input::all());
      $user =  User::create([
        'name' => $data['name'],
        'email' => $inp['email'],
        'password' => bcrypt($inp['password'])
      ]);

	    if ($user->id) {
	    	$user->confirmed = 1;
	    	$user->save();
	      $user->update($data);

	      if(count($roles['roles'])>0)
		      $user->roles()->attach($roles['roles']?$roles['roles']:[]);

	      if(count($zones['zones'])>0)
		      $user->zones()->attach($zones['zones']?$zones['zones']:[]);

				//$this->account->create($input);

				return Redirect::route('admin.accounts.index');
			}else{
		    $error = $user->errors()->all(':message');


			return Redirect::route('admin.accounts.create')
	                ->withInput(Input::except('password'));
	    }
    else:
            //$error = $user->errors()->all(':message');

        return Redirect::back()->withInput(Input::except('password'))
                ->withErrors($validation);
    endif;

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$account = $this->account->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.show', compact('account'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$account = $this->account->find($id);

		if (is_null($account))
		{
			return Redirect::route('admin.accounts.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.accounts.edit', compact('account'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		//$validation = Validator::make($input, Account::$rules);

    $data = Input::only(User::$profile_fields);
    $validation = Validator::make($data, User::$profile_rules);

    $roles = Input::only('roles');
    $zones = Input::only('zones');

    if($validation->passes() && ($this->hackValidate($zones['zones'], $roles['roles'])==true) ) 
		{
			$account = $this->account->find($id);
			if(strlen($input['password'])==0){
				unset($input['password']);
				unset($input['password_confirmation']);
			}else{
		    //$repo = App::make('UserRepository');
        $account->password = bcrypt($input['password']);
        //$account->password_confirmation = bcrypt($input['password']);//$input['password_confirmation'];
				$account->save();//$account);
			}

			$account->update($data);
			$account->save();
      //$account->update($data);
      if(count($roles['roles'])>0)
	      $account->roles()->sync($roles['roles']?$roles['roles']:[]);
  
      if(count($zones['zones'])>0)
	      $account->zones()->sync($zones['zones']?$zones['zones']:[]);

			return Redirect::route('admin.accounts.show', $id);
		}

		return Redirect::route('admin.accounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->account->find($id)->delete();

		return Redirect::route('admin.accounts.index');
	}

  public function getBan($id)
  {
    $account = $this->account->find($id);
    $account->markBan();
    $account->save();
    return Redirect::back();
  }

  public function getUnban($id)
  {
    $account = $this->account->find($id);
    $account->markUnban();
    $account->save();
    return Redirect::back();
  }


	public function reports(){
    return View::make('administration.accounts.reports');
	}

	public function data(){
    $input = Input::all();

    $start_date = isset($input['start_date'])?$input['start_date']:\Carbon\Carbon::now()->subDays(10);
    $end_date  = isset($input['end_date'])?$input['end_date']:\Carbon\Carbon::now()->addDays(2);

    $sql = ' SELECT COUNT( id ) as registers, DATE( created_at ) as dates FROM users ';
		$sql .=" WHERE created_at >= :start_date AND created_at <= :end_date ";
    $sql .=' GROUP BY DATE( users.created_at ) ORDER BY created_at ASC ';
    $d = DB::select(DB::raw($sql) , ['start_date'=>$start_date, 'end_date'=>$end_date]);
    $result = 	[	'data'=>[],
						    	'total_count'=>0,
						    	'range_count'=>0 ];

    foreach($d as $o){
      $dt = strtotime($o->dates)*1000;

    	$result['data'][] = ['dates'=>$dt, 'registers'=>$o->registers];
    	$result['range_count']+=$o->registers;
    }

    if($input['interval']=='week'){
      $c = 7;
      $dd = [];
      $current = 0;
      foreach($result['data'] as $day){
        $c++;
        if($c>=7){
          $c = 0;
          $current = $day['dates'];
          $dd[$current] = $day;
          $dd[$current]['interval'] = 'week';
        }else{
          $dd[$current]['registers']+= $day['registers'];
        }
      }
	    $result['data'] = array_values($dd);

    }



    $result['avg_count'] = count($result['data']) == 0 ? 0 : ceil($result['range_count']/count($result['data']));

    $result['total_count'] = DB::select(DB::raw('SELECT COUNT(id) as total_users FROM users'));
		$result['total_count'] = $result['total_count'][0]->total_users;

    return Response::Json($result);


	}

}
