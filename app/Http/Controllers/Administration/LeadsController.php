<?php
namespace App\Http\Controllers\Administration;


use Confide, BaseController, View, Config, Validator, Redirect, Input;
use App\Models\User;
use App\Models\Lead;

use App\Http\Controllers\Controller;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class LeadsController extends Controller {

	/**
	 * BrandLead Repository
	 *
	 * @var BrandLead
	 */
	protected $lead;

	public function __construct(Lead $lead)
	{
		$this->lead = $lead;
    $this->layout = 'layouts.admin-crm';		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$leads = $this->lead->query()->with('businesses')->byLocale();

		$params = Input::all();
		$bs = \App\Models\Business::select()->leadsAccessible()->byLocale()->get();

		$businesses = array();
		foreach($bs as $business){
			$businesses[$business->id] = $business->id.' - '.$business->name.'('.$business->zone_cache.')';
		}


		if(isset($params['search']) && (!empty($params['search']))){
			$leads = $leads->where('name', 'like', '%'.$params['name'].'%');
		}

		if(isset($params['is_allocated'])){
			if($params['is_allocated']=='0'){	
				$leads = $leads->isNotAllocated();
			}elseif($params['is_allocated']=='1'){	
				$leads = $leads->isAllocated();
			}

		}


		if(isset($params['interested']) && (!empty($params['interested']))){
			$leads = $leads->where('interested_in', 'like', '%'.$params['interested'].'%');
		}

		$leads = $leads->orderby('created_at', 'desc')->paginate(20);
		return View::make('administration.leads.index', compact('leads', 'params','businesses'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('administration.leads.create');
	}


	public function getImport()
	{
		$bs = \App\Models\Business::select()->leadsAccessible()->byLocale()->get();

		$businesses = array();
		foreach($bs as $business){
			$businesses[$business->id] = $business->id.' - '.$business->name.'('.$business->zone_cache.')';
		}


		return View::make('administration.leads.import', compact('businesses'));
	}

	public function postImport(Request $request)
	{

		$input = Input::all();
		$rules = array('csv'=>'required');

		$validation = Validator::make($input, $rules);

		if ($validation->passes()){

	    $path_name = storage_path().'/media/'.'csv/';
	    $file_name = 'csv-'.time().'.csv';
	    $request->file('csv')->move($path_name, $file_name);

	    $csv = Excel::load($path_name.$file_name, function($reader) {
      })->get();

	    $user_id = \Auth::user()->id;

	    $lead_ids = array();

	    foreach($csv as $row){
	    	if(empty($row->name)){
	    		continue;
	    	}
	    	$data = array('name'=>$row->name, 
	    								'phone'=>$row->phone, 
	    								'interested_in'=>$row->interested_in, 
	    								'email'=>$row->email, 
											'inquiry_at'=> \Carbon\Carbon::now(),

	    								'uploaded_by'=>$user_id);
	    	$lead = \App\Models\Lead::create($data);
				$lead->setCity();
				$lead->save();
	    	$lead_ids[] = $lead->id;
	    }

	    $input = Input::all();
	    $businesses = (isset($input['businesses']) && (count($input['businesses'])>0))?$input['businesses']:[];

			foreach($businesses as $business_id){
				$business = \App\Models\Business::find($business_id);
				$business->leads()->attach($lead_ids, ['allocated_at'=>\Carbon\Carbon::now()]);
			}

	    return Redirect::back()->with('message', 'You have imported '.count($lead_ids).' leads.'.(count($businesses)>0?' Into '.count($businesses).' venues':'' ));
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation);
	}



	public function attachLeads()
	{
    $input = Input::all();
    if(isset($input['lead_ids']) && (strlen($input['lead_ids'])>0)){

    	$lead_ids = explode(',', $input['lead_ids']);

	    if(count($input['businesses'])>0){
	  		foreach($input['businesses'] as $business_id){
	  			$business = \App\Models\Business::find($business_id);
					$business->leads()->detach($lead_ids);
					$business->leads()->attach($lead_ids, ['allocated_at'=>\Carbon\Carbon::now()]);
	  		}
	    }

	    return Redirect::back()->with('notice', 'Success - '.count($lead_ids).' leads allocated to '.count($input['businesses']).' venue(s)' );
    }

    return Redirect::back()->with('warning', 'Select leads to import');
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Lead::$rules);

		if ($validation->passes())
		{
			$data = Input::only(Lead::$fields);
			$data['inquiry_at'] = \Carbon\Carbon::now();
			$data['uploaded_by'] = \Auth::user()->id;
			$lead = $this->lead->create($data);
			$lead->setCity();
			$lead->save();
			return Redirect::route('admin.leads.index');
		}

		return Redirect::route('admin.leads.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$lead = $this->lead->findOrFail($id);
		return View::make('administration.leads.show', compact('lead'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$lead = $this->lead->find($id);

		if (is_null($lead)){
			return Redirect::route('admin.leads.index');
		}

		return View::make('administration.leads.edit', compact('lead'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Lead::$rules);

		if ($validation->passes())
		{
			$lead = $this->lead->find($id);

			$data = Input::only(Lead::$fields);
			$data['uploaded_by'] = \Auth::user()->id;
			unset($data['city_id']);

			$lead->update($data);

			return Redirect::route('admin.leads.show', $id);
		}

		return Redirect::route('admin.leads.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$lead = $this->lead->find($id);
		if(!$lead){
	    return Redirect::back()->with('warning', 'The lead has already been deleted');
		}

		$lead->delete();

		return Redirect::route('admin.leads.index');
	}

}
