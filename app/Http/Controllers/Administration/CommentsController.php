<?php

namespace App\Http\Controllers\Administration;

use View, Config, Validator, Redirect, Input;

use App\Models\Comment;
use App\Models\Category;

use App\Http\Controllers\Controller;

class CommentsController extends Controller {

	/**
	 * Comment Repository
	 *
	 * @var Comment
	 */
	protected $comment;

	public function __construct(Comment $comment)
	{
		$this->comment = $comment;
    $this->layout = 'layouts.admin-content';

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$comments = $this->comment->all();

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.comments.index', compact('comments'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.comments.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Comment::$rules);

		if ($validation->passes())
		{
			$this->comment->create($input);

			return Redirect::back();//('admin.comments.index');
		}

		return Redirect::back()//route('admin.comments.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$comment = $this->comment->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.comments.show', compact('comment'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$comment = $this->comment->find($id);

		if (is_null($comment))
		{
			return Redirect::back();//route('admin.comments.index');
		}

		//$this->layout = View::make('layouts.admin');
    return  View::make('administration.comments.edit', compact('comment'));
	}
	public function updatable()
	{
		$input = Input::all();

		$data = ['id' => $input['pk']];
		$data[$input['name']] = $input['value'];
		
		{
			$comment = $this->comment->find($data['id']);
			$comment->update($data);

			return Redirect::back();
		}

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Comment::$rules);

		if ($validation->passes())
		{
			$comment = $this->comment->find($id);
			$comment->update($input);

			return Redirect::back();////route('admin.comments.show', $id);
		}

		return Redirect::back()//route('admin.comments.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this->comment->find($id)->delete();

		return Redirect::back();//route('admin.comments.index');
	}

}
