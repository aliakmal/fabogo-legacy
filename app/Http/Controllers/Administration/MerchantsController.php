<?php
namespace App\Http\Controllers\Administration;

use Confide, BaseController, View, Response, Config, Validator, Redirect, Input;
use App\Models\Business;
use App\Models\Location;
use App\Models\Category;
use App\Models\Merchant;
use App\Models\User;	
use App\Models\Merchant_module;
use App\Models\Merchant_module_access;

use DB, MazkaraHelper, Excel, Image;

use App\Http\Controllers\Controller;

use GuzzleHttp\Client as HttpClient;

class MerchantsController extends Controller {

	/**
	 * Merchant Repository
	 *
	 * @var Merchant
	 */
	protected $merchant;

	public function __construct(Merchant $merchant)
	{
		$this->merchant = $merchant;
    $this->layout = 'layouts.admin-crm';
	}



	public function export(){

    $input = Input::all();

    $nm = 'Fabogo_Merchants_for_'.MazkaraHelper::getAdminLocale().'_'.time();

Excel::create($nm, function($excel) {

    $excel->sheet('Merchants', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


		$columns = ['merchant', 'venue_name', 'current_acd'];
		$data = [];

		$merchants = Merchant::select()->byLocale()->get();
		foreach($merchants as $merchant){
			
			foreach($merchant->businesses as $business){
				$row = array();
				$row['merchant'] = $merchant->name;
				$row['venue_name'] = $business->name;
				$row['venue_id'] = $business->id;
				$row['current_acd'] = $business->isACDPhoneAllocated();
				$data[] = $row;
			}
		}


        $sheet->fromArray($data);

    });

})->export('csv');
	}



	public function getUsersForMerchant(){
		$input = Input::all();
		$users = User::byName($input['term'])->get()->toArray();
		foreach($users as $ii=>$user){
			$html = View::make('administration.merchants.partials.user', ['user' => $user]);
			$users[$ii]['html'] = (string) $html;
		}

		return Response::json($users);
	}

	public function getOutletsForMerchant(){
		$input = Input::all();
		$businesses = Business::query()->searchBasic($input['term'])->allocatableToMerchants()->get()->toArray();
		foreach($businesses as $ii=>$business){
			$business['preferred_email'] = join(',', $business['email']);
			$html = View::make('administration.merchants.partials.business', ['business' => $business]);
			$businesses[$ii]['html'] = (string) $html;
		}

		return Response::json($businesses);
	}

	public function getOutletsForMerchantInvoiceForm(){
		$input = Input::all();
		$merchant = Merchant::find($input['merchant_id']);

		$result = array();
		$view = View::make('administration.merchants.partials.business-for-invoice-form', ['merchant' => $merchant]);
		$result['html'] = $view->render();
		
		return Response::json($result);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function globalIndex(){
		$all_merchants = $this->getGlobalListOfMerchants();
    return View::make('administration.merchants.global', compact('params', 'all_merchants'));
	}

	public function getFormForModuleAccess($merchant_id, $user_id){
		$merchant = Merchant::find($merchant_id);
		$user = User::find($user_id);

		$merchant_user = \DB::table('merchant_user')->where('merchant_id', '=', $merchant_id)
															->where('user_id', '=', $user_id)
															->where('role', '=', 'merchant')->first();

		$modules = Merchant_module::all();
		$access = Merchant_module_access::select()->where('merchantUser_id', '=', $merchant_user->id)->get();
		$modules_access = array();
		foreach($modules as $module){
			$modules_access[$module->id] = false;
		}

		foreach($access as $one_access){	
			if(isset($modules_access[$one_access->module_id])){
				$modules_access[$one_access->module_id] = $one_access;
			}
		}

		$result = array();
		$view = View::make('administration.merchants.partials.module-access-form', compact('modules_access', 'merchant', 'user', 'merchant_user', 'modules', 'access'));
		$result['html'] = $view->render();
		
		return Response::json($result);
	}

	function postModuleAccess(){
		$input =  Input::all();

		foreach($input['modules'] as $module_data){
			if($module_data['id'] == 0){
				unset($module_data['id']);
				Merchant_module_access::create($module_data);
			}else{
				$merchant_module_access = Merchant_module_access::find($module_data['id']);
				$merchant_module_access->update($module_data);
			}
		}

		return Redirect::back();
	}

	public function exportGlobalIndex(){

    $nm = 'Global_Merchants_'.time();

Excel::create($nm, function($excel) {

    $excel->sheet('Global Merchants', function($sheet) {
    $input = Input::all();

    //$skip = 1000*$input['page'];


		$columns = ['global_id','local_id','name','tan','phone','email','location','bigreach','fabogo'];
		$data = [];

		$all_merchants = $this->getGlobalListOfMerchants();
		foreach($all_merchants as $merchant){
			
				$row = array();
				$row['global_id'] = $merchant['global_id'];
				$row['local_id'] = $merchant['id'];
				$row['name'] = $merchant['name'];
				$row['tan'] = $merchant['tan'];
				$row['phone'] = $merchant['phone'];
				$row['email'] = $merchant['email'];
				$row['location'] = $merchant['city_name'];
				$row['bigreach'] = $merchant['is_bigreach']?'true':'false';
				$row['fabogo'] = $merchant['is_fabogo']?'true':'false';

				$data[] = $row;
		}


        $sheet->fromArray($data);

    });

})->export('csv');

	}


	public function index(){
		$merchants = $this->merchant->query()->byLocale();

		$params = [];
		$input =  Input::all();
    if(Input::has('search') && ($input['search']!='')){
      $merchants->bySearch($input['search']);
      $params['search'] = $input['search'];
    }

    if(Input::has('phone') && ($input['phone']!='')){
      $merchants->byVirtualNumber($input['phone']);
      $params['phone'] = $input['phone'];
    }

		$merchants = $merchants->orderby('id', 'desc')->paginate(20);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.index', compact('params', 'merchants'));
	}

	private function getGlobalListOfMerchants(){
				$merchants = DB::table('merchants')
            ->join('zones', 'merchants.city_id', '=', 'zones.id')
            ->select('merchants.*', 'zones.name as city_name')
            ->get();
    $merchants = json_decode(json_encode($merchants), true);

		$fabogo_merchants = [];
		$client = new \GuzzleHttp\Client();
		$url = 'http://admin.bigreach.io/api/merchants';
    $res = $client->request('GET', $url);
    $response = $res->getBody();
    $bigreach_merchants = json_decode($response, TRUE);
    $all_merchants = [];
    foreach($merchants as $merchant){
  		$merchant['is_fabogo'] = true;
    	if(isset($bigreach_merchants[$merchant['global_id']])){
    		$merchant['is_bigreach'] = true;
    		unset($bigreach_merchants[$merchant['global_id']]);
    	}else{
    		$merchant['is_bigreach'] = false;
    	}
    	$all_merchants[$merchant['global_id']] = $merchant;
    }

    foreach($bigreach_merchants as $merchant){
    	$merchant['is_fabogo'] = false;
  		$merchant['is_bigreach'] = true;
    	$all_merchants[$merchant['global_id']] = $merchant;
    }
    return $all_merchants;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(){
		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::only(Merchant::$fillables);
		$validation = Validator::make($input, Merchant::$rules);
		$inp = Input::all();

		if ($validation->passes()){
			$merchant = $this->merchant->create($input);
			$merchant->setCity();
			$input = Input::only('businesses', 'users', 'sales_admins');
			$merchant->allocateUsersNPocs($input['users'], $input['sales_admins']);
			$businesses = null;
			if(count($inp['businesses'])>0){
				$businesses =  array();
				foreach($inp['businesses'] as $ii=>$vv){
					$businesses[$vv] = array('preferred_email'=>$inp['business_emails'][$ii]);
				}
			}

			//$merchant->allocateBusinesses($input['businesses']);
			
			$merchant->allocateBusinessesNAttributes($businesses);
			$merchant->generateGlobalID();
			$merchant->save();

			return Redirect::route('admin.merchants.index');
		}

		return Redirect::route('admin.merchants.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

  public function toggleLeadsAccessForBusiness($id){
    $business = Business::find($id);
    $business->toggleAccessLeadsReport();
    $business->save();
    return Redirect::back()->with('notice', 'Lead access has been updated');
  }

  public function toggleResumesAccessForBusiness($id){
    $business = Business::find($id);
    $business->toggleAccessResumesReport();
    $business->save();
    return Redirect::back()->with('notice', 'Resume access has been updated');
  }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$merchant = $this->merchant->findOrFail($id);

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.show', compact('merchant'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$merchant = $this->merchant->find($id);

		if (is_null($merchant))
		{
			return Redirect::route('admin.merchants.index');
		}

		//$this->layout = View::make('layouts.admin');
    return View::make('administration.merchants.edit', compact('merchant'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::only(Merchant::$fillables);
		$inp = Input::all();
		$validation = Validator::make($input, Merchant::$rules);

		if ($validation->passes())
		{
			$merchant = $this->merchant->find($id);
			$merchant->update($input);
			$input = Input::only('businesses', 'users', 'sales_admins');
			$merchant->allocateUsersNPocs($input['users'], $input['sales_admins']);
			//$merchant->allocateBusinesses($input['businesses']);
			$businesses = null;
			if(count($inp['businesses'])>0){
				$businesses =  array();
				foreach($inp['businesses'] as $ii=>$vv){
					$businesses[$vv] = array('preferred_email'=>$inp['business_emails'][$ii]);
				}
			}

			$merchant->allocateBusinessesNAttributes($businesses);

			return Redirect::route('admin.merchants.show', $id);
		}

		return Redirect::route('admin.merchants.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->merchant->find($id)->delete();

		return Redirect::route('admin.merchants.index');
	}

}
