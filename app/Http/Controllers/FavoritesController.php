<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;
use App\Models\Favorite;

class FavoritesController extends Controller {

	/**
	 * Favorite Repository
	 *
	 * @var Favorite
	 */
	protected $favorite;

	public function __construct(Favorite $favorite)
	{
		$this->favorite = $favorite;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
//	public function index()
//	{
//		$favorites = $this->favorite->all();
//
//		return View::make('favorites.index', compact('favorites'));
//	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('favorites.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Favorite::$rules);

		if ($validation->passes())
		{
			$this->favorite->create($input);

			return Redirect::route('favorites.index');
		}

		return Redirect::route('favorites.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$favorite = $this->favorite->findOrFail($id);

		return View::make('favorites.show', compact('favorite'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$favorite = $this->favorite->find($id);

		if (is_null($favorite))
		{
			return Redirect::route('favorites.index');
		}

		return View::make('favorites.edit', compact('favorite'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Favorite::$rules);

		if ($validation->passes())
		{
			$favorite = $this->favorite->find($id);
			$favorite->update($input);

			return Redirect::route('favorites.show', $id);
		}

		return Redirect::route('favorites.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
//	public function destroy($id)
//	{
//		$this->favorite->find($id)->delete();
//
//		return Redirect::route('favorites.index');
//	}

}
