<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;

class DiscountsController extends Controller {

	/**
	 * Discount Repository
	 *
	 * @var Discount
	 */
	protected $discount;

	public function __construct(Discount $discount)
	{
		$this->discount = $discount;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$discounts = $this->discount->all();

		return View::make('discounts.index', compact('discounts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('discounts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes())
		{
			$this->discount->create($input);

			return Redirect::route('discounts.index');
		}

		return Redirect::route('discounts.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$discount = $this->discount->findOrFail($id);

		return View::make('discounts.show', compact('discount'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$discount = $this->discount->find($id);

		if (is_null($discount))
		{
			return Redirect::route('discounts.index');
		}

		return View::make('discounts.edit', compact('discount'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Discount::$rules);

		if ($validation->passes())
		{
			$discount = $this->discount->find($id);
			$discount->update($input);

			return Redirect::route('discounts.show', $id);
		}

		return Redirect::route('discounts.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->discount->find($id)->delete();

		return Redirect::route('discounts.index');
	}

}
