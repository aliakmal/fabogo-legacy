<?php
namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use Auth, DB, Input, URL, View, Redirect, Response;
use Location, Request, Lang;
use MazkaraHelper;


use App\Models\Zone;
use App\Models\Ad;

use App\Models\Call_log;
use App\Models\Favorite;
use App\Models\Post;
use App\Models\Photo;
use App\Models\User;
use App\Models\Answer;

class AnswersController extends Controller {

	/**
	 * Post Repository
	 *
	 * @var Post
	 */
  protected $answer;
	protected $params;

	protected $filters;

	public function __construct( Answer $answer){
    $this->answer = $answer;
    $this->params = [];
		$this->filters = [];
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

  public function follow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);

    if(!$user->hasFavourited($class_name, $id)){
      Favorite::create(['user_id'=>$user->id,  'favorable_type'=>$class_name, 'favorable_id'=>$id]);
    }
  }

  public function unfollow($id){
    $user = Auth::user();
    $post = Post::find($id);
    $class_name = mzk_get_class($post);
    
    if($user->hasFavourited('Post', $id)){
      $user->favorites()->where('favorable_id', '=', $id)->where('favorable_type', '=', $class_name)->delete();
    }
  }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(){
		$input = Input::all();
    $data = Input::only(Answer::$fields);
    $data['type'] = Post::ANSWER;
		$answer = $this->answer->create($data);
    $answer->saveCover(isset($input['cover'])?$input['cover']:null);
    $answer->questions()->attach($input['question_id']);

    return Redirect::back();
	}




	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $slug)
	{
		$question = $this->question->find($id);
    $this->layout = View::make('layouts.parallax');

    $this->layout->content =  View::make('site.questions.show', compact('question'));
	}

  protected function setupAdsForDisplay($services = false, $zone = false){
    //$category = $this->getParams('category')?$this->getParams('category'):[];

    $categories = mzk_categories_from_services($services);
    $zone = $zone ? $zone : mzk_get_localeID();

    $zones = $zone ? array_merge([$zone], array_keys(Zone::where('city_id','=',$zone)->lists('id', 'id'))):[];

    //$business_zones = Business_zone::select()->byZones($zones)->lists('id', 'id');
    $business_zones = Zone::whereIn('id', $zones)->lists('business_zone_id','business_zone_id');
    $ads = mzk_get_ad_lists_to_show_today(['categories'=>$categories, 
                                                 'business_zones'=>$business_zones]);
    return $ads;
  }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->post->find($id);

    if (is_null($post) || !($post->isEditableBy(Auth::user()->id)))
		{
			return Redirect::back();
		}
    $user = User::find($post->author_id);//Auth::user()->id);

    $suggested_spas = Business::get()->take(5);
    $suggested_salons = Business::get()->take(5);
    
    $activities = [];//Activity::select()->byUserIds([$user->id])->orderby('id', 'desc')->paginate(10);

    $this->layout = View::make('layouts.parallax');

    $this->layout->content = View::make('site.users.selfies.edit', compact('post','user', 'activities', 'suggested_spas', 'suggested_salons'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Answer::$rules);

		if ($validation->passes())
		{
			$post = $this->post->find($id);
			$post->update(Input::only(Post::$fields));
			$post->saveCover(isset($input['cover'])?$input['cover']:null);

      return Redirect::to($post->url());
		}

		return Redirect::back()
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->post->find($id)->delete();

		return Redirect::back();
	}

}
