<?php

namespace App\Http\Middleware;

use Closure, Redirect;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

include_once app_path().'/../shieldsquare/ss2.php';

class ShieldSquare
{
    public function handle($request, Closure $next)
    {
      //if (\App::environment('production'))
      {
        $shieldsquare_username = "roy@mazkara.com"; // Enter the UserID of the user
        $shieldsquare_calltype = 1;
        $shieldsquare_pid = "";

        $shieldsquare_response = shieldsquare_ValidateRequest($shieldsquare_username, $shieldsquare_calltype, $shieldsquare_pid);

        if ($shieldsquare_response->responsecode != '0'){
          //return redirect('/suspicious');
        }
      }

      return $next($request);
    }

}