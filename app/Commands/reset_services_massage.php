<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class reset_services_massage extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:reset.massage.services';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Reset Massage Services.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$page = $this->argument('page');
		$page = $page == 1 ? 0 : $page - 1;
		$count = $this->argument('count');
		$skip_id = $this->option('skip_id');
		$services_to_replace = [39,40,54];
		$services_to_replace_with = [108];
		if($skip_id){
			//$photos = Photo::select()->where('imageable_type', '=', 'Business')->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$businesses = Business::select()->ofServices($services_to_replace)->where('id', '>', $skip_id)->orderby('id', 'asc')->take($count)->get();
			$this->line($count.' records after id '.$skip_id.' taken from page '.$page);
		
		}else{
			$businesses = Business::select()->ofServices($services_to_replace)->take($count)->skip($page*$count)->get();
			$this->line($count.' records taken from page '.$page);
		}

		foreach($businesses as $business){
			$srs = $business->services->lists('id', 'id');
			$srs[] = 108;

			foreach($services_to_replace as $s){
				if (($key = array_search($s, $srs)) !== false) {
			    unset($srs[$key]);
				}				
			}


			$business->services()->sync($srs);

			$this->info($business->id.' - '.$business->name.' services updated');
		}
		$this->line(count($businesses).' records updated');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('page', InputArgument::OPTIONAL, 'page to start on', 1),
			array('count', InputArgument::OPTIONAL, 'number of entries', 100),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('skip_id', null, InputOption::VALUE_OPTIONAL, 'From After ID.', null),
		);
	}

}
