<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class test_emailer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mazkara:test.email';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hack to test email.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

    $user = User::where('email', '=', 'ali@mazkara.com')->first();
    $posts = Post::with('cover')->onlyPosts()->isViewable()->take(5)->get();



    Mail::send(
      'emails.weekly.posts',
      compact('user', 'posts'),
      function ($message) use ($user) {
        $message->to($user->email, $user->username)
                ->subject('Howdy');
      }
    );

		$this->info('Email sent');

	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
