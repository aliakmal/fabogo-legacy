<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSluggableColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});
		Schema::table('businesses', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});
		Schema::table('zones', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});
		Schema::table('categories', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});

		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('services', function(Blueprint $table)
		{
			$table->dropColumnIfExists('slug');
		});
	}

}