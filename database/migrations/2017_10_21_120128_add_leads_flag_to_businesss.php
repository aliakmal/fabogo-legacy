<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLeadsFlagToBusinesss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_merchant', function($table)
        {
            $table->removeColumn('is_leads_accessible');
        });

        Schema::table('businesses', function($table)
        {
            $table->integer('is_leads_module_accessible')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
