<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotOfferServiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offer_service', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('offer_id')->unsigned()->index();
			$table->integer('service_id')->unsigned()->index();
			$table->foreign('offer_id')->references('id')->on('offers')->onDelete('cascade');
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offer_service');
	}

}
