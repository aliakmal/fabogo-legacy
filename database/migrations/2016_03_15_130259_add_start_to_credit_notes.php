<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Credit_note;

class AddStartToCreditNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_notes', function($table){
            $table->date('start_date');
            $table->date('end_date');
        });

//        $cs = Credit_note::all();
//        foreach($cs as $c){
//            $c->setInitDates();
//            $c->save();
//        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
