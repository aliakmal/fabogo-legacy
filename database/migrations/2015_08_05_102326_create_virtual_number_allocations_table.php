<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVirtualNumberAllocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('virtual_number_allocations', function(Blueprint $table) {
			$table->increments('id');
			$table->string('desc');
			$table->integer('business_id');
			$table->integer('virtual_number_id');
			$table->datetime('start');
			$table->datetime('end');
			$table->string('state');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('virtual_number_allocations');
	}

}
