<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCallLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('call_logs', function(Blueprint $table) {
			$table->increments('id');
			$table->date('dated');
			$table->time('timed');
			$table->string('called_number');
			$table->string('caller_number');
			$table->integer('caller_duration');
			$table->string('agent_list');
			$table->string('call_connected_to');
			$table->string('call_transfer_status');
			$table->integer('call_transfer_duration');
			$table->text('call_recording_url');
			$table->datetime('call_start_time');
			$table->datetime('call_pickup_time');
			$table->string('caller_circle');
			$table->integer('business_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('call_logs');
	}

}
