<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveStateToStuff extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function($table)
		{
    	$table->string('state')->default('active');
		});
		
		Schema::table('services', function($table)
		{
    	$table->string('state')->default('active');
		});

		Schema::table('highlights', function($table)
		{
    	$table->string('state')->default('active');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
