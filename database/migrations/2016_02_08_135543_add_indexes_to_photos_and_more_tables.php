<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToPhotosAndMoreTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('photos', function($table)
		{
			$table->index('imageable_id');
			$table->index('sort');
		});

		Schema::table('virtual_number_allocations', function($table)
		{
			$table->index('business_id');
			$table->index('state');
			$table->index('virtual_number_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
