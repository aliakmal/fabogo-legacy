<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        $sql = " 
                CREATE TABLE `api_business_service` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `business_id` int(11) NOT NULL,
                  `service_id` int(11) NOT NULL,
                  `starting_price` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

                CREATE TABLE `api_keys` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `user_id` int(11) NOT NULL,
                  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
                  `level` smallint(6) NOT NULL,
                  `ignore_limits` tinyint(1) NOT NULL,
                  `created_at` timestamp NULL DEFAULT NULL,
                  `updated_at` timestamp NULL DEFAULT NULL,
                  `deleted_at` timestamp NULL DEFAULT NULL,
                  `current_zone` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `api_keys_key_unique` (`key`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

                CREATE TABLE `api_logs` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `api_key_id` int(11) NOT NULL,
                  `route` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
                  `method` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
                  `params` text COLLATE utf8_unicode_ci NOT NULL,
                  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  `created_at` timestamp NULL DEFAULT NULL,
                  `updated_at` timestamp NULL DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `api_logs_api_key_id_index` (`api_key_id`),
                  KEY `api_logs_route_index` (`route`),
                  KEY `api_logs_method_index` (`method`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

                CREATE TABLE `api_mappointment` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `appointmentTime` varchar(255) DEFAULT NULL,
                  `status_id` int(11) DEFAULT NULL,
                  `description` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `voucherService_id` int(11) NOT NULL,
                  `appointmentDate` date NOT NULL,
                  `previousAppointmentDate` date NOT NULL,
                  `previousAppointmentTime` varchar(255) NOT NULL,
                  `clubID` int(11) NOT NULL,
                  `isReviewed` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `api_mappointment_status_id_35bb91a94d0797bb_uniq` (`status_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mappointmentactivitylogs` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `user` varchar(255) DEFAULT NULL,
                  `updateBy` varchar(255) DEFAULT NULL,
                  `remarks` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `appointment_id` int(11) NOT NULL,
                  `status_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mappointmentcallcustomer` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `callTiming` int(11) DEFAULT NULL,
                  `description` longtext,
                  `rating` int(11) NOT NULL,
                  `createdAt` date NOT NULL,
                  `updatedAt` date NOT NULL,
                  `appointment_id` int(11) NOT NULL,
                  `panelUser_id` int(11) NOT NULL,
                  `user_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mappointmentstatus` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `status` varchar(255) NOT NULL,
                  `displayOrderStatus` varchar(255) NOT NULL,
                  `title` varchar(255) NOT NULL,
                  `description` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `nextStatusOne_id` int(11) DEFAULT NULL,
                  `nextStatusTwo_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mawards` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `eventType` varchar(255) NOT NULL,
                  `credits` int(11) NOT NULL,
                  `message` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mblockslot` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `date` date NOT NULL,
                  `blockTime` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `business_id` int(11) NOT NULL,
                  `status` int(11) NOT NULL,
                  `day` varchar(255) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mbusinessservices` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) DEFAULT NULL,
                  `price` double NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `business_id` int(11) NOT NULL,
                  `service_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mcampaign` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `actionableItemID` varchar(255) DEFAULT NULL,
                  `status` int(11) NOT NULL,
                  `actionableItem` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `actionableColorHex` varchar(100) NOT NULL,
                  `actionableText` varchar(255),
                  `imageURL` varchar(255),
                  `message` varchar(255) NOT NULL,
                  `title` varchar(255),
                  `type` varchar(255),
                  `typeDescription` varchar(255) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mcouponcode` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `code` varchar(255) NOT NULL,
                  `discount` double NOT NULL,
                  `maxDiscount` double NOT NULL,
                  `discountType` int(11) NOT NULL,
                  `maxUsage` int(11) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `expiryDate` date NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mcouponcodelogs` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `code` varchar(255) NOT NULL,
                  `value` double NOT NULL,
                  `valueType` int(11) NOT NULL,
                  `couponType` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `user_id` int(11) DEFAULT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mdeals` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `numberOfVouchersUsed` int(11) NOT NULL,
                  `totalNumberOfVouchers` int(11) NOT NULL,
                  `price` double NOT NULL,
                  `expiryDate` date NOT NULL,
                  `status` int(11) NOT NULL,
                  `gender` varchar(50) NOT NULL,
                  `discountType` int(11) NOT NULL,
                  `discountPercent` int(11) NOT NULL,
                  `originalPrice` double NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `business_id` int(11) NOT NULL,
                  `doesExpire` int(11) NOT NULL,
                  `voucherType` int(11) NOT NULL,
                  `isLoyaltyDeal` int(11) NOT NULL,
                  `tags` longtext NOT NULL,
                  `expiryDays` int(11) NOT NULL,
                  `description` longtext NOT NULL,
                  `moreDetails` longtext NOT NULL,
                  `pricingDetailsJson` mediumtext NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mdeals_businessServices` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `mdeals_id` int(11) NOT NULL,
                  `mbusinessservices_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `mdeals_id` (`mdeals_id`,`mbusinessservices_id`),
                  KEY `D7ed644ca836594f61931465d616a4a7` (`mbusinessservices_id`),
                  CONSTRAINT `D7ed644ca836594f61931465d616a4a7` FOREIGN KEY (`mbusinessservices_id`) REFERENCES `api_mbusinessservices` (`id`),
                  CONSTRAINT `api_mdeals_businessS_mdeals_id_2924a802983d8ed5_fk_api_mdeals_id` FOREIGN KEY (`mdeals_id`) REFERENCES `api_mdeals` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mdealservice` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `businessService_id` int(11) NOT NULL,
                  `deal_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mfabogorate` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `rate` varchar(10) DEFAULT NULL,
                  `tag` varchar(255) DEFAULT NULL,
                  `description` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `user_id` int(11) NOT NULL,
                  `appointment_id` int(11),
                  PRIMARY KEY (`id`),
                  KEY `api_mfabogorate_54c91d3b` (`appointment_id`),
                  CONSTRAINT `api_mfabo_appointment_id_14b3c943ae7524e7_fk_api_mappointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `api_mappointment` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mloyaltydeals` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `numberOfVouchersUsed` int(11) NOT NULL,
                  `totalNumberOfVouchers` int(11) NOT NULL,
                  `points` double NOT NULL,
                  `expiryDate` date NOT NULL,
                  `status` int(11) NOT NULL,
                  `gender` varchar(50) NOT NULL,
                  `discountType` int(11) NOT NULL,
                  `discountPercent` int(11) NOT NULL,
                  `originalPrice` double NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mmerchantmodule` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) NOT NULL,
                  `iconName` varchar(255) NOT NULL,
                  `isDefault` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mmerchantmoduleaccess` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `access` int(11) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `merchantUser_id` int(11) NOT NULL,
                  `module_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mnotification` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(255) NOT NULL,
                  `descriptionText` varchar(255) NOT NULL,
                  `tag` varchar(255) NOT NULL,
                  `views` varchar(255) NOT NULL,
                  `itemType` varchar(255) NOT NULL,
                  `itemID` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mpayment` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `status` varchar(255) NOT NULL,
                  `secretToken` varchar(255) NOT NULL,
                  `transactionID` varchar(255) NOT NULL,
                  `transactionCharge` double NOT NULL,
                  `amountPaid` double NOT NULL,
                  `amountPayable` double NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `paymentGateway_id` int(11) NOT NULL,
                  `voucher_id` int(11) DEFAULT NULL,
                  `transactionDetails` longtext,
                  `user_id` int(11),
                  PRIMARY KEY (`id`),
                  KEY `api_mpayment_e8701ad4` (`user_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mpaymentgateway` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) NOT NULL,
                  `country` varchar(255) NOT NULL,
                  `transactionCharge` double NOT NULL,
                  `transactionFixedCharge` double NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mpaymentreconcile` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `amountPaid` double NOT NULL,
                  `balance` double NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `business_id` int(11) DEFAULT NULL,
                  `date` date,
                  `paymentMethod` varchar(255) NOT NULL,
                  `remarks` longtext NOT NULL,
                  `transactionID` varchar(255) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mpopup` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `message` varchar(255) NOT NULL,
                  `title` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `user_id` int(11) DEFAULT NULL,
                  `actionableColorHex` varchar(100) NOT NULL,
                  `actionableItem` varchar(255),
                  `actionableItemID` varchar(255),
                  `actionableText` varchar(255),
                  `imageURL` varchar(255),
                  `type` varchar(255),
                  `typeDescription` varchar(255) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `api_mpopup_user_id_4172ef707b94540d_fk_api_musers_id` (`user_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_muserdevice` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `deviceID` varchar(255) DEFAULT NULL,
                  `deviceType` varchar(255) DEFAULT NULL,
                  `firebaseID` varchar(255) DEFAULT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `user_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_musers` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) DEFAULT NULL,
                  `phoneNumber` varchar(255) NOT NULL,
                  `gender` varchar(255) DEFAULT NULL,
                  `shareCode` varchar(255) NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `loyalPoints` double NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `phoneNumber` (`phoneNumber`),
                  UNIQUE KEY `shareCode` (`shareCode`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mvoucher` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `code` varchar(255) NOT NULL,
                  `status` int(11) NOT NULL,
                  `type` int(11) NOT NULL,
                  `amountToBePaidToTheBusiness` double NOT NULL,
                  `expiryDate` date NOT NULL,
                  `updatedAt` datetime(6) NOT NULL,
                  `createdAt` datetime(6) NOT NULL,
                  `business_id` int(11) DEFAULT NULL,
                  `campaign_id` int(11) DEFAULT NULL,
                  `deal_id` int(11) DEFAULT NULL,
                  `user_id` int(11) DEFAULT NULL,
                  `dated` date NOT NULL,
                  `couponCode_id` int(11),
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `code` (`code`),
                  KEY `api_mvoucher_3b25f58c` (`couponCode_id`),
                  CONSTRAINT `api_mvouche_couponCode_id_4ad53ab1ac3c5e33_fk_api_mcouponcode_id` FOREIGN KEY (`couponCode_id`) REFERENCES `api_mcouponcode` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_mvoucherservice` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `code` varchar(255) NOT NULL,
                  `status` int(11) NOT NULL,
                  `voucher_id` int(11) NOT NULL,
                  `businessService_id` int(11),
                  PRIMARY KEY (`id`),
                  KEY `api_mvoucherservice_772126a5` (`businessService_id`),
                  CONSTRAINT `businessService_id_3ab0ad893dcc079e_fk_api_mbusinessservices_id` FOREIGN KEY (`businessService_id`) REFERENCES `api_mbusinessservices` (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                CREATE TABLE `api_zones` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

        DB::statement($sql);*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
