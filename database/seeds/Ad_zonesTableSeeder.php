<?php

class Ad_zonesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('ad_zones')->truncate();

    $categories = Category::all()->toArray();
    $cities = Zone::where('parent_id', '=', null)->get()->lists('name', 'id');
    $zones = Zone::whereIn('parent_id', array_keys($cities))->get()->toArray();

    foreach($categories as $category){
      foreach($zones as $zone){
        $a = Ad_zone::firstOrCreate(['zone_id'=>$zone['id'], 'category_id'=>$category['id']]);
        $a->title = str_plural($category['name']).' in '.$zone['name'];
        $a->save();
      }
    }

		// Uncomment the below to run the seeder
		// DB::table('ad_zones')->insert($ad_zones);
	}

}
