<?php

class HighlightsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('highlights')->truncate();

		$highlights = array('Parking Available', 'Appointment Only', 
                        'Walk Ins Allowed', 'Ladies Only', 'Unisex', 
                        'Mens Only', 'Couple Massage', 'Outdoor View', 
                        'Refreshments Served', 'Deals Available' );

    foreach($highlights as $highlight){
      $p = Highlight::firstOrCreate(['name'=>$highlight]);
      $p->save();
    }

		// Uncomment the below to run the seeder
		// DB::table('highlights')->insert($highlights);
	}

}
