<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class CategoriesTableSeeder extends Seeder {

	public function run()
	{

    $categories = [ 'Salon','Spa', 'Medi-Spa'];
    foreach($categories as $category){
      $p = Category::firstOrCreate(['name'=>$category]);
      $p->save();
    }

	}

}