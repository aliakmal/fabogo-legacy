<?php

class DealsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('deals')->truncate();

    $deals = Deal::all();
    foreach($deals as $deal){
      $deal->resluggify();
      $deal->save();
    }
		// Uncomment the below to run the seeder
		// DB::table('deals')->insert($deals);
	}

}
