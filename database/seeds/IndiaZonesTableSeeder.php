<?php

class IndiaZonesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('zones')->truncate();



$cities = ['Pune'=>[
  'Around Pune' => ['Expressway','Khandala','Lavasa','Lonavala','Old Mumbai-Pune Highway'],
  'Camp-MG Road Area' => ['Camp Area','East Street','MG Road'],
  'Deccan & Peths' => ['Budhwar Peth','Deccan Gymkhana','FC Road','JM Road','Law College Road',
                      'Model Colony','Rasta Peth','Sadashiv Peth','Shaniwar Peth','Shivaji Nagar',
                      'Shukrawar Peth','Swargate','Tilak Road'],
  'Kothrud Area' => ['Bavdhan','Erandwane','Karve Nagar','Khadakwasla','Kothrud','Lavale','Mulshi','Narhe',
                    'Warje'],
  'KP, Kalyani, Sassoon Road' => ['Bund Garden Road','Chandan Nagar','Dhanori','Dhole Patil Road',
                                  'Kalyani Nagar','Kharadi','Koregaon Park','Lohegaon','Sassoon Road',
                                  'Viman Nagar','Vishrantwadi','Wadgaon Sheri','Wagholi','Yerawada'],
  'Magarpatta, Pune Sholapur Road' => [ 'B.T. Kawade Road', 'Hadapsar', 'Magarpatta', 'Pune-Solapur Road'],
  'Pimpri Chinchwad Area' => ['Akurdi','Bhosari','Chakan','Chinchwad','Dange Chowk','Dehu Road','Hinjewadi',
                              'Nigdi','Pimple Gurav','Pimple Nilakh','Pimple Saudagar','Pimpri','Pradhikaran','Wakad'],
  'Pune Satara Road Area' => ['Katraj','Parvathi','Satara Road','Shivapur'],
  'SB Road - Aundh' => ['Aundh','Balewadi','Baner','Khadki','Pashan','Pune University',
                        'Senapati Bapat Road','Sinhgad Road'],
  'Wanowrie Kondhwa Area' => ['Bibvewadi','Dhankawadi','Fatima Nagar','Kondhwa','NIBM Road','Salunkhe Vihar Road','Wanowrie']
  ],
'Mumbai'=>['Bandra West' => ['Bandra Talao, Bandra West','Bandstand, Bandra West','Carter Road, Bandra West',
                  'Hill Road, Bandra West','Linking Road, Bandra West','Pali Hill, Bandra West',
                  'Reclamation, Bandra West'],
'Central Mumbai' => ['Andheri East','Bhandup','Chandivali','Chembur','Ghatkopar East',
                  'Ghatkopar West','Kurla','Mahakali','Marol','Matunga East','Matunga West',
                  'Mulund East','Mulund West','Powai','Sakinaka','Sion','Trombay','Vikhroli','Wadala'],
'Navi Mumbai' => ['Airoli','CBD-Belapur','Ghansoli','Kalamboli','Kamothe','Kharghar','Kopar Khairane',
                'Nerul','New Panvel','Old Panvel','Sanpada','Seawoods','Turbhe','Vashi'],
'South Mumbai' => ['Breach Candy','Byculla','Charni Road','Chowpatty','Churchgate','Colaba',
                  'Cuffe Parade','Dadar East','Dadar Shivaji Park','Dadar West','Fort','Girgaum',
                  'Grant Road','Kalbadevi','Kemps Corner','Lower Parel','Mahalaxmi','Mahim',
                  'Malabar Hill','Marine Lines','Mumbai Central','Mumbai CST Area','Nariman Point',
                  'Parel','Peddar Road','Prabhadevi','Tardeo','Worli'],
'Thane' => ['Castle Mill, Thane West','Ghodbunder Road','Hiranandani Estate, Thane West','Kalwa',
            'Kasarvadavli, Thane West','Khopat, Thane West','Majiwada, Thane West','Manpada, Thane West',
            'Naupada, Thane West','Owale, Thane West','Panch Pakhadi, Thane West','Thane Area',
            'Thane East','Upvan, Thane West','Vasant Vihar, Thane West','Wagle Estate, Thane West'],
'Thane Suburbs' => ['Dombivali East','Dombivali West','Kalyan','Mumbra','Ulhasnagar'],
'Western Suburbs' => ['7 Bungalows, Andheri West','Adarsh Nagar, Andheri West',
                      'Andheri Lokhandwala, Andheri West','Andheri West','Bandra East',
                      'Bandra Kurla Complex','Bhayandar','Borivali East','Borivali West','Dahisar',
                      'Gorai','Goregaon East','Goregaon West','Jogeshwari','Juhu','Kandivali East',
                      'Kandivali West','Khar','Malad East','Malad West','Marve','Mira Road',
                      'Nalasopara','Oshiwara, Andheri West','Santacruz East','Santacruz West',
                      'Vasai','Versova, Andheri West','Vile Parle East','Vile Parle West','Virar']],


'Delhi'=>['Central Delhi' => ['Asaf Ali Road','Aurangzeb Road','Barakhamba Road','Connaught Place',
                    'Dr. Zakir Hussain Marg','Feroze Shah Road','Gole Market','India Gate',
                    'ITO','Janpath','Khan Market','Lodhi Colony','Lodhi Road','Mandi House',
                    'Mansingh Road','Paharganj','Pandara Road Market','Parliament Street',
                    'Pragati Maidan','Sundar Nagar','Tilak Marg'],
'East Delhi'=> ['Anand Vihar','Azad Market','Chander Nagar','Dilshad Garden','Geeta Colony',
                'IP Extension','Karkardooma','Krishna Nagar','Laxmi Nagar','Mayur Vihar Phase 1',
                'Mayur Vihar Phase 2','Mayur Vihar Phase 3','Pandav Nagar','Patparganj','Preet Vihar',
                'Shahdara','Shakarpur','Shastri Park','Trilokpuri','Vasundhara Enclave',
                'Vikas Marg','Vivek Vihar'],
'Faridabad' => ['Badarpur Border','Badkal Lake','Ballabhgarh','Charmwood Village','Dayal Bagh',
                'Indraprastha Colony','Mathura Road','NIT','Sainik Colony','Sector 10','Sector 11',
                'Sector 12','Sector 14','Sector 15','Sector 16','Sector 17','Sector 19','Sector 21',
                'Sector 28','Sector 29','Sector 3','Sector 30','Sector 31','Sector 34','Sector 37',
                'Sector 41','Sector 42','Sector 43','Sector 46','Sector 49','Sector 7','Sector 81',
                'Sector 86','Sector 9','Suraj Kund','Tigaon'],

'Ghaziabad' => ['Ambedkar Road','Chander Nagar','Crossing Republik','Govind Puram','Indirapuram',
                'Kaushambi','Kavi Nagar','Loni','Mohan Nagar','Nehru Nagar','Pratap Vihar','Raj Nagar',
                'Rajendar Nagar','Ramprastha','Sahibabad','Shalimar Garden','Shastri Nagar','Surya Nagar',
                'Vaishali','Vaishali Extension','Vasundhara','Wazirabad'],

'Gurgaon' => ['Ambience Mall','Ardee City','DLF Cyber City','DLF Phase 1','DLF Phase 2','DLF Phase 3',
              'DLF Phase 4','DLF Phase 5','Golf Course Road','MG Road','Old Railway Road','Palam Vihar',
              'Sadar Bazar','Sector 12','Sector 14','Sector 15','Sector 17','Sector 21','Sector 22',
              'Sector 23','Sector 29','Sector 30','Sector 31','Sector 33','Sector 37','Sector 39',
              'Sector 4','Sector 43','Sector 44','Sector 45','Sector 5','Sector 50','Sector 53',
              'Sector 54','Sector 56','Sector 57','Sector 7','Sector 8','Sector 83','Sikandarpur',
              'Sohna Road','South City 1','South City 2','Sushant Lok','Udyog Vihar'],

'Noida' => ['Golf Course','Greater Noida','Sector 1','Sector 10','Sector 11','Sector 110',
            'Sector 12','Sector 125','Sector 127','Sector 132','Sector 15','Sector 16',
            'Sector 18','Sector 19','Sector 2','Sector 20','Sector 21','Sector 22',
            'Sector 23','Sector 25','Sector 26','Sector 27','Sector 28','Sector 29',
            'Sector 3','Sector 30','Sector 31','Sector 34','Sector 37','Sector 38',
            'Sector 39','Sector 40','Sector 41','Sector 44','Sector 47','Sector 48',
            'Sector 49','Sector 5','Sector 50','Sector 51','Sector 52','Sector 53',
            'Sector 55','Sector 56','Sector 58','Sector 59','Sector 6','Sector 60',
            'Sector 61','Sector 62','Sector 63','Sector 65','Sector 7','Sector 71',
            'Sector 72','Sector 8','Sector 83','Sector 93','Surajpur','Taj Express Highway'],

'North Delhi' => ['Alipur','Ashok Vihar Phase 1','Ashok Vihar Phase 2','Ashok Vihar Phase 3',
                  'Azadpur','Chandni Chowk','Chawri Bazar','Civil Lines','Daryaganj',
                  'Delhi University-GTB Nagar','GTB Nagar','Gujranwala Town','Jama Masjid',
                  'Kamla Nagar','Kashmiri Gate','Lawrence Road','Majnu ka Tila','Model Town 1',
                  'Model Town 2','Model Town 3','Mukherjee Nagar','Netaji Subhash Place','Pitampura',
                  'Prashant Vihar','Rana Pratap Bagh','Rohini','Shalimar Bagh','Vijay Nagar','Wazirpur'],

'South Delhi'=>['Aaya Nagar','Alaknanda','Anand Lok','Anand Niketan','Aurobindo Marg',
                'Bhikaji Cama Place','Chanakyapuri','Chhatarpur','Chittaranjan Park',
                'Defence Colony','East of Kailash','Friends Colony','Geetanjali Enclave',
                'Greater Kailash (GK) 1','Greater Kailash (GK) 2','Greater Kailash (GK) 3',
                'Green Park','Hauz Khas','Hauz Khas Village','IGI Airport','Jangpura',
                'Jasola','JNU','Jor Bagh','Kailash Colony','Kalkaji','Kapashera','Khanpur',
                'Khel Gaon Marg','Lajpat Nagar 1','Lajpat Nagar 2','Lajpat Nagar 3',
                'Lajpat Nagar 4','Mahipalpur','Malcha Marg','Malviya Nagar','Mathura Road',
                'Mehrauli','MG Road','Moti Bagh','Munirka','Nehru Place','Netaji Nagar',
                'New Friends Colony','Nizamuddin','Okhla Phase 1','Okhla Phase 2','Okhla Phase 3',
                'Panchsheel Park','Qutab Institutional Area','R K Puram','Race Course',
                'Rajokri','Safdarjung','Sainik Farms','Saket','Sardar Patel Marg','Sarita Vihar',
                'Sarojini Nagar','Satyaniketan','SDA','Shahpur Jat','Sheikh Sarai','Siri Fort Road',
                'South Extension 1','South Extension 2','Suraj Kund','Tughlakabad Institutional Area',
                'Uday Park','Vasant Kunj','Vasant Vihar','Yusuf Sarai','Zakir Nagar'],

'West Delhi' => ['Delhi Cantt.','Dwarka','East Patel Nagar','Inderlok','Jail Road','Janakpuri',
              'Karampura','Karol Bagh','Kirti Nagar','Mayapuri Phase 1','Mayapuri Phase 2',
              'Moti Nagar','Najafgarh','Nangloi','Naraina','Narela','Palam','Paschim Vihar',
              'Punjabi Bagh','Raja Garden','Rajendra Place','Rajinder Nagar','Rajouri Garden',
              'South Patel Nagar','Subhash Nagar','Tagore Garden','Tilak Nagar','Uttam Nagar',
              'Vikaspuri','West Patel Nagar']]
              ];

foreach($cities as $city_name=>$zones):
    $city = Zone::firstOrCreate(['name'=>$city_name]);

    foreach($zones as $parent=>$children){
      $p = Zone::firstOrCreate(['name'=>$parent]);
      $city->append($p);
      foreach($children as $child){
        $c = Zone::firstOrCreate(['name'=>$child]);
        $p->append($c);
        $c->country_code = 'IN';
        $c->save();

      }
      $p->country_code = 'IN';
      $p->save();
    }
    $city->country_code = 'IN';
    $city->save();
endforeach;









//    $in_zones = ['Pune',
//                 'Mumbai', 
//                 'Delhi'];
//
//    foreach($in_zones as $name){
//      $ind = Zone::firstOrCreate(['name'=>$name]);
//      $ind->state = 'inactive';
//      $ind->country_code = 'IN';
//      $ind->save();
//    }

    $zones = Zone::all();
    foreach($zones as $zone){
      if(!$zone->isCity()){
        $zone->city_id = $zone->ancestors()->first()->id;
        $zone->save();
      }
    }

		// Uncomment the below to run the seeder
		// DB::table('zones')->insert($zones);
	}

}
