<?php

class RolesTableSeeder extends Seeder {

	public function run(){
		// Uncomment the below to wipe the table clean before populating
		// DB::table('photos')->truncate();

    $roles = [  'admin'=>['manage_users', 'manage_categories', 
                          'manage_listings', 'manage_zones', 'manage_services', 
                          'manage_highlights', 'manage_merchants', 'manage_ads', 
                          'manage_campaigns', 'manage_virtual_numbers', 
                          'access_client_dashboards'],
                'moderator'=>['manage_listings'],
                'sales-admin'=>['manage_merchants', 'manage_ads', 'manage_campaigns', 
                                'manage_virtual_numbers', 'access_client_dashboards'],
                'sales'=>['access_client_dashboards'],
                'client'=>[],
                'user'=>[]];
    foreach($roles as $role=>$permissions){
      $r = Role::firstOrCreate(['name'=>$role]);
      $r->save();
      $ps = array();
      foreach($permissions as $permission){
        $p = Permission::firstOrCreate(['name'=>$permission]);
        $p->display_name = ucwords(str_replace('_', ' ', $permission));
        $p->save();
        $ps[] = $p->id;
      }
      $r->perms()->sync($ps);
    }

    $admin = Role::where('name', '=', 'admin')->first();
    $mod = Role::where('name', '=', 'moderator')->first();

    User::where('username', '=', 'admin')->first()->attachRole( $admin );
    $mods = User::select()->where('username', '<>', 'admin')->get();
    foreach($mods as $one_mod){
      $one_mod->attachRole( $mod );
      $one_mod->save();
    }

		// Uncomment the below to run the seeder
		// DB::table('photos')->insert($photos);
	}

}
