<?php

class ZonesTimeZonesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('zones')->truncate();

    $tz = [ 'AE'=>'Asia/Dubai', 
            'IN'=>'Asia/Kolkata'];

    foreach($tz as $cc=>$vv){
      DB::statement('update zones set time_zone ="'.$vv.'" where country_code="'.$cc.'"');//->where('country_code', $cc)->update(array('time_zone' => $vv));
    }

	}

}
