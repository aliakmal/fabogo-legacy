<?php

$app = new Illuminate\Foundation\Application;


$env = $app->detectEnvironment(function(){



  if(strstr((string)__DIR__, 'demo.mazkara.com') || strstr((string)__DIR__, 'cron.mazkara.com')){
    if(isset($_SERVER['REQUEST_URI']) && strstr($_SERVER['REQUEST_URI'], '/api/')){
      return 'demo-api';
    }

    return 'demo';
  }elseif(strstr((string)__DIR__, 'api.mazkara.com')){

    return 'production-api';

  }elseif(strstr((string)__DIR__, 'sandbox.mazkara.com')){

    return 'test-api';

  }elseif(strstr((string)__DIR__, 'test.mazkara.com')){

    if(isset($_SERVER['REQUEST_URI']) && strstr($_SERVER['HTTP_HOST'], 'sandbox')){
      return 'test-api';
    }

    return 'test';

  }elseif(strstr((string)__DIR__, 'mazkara.com')){

    if(isset($_SERVER['REQUEST_URI']) && strstr($_SERVER['REQUEST_URI'], '/api/')){
      return 'production-api';
    }

    return 'production';

  }elseif(strstr((string)__DIR__, 'mazkara.ajwa.me')){

    return 'staging';

  }elseif(strstr((string)__DIR__, 'mazkara.local') || strstr((string)__DIR__, 'mazkarav2.local')){
    
    if(isset($_SERVER['REQUEST_URI']) && strstr($_SERVER['REQUEST_URI'], '/api/')){
      
      return 'local-api';
    }

    return 'local';

  }
});

return $app;