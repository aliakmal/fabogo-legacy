// This is a manifest file that'll be compiled into dashboard.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear in whatever order it 
// gets included (e.g. say you have require_tree . then the code will appear after all the directories 
// but before any files alphabetically greater than 'dashboard.js' 
//
// The available directives right now are require, require_directory, and require_tree
//
//= require jquery
//= require jquery-ui/jquery-ui.min
//= require jquery.fblogin/dist/jquery.fblogin
//= require bootstrap/dist/js/bootstrap.min
//= require bootstrap-hover-dropdown/bootstrap-hover-dropdown
//= require magnific-popup/dist/jquery.magnific-popup.min
//= require gmaps/gmaps
//= require bootbox.js/bootbox
//= require bootstrap3-dialog/dist/js/bootstrap-dialog
//= require fakeloader/fakeLoader.min
//= require bootstrap-star-rating/js/star-rating
//= require jquery-character-counter/jquery.charactercounter.js
//= require jscroll/jquery.jscroll
//= require underscore/underscore
//= require string/lib/string.min
//= require bootstrap-select/dist/js/bootstrap-select
//= require jquery.imgpreload/jquery.imgpreload.min
//= require jquery-text-counter/textcounter.min
//= require jquery.smartbanner/jquery.smartbanner
//= require main

// __require_tree .