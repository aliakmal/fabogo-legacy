// This is a manifest file that'll be compiled into dashboard.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear in whatever order it 
// gets included (e.g. say you have require_tree . then the code will appear after all the directories 
// but before any files alphabetically greater than 'application.js' 
//
// The available directives right now are require, require_directory, and require_tree
//
//= require jquery
//= require jquery-ui/jquery-ui.min
//= require jQuery-Geolocation/jquery.geolocation
//= require jQuery-Geolocation/jquery.geolocation
//= require jquery-locationpicker-plugin/dist/locationpicker.jquery
//= require mapjs
//= require jquery-bonsai/jquery.bonsai.js
//= require jquery-qubit/jquery.qubit.js
//= require bootstrap/dist/js/bootstrap.min
//= require string/lib/string.min
//= require jt.timepicker/jquery.timepicker.min
//= require jt.timepicker/lib/bootstrap-datepicker
//= require Jcrop/js/jquery.Jcrop.min
//= require ekko-lightbox/dist/ekko-lightbox.min
//= require bootbox.js/bootbox
//= require bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all
//= require bootstrap-select/dist/js/bootstrap-select
//= require magnific-popup/dist/jquery.magnific-popup

//= require bootstrap3-dialog/dist/js/bootstrap-dialog

//= require flot/excanvas.min
//= require flot/jquery.flot
//= require flot/jquery.flot.pie
//= require flot/jquery.flot.resize
//= require flot/jquery.flot.time
//= require flot.tooltip/js/jquery.flot.tooltip

//= require mvpready-core
//= require mvpready-helpers
//= require app/client
// __require_tree .

