// This is a manifest file that'll be compiled into dashboard.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear in whatever order it 
// gets included (e.g. say you have require_tree . then the code will appear after all the directories 
// but before any files alphabetically greater than 'application.js' 
//
// The available directives right now are require, require_directory, and require_tree
//
//= require jquery
//= require jquery-ui/jquery-ui.min
//= require jQuery-Geolocation/jquery.geolocation
//= require jQuery-Geolocation/jquery.geolocation
//= require jquery-locationpicker-plugin/dist/locationpicker.jquery
//= require mapjs
//= require jquery-bonsai/jquery.bonsai.js
//= require jquery-qubit/jquery.qubit.js
//= require bootstrap/dist/js/bootstrap.min
//= require string/lib/string.min
//= require jt.timepicker/jquery.timepicker.min
//= require jt.timepicker/lib/bootstrap-datepicker
//= require Jcrop/js/jquery.Jcrop.min
//= require ekko-lightbox/dist/ekko-lightbox.min
//= require bootbox.js/bootbox
//= require bootstrap-select/dist/js/bootstrap-select
//= require bootstrap-toggle/js/bootstrap-toggle
//= require bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all
//= require x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min
//= require handlebars/handlebars
//= require country-select-js/build/js/countrySelect
//= require jquery-stupid-table/stupidtable
//= require summernote/dist/summernote
//= require raphael/raphael-min.js
//= require morrisjs/morris.js
// __require_tree .

