$(function(){


//$('.dropdown').on('show.bs.dropdown', function(e){
//    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
//  });
//
//  // ADD SLIDEUP ANIMATION TO DROPDOWN //
//  $('.dropdown').on('hide.bs.dropdown', function(e){
//    e.preventDefault();
//    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100, function(){
//      $('.dropdown').removeClass('open');
//        $('.dropdown').find('.dropdown-toggle').attr('aria-expanded','false');
//    });
    //
//  });

  $('.submit-parent-form').click(function(){
    $(this).parents().first().submit();
  });

  $('form.confirmable').submit(function(){
    return confirm('Are you sure?');  
  });

  //if(jQuery.isFunction('magnificPopup'))
  {
    $('.ajax-popup-link').magnificPopup({
      type: 'ajax',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    
  }

  $('.toggl').click(function(){
    elm = $(this).attr('ref');
    $('#'+elm).toggle();
  });

  $('[data-toggle="tooltip"]').tooltip();

  //if(jQuery.isFunction('fakeLoader'))
  {
    $(document).on('click', '.fakeloader-link', function(evt){
      if (evt.ctrlKey){

      }else if(evt.metaKey){

      }else{
        $("#fakeloader").fakeLoader({
          timeToHide:5000,
          zIndex: '89',
          bgColor:"#fff",
          spinner:"spinner7"
        });
      }
    });

    $('.fake-load-it a:not(.no-fake-load), .yamm-content a').click(function(evt){
      if (evt.ctrlKey){
        
      }else if(evt.metaKey){

      }else{

        $("#fakeloader").fakeLoader({
          timeToHide:5000,
          zIndex: '89',
          bgColor:"#fff",
          spinner:"spinner7"
        });
      }
    });
  }

  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })
  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');

    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });

  var MZK_PRELOAD = $('a.image').map(function() {
    return $( this ).attr('href');
  }).get();

  $.imgpreload(MZK_PRELOAD);

  /*
    Handler for pushpop History states

  */

  // we get a normal Location object

  /*
   * Note, this is the only difference when using this library,
   * because the object window.location cannot be overriden,
   * so library the returns generated "location" object within
   * an object window.history, so get it out of "history.location".
   * For browsers supporting "history.pushState" get generated
   * object "location" with the usual "window.location".
   */
//  var location = window.history.location || window.location;
//
//  // looking for all the links and hang on the event, all references in this document
//  $(document).on('click', 'a.link-single-venue', function() {
//    // keep the link in the browser history
//    history.pushState(null, null, this.href);
//
//    // here can cause data loading, etc.
//    // make ajax call to the link
//    // link returns json data
//    // select what the view will be here
//    // 
//
//    return false;
//  });
//
//  // hang on popstate event triggered by pressing back/forward in browser
//  $(window).on('popstate', function(e) {
//    // here can cause data loading, etc.
//    // just post
//  });

  /*
  $('.navbar-nav.yamm').on('show.bs.dropdown', function () {
    $('#fullscreen-overlay').show();
  });

  $('.navbar-nav.yamm').on('hide.bs.dropdown', function () {
    $('#fullscreen-overlay').hide();
  });
  */

$('.yamm .dropdown').hover(function() {
    if($('.yamm-fw.dropdown.parent.open').length>0){
      $('.yamm-fw.dropdown.parent.open').removeClass('open');
      $(this).addClass('open');
    }
  }, function() {
    $(this).find('.dropdown-menu').removeClass('open');
});

$('.yamm').on('show.bs.dropdown', function () {
  showOverLay(null);
});

$('.yamm').on('hidden.bs.dropdown', function () {
  hideOverLay();
});

$.smartbanner({
  title: 'Fabogo - Beauty and Wellness', // What the title of the app should be in the banner (defaults to <title>)
  author: null, // What the author of the app should be in the banner (defaults to <meta name="author"> or hostname)
  price: 'FREE', // Price of the app
  appStoreLanguage: 'us', // Language code for App Store
  inGooglePlay: 'Available in Google Play', // Text of price for Android
  button: 'INSTALL', // Text for the install button
  speedIn: 300, // Show animation speed of the banner
  speedOut: 400, // Close animation speed of the banner
  daysHidden: 0, // Duration to hide the banner after being closed (0 = always show banner)
  daysReminder: 1, // Duration to hide the banner after "VIEW" is clicked *separate from when the close button is clicked* (0 = always show banner)
  hideOnInstall: true, // Hide the banner after "VIEW" is clicked.
});

  $(function(){

    $('.lnk-download-app').magnificPopup({
      type: 'inline',
      showCloseBtn: false,
      fixedContentPos: true
    });
    
  });

  $('html').on('click', function(e) {
    if (typeof $(e.target).data('original-title') == 'undefined' &&
       !$(e.target).parents().is('.popover.in')) {
      $('[data-original-title]').popover('hide');
    }
  });

  $('.call-btn-mobile').magnificPopup({
    items: {
        src: '<div class="p10">Dynamically created popup</div>',
        type: 'inline'
    }
  });  

  $('.lnk-run-on-app').click(function(){
    if($(window).width() < 800) {
      $.magnificPopup.open({
        items: {
          src: $('#download-app').html(), // can be a HTML string, jQuery object, or CSS selector
          showCloseBtn: false,
          fixedContentPos: true,
          type: 'inline'
        }
      });

      return false;
    }
  });

});

function showOverLay(exclude_elems){
  $('#full-screen-overlay').show();
}

function hideOverLay(exclude_elems){
  $('#full-screen-overlay').hide();
}

function showOverLaySelector(){
$('#search-selector').css('z-index', 999);
$('#location-selector').css('z-index', 999);
$('#search-bar-button').css('z-index', 999);
$('#search-location-holder').css('z-index', 999);
$('#search-box-holder').css('z-index', 999);
  $('#full-screen-overlay').show();
}

function hideOverLaySelector(){
  $('#full-screen-overlay').hide();
  $('#search-selector').css('z-index', 1);
  $('#location-selector').css('z-index', 1);
  $('#search-bar-button').css('z-index', 1);
  $('#search-location-holder').css('z-index', 1);
  $('#search-box-holder').css('z-index', 1);
}
