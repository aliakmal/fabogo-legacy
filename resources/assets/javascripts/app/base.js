// Define the module
(function(){
  var mazkaraApp = angular.module('mazkaraApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<{');
    $interpolateProvider.endSymbol('}>');
  });

  mazkaraApp.controller('NavigationController', ['$scope', '$location', function($scope, $location){
    $scope.goToUrl = function(url){
      $location.path(url);
    };
  }]);
}());
