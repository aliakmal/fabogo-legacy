<?php 

  return array(
    'title'=>'Fabogo',
    'site_name'=>'Fabogo',
    'description'=>'Discover amazing offers on Beauty & Wellness services in your Neighborhood. View Ratings, Reviews, Photos and location of Salons, Spas and Fitness Centers.',
    'city-description'=>"Discover amazing offers on Beauty & Wellness services in %s. View Ratings, Reviews, Photos and location of Salons, Spas and Fitness Centers.",
    'listing-description'=>"Discover amazing offers on Beauty & Wellness services in %s. View Ratings, Reviews, Photos and location of Salons, Spas and Fitness Centers in %s.",
    'jobs_title'=>'Jobs at Fabogo',
    'jobs_description'=>'Exciting careers in IT, Sales and Marketing at Fabogo - the beauty and wellness services aggregate'
  );