<div class="container bg-white pb15 bb force-gray">
@include('site.businesses.partials.show.mobile.phone', ['business'=>$business, 'phones'=>$phones])
@include('site.businesses.partials.show.mobile.highlights', ['business'=>$business, 'highlights'=>$highlights])

</div>
<div class="container pb10  force-gray">
    <div class="row">
      <div class="col-md-12">
        @if($business->rateCards()->count() >0)
          <div class=" pl0">
            <h2 class="item-headers ">Rate Card of {{$business->name}}</h2>
            <div class="rate-card-holder">
              @foreach($business->rateCards as $ii=>$one_photo)
                @if($one_photo->hasImage())
                  <a  href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}" 
                      class="mr10 ba image border-radius-5 mb15" style="display:inline-block"
                        ><img src="{{ mzk_cloudfront_image($one_photo->image->url('small')) }}" 
                              data-sequence="{{($ii+1)}}" class="w150-mobile border-radius-5 " /></a>
                @endif
              @endforeach
            </div>  
          </div>
        @endif




      </div>
    </div>
</div>
<div class="container pl0-mobile pr0-mobile force-gray">

@include('site.businesses.partials.show.mobile.services', ['business'=>$business] )
    
    @include('site.businesses.partials.show.mobile.timings', ['business'=>$business])
    @include('site.businesses.partials.show.mobile.location-map', ['business'=>$business])
</div>

<div class="container pb15 pl0-mobile pr0-mobile force-gray">

  <div class="show-on-load p15 pb0 pt5 " style="" >
    <div class="show-only-mobile">
      @if(count($ads)>0)
        <small class=" pb10 ">SPONSORED</small>
        <div class="bxslider">
          @foreach($ads as $ad)
              @include('site.businesses.partials.ad-single')
          @endforeach
        </div>
      @endif
    </div>
  </div>

</div>

