<div class=" hide-only-mobile">
  <div class="container  bg-lite-gray ">
    <div class="row">
      <div class="col-md-3 ">
        @include('site.businesses.partials.show.cards.phone', ['business'=>$business, 'phones'=>$phones])
      </div>
      <div class="col-md-9 ">
        @include('site.businesses.partials.show.cards.highlights', ['business'=>$business, 'highlights'=>$highlights])
      </div>
    </div>
  </div>
</div>

<div class="container " >
<?php ob_start();?>
  @include('site.businesses.partials.rate-cards-packages', ['business'=>$business, 'rateCards'=>$rateCards])        
  @include('site.businesses.partials.photos', ['business'=>$business, 'photos'=>$photos])
<?php $page_images = ob_get_contents();ob_end_clean(); ?>
<div class="row">  
  <div class="row-height">

  <div class="col-md-3 col-height pt20 bg-lite-gray-3 col-height "  style="vertical-align:top;">
  <div class="hide-only-mobile pt10">
    <div  >

        @if(!$business->isNonLocationable())
          @include('site.businesses.partials.show.cards.location-map', ['business'=>$business])
          @include('site.businesses.partials.show.cards.timings', ['business'=>$business])
        @endif


    </div>
  </div>

        <div class="mt15">
          @include('site.businesses.partials.client-dashboard-link', ['business'=>$business])        
        </div>



  </div>

  <div class="col-md-6 col-height "  style="vertical-align:top;">
    <!-- End of Header -->
    <div class="row">
      <div class="col-md-12 pt20 bg-white ">
        @if($business->hasDescription())
        <div class="row ">
          
          <div class="col-md-12">

          <div class="pb15 ">
          <h2 class="item-headers ">ABOUT</h2>
            @if(strlen($business->description)>400 && (!$business->hasYoutubeVideo()))
              <span>{{ nl2br(substr($business->description, 0, 400)) }}</span><span class="hide-when-view-more-desc">... 
                <a class="lnk-view-more-desc pink" href="javascript:void(0)">
                  view more
                </a>
              </span><span style="display:none;" class="show-when-view-more-desc">{{ nl2br(substr($business->description, 400, strlen($business->description))) }}</span>
            @else
              {{ nl2br($business->description) }}
            @endif
          </div></div>
        </div>
        <div class="row ">
          <div class="col-md-12">
            <div class=" "></div>
          </div>
        </div>

        @endif

        <div class="hide-only-mobile ">
          @include('site.businesses.partials.offers', ['business'=>$business, 'offers'=>$offers])        
        </div>


        <div class="hide-only-mobile">
          {{$page_images}}
        </div>

        <div class=" ">
          @include('site.businesses.partials.show.services', ['business'=>$business] )
        </div>
        <div class=" ">
          @include('site.businesses.partials.reviews', ['business'=>$business, 'reviews'=>$reviews] )
        </div>
        <div class="  ">
          <p>
            <a  href="{{MazkaraHelper::getClaimUrl($business)}}" class="text-danger">Claim your Business</a>
          </p>
          <p>
            <a href="javascript:void(0)" class="text-danger report-link" id="report-link">Report a Problem?</a>
          </p>
        </div>
        <div class=" pt10 pb10">
          @include('site.businesses.partials.addendum', ['business'=>$business] )
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
<div class="col-md-3 bg-lite-gray-3 col-height pt20" style="vertical-align:top;">
  @if(!empty($business->pre_right_column_html))
    {{ $business->pre_right_column_html}}
  @endif
  @if($business->showAdsOnVenue())
    @include('site.businesses.partials.ad-list')
  @else
    @include('site.businesses.partials.fb-likebox')
  @endif

        <div class="fw500 pb10 mb10">FEATURED</div>

        @foreach($suggested_spas as $_business)
        <?php $_business->is_favourited = in_array($_business->id, $favorited_suggested_spas) ? true : false;?>
        
          @include('site.businesses.partials.business-niblet', ['business'=>$_business])
        @endforeach


  </div>
</div>
</div>
</div>
</div>
<script src="https://maps.google.com/maps/api/js?language=en&libraries=places&sensor=true"></script>
