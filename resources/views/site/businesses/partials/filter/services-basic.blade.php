<li class="list-group-item  bb0">
  <b>Service?</b>
</li>
<li class="filter-holder list-group-item bt0 bb0 pt0">
  <input class="filter form-control box-lite-shadow input-sm" rel="service-list"  placeholder="Type service" />
</li>

<li class="list-group-item bt0">
  <ul class="list-unstyled padded5 ">
    @foreach($services as $i=>$service)
    <?php
    $prms = $params;


    if((isset($prms['service'])) && in_array($service['id'], $prms['service'])){
      $key = array_search($service['id'], $prms['service']);
      unset($prms['service'][$key]);
      ?>
    <li >
      <a class="pull-left" href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $prms) }}">
        <div>
          <i class="fa fa-check-square selected mr5"></i>
          <b>{{ $service['name'] }}</b>
        </div>
      </a>
      <div class="clearfix"></div>
    </li>

      <?php
    }

    ?>
    @endforeach
</ul>
  <ul id="service-list"  class="list-unstyled padded5 " style="display:none">

    @foreach($services as $i=>$service)
    <?php 
    $parms = $params;
    unset($parms['city']);
    if(($service->parent_id == null)||($service->parent_id == 0)){
      continue;      
    }

    if(!isset($params['service'])){
      $parms['service'] = array();
    }
    if(in_array($service['id'], $parms['service'])){
      continue;
    }

    $parms['service'][] = $service['id'];
    ?>
    <li class="{{ $i>15 ? 'hidden':''}}">
      <a href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $parms) }}">
        <i class="fa gray fa-square-o mr5"></i>
        {{ $service['name'] }}
      </a>
    </li>
    @endforeach
  </ul>
</li>
