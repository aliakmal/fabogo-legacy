<div class=" pt5  pl0 pr0 mb10 pb5"><span class=""><b>SORT BY</b></span>
  <div class="mt5 bb  bt  pt10 pb10">
    <a href="javascript:void(0)" class="no-fake-load no-border-radius mt10 mb10  toggl" ref="sort-listings-box">
      {{ ucwords($params['sort']) }}&nbsp;&nbsp; <span class="pull-right fs75 mt2 fa fa-chevron-down"></span>
    </a>
  </div>
  <div id="sort-listings-box" class="bt pt5" style="display:none">
    <ul  class="fs90 mb0 list-unstyled" >
      <?php 
        $prms = $params;
        unset($prms['category']);
        unset($prms['service']);
        unset($prms['city']);
        $prms['sort'] = $params['sort'];
      ?>
      @foreach(['popularity','rating','name'] as $ii=>$vv)
        <?php  $prms['sort'] = $vv;?>
        <li class="bb pb5 pt5">
            <a class="clearfix" href="{{ URL::to(URL::current().'?'.http_build_query($prms))}}">
              <span class="pull-left fs110 pr5">
              @if($params['sort']==$vv)
                <i class="fa fa-dot-circle-o selected mr5"></i>
              @else
                <i class="fa fa-circle-o mr5"></i>
              @endif
              </span>
              <span class="pull-left fw900 ">
                {{ ucwords($vv) }} (high to low)
              </span>
            </a>
          </li>
      @endforeach
    </ul>
  </div>
</div>

<?php $genders = [4,5,6];?>
@include('site.businesses.partials.filter.zones-basic')

<div class="pt5 mb10 pb5"  classss="p15 bg-white pt5 border-radius-3 pb5 mb10">
  <div class="  pt5 pl0 pr0 p15 pb5 bb mb5"><b>RELATED SERVICES</b></div>
  <ul id="service-list"  class="list-unstyled  mb0" style="">

    @foreach($filterable_services as $i=>$service)
    <?php 
    $parms = $params;
    unset($parms['city']);
    if(($service->parent_id == null)||($service->parent_id == 0)){
      continue;      
    }

    if(!isset($params['service'])){
      $parms['service'] = array();
    }

    if((isset($parms['service'])) && in_array($service['id'], $parms['service'])):
      //$key = array_search($service['id'], $prms['service']);
      unset($parms['service']);
      ?>
    <li class="bb pb5 ovf-hidden nowrap pt5">
      <a  href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $parms) }}">
        <div>
          
          <span class="relative nowrap pull-left ovf-hidden  mt2 fw900" style="padding-left:23px;padding-bottom:1px;">
            <span class="fs125 absolute" style="left:0px;top:0px;"><i class="fa fa-dot-circle-o selected mr5"></i></span>
            {{ $service['name'] }}
          </span>

          
        </div>
      </a>
      <div class="clearfix"></div>
    </li>
  <?php else:
    $parms['service'] = [$service['id']];

  ?>

    <li class="{{ $i>5 ? 'hidden hideable':''}} ovf-hidden nowrap bb pb5 pt5">

      <a href="{{ MazkaraHelper::slugCityZone((isset($sub_zone)?$sub_zone:null), $parms) }}">

          <span class="relative nowrap pull-left ovf-hidden  mt2 fw900" style="padding-left:23px;padding-bottom:1px;">
            <span class="fs125 absolute" style="left:0px;top:-2px;"><i class="fa gray fa-circle-o mr5"></i></span>

            {{ $service['name'] }}
          </span>

      </a>
    </li>
  <?php endif;?>
    @endforeach
  </ul>
  @if($i>5)
    <a href="javascript:void(0)" class="no-fake-load mt10 dpb fw900 toggl-services turquoise">
      <span class="hidden toggl-show">Show Less</span>
      <span class="toggl-hide">Show {{ count($filterable_services)-6 }} More</span>
    </a>
  @endif

</div>

<div class="pt5 mb10 pb5"  classss="p15 bg-white pt5 border-radius-3 mb10 pb5">
  <div class="  pt5 pl0 pr0 p15 bb pb5 mb5"><b>GENDER</b></div>

  <ul class="list-unstyled mb0">
      <li class="  bt0 bb0">
        <ul class="list-unstyled mb0">
          @foreach($highlights as $highlight)
          <?php 
          if(!in_array($highlight['id'], $genders))
            continue;
          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['highlights'])){
            $prms['highlights'] = array();
          }

          $key = -1;

          if(in_array($highlight['id'], $prms['highlights'])){

            $key = array_search($highlight['id'], $prms['highlights']);
            unset($prms['highlights'][$key]);

          }else{
            // show only the current highlight
            //foreach($genders as $gender){
              //$key = array_search($gender, $prms['highlights']);
              //unset($prms['highlights'][$key]);
            //}
            $prms['highlights'][] = $highlight['id'];
            $key = -1;
          }
          ?>
          <li class="bb pb5 pt5">
            @if($key>=0)
              <a class="w100pc" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                <span class="pull-left fs125 pr5"><i class="fa fa-check-square selected mr5"></i></span>
                <span class="pull-left fw900 ">
                  <b>{{ $highlight['name'] }}</b>
                </span>
                <span class="pull-right fs125 pr5"><i class="{{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($highlight['id'])) }}"></i></span>
              </a>
            @else
              <a class="w100pc" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
              <span class="pull-left fs125 pr5"><i class="fa gray fa-square-o mr5"></i></span>
              <span class="pull-left mt2 fw900">
                {{ $highlight['name'] }}
              </span>
                <span class="pull-right fs125 pr5"><i class="{{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($highlight['id'])) }}"></i></span>
              </a>
            @endif
            <div class="clearfix"></div>
          </li>
          @endforeach
        </ul>
      </li>
</ul>
</div>


<div class="pt5 mb10 pb5" classss="p15 pt5 bg-white border-radius-3 mb10 pb5">
  <div class="  pt5 pl0 pr0 p15 pb5 bb mb5"><b>PRICE CATEGORY</b></div>
  <ul class="list-unstyled mb0">
      <li class="">
        <ul class="list-unstyled mb0">
          <?php $costOptions = Business::getCostOptions(); unset($costOptions[0]); ?>

          @foreach($costOptions as $cost_id=>$cost_name)
          <?php 
          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['cost'])){
            $prms['cost'] = array();
          }

          $key = -1;

          if(in_array($cost_id, $prms['cost'])){

            $key = array_search($cost_id, $prms['cost']);
            unset($prms['cost'][$key]);

          }else{
            $prms['cost'][] = $cost_id;
            $key = -1;
          }
          ?>
          <li class="bb pb5 pt5">
            @if($key>=0)
              <a class="w100pc" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                <span class="pull-left fs125 pr5"><i class="fa fa-check-square selected mr5"></i></span>
                <span class="pull-left  fw900">
                  <b>{{ $cost_name }}</b>
                </span>
                <span class="pull-right fs125 "><i class="{{ mzk_icon_cost_estimate($cost_id) }}"></i></span>
              </a>
            @else
              <a class="w100pc" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
              <span class="pull-left fs125 pr5"><i class="fa gray fa-square-o mr5"></i></span>
              <span class="pull-left mt2 fw900">
                {{ $cost_name }}
              </span>
                <span class="pull-right fs125 "><i class="{{ mzk_icon_cost_estimate($cost_id) }}"></i></span>
              </a>
            @endif
            <div class="clearfix"></div>
          </li>
          @endforeach

        </ul>
      </li>
  </ul>      
</div>

<div class="pt5 mb10 pb5"  classss="p15 bg-white pt5 border-radius-3 pb5 mb10">
  <div class="  pt5 pl0 pr0 p15 pb5 bb mb5"><b>MORE FILTERS</b></div>
  <ul class="list-unstyled">
      <li class="">
        <ul class="list-unstyled mb0">
          <li class="bb pb5 pt5">
          @if(isset($params['open']) && ($params['open']=='now'))
            <?php $prms = $params; unset($prms['open']);unset($prms['service']);unset($prms['category']);?>

              <a class="w100pc" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                                               
                <span class="pull-left fs125 pr5"><i class="fa fa-check-square selected mr5"></i></span>
                <span class="pull-left  fw900  ">
                <b>Open Now</b>
              </span>
              <span class="pull-right fs125 "><i class="fa fa-clock-o"></i></span>

            </a>
          @else
            <?php $prms = $params; unset($prms['city']);$prms['open'] = 'now';unset($prms['service']);unset($prms['category']);?>

              <a class="w100pc" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
  
                <span class="pull-left fs125 pr5"><i class="fa gray fa-square-o mr5"></i></span>
                <span class="pull-left mt2 fw900 ">

                  Open Now
                </span>
              <span class="pull-right fs125 "><i class="fa fa-clock-o"></i></span>
              </a>

          @endif
          <div class="clearfix"></div>
        </li>


          @foreach($highlights as $highlight)
          <?php 
          if(in_array($highlight['id'], $genders))
            continue;

          $prms = $params;
          unset($prms['service']);
          unset($prms['category']);
          unset($prms['city']);

          if(!isset($params['highlights'])){
            $prms['highlights'] = array();
          }

          $key = -1;

          if(in_array($highlight['id'], $prms['highlights'])){
            $key = array_search($highlight['id'], $prms['highlights']);
            unset($prms['highlights'][$key]);
          }else{
            $prms['highlights'][] = $highlight['id'];
            $key = -1;
          }
          ?>
          <li class="bb pb5 pt5">
            @if($key>=0)
              <a class="w100pc" rel="nofollow" title="Clear filter" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
                                               
                <span class="pull-left fs125 pr5"><i class="fa fa-check-square selected mr5"></i></span>
                <span class="pull-left  fw900  ">

                <b>{{ $highlight['name'] }}</b>
              </span>
                <span class="pull-right fs125 pr5"><i class="{{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($highlight['id'])) }}"></i></span>

              </a>

            @else
              <a class="w100pc" rel="nofollow" href="{{ URL::to(URL::current(). '?'.http_build_query($prms))}}">
  
                <span class="pull-left fs125 pr5"><i class="fa gray fa-square-o mr5"></i></span>
                <span class="pull-left mt2 fw900 ">

                  {{ $highlight['name'] }}
                </span>
                <span class="pull-right fs125 "><i class="{{ mzk_icon_highlights(MazkaraHelper::getHighlightSlug($highlight['id'])) }}"></i></span>
              </a>
            @endif
            <div class="clearfix"></div>
          </li>
          @endforeach
        </ul>
      </li>
</ul>      
</div>