<a href="{{MazkaraHelper::slugSingle($business)}}"><div class="native-ad border-radius-5">
    <div class="post-img-content border-top-radius-5">
      <img src="{{$business->thumbnail('medium')}}" class="img-responsive" />
    </div><div class="border-bottom-radius-5 content" style="margin-top:-6px;">
      <span class="post-title" style="position:relative"><span>{{strtoupper($business->name)}}</span></span>
      <div class="author">
        {{$business->zone_cache}} {{$business->name}}
      </div>
    </div>
</div></a>
