<hr/>

<div class="bb b_g-white  gray pl0 pt0 p15 " >
  <h2 class="item-headers fs125 gray pt5 pt0 mt0">
    Salons and Spas in {{ $business->zone_cache }}
  </h2>
  <?php $categories = Category::byLocaleActive()->get();?>
  <span class="gray">
  @foreach($categories as $ii=>$category)
      <a class="gray hover-underline fs90" href="{{ MazkaraHelper::slugCityZone($business->zone, ['category'=>[$category->id]])}}">{{MazkaraHelper::getPluralName($category->name)}} in {{$business->zone_cache}}
    @if ($ii+1 < count($categories))
      {{ ', ' }}
    @endif
    </a>
  @endforeach
  </span>
</div>
<div class="b_g-white pl0 pt15 p15" >
  <h2 class="item-headers fs125 gray pt5 pt0 mt0">
    Salons and Spas around {{ $business->zone_cache }}
  </h2>
  <?php $categories = Category::byLocaleActive()->take(3)->get();?>

  <?php $zones = Zone::where('parent_id', '=', $business->zone->parent_id)->take(8)->get();?>
  @foreach($categories as $ii=>$category)
<span class="gray">
    @foreach($zones as $iz=>$_zone)
      <a class="gray hover-underline fs90" href="{{ MazkaraHelper::slugCityZone($_zone, ['category'=>[$category->id]])}}">{{MazkaraHelper::getPluralName($category->name)}} in {{$_zone->name}}
      @if ($iz+1 < count($zones))
        {{ ', ' }}
      @endif
      </a>            
    @endforeach
  </span>
  @endforeach
</div>

<div class="b_g-white pl0 pt15 p15" >
  <h2 class="item-headers fs125 gray pt5 pt0 mt0">
    Related searches leading to this page
  </h2>
<?php 
  $urls = [];
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.', '.$business->city_name.'</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.', '.$business->zone_cache.'</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' location</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' phone number</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' Reviews</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' Offers</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' Prices</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.', '.$business->zone_cache.' '.$business->city_name.'</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' menu</a>';
  $urls[] = '<a class="gray hover-underline fs90" href="'.Request::url().'">'.$business->name.' rate card</a>';

  shuffle($urls);

?>
<span class="gray">{{ join(', ', $urls) }}</span>


</div>

