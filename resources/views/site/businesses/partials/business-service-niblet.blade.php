<div class="mb5 bg-white p10">
  <div class="row">
    <div class="col-md-12">
      <div class="">

        <span class="pull-left mr10">
          <a href="{{MazkaraHelper::slugSingle($business)}}?{{ _apfabtrack(['rhs','venue-image']) }}">

            {{ViewHelper::businessThumbnail($business, [ 'meta'=>'thumbnail', 'width'=>'60', 'height'=>'60', 
                                  'style'=>'height:60px;width:60px; padding-top:3px;'])}}
                </a>
        </span>
        <h5 class="search-item-name media-heading mb0 notransform bolder ovf-hidden nowrap ">
          <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}?{{ _apfabtrack(['rhs']) }}" class="result-title fs80">{{{ $business->getTrimmedName(12) }}} </a>
        </h5>
        <div class="pull-right text-center">
          <div class="fs90">
          @foreach($business->services as $servce)
            @if(isset($service))
              @if($servce->id == $service->id)
                <b>{{ mzk_currency_symbol($business->city_id) }} {{$servce->pivot->starting_price}}+</b>
              @endif
            @else
              @if($servce->pivot->starting_price > 0)

                <b>{{ mzk_currency_symbol($business->city_id) }} {{$servce->pivot->starting_price}}+</b>
              <?php $service = $servce; break;?>
              @endif

            @endif
          @endforeach
          <br/>
          {{ mzk_str_trim($service->name, 15) }}
          </div>
        </div>
        <span title="{{ $business->zone_cache }}" class="search-item-address fs90">
          › {{ $business->zone_cache }}
        </span><br/>
      {{ ViewHelper::starRateBasic($business->rating_average, 'xxs', false) }}

        


      </div>
    </div>
  </div>
</div>
