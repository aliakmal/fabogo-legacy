@if(!$business->isNonLocationable())

@if(count($business->timings)>0)
  <div class="ba  p15 bg-white pt5 border-radius-3 mb10 pb5 mt10 mb10" >
    <div class="  pt5 pl0 pr0 p15 bb pb5 mb5"><b>OPENING HOURS</b></div>

    <table class="table text-center  mr10 mb0" align="center">
      @foreach(['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', "Sat"] as $day)
        <tr>
          <th class="bt0 pt0">{{strtoupper($day)}} </th>
          <td  class="bt0 pt0 text-right" >
            <?php ob_start();?>
            @foreach($business->timings as $timing)
              @if(strstr( $timing->daysOfWeek, $day))
                {{ ViewHelper::time($timing->open)}} - 
                {{ ViewHelper::time($timing->close)}} <br/>
              @endif
            @endforeach
            <?php $str = ob_get_contents(); ob_end_clean();?>
            {{ trim($str) == '' ? '<b>CLOSED</b>' : $str }}
          </td>
        </tr>
      @endforeach
    </table>
    <span class="label hidden label-success">
Timings may change during the Holy month of Ramadan
    </span>


</div>



@endif
@endif