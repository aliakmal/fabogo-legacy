@if(is_array($phones))

  <a data-placement="top" data-business="{{$business->id}}"  
    class="call-button btn border-radius-5 btn-turquoise w100pc p10  mt10 " 
    data-phone="{{ $business->getMobilePhoneText($phones) }}" 
    data-container="body" type="button" data-html="true" href="{{ $business->getMobilePhoneText($phones) }}"
    ref="popover-content-for-desk-{{$business->id}}" id="call-to-book-desk-{{$business->id}}">
      <i class="fa fa-phone-square fs125"></i>
    &nbsp;  
    <span class="fs150 bolder">CALL NOW</span></a>
@endif
