@if(count($photos) >0)
  <div class="ba   pt5  pb5 ">
    <div class="  pt5 pl0 pr0 p15  pb5 mb5 "><b>PHOTOS</b></div>
    <div class="photos-holder">
      @foreach($photos as $ii=>$one_photo)
        <a  href="{{ mzk_cloudfront_image($one_photo->image->url('large')) }}" 
            class="image border-radius-3 mr5 {{ ($ii>=2)?'hide':''}}  mb5" 
            style="display:inline-block">
            <img src="{{ mzk_cloudfront_image($one_photo->image->url('thumbnail')) }}" 
                 alt="{{ $business->name }} + Photo {{ $ii+1 }}"
                 class="w80 border-radius-3" />
        </a>
      @endforeach
      @if(count($photos)>3)
        <a  style="display:inline-block; vertical-align:middle; text-align:center;background-image:url({{ mzk_cloudfront_image($one_photo->image->url('small'))  }})" 
            class="border-radius-3 trigger-gallery-open fs150 sq-card mb0 fw300 image btn btn-default p0 bg-lite-gray  w80 h80  " 
            href="javascript:void(0)">
          <div class="va-container va-container-v va-container-h border-radius-3 overlap-transparent">
            <div class="va-middle text-center border-radius-3 force-white" style="z-index:999;">
              {{count($photos)-3}}<br/>
              more
            </div>
          </div>
        </a>
      @endif
    </div>
  </div>
  <div class="show-only-mobile  pt0 pb10">
    <a class=" btn mb10 show-only-mobile border-radius-5 lnk-run-on-app btn-turquoise w100pc p5  mt10 " 
      href="javascript:void(0)">
      <span class="fs150 bolder">ADD IMAGE</span></a>
  </div>
@endif
