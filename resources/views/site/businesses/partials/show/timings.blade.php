
@if($business->timings()->count()>0)
  <div class="pt10 pb10 bbb ">
    <div class="dpb pb10">
      <span class="fs125 fw900  ">Opening Hours
    </span>
    <!--
      @if($business->isOpenNow())
        <span class="  label label-success mr10 "><small>OPEN NOW</small></span>
      @else
        <span class="  label mr10 "><small>CLOSED NOW</small></span>
      @endif
    -->
    </div>

    <table class="table">
      @foreach(['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', "Sat"] as $day)
        <tr>
            <th>{{strtoupper($day)}} </th>
            <td >
            <?php ob_start();?>
            @foreach($business->timings as $timing)
              @if(strstr( $timing->daysOfWeek, $day))
                {{ ViewHelper::time($timing->open)}} - 
                {{ ViewHelper::time($timing->close)}} <br/>
              @endif
            @endforeach
            <?php $str = ob_get_contents(); ob_end_clean();?>
            {{ trim($str) == '' ? '<b>CLOSED</b>' : $str }}
          </td>
        </tr>
      @endforeach
    </table>
    <span class="label hidden label-success">
Timings may change during the Holy month of Ramadan
    </span>
  </div>
@endif