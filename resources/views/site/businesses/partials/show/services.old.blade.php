@if(count($business->services)>0)
  <div class="pt10 pb10 bbb ">
    <span class="fs125 fw900 pb10 dpb">Services Offered here</span>
    <?php $s = [];?>
    @foreach($business->services()->orderby('name')->get() as $service)
      <?php ob_start();?>
      <a class=" fw300 " href="{{ MazkaraHelper::slugCity(null, ['service'=>[$service->id]]) }}">{{ ($service->name)}}</a> 
      <?php $s[] = trim(ob_get_contents()); ob_end_clean();?>
    @endforeach
    <p>
    <?php echo join(', ', $s);?>
  </p>
  </div>
@endif
