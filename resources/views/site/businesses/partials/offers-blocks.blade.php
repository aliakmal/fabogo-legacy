@if(!isset($offers))
  <?php $offers = $business->offers()->onlyActive()->get();?>
@endif
@if(count($offers) >0)  <div class=" {{$business->total_packages_count > 0?'':'pt10'}} pb10  bg-gray">
    <h2 class="item-headers dpib">{{ strtoupper(mzk_label('specials')) }}</h2>
    <a class="dpib medium-gray fs125 hide-only-mobile mb10" style="vertical-align:middle;" data-toggle="tooltip" data-placement="top" title="Call the venue directly to avail these specials. Don't forget to mention FABOGO!" href="#">
      <i class="fa fa-question-circle"></i>
    </a>
    <div class="package-card-holder">
      <div class="row">
        @foreach(mzk_secure_iterable($business->active_offers()->get()) as $ii=>$offer)
        <div class="col-md-6  ">
          <div class="bg-lite-gray p10 mb10" style="min-height:72px;">
            <span class="pull-left mr10 " style="background-color: rgb(255, 147, 145); padding-top:5px; text-align: center; display: inline-block; height: 50px; width: 50px; color: rgb(255, 255, 255); vertical-align: middle;">
              <b>{{ mzk_offer_price($offer, '<br/>') }}</b>
            </span>
            <div class="text-center pr20 pl20 pt10">
              {{ $offer->body }}
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <div class="green dpb text-left p5 show-only-mobile" style="font-size:12px;">
        <b>
          Call the venue directly to avail these {{ (mzk_label('specials')) }}. Don't forget to mention FABOGO!
        </b>
      </div>

<!--
      <div class="green dpb text-left p5" style="font-size:12px;">
        <b>
          
        </b>
      </div> -->
    </div>
  </div>
@endif
