@extends('layouts.master')
@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="pt10 mt10 mb10 pb10 ">
      <h1 class="text-center">TA<span class="yellow">LK</span></h1>
      <hr/>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-9  li-pl0 ">

    <div class="list-group mt20">
        <h3>POPULAR QUESTIONS</h3>
        <table class="table">
          <thead>
            <tr>
              <th width="60%" class="pl0">TOPICS</th><th >RESPONSES</th><th width="15%">POSTED BY</th><th width="15%">LAST UPDATE</th>
            </tr>
          </thead>
          <tbody>
            @foreach($popular_questions as $question)
              <tr>
                <td  class="pl0"><a href="{{ route('questions.show', [$question->id, $question->slug]) }}">{{ $question->title}}</a></td>
                <td>{{count($question->answers)}}</td>
                <td><a href="{{ route('users.profile.show', [$question->author->id])}}">{{ $question->author->name }}</a></td>
                <td>{{ mzk_f_date($question->updated_at) }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>


      @foreach($data as $servce)
				<h3>{{ $servce->name }}</h3>
				<table class="table">
					<thead>
						<tr>
              <th width="60%" class="pl0">TOPICS</th><th >RESPONSES</th><th width="15%">POSTED BY</th><th width="15%">LAST UPDATE</th>
						</tr>
					</thead>
					<tbody>
						@foreach($servce->questions as $question)
							<tr>
								<td  class="pl0"><a href="{{ route('questions.show', [$question->id, $question->slug]) }}">{{ $question->title}}</a></td>
								<td>{{count($question->answers)}}</td>
                <td><a href="{{ route('users.profile.show', [$question->author->id])}}">{{ $question->author->name }}</a></td>
								<td>{{ mzk_f_date($question->updated_at) }}</td>
							</tr>
						@endforeach
					</tbody>
	      </table>

        <div class="text-right pt10 pb10">
          <a href="{{route('questions.service', [$servce->slug])}}">Show all in {{$servce->name}}</a>
        </div>
      @endforeach
    </div>
  </div>
  <div class="col-md-3  pt20">
  	<div class="pb10 mb10">
      <a href="{{ route('questions.create') }}" class="btn btn-sm btn-turquoise btn-dpb">
        START A CONVERSATION
      </a>
  	</div>

    <div class="pb10 mb10">
      {{ Form::open(['url'=>'/talk', 'method'=>'GET'])}}
        <div class="input-group">
          <input type="text" class="form-control" name="search" placeholder="search">
          <div class = "input-group-btn">      
          <button type="submit" class="btn-square btn btn-turquoise"><i class="fa fa-search"></i></button></div>
        </div>
      {{ Form::close()}}
    </div>
    <div class="mt10 pt10  mb10">
      <small>POPULAR VENUES {{ isset($service)? 'FOR '.strtoupper($service->name) : '' }}</small>
    </div>
    <hr/>
    @foreach($businesses_with_prices as $business)
      @include('site.businesses.partials.business-service-niblet')
    @endforeach

    <div class="mt10 pt10 pb10 mb10">
      <small class=" pb10 mb10">BROWSE VIA SERVICES</small>
      <hr/>
      @foreach($services as $service)
        <div class="pb5">
          <a href="{{route('questions.service', [$service->slug])}}">{{$service->name}}</a>
        </div>
      @endforeach
    </div>
  </div>
</div>
@stop
@section('js')

@stop
