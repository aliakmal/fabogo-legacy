@extends('layouts.parallax')
@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1  ">
    <div class="pt10 mt10 mb10 pb10 ">
      <h1 class="text-center">SEL<span class="yellow">FIES</span></h1>
        <p class="text-center">by {{ $user->full_name}}</p>
      <hr/>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-10 col-md-offset-1  ">

		<div class="row">

        @foreach($posts as $post)
          @if(is_object($post))
            <div class="col-md-4">
              @include('site.posts.partials.selfie', ['post'=>$post])
            </div>
          @endif
        @endforeach
    </div>
    <div class="clearfix pt10">
      <div class="pull-left">
        {{ $posts->currentPage() }} of
        {{ $posts->lastPage() }} pages
      </div>
      <div class="pull-right">
        {{ $posts->appends($params)->render() }}        
      </div>
    </div>

  </div>
</div>
@stop
@section('js')

@stop
