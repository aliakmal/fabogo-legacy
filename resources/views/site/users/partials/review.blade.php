@include('site.users.partials.business-niblet', ['business'=>$business, 'no_border'=>true])
<div class="media pb10 pt5 bbb mt0" >
  <p>
    <small class="gray">{{{ Date::parse($review->updated_at)->ago() }}}</small>
  </p>
  <div>
<span class="dpib"><span class="dpib">RATED:</span> {{ ViewHelper::starRateSmallBasic($review, 'xs')}}</span>
    {{ nl2br($review->body) }}
  </div>
</div>