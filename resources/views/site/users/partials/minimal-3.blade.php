<div class="pt15 pb15 pl10 pr10 mb10 bg-white ba bbd border-radius-5">
  <div>

  <div class="pull-right">
    @if(Auth::check())
      @if($user->id != Auth::user()->id)
      <?php 
        $css_append = $user->isFollowed(Auth::user()->id) ? 'favourited' : 'favourite';
        $title = ($user->isFollowed(Auth::user()->id)? 'Following' : 'Follow ').' '.$user->full_name;
      ?>

        <a href="javascript:void(0)" 
           class="btn  border-radius-25 show-only-mobile fs75 btn-green-line mt5 user-follow 
                  {{ $css_append }}" 
            title="{{ $title }}"  
            data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
          <span class="show-when-followed">FOLLOWING</span>
          <span class="hide-when-followed">FOLLOW</span>
        </a>
        <a href="javascript:void(0)" 
           class="btn  border-radius-25 hide-only-mobile fs75 btn-green-line mt5 user-follow 
                  {{ $css_append }}" 
            title="{{ $title }}"  
            data-toggle="tooltip" data-placement="left"  data-route="users" rel="{{$user->id}}">
          <span class="show-when-followed">FOLLOWING</span>
          <span class="hide-when-followed">FOLLOW</span>
        </a>

      @endif
    @else

      <a href="/users/create" id="ajax-login-link" 
         class=" ajax-popup-link btn fs75  border-radius-25 
                btn-green-line mt5 user-follow btn-default" 
          title="Sign in to follow {{$user->full_name}}"  
          data-toggle="tooltip" data-placement="left"  
          data-route="users">
          FOLLOW
      </a>
    @endif

  </div>

    <div class=" ">
      <div class="pull-left mr5">
        {{ ViewHelper::userAvatar($user) }}
      </div>
      <div class=" fw900"><a href="{{route('users.profile.show', $user->id)}}">{{ $user->name }}</a></div>
      <div class="italic">{{ $user->authors_designation }}</div>
    </div>

  <div class="clearfix"></div>
  <div class=" p10 mt5 pb0">
    <div class="row">
    @foreach($user->posts()->take(4)->get() as $ii=>$post)
      <div class="col-md-3 col-xs-3 pl5 pr5"><img src="{{ $post->getCoverUrl('thumbnail')}}" width="100%" /></div>
    @endforeach
    </div>
    <div class="pt10 hidden mt5 ">
      <div class="row italic">
        @if(1)
        <div class="col-xs-4 col-md-4 ">
          <i class="fa fw900 fa-user-plus fs125 green"></i> {{$user->followers_count}} follower(s)
        </div>
        @endif
        <div class="col-xs-4 col-md-4 text-center">
          <i class="fa fw900 fa-edit fs125 blue"></i> {{count($user->posts)}} Post(s)
        </div>
        @if(1)
        <div class="col-xs-4 col-md-4 text-right">
          <i class="fa fw900 fa-heart fs125 red"></i> {{$user->follows_count}} like(s)
        </div>
        @endif
      </div>
    </div>
  </div>

  </div>
</div>