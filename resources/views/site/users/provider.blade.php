<div class="container">    
  <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    <div class="panel panel-default" >
      <div class="panel-heading">
        <div class="panel-title">Register with your {{$account->provider}} account</div>
      </div>
      <div style="padding-top:30px" class="panel-body" >


        {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->action('/users/provider/register')->encodingType('multipart/form-data') }}
            {{ BootForm::bind($user) }}

         @if (Session::get('error'))
            <div class="alert alert-error alert-danger">
              {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
            </div>
          @endif
          @if (Session::get('notice'))
            <div class="alert alert-success">
              {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
            </div>
          @endif
          {{ BootForm::text('Full name', 'name')->placeholder('Full Name') }}
          {{ BootForm::textarea('About', 'about')->placeholder('A bit about you') }}
          {{ BootForm::select('Gender', 'gender', array('male'=>'Male', 'female'=>'Female')) }}
          {{ BootForm::email('Email', 'email')->placeholder('Email address') }}
          {{ BootForm::password('Password', 'password')->placeholder('Password') }}
          {{ BootForm::password('Confirm Password', 'password_confirmation')->placeholder('password confirmation') }}

          {{ BootForm::hidden('provider', '')->value($account->provider) }}
          {{ BootForm::hidden('provider_id', '')->value( $account->provider_id) }}
          {{ BootForm::hidden('meta', '')->value( $account->meta) }}
          {{ BootForm::token() }}
          {{ BootForm::submit('Save Details') }}
      {{ BootForm::close() }}



      </div>                     
    </div>  
  </div>
</div>