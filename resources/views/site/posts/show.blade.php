@extends('layouts.parallax')
@section('preheader')
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "91cce174-96e7-4c79-8290-329bed8eca64", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>
@stop
@section('content')
<div class="container ">
  <div class="row">
    <div class="col-md-8">
    <div class="row-height">
      <!-- <div class="row bg-lite-gray pb10">
        <div class="col-md-2 hide-only-mobile c_ol-height "></div>
        <div class="col-md-10 co_l-height " style="vertical-align:top">
          
        </div>
      </div>-->
      <div class="row">
        <div class="col-md-2 hide-only-mobile c_ol-height "></div>
        <div class="col-md-10 co_l-height " style="vertical-align:top">
          <div class="inside-full-height">
            <h1 class="pt10 mt10  fw500 item-name mt20">{{$post->title}}</h1>
            @include('site.posts.partials.show.sharables')
            <div class="mb10 mt10 fs125">
              {{ $post->caption }}
            </div>
            @if($post->isVideo())
              <iframe width="100%" height="450" src="{{ $post->videoUrl() }}" frameborder="0" allowfullscreen>
              </iframe>
              <div class="well fs80">
                {{ $post->caption }}
              </div>
            @else
              @if($post->hasCover())
                <img src="{{ $post->cover->image->url('xlarge') }}" title="{{ $post->getCoverPhotoTitle() }}" alt="{{ $post->getCoverPhotoAlt() }}" width="100%" class="ba"  />
              @endif
              <div class="pt10 page-font-override">
                {{ $post->body}}
              </div>

            @endif
            @include('site.posts.partials.show.author')
            @include('site.posts.partials.show.subscribe-newsletter')
            <hr />
      @include('site.posts.partials.show.comments')
      <p class="p10 ">&nbsp;</p>
</div>
    </div></div></div></div>
    <div class="col-md-3 hide-only-mobile col-height bg-lite-gray-3 pt20" style="vertical-align:top">
      @include('elements.coming-soon')
      @if($service)
        <div class="fw900 pb10 mb10">POPULAR VENUES FOR {{ strtoupper($service->name)}}</div>

        @foreach($businesses_with_prices as $business)
          @include('site.businesses.partials.business-service-niblet')
        @endforeach
      @endif

      @if(count($suggested_posts)>0)
        <div class="p15 pt10 mt5 pb10 bg-white  mb10">
          <div class="fw900 pb10  ">TRENDING STORIES</div>
            @foreach($suggested_posts as $post)
              @include('site.posts.partials.post-niblet')
            @endforeach
          @endif
        </div>

      @include('site.businesses.partials.ad-list')

    </div>
        <div class="col-md-1 hide-only-mobile col-height bg-lite-gray-3" style="vertical-align:top"></div>

  </div>
  </div>
</div>
@stop
@section('js')
<script type="text/javascript">
$(function(){
  $('.subscribe-popup-link').magnificPopup({
    type: 'ajax',
    showCloseBtn:true,
    closeBtnInside:true,
    fixedContentPos: 'auto',
    closeOnBgClick:true,

      callbacks: {
      open: function() {
        $('.subscribe-popup-link').parents('div').first().remove();
      }
    }
  });    

  $(document).on('click', '.favourite', function(){
    $(this).removeClass('favourite');
    $(this).addClass('favourited');
    v = $('#likes-count-holder').html();
    v = Number(v)+1;
    $('#likes-count-holder').html((v).toString());

    $.ajax({
      type: 'POST',

      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/follow',
      success:function(data){

      }
    })

  });

  $(document).on('click', '.favourited', function(){
    $(this).removeClass('favourited');
    $(this).addClass('favourite');
    v = $('#likes-count-holder').html();
    v = Number(v)-1;
    $('#likes-count-holder').html((v).toString());


    $.ajax({
      type: 'POST',
      url:'/'+$(this).data('route')+'/'+$(this).attr('rel')+'/unfollow',
      success:function(data){

      }
    })


  });


});
</script>

@stop