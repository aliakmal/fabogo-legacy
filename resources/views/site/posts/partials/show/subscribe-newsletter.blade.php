@if(Auth::user())
  @if(Auth::user()->isRecievingNewsletter()!=false)
    <div class="p10 text-center">
      <p><br/></p>
      <p class="lead">
        <a class="subscribe-popup-link btn btn-lg btn-turquoise" href="/newsletters/subscribe">Subscribe</a> to our newsletter
      </p>
      <p><br/></p>
    </div>
  @endif
@else
  <div class="p10 text-center">
    <p><br/></p>
    <p class="lead"><a class="ajax-popup-link  btn-lg  btn btn-turquoise" href="/users/create">Sign Up</a> to subscribe to our newsletter</p>
    <p><br/></p>
  </div>
@endif
