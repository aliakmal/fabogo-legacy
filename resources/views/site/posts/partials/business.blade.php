<div  class="hide-only-mobile relative clearfix   ">
  <span class="absolute tpos10 rpos10 zindex99">
    @if(Auth::check())
      <a href="javascript:void(0)" style="font-size:20px;" title="Favorite {{ $business->name }}" class="lnk-run-on-app {{ Auth::check()? ($business->is_favourited == true ? 'favourited' : 'favourite'):'favourite' }} " data-route="businesses" rel="{{$business->id}}">
        <i class="fa fa-heart hide-when-unfavorite"></i> 
        <i class="fa fa-heart-o hide-when-favorite" style="color:#fff;"></i> 
      </a>
    @else
      <a href="/users/login" style="font-size:20px;" title="Favorite {{ $business->name }}" class="lnk-run-on-app page-scroll ajax-popup-link" data-route="businesses" rel="{{$business->id}}">
        <i class="fa fa-heart-o " style="color:#fff;"></i> 
      </a>
    @endif
  </span>
  <a href="{{ MazkaraHelper::slugSingle($business) }}?{{_apfabtrack('body', 'venue-image')}}" class="relative  bb0  dpb" >
    {{
      ViewHelper::businessBgThumbnail($business, 
                                      [  'width'=>'100%', 'height'=>'100%', 
                                          'class'=>'  fw300', 'meta'=>'medium',
                                          'no-fake'=>' ', 
                          'style'=>'max-width:250px; max-height:173px; display:block;vertical-align:middle;font-size:40px;'])
    }}


    <div class="tpos0 absolute overlap-transparent-3x dpb va-container-v va-container-h ">
    </div>

      <div class="absolute bpos10  rpos10">
        <span class="force-white"><b><small>{{$business->accumulatedReviewsCount()}} votes&nbsp;</small></b></span>
        <span class="dark-gray">{{ ViewHelper::starRateBasic($business->rating_average, 'm', false, $business->accumulatedReviewsCount() ) }}</span>
      </div>
    </a>


    <div class=" p5 bg-white bg-white ba bt0  mb5" style="max-height:65px;">
    <div class="text-left">
         
      <span class="">
        <span class="">
          
          <h5 class=" fw900 media-heading mb0 notransform  ">
            <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}?{{_apfabtrack(['body', 'venue-name'])}}" class="dpb h24 w100pc dark-gray nowrap ovf-hidden fs80 ">
              <span class="fs150 show-only-mobile">{{{ $business->getTrimmedName(34) }}}</span>
              <span class="fs110 hide-only-mobile ">{{{ $business->name }}}</span>
            </a>
          </h5>
          <span title="{{ $business->zone_cache }}" class="dpb  medium-gray search-item-address ">
            {{ mzk_str_trim($business->zone_cache, 22) }}
          </span>
        </span>
          <?php 
            $phones = $business->getMeta('current_numbers');
            $phone_text = count($phones)>0 ? join('|', $phones) : 'No phone number available.';
          ?>
          <span class="clearfix"></span>

        </span>

        <span class="hidden">
        <h5 class="search-item-name media-heading mb0 notransform bolder ">
          <a title="{{{ $business->name }}}" href="{{ MazkaraHelper::slugSingle($business) }}?{{_apfabtrack(['body', 'venue-name'])}}" class=" fs80">
            {{{ $business->getTrimmedName(25) }}} 
          </a>
        </h5>
        <span title="{{ $business->zone_cache }}" class="search-item-address ">
          {{ mzk_str_trim($business->zone_cache, 28) }}
        </span>
      </span>

    </div>

      
    </div>
  </div>
  <div class="show-only-mobile mb10">
    <?php $no_offers = true;?>
  @include('site.businesses.partials.single-mobile')
  </div>
