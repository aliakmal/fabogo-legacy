
    <div  class="bg-white clearfix">

      @if($post->hasCover())
      <div class="pull-left pr10 relative">
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body-mobile', 'image'])}}" class="  " >
          <img src="{{ $post->cover->image->url('medium') }}" width="270" class="ba"  />
        </a>
      </div>
      @endif

      <div class="">
        <div class="pos-relative p10 pt0" >
          <div class="mb10 pl10">
            <div class="text-left bb ml0  m10 mb0 pt0 pr0 pb5">

              <div class="pull-right">
                <span class="">
                  <i class=" flaticon flaticon-eye110 op80 medium-gray"></i>
                  <span class="medium-gray fs90"><b>{{ $post->views }} Views</b></span>
                </span>
                
                &nbsp;
                <span class="">
                  <i class="fa fa-thumbs-up op80 {{ $post->num_likes()>0 ? 'lite-blue':'medium-gray' }} "></i>
                </span>
                <span class="fs90 medium-gray"><b> {{ $post->num_likes() }} Likes</b></span>
              </div>
              
              <div class="fs90 medium-gray">
                {{ strtoupper(join(', ', $post->services()->lists('name','name')->all())) }}

              </div>
            </div>
          </div>
          <h3 class=" fw500 ovf-hidden mt10 mb5" style="margin-top:0px;">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body-mobile', 'post-title'])}}" class="hover-underline dpib ">
              {{{ ($post->title) }}}
            </a>
          </h3>
          <div class="mt5 pt5  mb2">
            {{{ ($post->caption) }}}
          </div>
          <div class="mt10 fs90">
            <a title="{{{ $post->title }}}" href="{{ $post->url() }}?{{_apfabtrack(['body-mobile', 'read-more'])}}" >Read More <i class="fa fs125 fa-caret-right"></i></a>
          </div>

          <div class="mt10   ">
            <div class="pull-left pr15">
              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['body-mobile','post-author-image', 'post-id-'.$post->id])}}">
                {{ ViewHelper::userAvatar($post->author)}}
              </a>
            </div>

              <a href="{{route('users.profile.show', $post->author->id)}}?{{_apfabtrack(['body-mobile','post-author-name', 'post-id-'.$post->id])}}">
                {{ $post->authors_full_name }}
              </a>
              <span class="medium-gray">&nbsp;&nbsp;<i>posted {{{ Date::parse($post->published_on)->ago() }}}</i></span>
            <br/>
            <span class="medium-gray">{{ $post->authors_designation }}</span>

              <div class="clear"></div> 

          </div>
          <div class=""></div>

        </div>

        
      </div>
    </div>

