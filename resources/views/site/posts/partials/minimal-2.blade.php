<div class="pt15 pb15 mb10  bg-white ba border-radius-5">

<div class="pl10 pr10">
      <div class="pull-left mr5">
        {{ ViewHelper::userAvatar($user) }}
      </div>
      <div class="fs110 fw900">
        <a href="{{route('users.profile.show', $user->id)}}">{{ $user->name }}</a>
      </div>
      <div class="italic">{{ $user->authors_designation }}</div>

  <div class="clearfix"></div>

  <div class="fw900 ovf-hidden mb5 mt10" >
    <a title="{{{ $post->title }}} " href="{{ $post->url() }}" class=" dpib  fs150">
      {{ $post->title }}
    </a>
  </div>
  <p class="dark-gray ">{{mzk_str_trim($post->caption, 100)}}</p>
</div>
      @if($post->hasCover())
        <a title="{{{ $post->title }}}" href="{{ $post->url() }}" class="  " >
          <img src="{{ $post->cover->image->url('xlarge') }}" width="100%" class="ba"  />
        </a>
      @endif
<div class="p10 pb0">

<div class="pb15 mt5 pt15 ">

  @foreach($post->services()->lists('name', 'name')->all() as $s)
    <span class="fs110 dpib p5 pl10 pr10 medium-gray baturquoise border-radius-10 mr5">{{ $s }}</span>
  @endforeach
</div>

<div class="dark-gray  italic bbd btd pt10 pb10">
{{ $post->views }} Views &nbsp;&nbsp;&nbsp;{{ count($post->likes) }} Likes
</div>
<div class="pt15 " >
    @if(Auth::check())
      <a href="javascript:void(0)" class="dpib lnk-run-on-app {{ Auth::check() ? ((isset($post->isFavorite) && ($post->isFavorite == true)) ||  $post->liked(Auth::user()->id) ? 'favourited' : 'favourite'):'favourite' }} " rel="{{$post->id}}"  data-route="posts">
          <span class="show-when-followed"><i class="fs175 fa fa-heart "></i> </span>
          <span class="hide-when-followed"><i class="fs175 fa fa-heart "></i> </span>

        
      </a>
    @else
      <a href="/users/login" class="red dpib lnk-run-on-app ajax-popup-link " rel="{{$post->id}}">
        <i class="fa fa-heart"></i> 
      </a>
    @endif

    <a href="javascript:void(0)" class="dpib fs175 ml30 pl10" onclick="alert('work in progress')">
      <i class="fa fa-comment-o"></i>
    </a>

    <a href="javascript:void(0)" class="dpib fs175 ml30 pl10" onclick="alert('work in progress')">
      <i class="fa fa-bookmark-o"></i>
    </a>

    <a href="javascript:void(0)" class="dpib fs175 ml30 pl10" onclick="alert('work in progress')">
      <i class="fa fa-share"></i>
    </a>


</div>
</div>
</div>


