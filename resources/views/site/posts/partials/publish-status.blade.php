@if($post->state == 'draft')
  <div class="alert alert-info mt10 mb10">
    This is in draft mode - only you or an admin can see this.
    <div class="pull-right">
      {{ Form::open(['url'=>route('posts.draft.to.review', [$post->id])] )}}
        <button class="btn btn-primary btn-xs">Publish for Review</button>
      {{ Form::close() }}
    </div>

  </div>
@elseif($post->state == 'publish-for-review')
  <div class="alert alert-warning mt10 mb10">
    Your post is pending review for publish.

    <div class="pull-right">
      {{ Form::open( ['url'=>route('posts.review.to.draft', [$post->id])] )}}
        <button class="btn btn-primary btn-xs">Reset as Draft</button>
      {{ Form::close() }}
    </div>
  </div>
@elseif($post->state == 'archive')
  <div class="alert alert-warning mb10 mt10">
    Your post has been archived.
  </div>
@elseif($post->state == 'reject')
  <div class="alert alert-danger mb10 mt10">
    Your post has been rejected.
  </div>
@endif
