@extends('layouts.scaffold')

@section('main')

<h1>All Invoice_items</h1>

<p>{{ link_to_route('invoice_items.create', 'Add New Invoice_item', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($invoice_items->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Desc</th>
				<th>Price</th>
				<th>Qty</th>
				<th>Total</th>
				<th>Invoice_id</th>
				<th>State</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($invoice_items as $invoice_item)
				<tr>
					<td>{{{ $invoice_item->desc }}}</td>
					<td>{{{ $invoice_item->price }}}</td>
					<td>{{{ $invoice_item->qty }}}</td>
					<td>{{{ $invoice_item->total }}}</td>
					<td>{{{ $invoice_item->invoice_id }}}</td>
					<td>{{{ $invoice_item->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('invoice_items.destroy', $invoice_item->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('invoice_items.edit', 'Edit', array($invoice_item->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no invoice_items
@endif

@stop
