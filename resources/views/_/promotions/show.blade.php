@extends('layouts.scaffold')

@section('main')

<h1>Show Promotion</h1>

<p>{{ link_to_route('promotions.index', 'Return to All promotions', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
				<th>Description</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>Type</th>
				<th>Business_id</th>
				<th>Status</th>
				<th>Amount</th>
				<th>Discounted_as</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $promotion->title }}}</td>
					<td>{{{ $promotion->description }}}</td>
					<td>{{{ $promotion->starts }}}</td>
					<td>{{{ $promotion->ends }}}</td>
					<td>{{{ $promotion->type }}}</td>
					<td>{{{ $promotion->business_id }}}</td>
					<td>{{{ $promotion->status }}}</td>
					<td>{{{ $promotion->amount }}}</td>
					<td>{{{ $promotion->discounted_as }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('promotions.destroy', $promotion->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('promotions.edit', 'Edit', array($promotion->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
