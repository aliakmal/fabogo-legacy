@extends('layouts.scaffold')

@section('main')

<h1>All Discounts</h1>

<p>{{ link_to_route('discounts.create', 'Add New Discount', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($discounts->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Caption</th>
				<th>Description</th>
				<th>Fine_print</th>
				<th>Offer_amount</th>
				<th>Original_amount</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($discounts as $discount)
				<tr>
					<td>{{{ $discount->title }}}</td>
					<td>{{{ $discount->caption }}}</td>
					<td>{{{ $discount->description }}}</td>
					<td>{{{ $discount->fine_print }}}</td>
					<td>{{{ $discount->offer_amount }}}</td>
					<td>{{{ $discount->original_amount }}}</td>
					<td>{{{ $discount->starts }}}</td>
					<td>{{{ $discount->ends }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('discounts.destroy', $discount->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('discounts.edit', 'Edit', array($discount->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no discounts
@endif

@stop
