@extends('layouts.page')
@section('content')
<div id="tf-home" class="container text-center" style="position:absolute; height:375px;;width:100%;z-index:0;background:url({{mzk_assets('assets/contact-us.jpg')}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)">

</div>
    <div class="text-center" style="height:350px;">
      <div class="content" >
        <div class="container">
        <div class="fs150 mt50 text-left pull-left " style="width:350px;">

        <h1 class="text-left fw300 notransform yellow">Talk to us</h1>
        <p class="lead text-left ">
          We'd love to chat
        </p>
      </div>
    </div>

      </div></div>
      <div class="container">
<div class="row">            
  <style type="text/css">
    label{text-align:left !important;}
  </style>

    <div class="col-md-7 col-md-offset-1 bg-white pt20 ">
      {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->encodingType('multipart/form-data')->attribute('onsubmit', "document.getElementById('submit-btn').disabled=true;") }}
        @if (Session::get('error'))
          <div class="alert alert-error alert-danger">
            {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
          </div>
        @endif
        @if (Session::get('notice'))
          <div class="alert alert-success">
            {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
          </div>
        @endif
        {{ BootForm::text('Full name*', 'name')->placeholder('Full Name')->required() }}
        {{ BootForm::text('Phone*', 'phone')->placeholder('Phone Number')->required() }}
        {{ BootForm::email('Email*', 'email')->placeholder('Email address')->required() }}
        {{ BootForm::textarea('Message*', 'message')->placeholder('Talk to us')->required() }}
        <div class="form-group">
          <label for="message" class="col-lg-3 control-label"> </label>
          <div class="col-lg-8">
            {{ Form::submit('Send', array('id'=>'submit-btn', 'class' => 'btn  btn-turquoise')) }}
          </div>
        </div>

      {{ BootForm::close() }}
    </div>
    <div class="col-md-3 ">
      <div class="mb25">
        <p class="mb10">
          <a href="/add-your-business" class="p10 dpb text-left" style="background-color:#dadada;">Add Business</a>
        </p>        
        <p class="mb10">
        Own a Salon or Spa? Get in touch with us.
        </p>
      </div>
      <div class="mb25">
        <p class="mb10">
          <a href="/jobs"  class="p10 dpb text-left" style="background-color:#dadada;">Work with us</a>
        </p>
        <p>
        We're hiring! Check out all open positions.
      </p>
      </div>
      <div class="hidden">
            <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
            <address>
                <strong>Twitter, Inc.</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                <abbr title="Phone">
                    P:</abbr>
                (123) 456-7890
            </address>
            <address>
                <strong>Full Name</strong><br>
                <a href="mailto:#">first.last@example.com</a>
            </address>
        </div>
    </div>
</div>
</div>
@stop
