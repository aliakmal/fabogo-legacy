@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-12">
  <div class="pull-right">
    <p></p>
<p>{{ link_to_route('admin.virtual_numbers.create', 'Add New Virtual number', null, array('class' => 'btn btn-sm btn-success')) }}</p>

  </div>
  <h1>All Virtual numbers</h1>
</p>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control mr5 col-md-6', 'placeholder'=>'Search')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>

@if ($virtual_numbers->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Body</th>
				<th>Current?</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($virtual_numbers as $virtual_number)
      <?php $has_current_business = $virtual_number->current_business();?>
				<tr>
          <td>{{{ $virtual_number->id }}}</td>
					<td>{{{ $virtual_number->body }}}</td>
					<td>{{ $has_current_business!=false ? link_to_route('admin.businesses.show', $virtual_number->current_business_name(), [$virtual_number->current_business()->id]):'<span class="label label-default">none</span>' }}</td>
          <td>
            @if($has_current_business==false)
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Are you sure?\');', 'method' => 'DELETE', 'route' => array('admin.virtual_numbers.destroy', $virtual_number->id))) }}
                {{ Form::submit('Deactivate', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
            @endif
            {{ link_to_route('admin.virtual_numbers.edit', 'Edit', array($virtual_number->id), array('class' => 'btn btn-xs btn-info')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $virtual_numbers->render() }}
  <div class="pull-right">
    {{ count($virtual_numbers) }} / {{ $virtual_numbers->total() }} entries
  </div></div>
</div>


@else
	There are no virtual_numbers
@endif
</div></div>

@stop
