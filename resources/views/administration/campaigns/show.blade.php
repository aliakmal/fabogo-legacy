@extends('layouts.admin-crm')
@section('content')

<h2>Campaign #{{{ $campaign->id }}}: {{{ $campaign->title }}}</h2>
<p class="lead">{{ $campaign->caption }}</p>
<b>{{$campaign->starts}} to {{$campaign->ends}}</b>
<hr />
<!--add adsets-->
<div class="well adset-form-holder">
  <p class="lead">Add an existing Ad creative here</p>
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.ad_sets.store')) }}
  <div class="row">
    <div class="col-md-10">
    {{ BootForm::select('Category', 'category_id', Category::all()->lists('name', 'id')->all())->addClass('select-category') }}
    {{ BootForm::select('Business Zone', 'business_zone_id', Business_zone::all()->lists('name', 'id')->all())->addClass('select-business-zone') }}
    {{ BootForm::select('Slot', 'slot', [])->addClass('select-slot') }}
    {{ BootForm::hidden('campaign_id', '')->value($campaign->id) }}

    {{ BootForm::token() }}
    </div>
    <div class="col-md-2">
      {{ Form::submit('Add', array('class' => 'btn btn btn-primary')) }}
    </div>
  </div>
  {{ BootForm::close() }}
</div>
<div id="adset-holder">
  <table class="table ">
    <thead>
      <tr>
        <th></th>
        <th>Creative(s)</th>
      </tr>
    </thead>
    <tbody>
  @foreach($campaign->adsets as $adset)
    @include('administration.campaigns.partials.adset')
  @endforeach
  </tbody>
  </table>
</div>
<p>{{ link_to_route('admin.campaigns.index', 'Return to All campaigns', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<!-- Create Ad
- click on allocate ad button to open popup with a select box to select sets and slots
- change sets to see available slots for the start and end dates
Select Set + slot
Set start and end dates
Activate

once activated start date can never ever be changed ever

===update===
create ad
select set
edit existing ad content
edit end date -->

<script type="text/javascript">
$(function(){

  $('.btn-add-creative').click(function(){
    adsetId = $(this).data('adsetID');
    bootbox.dialog({
                title: "Add a Creative to this Ad Set.",
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal"> ' +
                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="name">Title</label> ' +
                    '<div class="col-md-4"> ' +
                    '<input id="title" name="title" type="text" placeholder="Ad Title" class="form-control input-md"> ' +
                    '<span class="help-block">Title of the ad</span> </div> ' +
                    '</div> ' +

                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="name">Caption</label> ' +
                    '<div class="col-md-4"> ' +
                    '<input id="caption" name="caption" type="text" placeholder="Ad Caption" class="form-control input-md"> ' +
                    '<span class="help-block">Caption of the ad</span> </div> ' +
                    '</div> ' +


                    '<div class="form-group"> ' +
                    '<label class="col-md-4 control-label" for="awesomeness">How awesome is this?</label> ' +
                    '<div class="col-md-4"> <div class="radio"> <label for="awesomeness-0"> ' +
                    '<input type="radio" name="awesomeness" id="awesomeness-0" value="Really awesome" checked="checked"> ' +
                    'Really awesome </label> ' +
                    '</div><div class="radio"> <label for="awesomeness-1"> ' +
                    '<input type="radio" name="awesomeness" id="awesomeness-1" value="Super awesome"> Super awesome </label> ' +
                    '</div> ' +
                    '</div> </div>' +
                    '</form> </div>  </div>',
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            Example.show("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        );    
  });
  
  function updateAvailableSlots(select_category, select_business_zone, select_slot){
    $.ajax(
            {
              url:'/crm/campaigns/{{$campaign->id}}/get-available-slots', 
              data:{ category_id: $(select_category).val(),
                     business_zone_id: $(select_business_zone).val(),
                     }
            })
      .done(function(data){
        var options = $(select_slot);
        options.html('');

        for(i in data){
          options.append($("<option />").val(i).text(data[i]));
        }
      });
  }

  $('body').on('change', '.select-category, .select-business-zone', function(){
    holder = $(this).parents('.adset-form-holder').first();
    if($(this).hasClass('select-category')){
      select_category = $(holder).find('.select-category').first();
      select_business_zone = $(holder).find('.select-business-zone').first();
    }else{
      select_category = $(holder).find('.select-category').first();
      select_business_zone = $(holder).find('.select-business-zone').first();
    }
    select_slot = $(holder).find('.select-slot');
    updateAvailableSlots(select_category, select_business_zone, select_slot);
  })

  $('body').on('change', '.select-ad-zone', function(){
    $.ajax(
            {
              url:'/crm/campaigns/'+$(this).data('campaign')+'/get-available-slots', 
              data:{ad_zone_id:$(this).val(), ad_set_id:$(this).data('adset')}
            })
      .done(function(data){
        var options = $(".select-ad-slot");
        options.html('');

        for(i in data){
          options.append($("<option />").val(i).text(data[i]));
        }
      });
  })
  $('.btn-allocate').click(function(){
    $.ajax({url:'/crm/ad_sets/'+$(this).attr('rel')+'/edit'}).done(function(data){
      bootbox.dialog({
        title: "Edit Ad Set here",
        message: data,
        buttons: {
          success: {
            label: "Save",
            className: "btn-success",
            callback: function () {
              frm = $('#adset-form form').first();
              frm.submit();
              return false;
            }
          }
        }
      });
    });
  });
});
</script>

@stop
