@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-12">
  <div class="pull-right">
    <p></p>
    {{ link_to_route('admin.campaigns.create', 'Add New Campaign', null, array('class' => 'btn btn-sm btn-success')) }}
  </div>
  <h1>All Campaigns</h1>
</p>

<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control mr5 col-md-6', 'placeholder'=>'Search')) }}
    {{ Form::select('merchant_id', ([''=>'Merchant?'] + Merchant::orderby('name', 'asc')->lists('name', 'id')->all()), Input::get('merchant_id'), array('class'=>'form-control mr5 ', 'style'=>'width:200px;')) }}
    <div class="checkbox mr5"><label>{{ Form::checkbox('active', 'active', Input::get('active')) }} Active Only?</label></div>
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($campaigns->count())
	<table class="table table-striped">
		<thead>
			<tr>
        <th>ID</th>
				<th>Title</th>
				<th>Caption</th>
				<th>Starts</th>
				<th>Ends</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($campaigns as $campaign)
				<tr>
          <td>{{{ $campaign->id }}}</td>
					<td>{{{ $campaign->title }}}</td>
					<td>{{{ $campaign->caption }}}</td>
					<td>{{{ $campaign->starts }}}</td>
					<td>{{{ $campaign->ends }}}</td>
          <td>
              {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm(\'Are you sure\')', 'method' => 'DELETE', 'route' => array('admin.campaigns.destroy', $campaign->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-xs btn-danger')) }}
              {{ Form::close() }}
              {{ link_to_route('admin.campaigns.show', 'View', array($campaign->id), array('class' => 'btn btn-xs btn-success')) }}
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>

  <div class="row">
    <div class="col-md-12">
  {{ $campaigns->render() }}
  <div class="pull-right">
    {{ count($campaigns) }} / {{ $campaigns->total() }} entries
  </div></div>
</div>


@else
	There are no campaigns
@endif
</div></div>

@stop
