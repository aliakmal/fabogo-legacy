@extends('layouts.admin-content')
@section('content')
<p class="pull-right">
  {{ link_to(route('admin.businesses.meta.update', [$business->id]), 'Update Cache', array('class'=>'btn btn-sm btn-warning')) }}
  {{ link_to_route('admin.businesses.index', 'Return to All businesses', null, array('class'=>'btn btn-sm btn-primary')) }}
</p>
<h1>{{ $business->id }} - {{ $business->name }}</h1>


<div role="tabpanel">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
    <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos Etc</a></li>
    <!-- <li role="presentation"><a href="#dpps" aria-controls="dpps" role="tab" data-toggle="tab">DPP's</a></li>-->
    <li role="presentation"><a href="#packages" aria-controls="packages" role="tab" data-toggle="tab">Packages</a></li>
    <li role="presentation"><a href="#offers" aria-controls="offers" role="tab" data-toggle="tab">Offers</a></li>
    <li role="presentation"><a href="#items" aria-controls="items" role="tab" data-toggle="tab">Items</a></li>
    <li role="presentation"><a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">Comments</a></li>
    <li role="presentation"><a href="#rating-cheat" aria-controls="rating-cheat" role="tab" data-toggle="tab">Ratings(Cheat)</a></li>
    <li role="presentation"><a href="#activities"  aria-controls="activities" role="tab" data-toggle="tab">Activities</a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane pt10 active" id="details">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Basic Information</h3>
        </div>
        <div class="panel-body">
          <dl class="dl-horizontal">
            <dt>Business ID</dt>
            <dd><b style="text-decoration:underline;">{{ $business->id }}</b></dd>
            <dt>Type</dt>
            <dd><b>{{ $business->type }}</b></dd>
            <dt>Slip ID</dt>
            <dd><b>{{ $business->ref }}</b></dd>
            <dt>Lot ID</dt>
            <dd><b>{{ $business->lot_id }}</b></dd>
            <dt>Slug</dt>
            <dd><b>{{ $business->slug }}</b></dd>
            <dt>Search Terms/Aliases</dt>
            <dd><b>{{ $business->aliases }}</b></dd>
      		  <dt>Phone</dt>
      		  <dd>{{ join(',', $business->phone) }}</dd>
      		  <dt>Email</dt>
      		  <dd>{{ join(',', $business->email) }}</dd>
            @if($business->isNonLocationable())
              <dt>Zone Based</dt>
            @else
              <dt>Zone</dt>
            @endif
            <dd>{{ $business->zone_id > 0 ? Zone::find($business->zone_id)->stringPath():'' }}</dd>

            <dt>Cost Estimate</dt>
            <dd>{{ Business::getCostOptions()[$business->cost_estimate] }}</dd>
            <dt>State</dt>
            <dd>
              <span class="label label-{{ $business->active == 'active' ?'success':'warning'}}">{{{ $business->active }}}</span>
            </dd>

            <dt>Chain?</dt>
            <dd>{{ count($business->chain) > 0 ? '<a href="'.route('admin.groups.show', $business->chain_id ).'">'.$business->chain->name.'</a>' : 'None' }}</dd>
      		  <dt>Website</dt>
      		  <dd>{{ $business->website }}</dd>
      		  <dt>Facebook</dt>
      		  <dd>{{ $business->facebook }}</dd>
      		  <dt>Twitter</dt>
      		  <dd>{{ $business->twitter }}</dd>
      		  <dt>Google Plus</dt>
      		  <dd>{{ $business->google }}</dd>
      		  <dt>Categories</dt>
      		  <dd>
            @foreach($business->categories as $category)
              <span class="label label-info">
                {{ $category->name }}
              </span>&nbsp; 
            @endforeach

      		  </dd>
            <dt>Last Updated</dt>
            <dd>{{{ Date::parse($business->updated_at)->ago() }}}</dd>

          <dt>No. Reviews/Ratings</dt><dd>{{ $business->accumulatedReviewsCount() }}</dd>
          <dt>Avg. rating</dt><dd> {{ $business->average_rating}}</dd>


      		</dl>
        </div>
        <div class="panel-footer">
          {{ link_to_action('Administration\\BusinessesController@getBasic', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
          <a title="View Front End"  href="/{{ MazkaraHelper::getLocale().'/'.$business->slug}}" class="btn btn-success">
            <i class="fa fa-eye"></i>
          </a>
          @if(Auth::user()->hasRole('admin'))
            <a href="{{ route('admin.businesses.resluggify', $business->id) }}" class="btn btn-danger" onclick="return confirm('This will change the slug - the old slug cannot be accessed. You have been warned!')">Resluggify?</a>
          @endif
        </div>  
      </div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Services</h3>
  </div>
  <div class="panel-body">
    <table class="table">
      @foreach($business->services as $service)
        <tr>
          <td>
          {{ $service->stringPath() }}
        </td>
        <td>
          {{ $service->getStartingPrice(MazkaraHelper::getCitySlugFromID($business->city_id)) }}
        </td>
        </tr>
      @endforeach
  	</table>


    @if(count($business->chain) > 0)
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-title">Chain Outlet</div>
        </div>
        <div class="panel-body">
          {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.businesses.copy.services'))
                                          ->encodingType('multipart/form-data') }}
            <div class="row">
              <div class="col-md-8">            {{ BootForm::select('Copy Menu From', 'source_business', 
                                $business->chain->businesses()
                                         ->where('businesses.id', '<>', $business->id)
                                         ->get()->lists('showname', 'id')->all()) }}
            {{ Form::hidden('target_business', $business->id)}}
          </div>
              <div class="col-md-4">
            {{ BootForm::token() }}
            <button type="submit" class="btn btn-primary">COPY</button>

              </div>
            </div>

          {{ BootForm::close() }}
        </div>
      </div>
    @endif


  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getServices', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>

  {{ Form::open(array('url' => route('admin.businesses.post.brands', [$business->id]),'files'=>true, 'class' => 'form-horizontal', 'method'=>'POST')) }}


  <div class="panel panel-default">
    <div class="panel-heading">
      <div class="panel-title">Brands</div>
    </div>
    <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <ul class="list-unstyled row">
              <?php $brands = $business->brands()->orderby('name', 'asc')->get()->lists('id','id')->all();?>
              @foreach(App\Models\Brand::orderby('name', 'asc')->get() as $brand)
                <li class="col-md-2">
                  {{ Form::checkbox('brands[]', $brand->id, in_array($brand->id, $brands) ) }}&nbsp;{{ $brand->name }}
                </li>
              @endforeach
            </ul>

      </div>
          <div class="col-md-4">

          </div>
        </div>

    </div>
    <div class="panel-footer">
      <button type="submit" class="btn btn-primary">SAVE</button>
    </div>    
  </div>
  {{ Form::close() }}



<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Highlights</h3>
  </div>
  <div class="panel-body">
  	<ul>
      @foreach($business->highlights as $highlight)
        <li>
          {{ $highlight->name }}
        </li>
      @endforeach
  	</ul>
  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getHighlights', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>

<!-- <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Rating</h3>
  </div>
  <div class="panel-body">
    <dl class="dl-horizontal">
      <dt>Total Votes</dt>
      <dd id="total-votes-holder">{{ $business->accumulatedReviewsCount() }}</b></dd>
      <dt>Average Rating</dt>
      <dd id="average-rating-holder">{{ $business->rating_average }}</b></dd>
    </dl>
  </div>
  <div class="panel-footer">
    <?php 
    $os = [];
    for($i=1;$i<=5;$i+=0.5){
      $os[(string)$i] = $i;
    }
    ?>
    <div class="row">
      <div class="col-md-6">{{ Form::select('cheat',  $os, '', ['id'=>'cheat-rating', 'class'=>'form-control'])}}</div>
      <div class="col-md-6">
        <a href="javascript:void(0)" class="btn btn-danger" id="lnk-cheat-rate">CHEAT RATE</a>

      </div>
    </div>
    


  </div>  
</div>

-->
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Timings</h3>
  </div>
  <div class="panel-body">
    @foreach($business->timings as $timing)
    	<div class="row">
    		<div class="col-md-6">
		    	{{$timing->daysOfWeek}}
		    </div>
    		<div class="col-md-6">
		    	{{$timing->open}} to
		    	{{$timing->close}}
		    </div>
	    </div>

    @endforeach
  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getTimings', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>


@if($business->isNonLocationable())
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Locations Served</h3>
  </div>
  <div class="panel-body">
    <ul>
      @foreach($business->zones as $zone)
        <li>{{ $zone->name }}</li>
      @endforeach
    </ul>
  </div>
</div>

@else


<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Location</h3>
  </div>
  <div class="panel-body">
    <dl class="dl-horizontal">
		  <dt>Address</dt>
		  <dd>{{ $business->landmark }}</dd>
		  <dt>Geolocation latitude</dt>
		  <dd>{{ $business->geolocation_latitude }}</dd>
		  <dt>Geolocation longitude</dt>
		  <dd>{{ $business->geolocation_longitude }}</dd>
		  <dt>Geolocation address</dt>
		  <dd>{{ $business->geolocation_address }}</dd>
		  <dt>Geolocation city</dt>
		  <dd>{{ $business->geolocation_city }}</dd>
		  <dt>Geolocation state</dt>
		  <dd>{{ $business->geolocation_state }}</dd>
		  <dt>Geolocation country</dt>
		  <dd>{{ $business->geolocation_country }}</dd>
		</dl>



  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getLocation', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
  </div>  
</div>

@endif

    </div>
    <div role="tabpanel" class="tab-pane pt10" id="photos">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Photos</h3>
  </div>
  <div class="panel-body">
    @foreach($business->photos as $one_photo)
      <a href="{{ $one_photo->image->url() }}" data-toggle="lightbox"><img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" /></a>
    @endforeach
  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getPhotos', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Cover Photo</h3>
  </div>
  <div class="panel-body">
    @if($business->hasThumbnail() || $business->hasCover() || $business->useStockImage())
      <img src="{{ $business->getCoverUrl('medium') }}" class="img-thumbnail " width="200" />
      @if($business->useStockImage())
        <div class="alert alert-info">THIS IS A STOCK PHOTO</div>
      @endif
    
    @else
      No Cover selected
    @endif

  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getPhotos', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}

  </div>  
</div>


<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Rate Cards</h3>
  </div>
  <div class="panel-body">
    @foreach($business->rateCards as $one_card)
      <a href="{{ $one_card->image->url('large') }}" data-toggle="lightbox"><img src="{{ $one_card->image->url('thumbnail') }}"  class="img-thumbnail" /></a>
    @endforeach
  </div>
  <div class="panel-footer">
    {{ link_to_action('Administration\\BusinessesController@getRateCards', 'Edit', array($business->id), array('class' => 'btn btn-primary')) }}
  </div>  
</div>

    </div>

    <div rolw="tabpanel" class="tab-pane pt10" id="packages" >

      <table class="table">
      @foreach($business->packages as $package)
        <tr>
          <td>
            @if(count($package->photo) > 0)
              <a href="{{ $package->photo->image->url('watermarked') }}"><img src="{{ $package->photo->image->url('thumbnail') }}" class="img-thumbnail" /></a>
            @endif

          </td>
          <td>{{ $package->title }}
          @if(isset($package->photo->id))
          <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $package->photo->id, 90) ) }}" class="btn btn-info btn-xs">
            <i class="fa fa-rotate-left"></i>
          </a>

          <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $package->photo->id, -90) ) }}" class="btn btn-info btn-xs">
            <i class="fa fa-rotate-right"></i>
          </a>
          @endif

          </td>
          <td>
            {{ join(', ', ($package->services()->lists('services.name')->all()))}}
          </td>
          <td>
            {{ Form::text("orderables[$package->id]", $package->sort, array('class'=>'orderables form-control', 'rel'=>$package->id) ) }}


          </td>
          <td>

            {{ Form::open(array('style' => 'display: inline-block;', 
                                'method' => 'DELETE', 'route' => array('admin.special_packages.destroy', $package->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger btn-sm')) }}
            {{ Form::close() }}
          </td>
        </tr>

      @endforeach        
      </table>
      <div class="well">
        <a href="javascript:void(0)" data-loading-text="Loading..." class="btn btn-warning" id="btn-submit-sort-order">Save Sort Order</a>
      </div>

  {{ Form::open(array('url' => route('admin.special_packages.bulk'), 'files'=>true, 'class' => 'form-horizontal', 'method'=>'POST')) }}

    <div class="panel panel-default">
        <div class="panel-heading">Add packages(s) - Bulk</div>
        <div class="panel-body">
            {{ Form::hidden('business_id', $business->id) }}

            {{ Form::file('packages[]', array( 'accept'=>"image/*", 'multiple'=>'true')) }}
                
        </div>
        <div class="panel-footer">
          {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
        </div>
    </div>
    {{ Form::close() }}



    </div>
    <div role="tabpanel" class="tab-pane pt10" id="offers">
      <!-- include('administration.businesses.partials.offers', ['business'=>$business]) -->
    </div>
    <div role="tabpanel" class="tab-pane pt10" id="items">
      <!-- include('administration.businesses.partials.items', ['business'=>$business]) -->
    </div>
    <div role="tabpanel" class="tab-pane pt10" id="comments">
@include('administration.businesses.partials.comments', [ 'commentable_type'=>'Business', 
                                                          'comments'=>$business->comments,
                                                          'commentable_id'=>$business->id])


    </div>
    <div role="tabpanel" class="tab-pane pt10" id="rating-cheat">
      @include('administration.businesses.partials.rate-cheat', ['business'=>$business])
    </div>
    <div role="tabpanel"  class="tab-pane pt10" id="activities" >

    <div id="activities-holder" class="activity-feed">

      @foreach ($activities as $activity)
      <div class="feed-item">
        <div class="date">{{{ Date::parse($activity->updated_at)->ago() }}}</div>
        @include('elements.activities.business.'.$activity->verb, array('activity'=>$activity))
      </div>

      @endforeach
    </div>

    </div>

  </div>




<script type="text/javascript">
$(function(){
 $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) { event.preventDefault(); $(this).ekkoLightbox(); }); 
 $('#lnk-cheat-rate').click(function(){
    dt = { 
            business_id:{{$business->id}}, 
            rating:$('#cheat-rating').val() 
          };
    $.ajax({
      url:'/content/businesses/rate/cheat', 
      type:'GET',
      data:dt
    }).done(function(data){
      $('#total-votes-holder').html(data['count']);
      $('#average-rating-holder').html(data['average']);
    });
 });

  $('#btn-submit-sort-order').click(function(){
    orderables = {};
    $("input.orderables").each(function () {
      orderables[$(this).attr('rel')] = $(this).val();
    });
    var $btn = $(this).button('loading');

    $.ajax({
      url:'/content/special_packages/sort', 
      type:'POST',
      data:{orderables:orderables}
    }).done(function(data){
      window.location.reload();
    });
  });

});
</script>
@stop