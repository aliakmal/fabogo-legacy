<?php
// $tag_group = \DB::table('tagging_tag_groups')->select('id')
//                             ->where('slug', '=', 'photo')->first()
// 
// $taggables = \DB::table('tagging_tags')->select('name')->where('tag_group_id', '=', $tag_group->id)->lists('name','name');

?>
{{ Form::model($business, array('class' => 'form-horizontal', 
                                'method' => 'POST', 'files'=>true, 
                                'action' => array('Administration\\BusinessesController@postPhotos', 
                                                  $business->id))) }}
  <div class="panel panel-default">
    <div class="panel-heading">Edit Image(s)</div>
      <div class="panel-body">
        <div class="form-group">
          <div class="row">
            <div class="col-sm-10 ">
            @foreach($business->photos as $one_photo)
              <div class="content col-sm-3">
                <div class="panel panel-default text-center">
                  <div class="panel-body">
                    <a href="{{ $one_photo->image->url() }}" class="lightbox">
                      <img src="{{ $one_photo->image->url('thumbnail') }}" class="img-thumbnail" />
                    </a>
                    <span class="input-group">
                      <span class="input-group-addon">SORT</span>
                      {{ Form::text("orderables[$one_photo->id]", $one_photo->sort, array('class'=>'form-control') ) }}
                    </span>
                    <span class="input-group">
                      <span class="input-group-addon">COVER?</span>
                      <span class="form-control">
                        {{ Form::radio("is_cover", $one_photo->id, $one_photo->isCover()) }}
                      </span>
                    </span>
                    @if(!$one_photo->isCover())
                      <span class="input-group">
                        <span class="input-group-addon">SERVICE?</span>
                        <span class="form-control">
                          {{ Form::checkbox("is_service[]", $one_photo->id, $one_photo->isService()) }}
                        </span>
                      </span>
                    @endif
            <div class="input-group">
               @include('administration.tags.machine', [ 'obj'=>$one_photo,
                                                        'taggable_type'=>'Photo',
                                                        'taggable_id'=>$one_photo->id]) 

            </div>

                  </div>
                  <div class="panel-footer">
                    {{ Form::checkbox("deletablePhotos[]", $one_photo->id, false, array('style'=>'display:none;', 'class'=>'deletable') ) }}
                    <a href="javascript:void(0)" class="mr5 delete-existing btn btn-xs btn-danger">
                      <i class="fa fa-times"></i>
                    </a>
                    <a href="{{ route('admin.businesses.getCrop.photo', [$business->id, $one_photo->id]) }}" class="mr5 btn btn-xs btn-warning">
                      <i class="fa fa-cut"></i>
                    </a>

                    <a href="{{ route('admin.businesses.show.photo', [$business->id, $one_photo->id]) }}" class="mr5 btn btn-xs btn-warning">
                      <i class="fa fa-crop"></i>
                    </a>
                    <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $one_photo->id, 90) ) }}" class="mr5 btn btn-info btn-xs">
                      <i class="fa fa-rotate-left"></i>
                    </a>
                    <a href="{{ route('admin.businesses.rotate.photo', array($business->id, $one_photo->id, -90) ) }}" class=" btn btn-info btn-xs">
                      <i class="fa fa-rotate-right"></i>
                    </a>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>

    <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}

@include('administration.tags.machine-code')

{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 
                                'files'=>true, 
                                'action' => array('Administration\\BusinessesController@postPhotos', 
                                                  $business->id))) }}
  <div class="panel panel-default">
    <div class="panel-heading">Add Image(s) - Bulk</div>
    <div class="panel-body">
      {{ Form::file('images[]', array( 'accept'=>"image/*", 'id'=>'input-bulk-image-upload', 'multiple'=>'true')) }}
    </div>
    <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}

{{ Form::model($business, array('class' => 'form-horizontal ', 'method' => 'POST', 
                                'files'=>true, 
                                'action' => array('Administration\\BusinessesController@postPhotos', 
                                                  $business->id))) }}
  <div class="panel panel-default">
    <div class="panel-heading">Add Image(s) - one at a time, camera friendly</div>
    <div class="panel-body">
      <div class="col-sm-10">
        <a style="margin:10px 0px;" href="javascript:void(0)" class="btn btn-default btn-sm btn-info" id="lnk-add-images">Add Image(s)</a>
        <div class="well" id="images-here">{{ Form::file('images[]', array( 'accept'=>"image/*", 'capture'=>'camera')) }}</div>
      </div>
    </div>
    <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}

{{ Form::model($business, array('class' => 'form-horizontal', 'method' => 'POST', 
                                'action' => array('Administration\\BusinessesController@postStockPhoto', 
                                $business->id))) }}
  <div class="panel panel-default">
    <div class="panel-heading">Use Stock Image for Photos</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-2">
          No Stock Image
          {{ Form::radio('has_stock_cover_image', 0, ($business->has_stock_cover_image == 0)) }}
        </div>
        @foreach(Business::getStockPhotos() as $opt)
          <div class="col-md-2">
            <img src="{{ mzk_assets('assets/stock/'.$opt.'.jpg') }}" class="img-thumbnail" height="100" />
            {{ Form::radio('has_stock_cover_image', $opt, ($business->has_stock_cover_image == $opt)) }}
            {{$opt}}
          </div>
        @endforeach
      </div>
    </div>
    <div class="panel-footer">
      {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
    </div>
  </div>
{{ Form::close() }}



<div id="image_holder" style="display:none"><p>{{ Form::file('images[]', array('accept'=>"image/*", 'capture'=>'camera')) }}</p></div>
  <div class="well">
    {{ Form::model($business, array('class' => 'form-horizontal', 'onsubmit'=>"return confirm('Are you sure?');", 'method' => 'POST', 'files'=>true, 'action' => array('Administration\\BusinessesController@fixPhotos', $business->id))) }}
      {{ Form::submit('Fix all Photos', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}
  </div>
<script>

$(function(){
  $(document).on('click', '.delete-existing', function(){
    $(this).parents('.content').first().hide();
    $(this).parents('.content').first().find('.deletable').prop('checked', true);
  });

$("#input-bu__lk-image-upload").fileinput({
    uploadUrl: "{{ action('Administration\\BusinessesController@postBulkPhotos', $business->id) }}",
    allowedFileExtensions: ["jpg", "png", "gif"],
    // minImageWidth: 1200,
    // minImageHeight: 900,
    // uploadAsync: false,
    // overwriteInitial: false,
    // progressall: function (e, data) {
    //   var progress = parseInt(data.loaded / data.total * 100, 10);
    //   $('#progress .progress-bar').css('width', progress + '%');
    // },
    done: function (e, data) {
      console.log('upload done', data);
    }    
});


  $('#lnk-add-images').click(function(){
    $('#images-here').prepend($('#image_holder').html());
  });
});

</script>

