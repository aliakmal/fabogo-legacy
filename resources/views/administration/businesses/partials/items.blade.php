<div class="row">
  <div class="col-md-12">
    @if(count($business->chain) > 0)
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-title">Chain Outlet</div>
        </div>
        <div class="panel-body">
          {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.businesses.copy.menu'))
                                          ->encodingType('multipart/form-data') }}
            <div class="row">
              <div class="col-md-8">            {{ BootForm::select('Copy Menu From', 'source_business', 
                                $business->chain->businesses()
                                         ->where('businesses.id', '<>', $business->id)
                                         ->get()->lists('showname', 'id')->all()) }}
            {{ Form::hidden('target_business', $business->id)}}
          </div>
              <div class="col-md-4">
            {{ BootForm::token() }}
            <button type="submit" class="btn btn-primary">COPY</button>

              </div>
            </div>

          {{ BootForm::close() }}
        </div>
      </div>
    @endif

    <div class="panel panel-default " data_-spy="affix" data-offset_-top="177" style="top:0px;z-index:999; background-color:white;">
      <div class="panel-body">
        @include('administration.businesses.partials.form-items')
      </div>
    </div>
    <div class="panel-footer panel-warning">
      <div class="">
        {{ link_to_action('Administration\\BusinessesController@getServices', 'Jump To Services', array($business->id), array('class' => 'btn btn-xs btn-primary')) }}
        <span class="btn-group">
          @if($business->has_sample_menu == 0)
            <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}" class="btn btn-xs btn-success">ACTUAL MENU</a>
            <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}" class="btn btn-xs btn-default">CLICK TO CHANGE</a>
          @else
            <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}" class="btn btn-xs btn-danger">SAMPLE MENU</a>
            <a href="{{ route('admin.businesses.toggle.sample.menu', $business->id) }}" class="btn btn-xs btn-default">CLICK TO CHANGE</a>
          @endif
        </span>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <form method="POST" action="{{ route('admin.menu.groups.bulk.allocate') }}">
      <div id="items-box-holder" style="padding:10px; width:100%;  ">
        @foreach(App\Models\Menu_group::optionables(['business_id'=>$business->id]) as $ix=>$vx)
          <h4>
            @if($ix!='0')
              <a href="javascript:void(0)"  class="item-inline-editable" id="name" data-type="text" data-pk="{{$ix}}" data-url="{{ route('admin.menu.groups.update') }}" data-title="Enter name">
                {{$vx}}
              </a>
            @else
              {{$vx}}
            @endif
          </h4>
          <table class="table">
            <thead>
              <tr>
                <th><small>SERVICES</small></th>
                <th><small>NAME</small></th>
                <th><small>DESCRIPTION</small></th>
                <th><small>COST</small></th>
                <th><small>COST TYPE</small></th>
                <th><small>DURATION</small></th>
                <th><small>MTS?HRS?</small></th>
                <th></th>
              </tr>
            </thead>
            <tbody data-menu_group="{{$ix}}"  id="service-item-list-for-group-{{$ix}}" class="droppable"  tabindex="{{ $service['id'] }}">
              @foreach($business->serviceItems()->byMenuGroup($ix)->get() as $item)
                @include('administration.businesses.partials.item', ['item'=>$item])
              @endforeach
            </tbody>
          </table>
        @endforeach
      </div>
      <div class="well">
        <div class="row">
          <div class="col-md-9">
            Bulk Allocate Header
            {{ Form::select('allocate_menu_group_id', App\Models\Menu_group::optionables(['business_id'=>$business->id]),
                             '', array('class'=>'form-control', 'id'=>'allocate_menu_group_id' )) }}
          </div>
          <div class="col-md-3">
            <button type="submit" class="btn btn-default btn-primay">UPDATE</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@include('administration.businesses.partials.item_handlebars')

<script type="text/javascript">

$(function(){
  function IsNumeric(val) {
    return Number(parseFloat(val))==val;
  }

  $('#lnk-add-new-menu-group').click(function(){
    bootbox.prompt("Enter name of new Group", function(result) {                
      if (result != null) {                                             
        $.ajax({
            type        : 'POST',
            url         : '/content/menu_groups',
            data        : { name:result, business_id:{{$business->id}} },
            dataType    : 'json',
            encode      : true
        }).done(function(data) {

          htm = '<h4>';
          htm = htm + '<a href="javascript:void(0)"  class="item-inline-editable" id="name" data-type="text" data-pk="'+data['id']+'" data-url="/content/menu_groups/updatable" data-title="Enter name">';
          htm = htm + data['name'];
          
          htm = htm + '</a>';

          htm = htm + '</h4>';
          htm = htm + '<table class="table">';
          htm = htm + '<thead>';
          htm = htm + '<tr>';
          htm = htm + '<th><small>SERVICES</small></th><th><small>NAME</small></th><th><small>DESCRIPTION</small></th><th><small>COST</small></th>';
          htm = htm + '<th><small>COST TYPE</small></th><th><small>DURATION</small></th><th><small>MTS?HRS?</small></th><th></th>';
          htm = htm + '</tr>';
          htm = htm + '<tbody data-menu_group="'+data['id']+'" class="droppable" id="service-item-list-for-group-'+data['id']+'" >';
          htm = htm + '</thead><tbody></tbody></table>';
          $('#items-box-holder').append(htm);
          
          item_grouping = $('form#item-form').find('select[name=menu_group_id]');
          bulk_grouping = $('#allocate_menu_group_id');

          $(item_grouping).html('');
          $(bulk_grouping).html('');

          pre_html = '<option value="select">Select a Header</option>';
          html = '';
          for(i in data['menu_groups']){
            html = html + '<option value="' + i + '">' + data['menu_groups'][i] + '</option>';
          }

          $(item_grouping).html(pre_html + html);
          $(bulk_grouping).html(html);

          $(item_grouping).val(data['id']);


        });

      }
    });
  })


$('#item-state-inactive-checker').change(function(){
  if(this.checked){
    $('.deactivable').val('');
    $('.deactivable').prop('disabled', true);
  }else{
    $('.deactivable').prop('disabled', false);
  }
});



  item_grouping = $('form#item-form').find('select[name=grouping]');
  item_grouping_text = $('form#item-form').find('input[name=grouping_text]');

  $(item_grouping).change(function(){
    if( ($(this).val()=='0') || ($(this).val()=='Enter New Group') ){
      $(item_grouping_text).removeAttr('disabled');
    }else{
      $(item_grouping_text).prop('disabled', true);
    }
  });

  $('#increase-box-lnk').click(function(){
    h = $('#items-box-holder').css('height');
    h = parseInt(h, 10);
    $('#items-box-holder').css('height', (h+200).toString()+'px');
  });

  $('#item-service_ids').selectpicker().change(function(){
    var itms = $("#item-service_ids option:selected").map(function() {
        return $(this).text();
    }).get();
    $('#item-form').find('input[name=name]').val(itms.join('+'));
  });

  $('.link-quick-add-service').click(function(){
    s = $('#item-service_id');
    $(s).find('option').removeAttr('selected');
    $(s).val($(this).data('service'));
    $('#item-form').find('input[name=name]').val($('#item-service_id option:selected').text()); 
    $('#item-form').find('input[name=name]').focus();
    clearAllServiceItemFields();
  });

  var source   = $("#service-item-template").html();
  var template = Handlebars.compile(source);

  $('form#item-form').submit(function(event) {
    event.preventDefault();
    var data = {
                  business_id: {{$business->id}}
               };

        inps = $('form#item-form').find('input, select, textarea');
        flds = ['name','cost','services','cost_type','menu_group_id', 'duration','duration_type','description'];
        required = ['name','services','cost_type'];

        grouping = $('form#item-form').find('select[name=grouping]');
        grouping_text = $('form#item-form').find('input[name=grouping_text]');
        data['grouping'] = (($(grouping).val()=='0') ||($(grouping).val()=='Enter New Group')) ? $(grouping_text).val() : $(grouping).val();

        for(i in inps){
          elem = inps[i];
          if($.inArray(elem.name, flds) >=0 ){
            {
              data[elem.name] = $(elem).val();
            }
          }
        }

        missing = [];
        for(d in data){
          if($.inArray(d, required) >=0 ){
            if(data[d]==''){
              missing.push(d);
            }
          }
        }

        if(data['menu_group_id']=='select'){
          missing.push('Header');
        }

        {
          if((data['cost_type']!='price-on-consultation')&&(data['cost']=='')){
            missing.push['cost type'];
          }

          if(missing.length > 0){
            alert(missing.join(',')+' are required fields');
            return;
          }

          if((data['cost_type']!='price-on-consultation')&&!IsNumeric(data['cost'])){
            alert('Please enter the cost as a number');
            return;
          }

          if(data['duration'] == ''){
            data['duration_type'] = '';
          }

        }

        data['state'] = 'active';

        $.ajax({
            type        : 'POST',
            url         : $(this).attr('action'),
            data        : data,
            dataType    : 'json',
            encode      : true
        }).done(function(data) {
          var html    = template(data);

          $('#service-item-list-for-group-' + data['menu_group_id']).append(html);
          $('form#item-form').parents('.panel').first().effect('highlight');
          $('#service-item-list-for-group-' + data['menu_group_id']).focus();
          resetEditables()
        });
        $('form#item-form input').val('');
        $('form#item-form textarea').val('');
        $('form#item-form textarea').html('');
        $('form#item-form input:first').focus();

        $('form#item-form').find('select[name=cost_type]').val('fixed');
    });

  $('#items-box-holder').on('click', '.deletable-form-btn', function(event){
    _this = $(this).parents('tr').first();
    $(_this).effect('highlight', {color:'red'}, 1000, function(){
        $(this).fadeOut('fast', function(){
            $(this).remove();
        });
    });
    event.preventDefault();
    $.ajax({
        type        : 'POST',
        url         : '/content/service_items/'+$(this).data('item'),
        data        : {_method : 'DELETE'},
        dataType    : 'json',
        encode      : true
    }).done(function(data) {
      $(_this).remove();
    });

    return false;


  });


  $(document).ready(function() { 
    $('#item-form').find('#item-service_id').change(function(){
      $('#item-form').find('input[name=name]').val($('#item-service_id option:selected').text()); 
    })
  });

  function resetEditables(){
    $('.item-inline-editable').not('a[id=cost_type], a[id=duration_type]').editable();

    $('a.item-inline-editable[id=cost_type]').editable({
      source: [ {value:'fixed', text:'Fixed Price'}, 
                {value:'onwards', text:'Onwards'},
                {value:'consultation-fee', text:'Consultation Fee'}, 
                {value:'price-on-consultation', text:'Price on Consultation'}
                ]
    });
    $('a.item-inline-editable[id=duration_type]').editable({
      source: [ {value:'minutes', text:'Minutes'}, 
                {value:'hours', text:'Hours'}, 
                {value:'days', text:'Days'}, 
                {value:'months', text:'Months'}]
    });    
  }

  resetEditables();

  function clearAllServiceItemFields(){
    $('form#item-form').find('input[name=cost]').val('');
    $('form#item-form').find('input[name=business_id]').val('');
    $('form#item-form').find('input[name=duration]').val('');
    $('form#item-form').find('select[name=cost_type]').val('fixed');
    $('form#item-form').find('select[name=duration_type]').val('minutes');
    $('form#item-form').find('textarea[name=description]').val('');
  }

})
</script>