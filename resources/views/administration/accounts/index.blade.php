@extends('layouts.admin-users')
@section('content')

<h1>All Accounts</h1>

<p>{{ link_to_route('admin.accounts.create', 'Add New Account', null, array('class' => 'btn btn-lg btn-success')) }}</p>
<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2',  'placeholder'=>'Search')) }}
    {{ Form::select('roles', ([''=>'All?'] + Role::lists('name', 'id')->all()), Input::get('roles'), array('class'=>'form-control ', 'style'=>'width:150px;')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
  <a href="{{ route('admin.accounts.export')}}" class="btn btn-info pull-right">
    Export To Excel
  </a>
  
</div>

@if ($accounts->count())
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
        <th>ID</th>
        <th colspan="2">Name</th>
				<th>Username/Email</th>
        <th>Account(s)?</th>
        <th>Roles</th>
				<th>Confirmed</th>
        <th>Joined</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($accounts as $account)
				<tr class="{{ $account->isBlocked() ? 'danger':''}}" title="{{ $account->isBlocked() ? 'BANNED':''}}">
          <td>{{{ $account->id }}}</td>
          <td>
            {{ ViewHelper::userAvatar($account, [ 'class'=>'img-circle', 'size'=>'micro', 
                                                  'style'=>'width:40px; height:40px; padding-top:11px;display:inline-block;text-align:center;']) }}
          </td>
          <td>
            {{{ $account->full_name }}}
          </td>
					<td>
            {{{ $account->username != ''?'('.$account->username.')':'' }}}{{{ $account->email }}}
          </td>
          <td>
            @if(trim($account->username)!='')
              <span class="label label-default" title="Signed up with email and password"><i class="fa fa-lock"></i></span>
            @endif
            @foreach($account->accounts as $acc)
              @if($acc->provider=='facebook')
              <span class="label label-primary"  title="Signed up with facebook"><i class="fa fa-facebook"></i></span>&nbsp;
              @endif
              @if($acc->provider=='google')
              <span class="label label-danger"  title="Signed up with google"><i class="fa fa-google"></i></span>&nbsp;
              @endif
            @endforeach
          </td>
          <td>
            {{ join(',', $account->roles()->lists('roles.name','roles.name')->all()) }}
          </td>
					<td>{{ $account->confirmed > 0 ? '<span class="label label-success">CONFIRMED</span>':'<span class="label label-danger">UNCONFIRMED</span>' }}</td>
          <td>
            {{{ Date::parse($account->created_at)->ago() }}}
          </td>
          <td>
            <span style="display:none;">
              {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.accounts.destroy', $account->id))) }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
              {{ Form::close() }}
            </span>
            {{ link_to_route('admin.accounts.edit', 'Edit', array($account->id), array('class' => 'btn btn-xs btn-info')) }}&nbsp;
            <a href="{{ route('users.profile.show', array($account->id)) }}" class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>
            @if($account->isBlocked())
            <a href="{{ route('admin.auth.users.unban', array($account->id)) }}" class="btn btn-xs btn-danger">UNBAN</a>
            @else
            <a href="{{ route('admin.auth.users.ban', array($account->id)) }}" class="btn btn-xs btn-danger">BAN</a>
            @endif
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
  <div class="row">
    <div class="col-md-12">
      {{ $accounts->render() }}
      <div class="pull-right">
        {{ count($accounts) }} / {{ $accounts->total() }} entries
      </div>
    </div>
  </div>
@else
	There are no accounts
@endif
@stop