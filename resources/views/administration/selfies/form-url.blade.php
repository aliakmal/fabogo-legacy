<?php $existing_services = [];?>
@if(isset($post))
  {{ Form::model($post, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.url.update', $post->id))) }}
  <?php $existing_services = $post->services()->lists('service_id','service_id')->all();?>
@else
  {{ Form::open(array('route' => 'admin.selfies.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif

<div class="form-group">
  {{ Form::label('title', 'Title:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('title', Input::old('title'), array('class'=>'form-control', 'placeholder'=>'Title')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('url', 'Url:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('url', Input::old('url'), array('class'=>'form-control', 'placeholder'=>'Url')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('share_url_title', 'Share URL Title:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('share_url_title', Input::old('share_url_title'), array('class'=>'form-control', 'placeholder'=>'share_url_title')) }}
  </div>
</div>
<div class="form-group">
  {{ Form::label('share_url_image', 'Share URL Image link:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::text('share_url_image', Input::old('share_url_image'), array('class'=>'form-control', 'placeholder'=>'share_url_title')) }}
  </div>
</div><div class="form-group">
  {{ Form::label('state', 'State:', array('class'=>'col-md-2 control-label')) }}
  <div class="col-sm-10">
    {{ Form::select('state', Post::getStates(), Input::old('state'), array('class'=>'form-control', 'placeholder'=>'State')) }}
  </div>
</div>



<div class="form-group">
  <label class="col-sm-2 control-label">&nbsp;</label>
  <div class="col-sm-10">
    {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
  </div>
</div>

{{ Form::close() }}

<script type="text/javascript">
$(function(){
  var $summernote = $('#post-body');
  $summernote.summernote({
    height: 500,
    fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36'],
    onInit: function(){
      $('.note-image-input').prop('name', 'files[]');
    },

    onImageUpload: function(files, editor, welEditable) {
      data = new FormData();
      data.append("file", files[0]);
      editr = editor;

      console.log(editor);
      console.log(editr);

      $.ajax({
        data: data,
        type: "POST",
        url: "/content/photos/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function(result) {
          $summernote.summernote('editor.insertImage', result.url);
        }
      });
    }    
  });

  $('#service_ids').selectpicker();
  $('#service_ids').selectpicker('val', {{ json_encode(array_values($existing_services)) }});
  $('.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yy-mm-dd", autoclose:true });

});


</script>