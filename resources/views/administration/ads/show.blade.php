@extends('layouts.admin-crm')
@section('content')
<h1>Show Ad</h1>

<p>{{ link_to_route('admin.ads.index', 'Return to All ads', null, array('class'=>'btn btn-lg btn-primary')) }}</p>


<div class="row">
  <div class="col-md-4">
  <a href="{{ $ad->url }}"><div class="native-ad">
      <div class="post-img-content">
          <img src="{{ $ad->photo->image->url('large')}}" class="img-responsive" />
          <span class="post-title"><span>{{$ad->title}}</span></span>
      </div>
      <div class="content">
          <div class="author">
              {{$ad->caption}}
          </div>
      </div>
  </div></a>
  </div>
</div>


{{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.ads.destroy', $ad->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
{{ Form::close() }}
{{ link_to_route('admin.ads.edit', 'Edit', array($ad->id), array('class' => 'btn btn-info')) }}
@stop