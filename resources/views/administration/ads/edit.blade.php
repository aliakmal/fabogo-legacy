@extends('layouts.admin-crm')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Ad</h1>

    </div>
</div>
@include('administration.ads.form')
{{ link_to_route('admin.ads.show', 'Cancel', $ad->id, array('class' => 'btn btn-lg btn-default')) }}
@stop