@if(isset($advert_campaign))
  {{ Form::model($advert_campaign, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'PATCH', 'route' => array('admin.advert_campaigns.update', $advert_campaign->id))) }}
@else
  {{ Form::open(array('route' => 'admin.advert_campaigns.store', 'files'=>true, 'class' => 'form-horizontal')) }}
@endif


  <div class="form-group">
    {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::textarea('description', isset($advert_campaign)?$advert_campaign->description:Input::old('description'), array('class'=>'form-control', 'placeholder'=>'description')) }}
    </div>
  </div>

<!--  <div class="form-group">
    {{ Form::label('invoice_number', 'Invoice no:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('invoice_number', Input::old('invoice_number'), array('class'=>'form-control', 'placeholder'=>'Invoice no:')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('total_value', 'Total value:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('total_value', Input::old('total_value'), array('class'=>'form-control', 'placeholder'=>'Total value')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('paid_value', 'Paid value:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('paid_value', Input::old('paid_value'), array('class'=>'form-control', 'placeholder'=>'Paid value')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('balance_value', 'Balance value:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('balance_value', Input::old('balance_value'), array('class'=>'form-control', 'placeholder'=>'Balance value')) }}
    </div>
  </div>-->
  <div class="form-group">
    {{ Form::label('merchant_id', 'Merchant:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('merchant_id', $merchants, isset($advert_campaign)?$advert_campaign->merchant_id:Input::old('merchant_id'), array('class'=>'form-control', 'placeholder'=>'Merchant')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('start_date', 'Starts:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('start_date', isset($advert_campaign)?$advert_campaign->start_date:Input::old('start_date'), array('class'=>'dateinput form-control', 'placeholder'=>'Starts')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('end_date', 'Ends:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('end_date', isset($advert_campaign)?$advert_campaign->end_date:Input::old('end_date'), array('class'=>'dateinput form-control', 'placeholder'=>'Ends')) }}
    </div>
  </div>
  <!--
  <div class="form-group">
    {{ Form::label('old_id', 'Old ID:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('old_id', Input::old('old_id'), array('class'=>'form-control', 'placeholder'=>'Old ID')) }}
    </div>
  </div>-->

  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
      @if(isset($lead))
        {{ link_to_route('admin.leads.show', 'Cancel', $lead->id, array('class' => 'btn btn-lg btn-default')) }}
      @endif
    </div>
  </div>
{{ Form::close() }}
<script type="text/javascript">
$(function(){
  @if(isset($advert_campaign))
    $('input.dateinput[name=start_date]').datepicker({ format: "yyyy-mm-dd", autoclose:true, endDate: '{{ $start_at }}'});
    $('input.dateinput[name=end_date]').datepicker({ format: "yyyy-mm-dd", autoclose:true, startDate: '{{ $end_at }}'});  
  @else
    $('input.dateinput').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  @endif
})
</script>