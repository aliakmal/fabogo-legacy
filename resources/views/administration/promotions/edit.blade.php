
<div class="row">
  <div class="col-md-12">
    <h1>Edit Promotion for {{ $business->name }}</h1>
{{ link_to_route('admin.businesses.show', 'Return', array($business->id), array('class' => 'btn btn-info btn-xs')) }}

    @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
        </div>
    @endif



{{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])
          ->post()
          ->action(
                    route('admin.businesses.update.promotion', array($business->id, $promotion->id))
                  )->encodingType('multipart/form-data') }}
{{ BootForm::bind($promotion)}}
   @if (Session::get('error'))
    <div class="alert alert-error alert-danger">
      {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
    </div>
  @endif
  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Title', 'title')->placeholder('Promotion Title') }}
  {{ BootForm::textarea('Description', 'description')->placeholder('Description') }}
  {{ BootForm::file('Photo', 'artwork') }}
      @if(isset($promotion))
        @if($promotion->artwork)
          <a href="{{ $promotion->artwork->url() }}" class="lightbox"><img src="{{ $promotion->artwork->image->url('thumbnail') }}" class="img-thumbnail" /></a>
          {{ Form::checkbox("deletablePhotos[]", $promotion->artwork->id, false ) }}
          Delete?

        @endif

      @endif

  {{ BootForm::date('Starts', 'starts')->placeholder('starts on')->addClass('dateinput') }}
  {{ BootForm::date('Ends', 'ends')->placeholder('ends on')->addClass('dateinput') }}
  {{ BootForm::select('Promotion Type', 'type', Promotion::$types) }}
  {{ BootForm::select('Status', 'status', Promotion::$statuses) }}
  {{ BootForm::text('Amount', 'amount')->placeholder('Amount Discounted by') }}
  {{ BootForm::select('Discounted As?', 'discounted_as', ['percent', 'amount']) }}

  {{ BootForm::token() }}
  {{ BootForm::submit('Create') }}

{{ BootForm::close() }}

  </div>
</div>
<script type="text/javascript">
$(function(){
  $('.dateinput input').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yy-mm-dd", autoclose:true });

})
</script>
