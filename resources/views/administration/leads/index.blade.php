@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="pull-right btn-group">{{ link_to_route('admin.leads.create', 'Add New lead', null, array('class' => 'btn  btn-success')) }} {{ link_to_route('crm.leads.get.import', 'Import leads', null, array('class' => 'btn  btn-default')) }}</div>
    <h1>All leads</h1>

    @include('elements.messages')

    <div class="well">
    <form method="GET" class="form-inline">
      {{ Form::text('name', isset($params['name'])?$params['name']:'', array('class'=>'form-control', 'placeholder'=>'Search') ) }}
      {{ Form::select('is_allocated', array('0'=>'Not allocated', '1'=>'Yes is allocated'), isset($params['is_allocated'])?$params['is_allocated']:'', array('class'=>'form-control', 'placeholder'=>'Is allocated?') ) }}

      {{ Form::text('interested', isset($params['interested'])?$params['interested']:'', array('class'=>'form-control', 'placeholder'=>'Interested In') ) }}
      <button class="btn btn-sm btn-primary">Filter</button>
      <a href="?" class="btn btn-sm btn-default">Reset</a>
    </form>
  </div>
    @if ($leads->count())
    	<table class="table table-striped">
    		<thead>
    			<tr>
            <th></th>
    				<th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Interested In</th>
            <th>Allocated to?</th>
            <th>Created at</th>
    				<th>&nbsp;</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach ($leads as $lead)
    				<tr>
              <td>{{ Form::checkbox('lead_ids[]', $lead->id, null, array('class'=>'chk_lead_id')) }}</td>
    					<td>{{{ $lead->name }}}</td>
              <td>{{{ $lead->phone }}}</td>
              <td>{{{ $lead->email }}}</td>
              <td>{{{ $lead->interested_in }}}</td>
              <td>
                @if($lead->businesses->count()>0)
                    {{ join(',<br/>', $lead->businesses->lists('name', 'name')->all()) }}
                @endif
              </td>
              <td>{{{ $lead->created_at->toDateString() }}}</td>
              <td>
                <div class="btn-group"> 
                  <button class="btn btn-default btn-xs dropdown-toggle" 
                          type="button" data-toggle="dropdown" 
                          aria-haspopup="true" aria-expanded="false"> 
                      <span class="fa fa-cog"></span> 
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right text-center"> 
                    <li>
                      {{ link_to_route('admin.leads.edit', 'Edit this lead', array($lead->id) ) }}
                    </li>
                    <li>
                      {{ Form::open(array('style' => 'display: inline-block;', 'onsubmit'=>'return confirm("Are you sure you want to delete this lead?")', 'method' => 'DELETE', 'route' => array('admin.leads.destroy', $lead->id))) }}
                        <button style="border:0px; background-color: transparent;"> Delete this lead</button>
                      {{ Form::close() }}
                    </li>
                  </ul>
                </div>
              </td>
    				</tr>
            
    			@endforeach
    		</tbody>
    	</table>
      {{ Form::open(array('route' => 'crm.leads.attach', 'method'=>'POST', 'class' => 'form-horizontal')) }}
        <div class="well">
          <div class="row">
            <div class="col-md-4">
              {{ Form::select('businesses[]', $businesses,'',  array('class'=>'form-control', 'multiple'=>true, 'id'=>'businesses',  'data-live-search'=>"true")) }}
              {{ Form::hidden('lead_ids', '',  array('class'=>'form-control', 'id'=>'lead_ids_list')) }}
            </div>
            <div class="col-md-3">
              <a class="btn btn-sm btn-default" id="lnk-attach">ATTACH</a>
            </div>
          </div>
        </div>
      {{ Form::close() }}
      {{ $leads->appends($params)->render() }}
    @else
    	There are no leads
    @endif
  </div>
</div>
<script type="text/javascript">
  $(function(){
    $('#lnk-attach').click(function(){
      lead_ids = $('input.chk_lead_id:checked').map(function() {
          return this.value;
        }).get();
      $('#lead_ids_list').val(lead_ids.join(','));
      $(this).parents('form').first().submit();

    });

  $('#businesses').selectpicker({
    style: 'btn-default',
    size: 8
  });


  });
</script>
@stop