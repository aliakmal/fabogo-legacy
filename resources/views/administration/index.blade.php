@extends('layouts.admin-crm')
@section('content')

<h1>Listings{{ isset($params['rich'])?'('.$params['rich'].')':'' }}</h1>

<div class="well">
  {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline', 'method' => 'GET')) }}
    {{ Form::text(  'byID', Input::get('byID'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;',
                    'placeholder'=>'By Business ID')) }}

    {{ Form::text('search', Input::get('search'), array('class'=>'form-control col-md-2', 'style'=>'width:150px;', 'placeholder'=>'Search')) }}
    
    {{ Form::select('rich', ([ 
                              'all'=>'All Listings', 
                              'rich'=>'Rich Only', 
                              'unfeatured'=>'Non Rich/Poor/Ghareeb Only', 
                              ]),
                           Input::get('featured'), array('class'=>'form-control', 'style'=>'width:150px;', 'placeholder'=>'Featured?')) }}
    {{ Form::select('sort', ([ ''=>'Sort by?', 
                              'idAsc'=>'ID Ascending', 
                              'idDesc'=>'ID Descending', 
                              'nameAsc'=>'Name Ascending', 
                              'nameDesc'=>'Name Descending', 
                              'lastUpdateAsc'=>'Last Update Ascending',
                              'lastUpdateDesc'=>'Last Update Descending',
                              ]),
                           Input::get('sort'), array('class'=>'form-control', 'style'=>'width:150px;', 'placeholder'=>'Service')) }}
    <button type="submit" class="btn btn-default">Filter</button>
  {{ Form::close() }}
</div>


@if ($businesses->count())
  <div class="list-group">
		@foreach ($businesses as $business)
      <div class="list-group-item">
        <div class="pull-right text-right">
          <small>Last Updated {{{ Date::parse($business->updated_at)->ago() }}}</small>
          <br/>
          @if($business->is_rich != 0)
            <span class="label label-success">FEATURED</span>
            <a href="{{ route('admin.rich-listings.unmark', [$business->id])}}" class="btn btn-xs btn-default">MARK AS UNFEATURED</a>
          @else
            <span class="label label-default">NOT FEATURED</span>
            <a href="{{ route('admin.rich-listings.mark', [$business->id])}}" class="btn btn-xs btn-default">MARK AS FEATURED</a>
          @endif
        </div>
        <h4 class="list-group-item-heading">
          {{{ $business->id }}} -          
          {{{ $business->name }}}
        </h4>
        <p class="list-group-item-text">
          <b>Phone:</b>{{{ join( ',', $business->phone) }}}
          <b>Email:</b>{{{ join( ',',$business->email) }}}
          <br/>
          <b>Website:</b>{{{ $business->website }}}
          <b>Zone:</b>{{{ $business->zone_cache }}}
        </p>
      </div>
		@endforeach      
  </div>

  <div class="row">
    <div class="col-md-12">
	{{ $businesses->appends($params)->render() }}
  <div class="pull-right">
    {{ count($businesses) }} / {{ $businesses->total() }} entries
  </div></div>
</div>
@else
	There are no businesses
@endif

@stop

