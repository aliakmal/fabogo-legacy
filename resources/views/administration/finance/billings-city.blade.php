@extends('layouts.admin-finance')
@section('content')
<div class="row">
  <div class="col-md-12">
  <h2>Billing Report by city for <?php $m = mzk_month_array(); echo $m[$start_month];?> {{$start_year}} to {{$m[$end_month]}}  {{$end_year}}</h2>
    <div class="well">
      {{ Form::open(array('style' => 'display: inline-block;', 'class'=>'form-inline frm-filter', 'method' => 'GET')) }}
        {{ Form::select('start_month', mzk_month_array(), $start_month, array('class'=>'form-control ' )) }}
        {{ Form::select('start_year', mzk_year_array(), $start_year, array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
        {{ Form::select('end_month', mzk_month_array(), $end_month, array('class'=>'form-control ' )) }}
        {{ Form::select('end_year', mzk_year_array(), $end_year, array('class'=>'form-control ', 'style'=>'width:150px;' )) }}
        {{ Form::checkbox('aed', 1, $aed, array('class'=>'form-control ' )) }} AED
        {{ Form::checkbox('usd', 1, $usd, array('class'=>'form-control ' )) }} USD
        {{ Form::checkbox('inr', 1, $inr, array('class'=>'form-control ' )) }} INR
        {{ Form::hidden('csv', 0, array('class'=>'csv-flag ' )) }}
        <a href="javascript:void(0)" class="btn btn-warning btn-csv">DOWNLOAD CSV</a>
        <button type="submit" class="btn btn-default">GENERATE REPORT</button>
      {{ Form::close() }}
    </div>
    <div style="width:100%;overflow:scroll;">

  <table  >
    <tr>
        <td style="border-right:1px solid #ccc;">
        <table class="table table-striped">
          <tr>
            <th>&nbsp;</th>
          </tr>
          <tr>
            <th><small>&nbsp;</small></th>
          </tr>

          @foreach($all_cities as $city)
            <tr><td>{{ str_replace(' ', '&nbsp;', $city->name) }}</td></tr>
          @endforeach
          <tr>
            <th>TOTAL</th>
          </tr>
          <!-- <tr class="hidden danger">
            <th>CREDIT</th>
          </tr> -->
          <tr class="warning">
            <th>EXPENSES</th>
          </tr>
          <!-- <tr>
            <th>FINAL&nbsp;TOTAL</th>
          </tr>-->
          <tr>
            <th>EBR</th>
          </tr>

        </table>
      </td>

      @foreach($result as $period)
        <td style="border-right:1px solid #ccc;">
        <table class="table table-striped">
          <tr>
            <th colspan="3" class="text-center">
              <b><?php $m = mzk_month_array(); echo $m[$period['month']];?>&nbsp;{{$period['year']}}</b>

            </th>
          </tr>
          <tr>
            @if($aed==1)
              <th><small>AED</small></th>
            @endif
            @if($inr==1)
              <th><small>INR</small></th>
            @endif
            @if($usd==1)
              <th><small>USD</small></th>
            @endif
          </tr>

          @foreach($all_cities as $city)
            <tr>
              @if(isset($period['cities'][$city->id]))
                @if($aed==1)
                  <td>{{ number_format(($period['cities'][$city->id]['aed'] - $period['cities'][$city->id]['credit_aed']), 2) }}</td>
                @endif
                @if($inr==1)
                  <td>{{ number_format(($period['cities'][$city->id]['inr'] - $period['cities'][$city->id]['credit_inr']), 2) }}</td>
                @endif
                @if($usd==1)
                  <td>{{ number_format(($period['cities'][$city->id]['usd'] - $period['cities'][$city->id]['credit_usd']), 2) }}</td>
                @endif
              @else
                @if($aed==1)
                  <td>0</td>
                @endif
                @if($inr==1)
                  <td>0</td>
                @endif
                @if($usd==1)
                  <td>0</td>
                @endif
              @endif
            </tr>
          @endforeach
          <tr>
            @if($aed==1)
              <th>{{ number_format($period['total_aed'] - ($period['credit_total_aed']) , 2) }}</th>
            @endif
            @if($inr==1)
              <th>{{ number_format($period['total_inr'] - ($period['credit_total_inr']), 2) }}</th>
            @endif
            @if($usd==1)
              <th>{{ number_format($period['total_usd'] - ($period['credit_total_usd']), 2) }}</th>
            @endif

          </tr>
        <!--   
          <tr class="hidden danger">
            @if($aed==1)
              <th>{{ number_format($period['credit_total_aed'], 2) }}</th>
            @endif
            @if($inr==1)
              <th>{{ number_format($period['credit_total_inr'], 2) }}</th>
            @endif
            @if($usd==1)
              <th>{{ number_format($period['credit_total_usd'], 2) }}</th>
            @endif
          </tr>
        -->
          <tr class="warning">
            @if($aed==1)
              <th>{{ number_format($period['expenses_aed'], 2) }}</th>
            @endif
            @if($inr==1)
              <th>{{ number_format($period['expenses_inr'], 2) }}</th>
            @endif
            @if($usd==1)
              <th>{{ number_format($period['expenses_usd'], 2) }}</th>
            @endif
          </tr>
          <!-- <tr >
            @if($aed==1)
              <th>{{ number_format(($period['total_aed']- ($period['credit_total_aed'])), 2) }}</th>
            @endif
            @if($inr==1)
              <th>{{ number_format(($period['total_inr']- ($period['credit_total_inr'])), 2) }}</th>
            @endif
            @if($usd==1)
              <th>{{ number_format(($period['total_usd']- ($period['credit_total_usd'])), 2) }}</th>
            @endif
          </tr>-->
          <tr >
            <th colspan="3">
              @if($period['expenses_aed']==0)
                {{ number_format( (($period['total_aed'] - $period['credit_total_aed'])/1)*100, 2) }}
              @else
                {{ number_format( (($period['total_aed'] - $period['credit_total_aed'])/$period['expenses_aed'])*100, 2) }}
              @endif
            </th>
          </tr>


        </table>
        </td>
      @endforeach
    </tr>
  </table>
  
    </div>

    </div>

</div>
<script type="text/javascript">
$('.btn-csv').click(function(){
  $('.csv-flag').val('1');
  $('.frm-filter').submit();
  $('.csv-flag').val('0');
});
</script>
@stop
