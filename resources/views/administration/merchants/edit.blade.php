@extends('layouts.admin-finance')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-2">
        <h1>Edit Merchant</h1>
    </div>
</div>

@include('administration.merchants.partials.form')

@stop