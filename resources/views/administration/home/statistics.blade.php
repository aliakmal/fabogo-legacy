@extends('layouts.admin-base')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h3>Calls Tracked</h3>
    
    <div class="row">
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>TODAY</b></small>
            <h1 id="total-calls-today" class=" ">&nbsp;</h1>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>LAST WEEK</b></small>
            <h1 id="total-calls-last-week" class=" ">&nbsp;</h1>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>LAST MONTH</b></small>
            <h1 id="total-calls-last-month" class=" ">&nbsp;</h1>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>SINCE BEGINNING</b></small>
            <h1 id="total-calls-ever" class=" ">&nbsp;</h1>
          </div>
        </div>
      </div>
    </div>
    <h3>Business Generated</h3>
    <div class="row">
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>TODAY</b></small>
            <h2 id="total-cash-today" class=" ">&nbsp;</h2>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>LAST WEEK</b></small>
            <h2 id="total-cash-last-week" class=" ">&nbsp;</h2>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>LAST MONTH</b></small>
            <h2 id="total-cash-last-month" class=" ">&nbsp;</h2>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body text-center">
            <small><b>SINCE BEGINNING</b></small>
            <h2 id="total-cash-ever" class=" ">&nbsp;</h2>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8">
        <div id="reports-line-chart"></div>
        <div class="alert alert-warning">
          <ul>
            <li>Average cost per lead is estimated at USD 15.</li>
            <li>IP's are tracked to avoid inaccuracies in logging of multiple concurrent clicks.</li>
            <li>Two accurate concurent calls are assumed when the interval between concurrent calls from a single ip is above 60 seconds.</li>
            <li>Duplicate calls to the same business from the same ip are considered as a single call when made within an interval of 60 seconds.</li>
            <li>Calls are tracked in realtime.</li>
            <li>All ips are logged in realtime.</li>
          </ul>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Live Feed
            </h3>
          </div>
          <div class="panel-body">
            <div id="feed" style="max-height:450px;overflow-y:auto;"></div>
          </div>
        </div>

      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Booking Calls Made Between Dates
            </h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">

                  <input class="form-control datepicker" value="{{ \Carbon\Carbon::now()->subDays(10)->format('Y-m-d') }}" id="start_date" />
                </div>
                <div class="form-group">
                  <input class="form-control datepicker"  value="{{ \Carbon\Carbon::now()->addDays(2)->format('Y-m-d') }}" id="end_date" />
                </div>
                <div class="form-group">
                  <button style="display:block;" id="btn-get-live-statistics" class="btn btn-primary">SUBMIT</button>
                  <span clas="preloader-statistics" style="display:none;">Loading...</span>
                </div>

              </div>
              <div class="col-md-6 text-right">
                <small class="text-right">CALLS MADE</small>
               <h1 id="total-calls-dates" class="text-right" style="margin-top:0px;"></h1>
                <small class="text-right">BUSINESS GENERATED</small>
               <h2 id="total-cash-dates" class="text-right" style="margin-top:0px;"></h2>

              </div>
            </div>
          </div>

        </div>
        <div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Top 50 Venues           </h3>
          </div>
          <div class="panel-body" style="max-height:450px;overflow-y:auto;">

          <table class="table">
            <thead>
              <tr>
                <th>Business</th>
                <th>Calls</th>
              </tr>
            </thead>
            <tbody id="top_10"></tbody>
          </table>
        </div>
      </div>
        </div>






      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
  $('.datepicker').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  var flag_in_progress = false;
  var startEndDates = {
    start_date: $('#start_date').val(),
    end_date : $('#end_date').val()
  };

var lastFlashedFeed = '';
$('#btn-get-live-statistics').click(function(){
  startEndDates['start_date'] =  $('#start_date').val();
  startEndDates['end_date'] =  $('#end_date').val();
  $('.preloader-statistics').show();
  flag_in_progress = false;

});

var getStartEndDates = function(){
  return startEndDates;
};

var chart = new Morris.Line({
  element: 'reports-line-chart',
  xkey: 'date',
  ykeys: ['calls'],
  labels: ['Calls']
});


var getLiveStatistics = function(){
  
  if(flag_in_progress== true){
    return;
  }
  
  flag_in_progress = true;
  $.ajax({
    data:getStartEndDates(),
    url:'/admin/get-live-statistics'
  }).done(function(result){
    $('#total-calls-today').html(result['total_calls_today']);
    $('#total-calls-last-week').html(result['total_calls_week']);
    $('#total-calls-last-month').html(result['total_calls_month']);
    $('#total-calls-ever').html(result['total_calls_ever']);
    $('#total-calls-dates').html(result['total_calls_dates']);

    $('#total-cash-today').html('$'+(15*result['total_calls_today']).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
    $('#total-cash-last-week').html('$'+(15*result['total_calls_week']).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
    $('#total-cash-last-month').html('$'+(15*result['total_calls_month']).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
    $('#total-cash-ever').html(      '$'+(15*result['total_calls_ever']).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
    $('#total-cash-dates').html(     '$'+(15*result['total_calls_dates']).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

    /*//$('#start_date').val(result['start_date']);
    //$('#end_date').val(result['end_date']);*/

    $('#feed').html(result['feed']);
    $('#top_10').html(result['top_html']);

    /*//startEndDates['start_date'] =  $('#start_date').val();
    //startEndDates['end_date'] =  $('#end_date').val();*/

    chart.setData(result['data']);
    flag_in_progress = false;
    $('.preloader-statistics').hide();
    if(lastFlashedFeed != $('#feed').find('div:first').html()){
      lastFlashedFeed = $('#feed').find('div:first').html();
      $('#feed').find('div:first').effect('highlight');
    }
  });
};

  setInterval(getLiveStatistics, 5000);

});
</script>

@stop