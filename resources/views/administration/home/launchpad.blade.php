@extends('layouts.admin-base')
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="row">
      @foreach($dashboards as $dashboard)
        <div class="col-md-4">
      
          <div class="panel panel-primary">
            <div class="panel-heading">
              <div class="row">
                <div class="col-xs-3 ">
                    @if($dashboard == 'sms')
                        <i class="fa fa-phone  fa-3x"></i>
                    @endif
                    @if($dashboard == 'restricted')
                        <i class="fa fa-shield  fa-3x"></i>
                    @endif

                    @if($dashboard == 'finance')
                        <i class="fa fa-dollar  fa-3x"></i>
                    @endif
                    @if($dashboard == 'crm')
                        <i class="fa fa-tag  fa-3x"></i>
                    @endif
                    @if($dashboard == 'editor')
                        <i class="fa fa-pencil  fa-3x"></i>
                    @endif
                    @if($dashboard == 'content')
                        <i class="fa fa-file  fa-3x"></i>
                    @endif
                </div>
                <div class="col-xs-9 text-right">

                  <small>DASHBOARD FOR</small>
                  <div class="huge">{{strtoupper($dashboard == 'restricted'?'users':$dashboard)}}</div>
                </div>
              </div>
            </div>
            <div class="panel-footer">
              <a class="btn btn-default btn-xs" href="/{{$dashboard}}">Jump</a>
            </div>
          </div>


        </div>
      @endforeach
    </div>
  </div>
</div>
@endSection