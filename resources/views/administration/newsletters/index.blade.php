@extends('layouts.admin-crm')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>Newsletters HTML</h1>
    <hr/>
    <p>
      <a class="btn btn-primary" href="{{ route('admin.newsletters.weekly.posts') }}" target="_blank">GENERATE WEEKLY ARTICLES NEWSLETTER</a>
    </p>
    <form action="{{ route('admin.newsletters.custom.posts') }}" method="post">
      <input name="ids" class="form-control" placeholder="Enter Ids of posts seperated by commas" />
      <input type="submit" class="btn btn-primary" value="GENERATE CUSTOM" />
    </form>
  </div>
</div>
@stop
