@if(isset($invoice))
  {{ Form::model($invoice, array( 'class' => 'form-horizontal', 'method' => 'PATCH', 
                                  'route' => array('admin.invoices.update', $invoice->id))) }}
  <h3>{{ $invoice->title }}</h3>
@else
  {{ Form::open(array('route' => 'admin.invoices.store', 'class' => 'form-horizontal')) }}
@endif

  <div class="form-group">
    {{ Form::label('merchant_id', 'Merchant_id:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('merchant_id', Merchant::orderby('name', 'asc')->lists('name', 'id')->all(), Input::old('merchant_id'), array('id'=>'merchant-id-selector', 'class'=>'form-control')) }}
    </div>
  </div>
  <div id="businesses-for-merchant-holder" class="form-group">
    @if(isset($invoice))
      @include('administration.merchants.partials.business-for-invoice-form')
    @endif
  </div>

  <div class="form-group">
    {{ Form::label('user_id', 'Sales POC:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('user_id', User::select()->byPocs()->get()->lists('selectable_full_name', 'id')->all(), Input::old('user_id'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('city_id', 'City:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('city_id', Zone::query()->cities()->get()->lists('name', 'id')->all(), Input::old('city_id'), array('class'=>'form-control')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('entity_id', 'Mazkara Entity:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('entity_id', mzk_get_entities_as_array(), Input::old('entity_id'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('currency', 'Currency:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4">
      {{ Form::select('currency', Payment::getCurrencies(), Input::old('currency'), array('class'=>'form-control')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('dated', 'Dated:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('dated', Input::old('dated'), array('class'=>' form-control', 'placeholder'=>'Dated')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('start_dated', 'Start Date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('start_date', Input::old('start_date'), array('class'=>' form-control', 'placeholder'=>'Start Date')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('end_date', 'End Date:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('end_date', Input::old('end_date'), array('class'=>' form-control', 'placeholder'=>'End Date')) }}
    </div>
  </div>
  <div class="well col-sm-8">
  <div class="form-group ">
    <div class="col-sm-3 ">
      <a href="javascript:void(0)" class="btn btn-warning" id="lnk-add-invoice-item">ADD ITEM</a>
    </div>
    <div class="col-sm-9 ">
      <table class="table">
        <thead>
          <tr>
            <th width="70%">DESC</th>
            <th>PRICE</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="items-here">
          @if(isset($invoice))
            @foreach($invoice->items()->onlyItems()->get() as $item)
              <tr>
                <td>
                  {{ Form::textarea('existing_items['.$item->id.'][desc]', $item->desc, ['class'=>'form-control']) }}
                  {{ Form::hidden('existing_items['.$item->id.'][price]', $item->price, ['class'=>'form-control amount']) }}
                  {{ Form::hidden('existing_items['.$item->id.'][qty]', $item->qty, ['class'=>'form-control qty']) }}

                  {{ Form::hidden('existing_items['.$item->id.'][total]', $item->total, ['class'=>'form-control total']) }}
                  {{ Form::checkbox('deletable_items[]', $item->id, false, ['class'=>'form-control deletable-check']) }}
                  <span class="sub-total hidden"></span>
                </td>
                <td>
                  <a href="javascript:void(0)" class="btn deletable-existing btn-xs btn-danger">
                    <i class="fa fa-times" ></i></a>
                </td>
              </tr>
            @endforeach
          @endif

        </tbody>
        <tfoot>
          <tr>
            <td colspan="1">Total</td>
            <td colspan="2" id="total-invoice">
              {{ Form::text('amount', (isset($invoice) ? $invoice->amount : ''), array('class'=>'form-control total-amount', 'placeholder'=>'Amount')) }}
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}

<script id="item-template" type="text/html">
<tr>
  <td>{{Form::textarea('items[__REPLACE__][desc]', '', ['class'=>'form-control'])}}
  {{Form::hidden('items[__REPLACE__][price]', '', ['class'=>'form-control amount'])}}
      {{Form::hidden('items[__REPLACE__][qty]', 1, ['class'=>'form-control qty'])}}
      {{Form::hidden('items[__REPLACE__][total]', '', ['class'=>'form-control total'])}}
      <span class="sub-total hidden"></span>
  </td>
  <td>

    <a href="javascript:void(0)" class="btn deletable btn-xs btn-danger"><i class="fa fa-times"></i></a>
  </td>
</tr>
</script>
<script type="text/javascript">
$(function(){
  $('.dateinput input').datepicker({ format: "yyyy-mm-dd", autoclose:true });
  $('input.dateinput').datepicker({ dateFormat: "yyyy-mm-dd", autoclose:true });

  $('#merchant-id-selector').change(function(){
    $.ajax({
      url: '{{ route('admin.merchants.call_logs.businesses') }}', 
      type:'GET',
      data:{ merchant_id: $(this).val()}
    }).done(function(data){
      $('#businesses-for-merchant-holder').html(data['html']);
    });
  });

  $('#lnk-add-invoice-item').click(function(){
    htm = $('#item-template').html();
    htm = htm.replace(/__REPLACE__/g, Date.now());
    $('#items-here').append(htm);
  });

  $('#items-here').on('click', '.deletable', function(){
    $(this).parents('tr').first().remove();
  });

  $('.deletable-existing').click(function(){
    cn = $(this).parents('tr').first();
    $(cn).find('.deletable-check').prop('checked', true);
    $(cn).hide();
  });

  $('#items-here').on('keyup', 'input.qty, input.amount', function(){

    qtys = $('#items-here input.qty');
    amounts = $('#items-here input.amount');
    totals = $('#items-here input.total');
    subs = $('#items-here .sub-total');
    total = 0;
    $(qtys).each(function(i, val){
      v = $(qtys[i]).val();

      if(v==''){
        v = 0;
        $(qtys[i]).val(v);
      }

      z = $(amounts[i]).val();

      if(z==''){
        z = 0;
        $(amounts[i]).val(z);
      }

      sm = z*v;

      $(subs[i]).html(sm);
      total = total + sm;
      $(totals[i]).val(sm);

    });
  });
});
</script>
