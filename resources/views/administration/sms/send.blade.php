@extends('layouts.admin-sms')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h2>Send SMS</h2>
    <hr/>
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.sms.post'))->encodingType('multipart/form-data') }}

  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  {{ BootForm::text('Phone Number', 'phone')->placeholder('Phone number to sms') }}
  {{ BootForm::textarea('Body', 'body')->placeholder('Body') }}

  {{ BootForm::token() }}
<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Send Sms', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>
{{ BootForm::close() }}


  </div>
</div>
@stop