@extends('layouts.admin-sms')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h2>SMS Settings</h2>
    <hr/>
  {{ BootForm::openHorizontal(['sm' => [4, 8], 'lg' => [3, 8] ])->post()->action(route('admin.sms.post.settings'))->encodingType('multipart/form-data') }}

  @if (Session::get('notice'))
    <div class="alert alert-success">
      {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
    </div>
  @endif
  @foreach($settings as $setting)
    {{ BootForm::textarea(ucwords(str_replace('_', ' ' , $setting->name)), 'settings['.$setting->id.']['.$setting->name.']')->placeholder(ucwords(str_replace('_', ' ' , $setting->name)))->value($setting->value) }}
  @endforeach

  {{ BootForm::token() }}
<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Save Settings', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>
{{ BootForm::close() }}


  </div>
</div>
@stop