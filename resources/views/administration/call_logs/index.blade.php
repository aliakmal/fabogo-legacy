@extends('layouts.admin-crm')
@section('content')

<h1 >Call Logs for {{$business->name}}</h1>


@if ($call_logs->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Dated</th>
				<th>Timed</th>
				<th>Called</th>
				<th>Caller</th>
				<th>Duration</th>
				<th>Agent list</th>
				<th>Connected to</th>
				<th>Transfer status</th>
				<th>Transfer duration</th>
				<th>Knowlarity url</th>
				<th>Start_time</th>
				<th>Pickup_time</th>
				<th>Caller circle</th>
				<th>Is Downloaded</th>
				<th>Fabogo URL</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($call_logs as $call_log)
				<tr>
					<td>{{{ $call_log->dated }}}</td>
					<td>{{{ $call_log->timed }}}</td>
					<td>{{{ $call_log->called_number }}}</td>
					<td>{{{ $call_log->caller_number }}}</td>
					<td>{{{ $call_log->caller_duration }}}</td>
					<td>{{ $call_log->agent_list  }}</td>
					<td>{{{ $call_log->call_connected_to }}}</td>
					<td>{{{ $call_log->call_transfer_status }}}</td>
					<td>{{{ $call_log->call_transfer_duration }}}</td>
					<td><a href="{{{ $call_log->call_recording_url }}}">{{{ $call_log->call_recording_url }}}</a></td>
					<td>{{{ $call_log->call_start_time }}}</td>
					<td>{{{ $call_log->call_pickup_time }}}</td>
					<td>{{{ $call_log->caller_circle }}}</td>
					<td>{{{ $call_log->is_downloaded }}}</td>
					<td><?php 
								$media = $call_log->getMediaMatter();
								$media == 'none'?'<a href="'.$media.'">'.$media.'</a>':''; 
							?></td>
          <td class="text-right">
            <span class=""><!-- PUT CHECK FOR PPL HERE -->
              @if($call_log->isPPL())
                @if($call_log->isLead())
                  <i class="fa fa-money" title="This call is a potential lead and is billable."></i>
                @endif
                {{ mzk_call_logs_state_helper($call_log->state) }}
              @endif
            </span>
          </td>
				</tr>
			@endforeach
		</tbody>
	</table>
  {{ $call_logs->render() }}

@else
	There are no call_logs
@endif

@stop