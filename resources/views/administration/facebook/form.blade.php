@extends('layouts.admin-crm')
@section('content')

<div class="row">
  <div class="col-md-10 col-md-offset-2">
    <h1>Set Facebook Like Box for {{ $business->name }}</h1>

    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
      </div>
    @endif
  </div>
</div>

{{ Form::model($business, array('class' => 'form-horizontal', 'files'=>true, 'method' => 'POST', 'route' => array('admin.businesses.facebook.update', $business->id))) }}

  <div class="form-group">
    {{ Form::label('facebook_like_box_id', 'Facebook Page URL(with http):', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::text('facebook_like_box_id', Input::old('facebook_like_box_id'), array('class'=>'form-control', 'placeholder'=>'Facebook Page ID')) }}
    </div>
  </div>
  <div class="form-group">
    {{ Form::label('facebook_like_box_status', 'Activate Facebook Like Box:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-10">
      {{ Form::select('facebook_like_box_status',  ['inactive'=>'Inactive', 'active'=>'Active'],   
                      Input::old('facebook_like_box_status'), array('class'=>'form-control', 'placeholder'=>'State')) }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('facebook_like_box_valid_until', 'Valid Until:', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-4 dateinput">
      {{ Form::text('facebook_like_box_valid_until', Input::old('facebook_like_box_valid_until'), array('class'=>' form-control datepicker', 'placeholder'=>'Dated')) }}
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
  </div>

{{ Form::close() }}
<script type="text/javascript">
$(function(){

  $('.datepicker').datepicker({ format: "yyyy-mm-dd", autoclose:true });

  $('#business-search').autocomplete({
    source:'/content/groups/get-outlets',
    select: function(event, ui){
      $('#businesses-holder').append(ui.item.html);
    },
  }).autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( '<div >' + item.id + ' - ' +item.name + '<small>('+item.zone_cache+')</small></div>' )
      .appendTo( ul );
  };  

  $(document).on('click', '.deletable-link', function(){
    $(this).prevAll('.deletable').first().attr('checked', true);
    $(this).parents('li').first().css('backgroundColor', 'red').fadeOut( "slow", function() {
      $(this).remove();
    });
  });


});
</script>

@stop
