  <?php $business_url =  MazkaraHelper::slugSingle($business);?>
<article class="">
    <div class="search-item-rating ">
      <div class="search-item-stars pull-right">
        <span class="pull-right">
          {{ ViewHelper::rateBusinessNiblet($business) }}<br/>
        </span>
        <div class="clear"></div>
        <div class="search-item-rating-num-votes pt5 pull-right">
          <small >{{ $business->accumulatedReviewsCount() }} VOTES</small>
        </div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="pos-relative">
      <h3 class="search-item-name">
        <a title="{{{ $business->name }}}" href="{{ $business_url }}" class="result-title">
          {{{ $business->name }}}
        </a>
      </h3>
      <div class="mt2 mb2 ">
        <i class="fa fa-map-marker"></i>
        <b>{{ $business->zone_cache }}</b>
        <span title="{{ $business->getAddressAsString() }}" class="search-item-address">› {{ $business->getAddressAsString() }}</span>
      </div>
      <?php
      $cats =$business->getMeta('categories');
      $highlights =$business->getMeta('highlights');
      $cats = is_array($cats)?$cats:[];
      $highlights = is_array($highlights)?$highlights:[];
      ?>
      <div class="mt5 mb5 gray ">
        @foreach($cats as $ii=>$category)
          <a title="{{$category}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['category[]'=>$ii]) }}" class="search-item-text pr5 ">
            <i class="flaticon {{ViewHelper::iconCategory($ii)}} lite-blue"></i>
          </a>
        @endforeach
        @foreach($highlights as $ii=>$highlight)
          <a title="{{$highlight}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['highlights[]'=>$ii]) }}" class="search-item-text pr5">
            <i class="flaticon {{ViewHelper::iconHighlight($ii)}}"></i>
          </a>
        @endforeach
      </div>

    </div>

    <div class="search-item-links pt5 pb5">
        <?php       
          $srv = $business->getMeta('services');
          $services_block = '';
        ?>
        @if(is_array($srv) && (count($srv)>0))
          <a class="btn btn-lite-blue btn-xs mr10 mb5 toggler" rel="services-for-{{$business->id}}" href="javascript:void(0)">Services</a>
          <?php ob_start();?>
            <div class="hidden well" id="services-for-{{$business->id}}">
              @foreach($srv as $ii=>$service)
                <a title="{{$service}} in {{$business->zone_cache}}" href="{{ MazkaraHelper::slugCity(null, ['service[]'=>$ii]) }}" class="search-item-text">{{$service}}</a>, 
              @endforeach
            </div>
          <?php $services_block = ob_get_contents(); ob_end_clean();?>
        @endif          
        @if($business->rate_card_count > 0)
        <a class="btn btn-lite-gray btn-xs mr10 mb5" title="{{{ $business->name }}} Rate Card" href="{{ $business_url }}">
          Rate Card
        </a>
        @endif
        @if($business->image_count > 0)
        <a class="btn btn-lite-gray btn-xs mr10 mb5" title="{{{ $business->name }}} Photos" href="{{ $business_url }}">
          {{$business->image_count}} photo(s)
        </a>
        @endif
        @if($business->reviews_count > 0)
        <a class="btn btn-lite-gray btn-xs mr10 mb5" title="User reviews for {{{ $business->name }}}" href="{{ $business_url }}">
          {{$business->reviews_count}} Review(s)
        </a>
        @endif
      <div class="clear"></div>
      {{ $services_block }}
    </div>
  </article>
  <hr />
