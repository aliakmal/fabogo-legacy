@extends('layouts.client')
@section('content')

<div class="row">
    <div class="col-md-10">
        <h1>{{$business->name}} Photos</h1>

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
        	</div>
        @endif
    </div>
</div>

@include('client.businesses.partials.photos')
@stop
