<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
<link rel="apple-touch-icon" sizes="57x57" href="{{mzk_assets('assets/clients-favicons/apple-icon-57x57.png?v=2')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{mzk_assets('assets/clients-favicons/apple-icon-60x60.png?v=2')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{mzk_assets('assets/clients-favicons/apple-icon-72x72.png?v=2')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{mzk_assets('assets/clients-favicons/apple-icon-76x76.png?v=2')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{mzk_assets('assets/clients-favicons/apple-icon-114x114.png?v=2')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{mzk_assets('assets/clients-favicons/apple-icon-120x120.png?v=2')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{mzk_assets('assets/clients-favicons/apple-icon-144x144.png?v=2')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{mzk_assets('assets/clients-favicons/apple-icon-152x152.png?v=2')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{mzk_assets('assets/clients-favicons/apple-icon-180x180.png?v=2')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{mzk_assets('assets/clients-favicons/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{mzk_assets('assets/clients-favicons/favicon-32x32.png?v=2')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{mzk_assets('assets/clients-favicons/favicon-96x96.png?v=2')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{mzk_assets('assets/clients-favicons/favicon-16x16.png?v=2')}}">
<link rel="manifest" href="{{mzk_assets('assets/clients-favicons/manifest.json')}}">
<link rel="canonical" href="{{ Request::url() }}" />
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ mzk_assets('assets/clients-favicons/ms-icon-144x144.png?v=2') }}">

  <title>Fabogo Partner</title>


  <?php echo mzk_stylesheet_link_tag('client', true); ?>

  <?php echo mzk_js_tag('client', true); ?>



  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
    @include('elements.post-body')

  <div id="wrapper">
    @include('elements.menus.client-navbar')

    <div id="page-wrapper" class=" container">
      @yield('content')
    </div>
    <!-- /#page-wrapper -->
  </div>
<footer class="footer">
  <div class="container">
    <p class="pull-left">Copyright &copy; 2015 fabogo.com </p>
  </div>
</footer>  
  <!-- /#wrapper -->
  <script type="text/javascript">
    $(function(){
  $('.submit-parent-form').click(function(){
    $(this).parents().first().submit();
  });

  $('form.confirmable').submit(function(){
    return confirm('Are you sure?');  
  });

    $('.ajax-popup-link').magnificPopup({
      type: 'ajax',
      showCloseBtn:true,
      closeBtnInside:false,
      fixedContentPos: 'auto',
      closeOnBgClick:true,
    });    


      $(function(){
          $('input[type="date"]').datepicker({ format: "yyyy-mm-dd", autoclose:true });
      })
    });
  </script>
</body>

</html>
