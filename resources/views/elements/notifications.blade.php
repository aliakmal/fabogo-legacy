@if (Session::get('error'))
  <div class="alert alert-error alert-danger">
    {{{ is_array(Session::get('error'))?join(',', Session::get('error')):Session::get('error') }}}
  </div>
@endif
@if (Session::get('notice'))
  <div class="alert alert-success">
    {{{ is_array(Session::get('notice'))?join(',', Session::get('notice')):Session::get('notice') }}}
  </div>
@endif
