<?php
$all_zones = ViewHelper::zonesComboAsSelectableArray();
$default_subzone =  null;
$default_subzone_id =  null;
if(isset($current_subzone)){
  $current_subzone = is_object($current_subzone)?$current_subzone->toArray():$current_subzone;
  $default_subzone = $current_subzone['name'];
  $default_subzone_id = $current_subzone['id'];
}
if(isset($default_search)){
  $default_search = $default_search;
}else{
  $default_search = null;
}

?>

<nav id="tf-menu" class="navbar mb0 container  navbar-default no-radius on" >
  <div id="top-main-logo-container" >
  <div class="container">
    <a target="_self" class="pull-left ml10 mt10 mb10" href="/?{{ _apfabtrack(['header', 'logo']) }}">
     <img src="{{ mzk_assets('assets/current/fabogo-logo.svg')}}" />
    </a>


  @include('elements.top-menu-links')



    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
      <ul class="nav  navbar-nav navbar-right">
        <li >
          <span class="show-only-mobile">
          <a href="#download-app" style="margin-top:15px;" class="btn hidden show-only-mobile lnk-download-app btn-turquoise btn-sm pull-right mr5  btn dpib show-only-mobile btn-default ">
            <b>Download App</b>
          </a>
        </span>
        </li>
        @if(Auth::check())
          @if (Auth::user()->hasRole('client'))
            <li>
              <a target="_self" href="{{{ URL::to('/partner') }}}" class="page-scroll   ">Dashboard!</a>
            </li>
          @endif
        @endif

          @include('elements.menu')
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>
    </div>
    <div ng-app="navigationApp"  id="top-main-menu-container" >
    <div class=" relative">
    <div ng-controller="NavigationController" class="collapse pl0 pr0 navbar-collapse" id="bs-main-navbar-collapse-1">
    <ul class="nav navbar-nav yamm  navbar-secondary">
      @include('elements.main-menu-angular')
    </ul>


    </div></div>

  </div><!-- /.container-fluid -->
</nav>
<div class="bg-lite-gray-3 container">
<div class="row">
  <div class="col-md-12">
    <div class="form-inline  pl10 pr10 navbar-form navbar-left   collapse navbar-collapse " id="top-navbar-search-form" role="navigation"  style="width:100%;">
      <div class=" " style="padding-left:30px;padding-right:40px;width:100%;">
        <div id="search-box-holder" class="inner-addon left-addon" style="position:relative;">
          <label class="glyphicon glyphicon-search  " style="font-size:14px;padding:13px 10px; ;" rel="tooltip "></label>
          {{ Form::text('search', $default_search, 
                                array('class'=>'p10 no-border-size no-box-shadow  
                                                border-radius-5-mobile 
                                                border-left-radius-5 
                                                no-border-radius 
                                                pointer-cursor pr15 pl15 
                                                form-control selectable', 
                                      'id'=>'search-selector',
                                      'style'=>'line-height:1.7;border-right:1px solid #dadada !important;height:auto;font-size:14px;',
                                      'data-content'=>"Hey! Tell us what you're looking for.",                                          
                                      'data-placement'=>"bottom",
                                      'placeholder'=>'Search for salon, spa or a service')) }}
          <div style="width:100%;z-index:9999;text-align:center;height:80px;background-color:#fff;top:44px;position:absolute;border: 1px solid #ccc; padding-top:30px;z-index:998;display:none;" class="search-box-preloader bt0  border-bottom-radius-5">
            <b>Coming right up!</b> <img src="{{ mzk_assets('assets/indicator.gif')}}" alt="..." />
          </div>
        </div><div id="search-location-holder" class="inner-addon left-addon" style="width:30%;">
          <label class="glyphicon glyphicon-map-marker  " rel="tooltip " style="font-size:14px;padding:13px 10px; ;"></label>
          {{ Form::text(' ', isset($where)?$where:MazkaraHelper::getLocaleLabel(), 
                                  array('class'=>'form-control no-box-shadow 
                                                  no-border-size no-border-radius 
                                                  border-radius-5-mobile  
                                                  pointer-cursor selectable 
                                                  pr15 pl15 p10', 
                                        'id'=>'location-selector', 
                                        'style'=>'height:auto;font-size:14px; vertical-align: middle;float:none;line-height:1.7;',
                                        'placeholder'=>'Please Type a Location')) }}
        </div><div class="input-group-btn" style="display:inline-block;">
          <button id="search-bar-button" style="margin-left: 0px; min-width: 120px; line-height: 1.7; height: auto; font-size: 14px; padding-top: 10px; padding-bottom: 8px; font-weight: 800;" type="submit" class="btn border-radius-5-mobile no-border-radius border-right-radius-5 btn-turquoise">
            SEARCH
          </button>
        </div>
      </div>
    </div>
    <form id="search-bar-form" class="hidden navbar-left navbar-form" action="/search/businesses" method="POST">
      {{ Form::hidden('zone[]', $default_subzone_id, array('class'=>'form-control selectable', 'id'=>'location-selector-name'))}}
      {{ Form::hidden('search', $default_search, array('class'=>'', 'id'=>'search-selector-name'))}}
      {{ Form::hidden('category[]', (isset($default_category)?$default_category:''), array('class'=>'form-control selectable', 'id'=>'location-selector-category'))}}
      {{ Form::hidden('service[]', (isset($default_service)?$default_service:''), array('class'=>'form-control selectable', 'id'=>'location-selector-service'))}}
    </form>
</div>
</div>
</div>
<div id="fullscreen-overlay"></div>