<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
  @include('elements.menus.admin-navbar-top')

  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
      <ul class="nav" id="side-menu">
        <li>
            <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        </li>
        @if(Auth::user()->can("manage_virtual_numbers") || Auth::user()->can('manage_merchants'))
        <li>
          <a href="#"><i class="fa fa-star fa-fw"></i> CRM Panel<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
          @if(Auth::user()->can('manage_merchants'))
            <li>
              <a href="{{ route('admin.merchants.index') }}">Merchants</a>
            </li>
          @endif
          @if(Auth::user()->can("manage_virtual_numbers"))
            <li>
              <a href="{{ route('admin.virtual_numbers.index') }}">Virtual Numbers</a>
            </li>
            <li>
              <a href="{{ route('admin.virtual_number_allocations.index') }}">Businesses > Numbers</a>
            </li>
          @endif

          </ul>
          <!-- /.nav-second-level -->
        </li>
        @endif
        @if(Auth::user()->can("manage_ads") || Auth::user()->can('manage_campaigns'))
        <li>
          <a href="#"><i class="fa fa-star fa-fw"></i> Native Ad's Panel<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            @if(Auth::user()->can('manage_campaigns'))
            <li>
              <a href="{{ route('admin.campaigns.index') }}">Campaigns</a>
            </li>
            @endif
            @if(Auth::user()->can('manage_ads'))
            <li>
              <a href="{{ route('admin.business_zones.index') }}">Business Zones</a>
            </li>
            <li>
              <a href="{{ route('admin.ad_zones.index') }}">Ad Zones</a>
            </li>
            <li>
              <a href="{{ route('admin.ads.index') }}">Ads</a>
            </li>
            @endif
          </ul>
          <!-- /.nav-second-level -->
        </li>
        @endif
        @if(Auth::user()->can("manage_listings"))

          <li>
            <a href="{{ route('admin.businesses.index') }}">Business</a>
          </li>
          <li>
            <a href="{{ route('admin.chains.index') }}">Chains</a>
          </li>

          <li>
            <a href="{{ route('admin.groups.index') }}">Groups</a>
          </li>

          <li>
            <a href="{{ route('admin.reviews.index') }}">Reviews/Tips</a>
          </li>
        @endif

        @if(Auth::user()->can("manage_highlights"))
          <li>
            <a href="{{ route('admin.highlights.index') }}">Highlights</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_zones"))
          <li>
            <a href="{{ route('admin.zones.index') }}">Zones</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_categories"))
          <li>
            <a href="{{ route('admin.categories.index') }}">Categories</a>
          </li>
          <li>
            <a href="{{ route('admin.services.index') }}">Services</a>
          </li>
        @endif
        @if(Auth::user()->can("manage_users"))
          <li>
            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Administration<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="{{ route('admin.accounts.index') }}">Users</a>
              </li>
              <li>
                <a href="{{ route('admin.jobs.index') }}">Jobs</a>
              </li>

            </ul>
            <!-- /.nav-second-level -->
          </li>
        @endif
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- /.navbar-static-side -->
</nav>