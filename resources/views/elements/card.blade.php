<div class="sq-card relative bg-lite-blue h200 mb25" style="background-image:url({{$card['image']}})">
  <a href="{{$card['url']}}" class="force-white">
  <div class="va-container va-container-v va-container-h ">
    <div class="va-middle text-center text-contrast">
      <div class="fs150 ">
        <b>{{ strtoupper($card['name'])}}</b>
      </div>
    </div>
  </div>
</a>
</div>
