<?php 
  $user = $activity->user;
  $payment = $activity->itemable;
  $invoice = $activity->meta['invoice'];  
?>
  <div class="feed-item">
    <div class="date">{{ $activity->created_at }}</div>
    <div class="text">{{$user->name}} allocated Payment {{'#'.$payment->id}} from invoice {{$invoice->title}} </div>
  </div>
