<?php 
  $owner = $activity->meta['owner'];
  $business = $activity->meta['business'];
?>
<div class="text">
  <i class="glyphicon glyphicon-star"></i>
  {{ $owner->name }} created <a href="{{ route('admin.businesses.show', [$business->id]) }}">{{ $business->name }}, {{ $business->zone_cache }}</a> 

</div>

