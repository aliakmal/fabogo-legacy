<?php 
  $user = $activity->user;
  $business = $activity->itemable;
?>
<div class="bbb mb10 pb5 ">
@include('elements.activities.business-niblet', ['business'=>$business])

  <div class="media pb5  ">
    <div class="pull-right gray ">
      <small>
        <i class="fa fa-thumbs-o-up"></i>  FAVOURITED
      </small>
    </div>
    <div class="media-left">
      <a href="/users/{{$user->id}}/profile" >
        {{ViewHelper::userAvatar($user)}}
      </a>
    </div>
    <div class="media-body">
      <b class="media-heading   pt5">
        <a title="{{{ $user->full_name }}}" href="/users/{{$user->id}}/profile" class="result-title">
          {{{ ViewHelper::formatUsername($user) }}} 
        </a>
      </b>
        Liked 
        <a title="{{{ $business->name }}}" href="{{MazkaraHelper::slugSingle($business)}}" class="result-title">
          {{{ $business->name }}} 
        </a>

      <br/>
      <small class="gray">{{{ Date::parse($activity->updated_at)->ago() }}}</small>
      <br/>
    </div>
  </div>

</div>
