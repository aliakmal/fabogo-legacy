@if($activity->itemable!= null)
  <div class="row single-activity-element">
    <div class="col-md-12">
      @include('elements.activities.'.strtolower($activity->itemable_type).'.'.strtolower($activity->verb), ['activity'=>$activity])
    </div>
  </div>
@endif