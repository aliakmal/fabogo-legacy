@extends('layouts.scaffold')

@section('main')

<h1>Show Voucher</h1>

<p>{{ link_to_route('vouchers.index', 'Return to All vouchers', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Offer_id</th>
				<th>User_id</th>
				<th>Code</th>
				<th>State</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $voucher->offer_id }}}</td>
					<td>{{{ $voucher->user_id }}}</td>
					<td>{{{ $voucher->code }}}</td>
					<td>{{{ $voucher->state }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('vouchers.destroy', $voucher->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('vouchers.edit', 'Edit', array($voucher->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
