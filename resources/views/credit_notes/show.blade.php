@extends('layouts.scaffold')

@section('main')

<h1>Show Credit_note</h1>

<p>{{ link_to_route('credit_notes.index', 'Return to All credit_notes', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Invoice_id</th>
				<th>Amount</th>
				<th>Desc</th>
				<th>Type</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $credit_note->invoice_id }}}</td>
					<td>{{{ $credit_note->amount }}}</td>
					<td>{{{ $credit_note->desc }}}</td>
					<td>{{{ $credit_note->type }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('credit_notes.destroy', $credit_note->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('credit_notes.edit', 'Edit', array($credit_note->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
