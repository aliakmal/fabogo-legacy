@extends('layouts.page')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
<h1 class="text-center pt20 pb20 notransform">
Cookie Policy for&nbsp;Fabogo</h1>
<h3>What Are Cookies</h3>
<p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience.&nbsp;This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>

For more general information on cookies see the <a href="http://en.wikipedia.org/wiki/HTTP_cookie" target="_blank">Wikipedia article on HTTP Cookies...</a>
<h3>How We Use Cookies</h3>
<p>We use cookies for a variety of reasons detailed below. Unfortunately is most cases&nbsp;there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>
<h3>Disabling Cookies</h3>
You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site. Therefore it is recommended that you do not disable cookies.
<h3>The Cookies We Set</h3>
<p>If you create an account with us then we will use cookies for the management of the signup process and general administration. These cookies will usually be deleted when you log out however in some cases they may remain afterwards to remember your site preferences when logged out.</p>

We use cookies when you are logged in so that we can remember this fact. This prevents you from having to log in every single time you visit a new page. These cookies are typically removed or cleared when you log out to ensure that you can only access restricted features and areas when logged in.

<p>When you submit data to through a form such as those found on contact pages or comment forms cookies may be set to remember your user details for future correspondence.</p>

In order to provide you with a great experience on this site we provide the functionality to set your preferences for how this site runs when you use it. In order to remember your preferences we need to set cookies so that this information can be called whenever you interact with a page is affected by your preferences.


<h3>Third Party Cookies</h3>
<p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>

This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.

<p>For more information on Google Analytics cookies, see the <a href="https://developers.google.com/analytics/resources/concepts/gaConceptsCookies" target="_blank">official Google Analytics page</a>.</p>

From time to time we test new features and make subtle changes to the way that the site is delivered. When we are still testing new features these cookies may be used to ensure that you receive a consistent experience whilst on the site whilst ensuring we understand which optimisations our users appreciate the most.

<p>As we sell products it's important for us to understand statistics about how many of the visitors to our site actually make a purchase and as such this is the kind of data that these cookies will track. This is important to you as it means that we can accurately make business predictions that allow us to monitor our advertising and product costs to ensure the best possible price.</p>

We also use social media buttons and/or plugins on this site that allow you to connect with your social network in various ways. For these to work the following social media sites including; facebook, twitter, google plus, will set cookies through our site which may be used to enhance your profile on their site or contribute to the data they hold for various purposes outlined in&nbsp;their&nbsp;respective&nbsp;privacy&nbsp;<wbr>policies.


<h3>More Information</h3>
<p>Hopefully that has&nbsp;clarified&nbsp;things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site. However if you are still looking for more information then you can contact us through one of our preferred contact methods.</p>

<strong>Email:</strong> <a href="mailto:hello@fabogo.com" target="_blank">hello@fabogo.com</a>
<h3>Contacting Us</h3>

<p>If there are any questions regarding this privacy policy you may contact us using the information below.<p>

www.fabogo.com<br/>
Creative City<br/>
Fujairah<br/>
PO.Box: 4422<br/>
United Arab Emirates<br/>
hello@fabogo.com<br/>

</div>
    </div>
</div>

@stop
