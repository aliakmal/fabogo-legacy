<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Claim for Business ID {{ $data['business_id']}} from Fabogo</h2>
    <p>Sent from Mazkara</p>
    <div>
      Name: {{ $data['name']}}<br/>
      Email: {{ $data['email']}}<br/>
      Phone: {{ $data['phone']}}<br/>
      BusinessID: {{ $data['business_id']}}<br/>
      Business Name: {{ $data['business']->name}}<br/>
    </div>
    <hr/>
  </body>
</html>
