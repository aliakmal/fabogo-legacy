<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Contact from Fabogo</h2>
    <p>Sent from Fabogo</p>
    <div>
      Name: {{ $data['name']}}<br/>
      Email: {{ $data['email']}}<br/>
      Phone: {{ $data['phone']}}<br/>
      Message: {{ nl2br($data['message'])}}<br/>
    </div>
    <hr/>
  </body>
</html>
