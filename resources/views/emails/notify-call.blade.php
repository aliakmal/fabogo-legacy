<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>Hello,</p>
    <p>You've just {{ (strstr($data['call_transfer_status'], 'Connected') ? 'received' : 'missed') }} a call from Fabogo.</p>
    <div>
      {{ $data['business_name']}}<br/>
      Call From: {{ $data['caller_number']}}<br/>
      Date: {{ $data['date_time']}}<br/>
      Duration: {{ $data['caller_duration']}} seconds<br/>
    </div>
    Please login to your account at www.fabogo.com/partner/{{ $data['business_id']}} to view logged calls.
    <p>Have a nice day</p>
  </body>
</html>
