@extends('layouts.email')

@section('content')
<div class='movableContent'>
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
    <tr>
      <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:25px;">
        <div class="contentEditableContainer contentTextEditable">
                  <div class="contentEditable" align='center' >
                      <h2 >Fabulous! Here's your Voucher!</h2>
                  </div>
                </div>
      </td>
    </tr>
    <tr>
      <td width="100">&nbsp;</td>
      <td width="400" align="center">
        <div class="contentEditableContainer contentTextEditable">
          <div class="contentEditable" align='left' >
              <p >Hi {{ $data['user']['name'] }},
                <br/>
                <br/>
       You've just secured the attached voucher for {{ $data['offer']['title']}}, just take it with you to the venue to redeem your voucher.
                <br/>
                <br/>
      Feel free to send us any feedback on hello@mazkara.com and do leave a tip on your experience at the venue's profile page on www.mazkara.com.
                <br/>
                <br/>
Cheers!
              </p>
          </div>
        </div>
      </td>
      <td width="100">&nbsp;</td>
    </tr>
  </table>
</div>

@stop