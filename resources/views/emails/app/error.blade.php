<h2>{{$data['url']}}</h2>

@foreach($data as $ii=>$vv)
  @if($ii=='server')
    <?php continue;?>
  @endif
  <div>
      <small><b>{{$ii}}</b></small><br/>
      <?php $vv =  explode("\n", ($vv)) ;?>
      @foreach($vv as $ix=>$vx)
        @if($ix==0)
          <h3>{{$vx}}</h3>
          <ul>
        @else
          <li>{{$vx}}</li>


        @endif
        </ul>

      @endforeach
  </div>
  <hr/>
@endforeach
<h3>SERVER VARS</h3>
@foreach($data['server'] as $ix=>$vx)
    <p><b>{{ is_array($ix)?var_dump($ix):$ix}}:</b> {{is_array($vx)?var_dump($vx):$vx}}</p>



@endforeach
