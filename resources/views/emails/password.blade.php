@extends('layouts.email')

@section('content')
  <div class='movableContent'>
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="80%" class="container">
    <tr>
      <td  colspan="2" align="center" style="padding-bottom:10px;padding-top:25px;">
        <div class="contentEditableContainer contentTextEditable">
          <p style="text-align:center;font-size:30px;" >
              Hey {{ $user['name']}}!
          </p>
        </div>
      </td>
    </tr>
    <tr>
      <td  colspan="2" align="center" style="padding-bottom:10px;padding-top:0px;">
        <div class="contentEditableContainer contentTextEditable">
          <p style="text-align:center;margin-top:10px; padding-left:20px; padding-right:20px; margin-bottom:0px;" >
              We're sorry you're having trouble with your password.
          </p>
          <p style="text-align:center;margin-top:10px; padding-left:20px; padding-right:20px; margin-bottom:0px;" >
              Don't worry! Click on the link below and follow the instructions to reset your password.
          </p>
        </div>
      </td>
    </tr>
  </table>
  <div style="padding-top:30px; padding-bottom:50px;">
  <table cellpadding="0" cellspacing="0" border="0" align="center" width="60%" class="container" >
    <tr>
            <td bgcolor="#FFCE44" align="center" style="border-radius:4px;" width="400" height="50">
              <div class="contentEditableContainer contentTextEditable">
                <div class="contentEditable" align='center' >
                    <a target='_blank' href="{{ URL::to('users/reset_password/'.$token) }}" class='link2' style="color:#000;">RESET</a>
                </div>
              </div>
            </td>
    </tr>
  </table>
</div>

</div>
@stop