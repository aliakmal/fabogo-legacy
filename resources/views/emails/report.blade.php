<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Report from Mazkara</h2>
    <p>Sent from Mazkara</p>
    <div>
      Name: {{ $data['name']}}<br/>
      Email: {{ $data['email']}}<br/>
      URL: {{ $data['url']}}<br/>
      Message: {{ $data['message']}}<br/>
    </div>

    <hr/>
    
  </body>
</html>
