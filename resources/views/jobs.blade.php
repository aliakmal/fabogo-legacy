@extends('layouts.page')
@section('content')

<div id="tf-home" class="container text-center" 
style="position:absolute; height:350px;;width:100%;z-index:0;background:url({{mzk_assets('assets/fab-banner-hiring.jpg')}}) no-repeat fixed center center / cover rgba(200, 82, 126, 0)">

</div>
    <div class="text-center" style="height:355px;">
      <div class="content " >
<div class="container " style="margin-top:40px;">
      </div></div></div>

<div class="container  pb15">
    <div class="row">
    <!-- <div class="col-md-12 bg-white"> -->
    <div class="col-md-10 bg-white  col-md-offset-1">
            <h1 class="text-center notransform">Work @ Fabogo</h1>
            <p class="fs125 text-justify dark-gray" >
At Fabogo we believe fabulous is not just a feeling, but an undying state of mind. It brings us together, it fuels our passion, and most importantly, it drives us forward.
With a rebellious spirit and a young heart, we celebrate fab in everything we do. We challenge ourselves to learn and grow. We act with integrity and pursue creative perfection. We set great ambitions and work in harmony.
We’re looking out for top talent to join our expanding squad of Fabulists, aiming to bring a revolution in the beauty and wellness industry. Through slick technology and a fabulous idea coupled with amazing people, Fabogo is out to change the world of fab!
            </p>
            <p class="fs125 text-justify dark-gray" >
We're currently on the lookout for super top talent in Sales, PR, Marketing, Mobile and Application development
If you share our beliefs & passion we may have a place for you - send us your resume with a short tweet on why should we hire you?
            </p>
            <p class="fs125 text-center dark-gray" >Work hard. Play Fab!</p>

        </div>
    </div>
</div>

<h1 class="text-center notransform">Life @ Fabogo</h1>
<p>&nbsp;</p>
<section class="hide-only-mobile" style="background-size:40%;background-image:url('{{mzk_assets('assets/culture/bg.png')}}'); background-repeat:no-repeat; background-position:center center;">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-4">
            <div class="text-center" style="margin-left:50px;">
              <div style="height:60px;">&nbsp;</div>
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/stocked-pantry.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Stocked Pantry</h3>
              <p class="dark-gray">Free snacks, so you’re high on energy and an endless supply of chai & coffee!</p>
            </div>
            <div class="text-center">
              <div style="height:20px;">&nbsp;</div>
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/learn-hustle.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Learn to Hustle</h3>
              <p class="dark-gray">You'll take your work to the next level, become a game-changer and get shit done</p>
            </div>
            <div class="text-center " style="margin-left:50px;">
              <div style="height:20px;">&nbsp;</div>
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/exponential-growth.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Exponential Growth</h3>
              <p class="dark-gray">Create something super exciting, and be a part of our supersonic growth story</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="text-center">
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/fun-times.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Fun Times</h3>
              <p class="dark-gray">Post work chilling and office parties, cuz we encourage a healthy work-life balance</p>
            </div>
            <div class="dark-gray text-center" style="height:370px;padding-top:120px;font-weight:bold;font-size:300%;">
                Why Work<br/>With Us
            </div>
            <div class="text-center" >
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/change-world.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Change the World</h3>
              <p class="dark-gray">One bad haircut at a time</p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="text-center " style="margin-right:50px;" >
              <div style="height:60px;">&nbsp;</div>
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/free-grooming.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Free Grooming</h3>
              <p class="dark-gray">Complimentary Salon & Spa services for everyone, cuz you gotta look, feel, and be fab</p>
            </div>
            <div class="text-center">
              <div style="height:20px;">&nbsp;</div>
              <p ><img width="85" src="{{mzk_assets('assets/culture/no-text/salary-perks.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Competitive Salary & Incentives</h3>
              <p class="dark-gray">Your perks and pockets are as important to us too, you know</p>

            </div>
            <div class="text-center " style="margin-right:50px;" >
              <div style="height:20px;">&nbsp;</div>
              <p><img width="85" src="{{mzk_assets('assets/culture/no-text/young-passionate.png')}}" /></p>
              <h3 class="dark-gray fs110 mb5 mt0">Young & Passionate Team</h3>
              <p class="dark-gray">Work with go-getters and talented people who love what they do</p>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<p>&nbsp;</p>
<section class="show-only-mobile">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="text-center " style="font-weight:bold; font-size:200%;">
            Why Work With Us
        </div>
        <div class="text-center ">
          <p><img width="100" src="{{mzk_assets('assets/culture/no-text/stocked-pantry.png')}}" /></p>
          <h3>Stocked Pantry</h3>
          <p>Free snacks, so you’re high on energy and an endless supply of chai & coffee!</p>
        </div>
          <div class="text-center">
            <p><img width="100" src="{{mzk_assets('assets/culture/no-text/learn-hustle.png')}}" /></p>
            <h3>Learn to Hustle</h3>
            <p>You'll take your work to the next level, become a game-changer and get shit done</p>
          </div>
          <div class="text-center ">
            <p><img width="115" src="{{mzk_assets('assets/culture/no-text/exponential-growth.png')}}" /></p>
            <h3>Exponential Growth</h3>
            <p>Create something super exciting, and be a part of our supersonic growth story</p>
          </div>
          <div class="text-center">
            <p><img width="115" src="{{mzk_assets('assets/culture/no-text/fun-times.png')}}" /></p>
            <h3>Fun Times</h3>
            <p>Post work chilling and office parties, cuz we encourage a healthy work-life balance</p>
          </div>
          <div class="text-center">
            <p><img width="132" src="{{mzk_assets('assets/culture/no-text/salary-perks.png')}}" /></p>
            <h3>Competitive Salary & Killer Incentives</h3>
            <p>Your perks and pockets are as important to us too, you know</p>
          </div>
          <div class="text-center">
            <p><img width="100" src="{{mzk_assets('assets/culture/no-text/free-grooming.png')}}" /></p>
            <h3>Free Grooming</h3>
            <p>Complimentary Salon & Spa services for everyone, cuz you gotta look, feel, and be fab</p>
          </div>
          <div class="text-center">
            <p><img width="120" src="{{mzk_assets('assets/culture/no-text/change-world.png')}}" /></p>
            <h3>Change the World</h3>
            <p>One bad haircut at a time</p>
          </div>
        </div>
      </div>
  </div>
</section>



<style type="text/css">

#jobs-tab-list.nav-tabs > li > a{
  border:0px;
} 

#jobs-tab-list.nav-tabs > li.active > a, 
#jobs-tab-list.nav-tabs > li.active > a:focus, 
#jobs-tab-list.nav-tabs > li.active > a:hover {
  -moz-border-bottom-colors: none;
  -moz-border-left-colors: none;
  -moz-border-right-colors: none;
  -moz-border-top-colors: none;
  background-color: #eee;
  border-color: transparent;
  border-image: none;
  border-style: none;
  border-width: 0px;
  color: #555;
  cursor: default;
}


</style>
<div class="container  pb15">

  <div class="row">
    <div class="col-md-10 bg-white col-md-offset-1">
      <p>&nbsp;</p>
      <h1 class="text-center notransform">Openings</h1>
      <p>&nbsp;</p>
      <div class="row">
        <div class="col-md-3 ">
         <?php
          $pune = ['Senior iOS Developer ','Android Developer ','Software Engineer ','Front End Developer ','Senior Product Manager ',
          'Digital Marketing Manager ','HR & Recruiter ','Finance Controller ','Sales Manager ','Content Associate '];
          $dxb = ['Community Manager',
          'Sales Manager'];

          ?>
          <div role="tabpanel ba">
            <h3>City </h3>
            <ul id="jobs-tab-list" class="mt10 bb0  nav nav-tabs nav-stacked" role="tablist">
              @foreach($cities as $ii=>$vv)
                <li role="presentation" class="bb0 no-border-radius fs125">
                  <a class="bb0 no-border-radius dark-gray" href="#jobs-for-{{$ii}}" aria-controls="jobs-for-{{$vv}}" role="tab" data-toggle="tab">
                    {{$vv}}
                    <span class="pull-right"><i class="fa fa-chevron-circle-right"></i></span>
                  </a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-md-8 bl"  style="min-height: 275px">
          <div role="tabpanel">
            <div class="tab-content">
              @foreach($cities as $ii=>$vv)
                <div role="tabpanel" class="tab-pane {{$ii != 'dubai' ? '':'active'}}  pb5"  id="jobs-for-{{$ii}}">
                  <h3>Positions in {{$vv}} </h3>
                  <table class="table">
                    @foreach($jobs as $job)
                      @if($job->location == $ii)
                        <tr class="b0">
                          <td class="b0 dark-gray" >
                            <a class="turquoise-on-hover" href="/jobs/{{$job->id}}">
                              <span class="fs125 " >{{$job->title}}</span> 
                            </a>
                            <!--<a data-toggle="popover" title="Apply as {{$vv}}" data-content="Email your resume to jobs@fabogo.com" class="pull-right btn btn-xs btn-lite-blue hidden" href="mailto:jobs@fabogo.com&Subject={{$vv}}+in+Pune">Apply Now</a>-->
                          </td>
                        </tr>
                      @endif
                    @endforeach
                  </table>
              </div>
              @endforeach
            </div>
          </div>
          <p class="text-left  fs125">
            Email us on:
            <span class="dark-gray">
              <a  data-toggle="popover" title="Apply as {{$job->title}}" 
                  data-content="Email your resume to jobs@fabogo.com" class=" turquoise-on-hover " 
                  href="mailto:jobs@fabogo.com&Subject={{$job->title}}+in+{{$cities[$job->location]}}">
                  jobs@fabogo.com
              </a>
            </span>
          </p>

        </div>
    </div>

</div></div></div>
<script type="text/javascript">
$(function(){

    $(function () {
      $('[data-toggle="popover"]').popover({trigger:'hover', placement:'left'});
    })    
})
</script>

@stop
