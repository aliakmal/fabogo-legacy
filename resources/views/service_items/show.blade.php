@extends('layouts.scaffold')

@section('main')

<h1>Show Service_item</h1>

<p>{{ link_to_route('service_items.index', 'Return to All service_items', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Service_id</th>
				<th>Business_id</th>
				<th>Desc</th>
				<th>Cost</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $service_item->service_id }}}</td>
					<td>{{{ $service_item->business_id }}}</td>
					<td>{{{ $service_item->desc }}}</td>
					<td>{{{ $service_item->cost }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('service_items.destroy', $service_item->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('service_items.edit', 'Edit', array($service_item->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
